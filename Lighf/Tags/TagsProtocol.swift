//
//  TagsProtocol.swift
//  Lighf
//
//  Created by L.
//  Copyright © 2019 L. All rights reserved.
//

import Foundation
import UIKit

protocol TagsViewToPresenterProtocol: class {
    func getTags(from index: Int, to count: Int)
    func getTagsForText(_ text: String, from index: Int, to count: Int)
    func followTag(_ tag: Tag)
    func unfollowTag(_ tag: Tag)
}
protocol TagsPresenterToViewProtocol: BasePresenterToViewProtocol {
    func showTags(_ tags: [Tag])
    func changeTagFollowStatus(tag: Tag, isFollowed: Bool)
    func tagFetchError(_ error: String)
}
protocol TagsPresenterToInteractorProtocol: class {
    func getAllTags(from index: Int, to noOfTags: Int, searchText: String?)
    func followTag(_ tag: Tag, follow: Bool)
}
protocol TagsInteractorToPresenterProtocol: class {
    func handleTags(_ tags: [Tag], error: String?)
    func followedTag(_ tag: Tag, error: String?)
    func unfollowedTag(_ tag: Tag, error: String?)
}
protocol TagsPresenterToRouterProtocol: class {
}
