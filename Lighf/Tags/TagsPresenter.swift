//
//  TagsPresenter.swift
//  Lighf
//
//  Created by L.
//  Copyright © 2019 L. All rights reserved.
//

import Foundation
import UIKit

class TagsPresenter: NSObject {
    var interactor: TagsPresenterToInteractorProtocol?
    weak var view: TagsPresenterToViewProtocol?
    var isLoading: Bool = false
}

extension TagsPresenter: TagsInteractorToPresenterProtocol {
    //
    func handleTags(_ tags: [Tag], error: String?) {
        isLoading = false
        view?.dismissActivityIndicator()
        if error == nil {
            view?.showTags(tags)
        } else {
            view?.showToast(with: error!, completion: {})
            view?.tagFetchError(error!)
        }
    }
    //
    func followedTag(_ tag: Tag, error: String?) {
        view?.dismissActivityIndicator()
        if error == nil {
            view?.changeTagFollowStatus(tag: tag, isFollowed: true)
            view?.showToast(with: Message.followTag + tag.name, completion: {})
        } else {
            view?.showToast(with: error!, completion: {})
        }
    }
    //
    func unfollowedTag(_ tag: Tag, error: String?) {
        view?.dismissActivityIndicator()
        if error == nil {
            view?.changeTagFollowStatus(tag: tag, isFollowed: false)
            view?.showToast(with: Message.unfollowTag + tag.name, completion: {})
        } else {
            view?.showToast(with: error!, completion: {})
        }
    }
}

extension TagsPresenter: TagsViewToPresenterProtocol {
    func getTags(from index: Int, to count: Int) {
        if isLoading == false { // Logic to avoid concurrent loading
            isLoading = true
            interactor?.getAllTags(from: index, to: count, searchText: nil)
        }
    }
    //
    func getTagsForText(_ text: String, from index: Int, to count: Int) {
        view?.showActivityIndicator()
        interactor?.getAllTags(from: index, to: count, searchText: text)
    }
    //
    func followTag(_ tag: Tag) {
        view?.showActivityIndicator()
        interactor?.followTag(tag, follow: true)
    }
    //
    func unfollowTag(_ tag: Tag) {
        view?.showActivityIndicator()
        interactor?.followTag(tag, follow: false)
    }
}
