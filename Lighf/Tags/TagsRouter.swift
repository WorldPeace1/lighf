//
//  TagsRouter.swift
//  Lighf
//
//  Created by L.
//  Copyright © 2019 L. All rights reserved.
//

import Foundation
import UIKit

class TagsRouter: NSObject {
  weak var presentedViewController: UIViewController?
  static let viewIdentifier = "Tags"
  static func createModule() -> TagsViewsController? {
    let view = UIStoryboard(name: viewIdentifier, bundle: Bundle.main).instantiateViewController(withIdentifier: viewIdentifier) as? TagsViewsController
    let presenter = TagsPresenter()
    let router = TagsRouter()
    let interactor = TagsInteractor()
    //setting view delegates
    view?.presenter = presenter
    //setting presenter delegates
    presenter.view = view
    presenter.interactor = interactor
    //setting interactor delegates
    interactor.presenter = presenter
    //setting router presentedViewController
    router.presentedViewController = view
    return view
  }

}
extension TagsRouter: TagsPresenterToRouterProtocol {
}
