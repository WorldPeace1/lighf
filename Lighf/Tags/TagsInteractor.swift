//
//  TagsInteractor.swift
//  Lighf
//
//  Created by L.
//  Copyright © 2019 L. All rights reserved.
//

import Foundation
import UIKit

class TagsInteractor: NSObject, TagOperationProtoccol {
  weak var presenter: TagsInteractorToPresenterProtocol?
}

extension TagsInteractor: TagsPresenterToInteractorProtocol {
    //
    /// Method to fetch tags from the server
    ///
    /// - Parameters:
    ///   - index: The value from which the fetch should be started
    ///   - noOfTags: Number of tags required
    ///   - searchText: The string value based on which a search will be initiated. If it's nil general tags will be returned
    func getAllTags(from index: Int, to noOfTags: Int, searchText: String?) {
        let defaults = UserDefaults.standard
        var parameters: [String: Any] = [:]
        parameters["num_tags"] = noOfTags
        parameters["offset"] = index
        parameters["user_id"] = defaults.value(forKey: UserDefaultsKey.userID)
        //
        if searchText != nil {
            parameters["search_key"] = searchText
        }
        getTags(properties: parameters) { (tags, errorMessage) in
           self.presenter?.handleTags(tags, error: errorMessage)
        }
    }
    //
    /// Method to follow/unfollow a particular tag
    ///
    /// - Parameters:
    ///   - tag: The tag that must be followed/unfollowed
    ///   - follow: A bool value to indicate whether it is a follow or unfollow request. "True" means follow and "false" means unfollow
    func followTag(_ tag: Tag, follow: Bool) {
        let defaults = UserDefaults.standard
        var parameters: [String: Any] = [:]
        parameters["id"] = defaults.value(forKey: UserDefaultsKey.userID)
        parameters["tag"] = tag.id
        parameters["action"] = follow == true ? "follow" : "unfollow"
        //
        APIManager.post(endPoint: EndPoint.followTag, parameters: parameters) { (response, error) in
            guard let response = response, error == nil else {
                if follow {
                    self.presenter?.followedTag(tag, error: error!.message)
                } else {
                    self.presenter?.unfollowedTag(tag, error: error!.message)
                }
                return
            }
            let status = response[APIResponse.apiResponse][APIResponse.apiResponseStatus].stringValue
            if status == Constants.success {
                if follow {
                    self.presenter?.followedTag(tag, error: nil)
                } else {
                    self.presenter?.unfollowedTag(tag, error: nil)
                }
            } else {
                let message = response[APIResponse.apiResponse][APIResponse.apiResponseMessage].stringValue
                if follow {
                    self.presenter?.followedTag(tag, error: message)
                } else {
                    self.presenter?.unfollowedTag(tag, error: message)
                }
            }
        }
    }
}
