//
//  TagsViewsController.swift
//  Lighf
//
//  Created by L.
//  Copyright © 2019 L. All rights reserved.
//

import Foundation
import UIKit
import MBProgressHUD

class TagsViewsController: BaseViewController {
    var presenter: TagsViewToPresenterProtocol?
    var tagsSourceArray: [Tag] = []
    var didFinishLoading: Bool = false
    var isFirstLoad: Bool = false
    var shouldRefresh: Bool = false
    let tagsPerAPI: Int = 20
    let initialValue: Int = 0
    var availableTagCount: Int = 0
    var searchText: String?
    @IBOutlet weak var tagsTableView: UITableView!
    //
    override func viewDidLoad() {
        super.viewDidLoad()
        configureHeaderView()
        getInitialTags()
    }
    //
    /// Method to modify navigation header for the current view
    private func configureHeaderView() {
        let barView = UIView.init(frame: CGRect(x: 0, y: 0, width: self.view.frame.width - 150, height: 40))
        let notificationImage =  UIImageView.init(image: UIImage.init(named: "Search"))
        barView.addSubview(notificationImage)
        barView.backgroundColor = .clear
        // Autolayout for the activity indicator
        notificationImage.autoAlignAxis(.vertical, toSameAxisOf: barView)
        notificationImage.autoAlignAxis(.horizontal, toSameAxisOf: barView)
        navigationItem.titleView = barView
    }
    //
    /// Method to clean the datasource array
    private func clean() {
        tagsSourceArray.removeAll()
        availableTagCount = 0
        tagsTableView.reloadData()
    }
    //
    /// Method to get the first set of tags from the server
    private func getInitialTags() {
        showActivityIndicator()
        isFirstLoad = true
        presenter?.getTags(from: initialValue, to: tagsPerAPI)
    }
}

extension TagsViewsController: TagsPresenterToViewProtocol {
    func showTags(_ tags: [Tag]) {
        if isFirstLoad { // Logic to clean the global tags array
            clean()
            isFirstLoad = false
        }
        didFinishLoading = tags.count < tagsPerAPI // Logic to check whether all the tags are loaded from the server
        tagsSourceArray.append(contentsOf: tags)
        availableTagCount = tagsSourceArray.count
        tagsTableView.reloadData()
    }
    //
    func tagFetchError(_ error: String) {
        isFirstLoad = false
    }
    //
    func changeTagFollowStatus(tag: Tag, isFollowed: Bool) {
        if var selectedTag = tagsSourceArray.first(where: {$0.id == tag.id}) {
            selectedTag.isFollowed = isFollowed
            if let index = tagsSourceArray.firstIndex(where: {$0.id == selectedTag.id}) {
                tagsSourceArray[index] = selectedTag
                tagsTableView.reloadData()
            }
        }
    }
}

extension TagsViewsController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tagsSourceArray.isEmpty {
            return 0
        }
        return tagsSourceArray.count + 1 // +1 to show the activity indicator
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let tableCell = tableView.dequeueReusableCell(withIdentifier: "tagCell", for: indexPath)
        if indexPath.row == tagsSourceArray.count {
            return activityIndicatorCell(for: tableView, indexPath: indexPath)
        }
        let tagObject = tagsSourceArray[indexPath.row]
        tableCell.textLabel?.text = tagObject.name
        tableCell.textLabel?.font = UIFont.init(name: Font.sfProTextLight.rawValue, size: 24.0)
        tableCell.selectionStyle = .none
        return tableCell
    }
    func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        guard indexPath.row < tagsSourceArray.count else {
            return nil // Avoid swipe for the activity cell
        }
        let tag = tagsSourceArray[indexPath.row]
        if tag.isFollowed {
            let unfollowAction = UIContextualAction(style: .normal, title: "") { (_, _, completion) in
                self.presenter?.unfollowTag(tag)
                completion(true)
            }
            unfollowAction.backgroundColor = UIColor.white
            if let cgImage = UIImage(named: "unfollow")?.cgImage {
                unfollowAction.image = ImageWithoutRender(cgImage: cgImage, scale: UIScreen.main.nativeScale, orientation: .up)
            }
            let configuration = UISwipeActionsConfiguration(actions: [unfollowAction])
            configuration.performsFirstActionWithFullSwipe = false
            return configuration
        } else {
            let followAction = UIContextualAction(style: .normal, title: "") { (_, _, completion) in
                self.presenter?.followTag(tag)
                completion(true)
            }
            followAction.backgroundColor = UIColor.white
            if let cgImage = UIImage(named: "follow")?.cgImage {
                followAction.image = ImageWithoutRender(cgImage: cgImage, scale: UIScreen.main.nativeScale, orientation: .up)
            }
            let configuration = UISwipeActionsConfiguration(actions: [followAction])
            configuration.performsFirstActionWithFullSwipe = false
            return configuration
        }
    }
    //
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if indexPath.row == tagsSourceArray.count && didFinishLoading == false {
            if let searchText = searchText {
                presenter?.getTagsForText(searchText, from: availableTagCount, to: tagsPerAPI)
            } else {
                presenter?.getTags(from: availableTagCount, to: tagsPerAPI)
            }
        }
    }
    //
    func tableView(_ tableView: UITableView, editingStyleForRowAt indexPath: IndexPath) -> UITableViewCell.EditingStyle {
        return UITableViewCell.EditingStyle.none
    }
    private func activityIndicatorCell(for tableView: UITableView, indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell(frame: CGRect(x: 0, y: 0, width: tableView.frame.width, height: 20.0))
        if didFinishLoading == false {
            let activityIndicator = UIActivityIndicatorView.newAutoLayout()
            activityIndicator.style = .gray
            cell.addSubview(activityIndicator)
            activityIndicator.autoCenterInSuperview()
            activityIndicator.startAnimating()
        }
        return cell
    }
}
extension TagsViewsController: UISearchBarDelegate {
    //
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        searchBar.showsCancelButton = true
    }
    //
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchBar.showsCancelButton = false
        searchBar.text = Constants.emptyString
        searchText = nil
        searchBar.resignFirstResponder()
        // Reset to initial screen if tags are empty
        if shouldRefresh {
            shouldRefresh = false
            getInitialTags()
        }
    }
    //
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        if let text = searchBar.text, !text.removeWhiteSpace().isEmpty {
            isFirstLoad = true
            shouldRefresh = true
            searchText = text
            presenter?.getTagsForText(text, from: initialValue, to: tagsPerAPI)
            searchBar.resignFirstResponder()
            searchBar.showsCancelButton = false
        } else {
            let alert = UIAlertController.alert(with: Message.invalidSearchText)
            self.present(alert, animated: true, completion: nil)
        }
    }
}
