//
//  TagOperationProtocol.swift
//  Lighf
//
//  Created by L.
//  Copyright © 2019 L. All rights reserved.
//

import UIKit
import SwiftyJSON

protocol TagOperationProtoccol {
    func getTags(properties: [String: Any], completion: @escaping ([Tag], String?) -> Void)
    func getSearchTags(properties: [String: Any], completion: @escaping ([Tag], [Tag], String?) -> Void)
}

extension TagOperationProtoccol {
    func getTags(properties: [String: Any], completion: @escaping ([Tag], String?) -> Void) {
        APIManager.post(endPoint: EndPoint.getTags, parameters: properties) { (response, error) in
            guard let response = response, error == nil else {
                completion([], error!.message)
                return
            }
            let status = response[APIResponse.apiResponse][APIResponse.apiResponseStatus].stringValue
            if status == Constants.success {
                let jsonTags = response[APIResponse.apiResponse]["tags"].arrayValue
                let tags = jsonTags.map { Tag(id: $0["id"].intValue, name: $0["name"].stringValue, slug: $0["slug"].stringValue, isFollowed: $0["is_followed"].boolValue)}
                completion (tags, nil)
            } else {
                let message = response[APIResponse.apiResponse][APIResponse.apiResponseMessage].stringValue
                completion([], message)
            }
        }
    }
    //
    func getSearchTags(properties: [String: Any], completion: @escaping ([Tag], [Tag], String?) -> Void) {
        APIManager.post(endPoint: EndPoint.getTags, parameters: properties) { (response, error) in
            guard let response = response, error == nil else {
                completion([], [], error!.message)
                return
            }
            let status = response[APIResponse.apiResponse][APIResponse.apiResponseStatus].stringValue
            if status == Constants.success {
                let jsonActiveTags = response[APIResponse.apiResponse]["tags"]["active_tags"].arrayValue
                let activeTags = jsonActiveTags.map { Tag(id: $0["id"].intValue, name: $0["name"].stringValue, slug: $0["slug"].stringValue, isFollowed: $0["is_followed"].boolValue)}
                let jsonForYouTags = response[APIResponse.apiResponse]["tags"]["user_tags"].arrayValue
                let forYouTags = jsonForYouTags.map { Tag(id: $0["id"].intValue, name: $0["name"].stringValue, slug: $0["slug"].stringValue, isFollowed: $0["is_followed"].boolValue)}
                completion (activeTags, forYouTags, nil)
            } else {
                let message = response[APIResponse.apiResponse][APIResponse.apiResponseMessage].stringValue
                completion([], [], message)
            }
        }
    }
}
