//
//  IntroViewController.swift
//  Lighf
//
//  Created by L.
//  Copyright © 2019 L. All rights reserved.
//

import Foundation
import UIKit

class IntroViewController: BaseViewController, UIScrollViewDelegate {
    var slides: [Slide] = []
    var presenter: IntroViewToPresenterProtocol?
    @IBOutlet weak var baseScrollView: UIScrollView!
    @IBOutlet weak var pageControl: UIPageControl!
    @IBOutlet weak var nextButton: UIButton!
    var currentPage: Int = 1
    var introPlayStatus: IntroScreenOption = IntroScreenOption.initialPlay
    override func viewDidLoad() {
        super.viewDidLoad()
        slides = createSlides()
        baseScrollView.delegate = self
        setupSlideScrollView(slides: slides)
        pageControl.numberOfPages = slides.count
        pageControl.currentPage = 0
        if currentPage > 0 {
            var frame = baseScrollView.frame
            frame.origin.x = frame.size.width * CGFloat(currentPage)
            frame.origin.y = 0
            baseScrollView.scrollRectToVisible(frame, animated: true)
            pageControl.currentPage = currentPage
            updateNextButtonTitle()
        }
        view.bringSubviewToFront(pageControl)
    }
    //creating slides
    func createSlides() -> [Slide] {
        guard let slide1 = Bundle.main.loadNibNamed("Slide", owner: self, options: nil)?.first as? Slide else {
            return []
        }
        slide1.imageView.image = UIImage(named: "Onboarding_BG")
        slide1.firstScreenContainerView.isHidden = false
        slide1.secondScreenContainerView.isHidden = true
        slide1.thirdScreenContainerView.isHidden = true
        guard let slide2: Slide = Bundle.main.loadNibNamed("Slide", owner: self, options: nil)?.first as? Slide else {
            return []
        }
        slide2.imageView.image = UIImage(named: "introBG2")
        slide2.firstScreenContainerView.isHidden = true
        slide2.secondScreenContainerView.isHidden = false
        slide2.thirdScreenContainerView.isHidden = true
        guard let slide3: Slide = Bundle.main.loadNibNamed("Slide", owner: self, options: nil)?.first as? Slide else {
            return []
        }
        slide3.imageView.image = UIImage(named: "introBG3")
        slide3.firstScreenContainerView.isHidden = true
        slide3.secondScreenContainerView.isHidden = true
        slide3.thirdScreenContainerView.isHidden = false
        slide3.bringSubviewToFront(slide3.thirdScreenContainerView)
        return [slide1, slide2, slide3]
    }
    //setting up scrollview
    func setupSlideScrollView(slides: [Slide]) {
        baseScrollView.frame = CGRect(x: 0, y: 0, width: view.frame.width, height: baseScrollView.frame.height)
        baseScrollView.contentSize = CGSize(width: view.frame.width * CGFloat(slides.count), height: baseScrollView.frame.height)
        baseScrollView.isPagingEnabled = true
        for loopvar in 0 ..< slides.count {
            slides[loopvar].frame = CGRect(x: view.frame.width * CGFloat(loopvar), y: 0, width: view.frame.width, height: baseScrollView.frame.height)
            baseScrollView.addSubview(slides[loopvar])
        }
    }
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let pageIndex = round(scrollView.contentOffset.x/view.frame.width)
        pageControl.currentPage = Int(pageIndex)
        currentPage = Int(pageIndex)
        updateNextButtonTitle()
    }
    //button click event
   @IBAction func buttonTapped(sender: UIButton) {
        if currentPage < 2 {
        var frame = baseScrollView.frame
        frame.origin.x = frame.size.width * CGFloat(currentPage + 1)
            frame.origin.y = 0
            baseScrollView.scrollRectToVisible(frame, animated: true)
            pageControl.currentPage = currentPage + 1
            updateNextButtonTitle()
        } else if introPlayStatus == IntroScreenOption.playFromPausedState {
            if let homePageViewController = HomeRouter.createModule() {
                self.navigationController?.pushViewController(homePageViewController, animated: true)
                self.navigationController?.viewControllers = [homePageViewController]
                UserDefaults.standard.set(-1, forKey: UserDefaultsKey.introPageNum)
            }
        } else if introPlayStatus == IntroScreenOption.replay {
            self.navigationController?.isNavigationBarHidden = false
            self.navigationController?.view.backgroundColor = UIColor.appGreenColor
                self.navigationController?.popViewController(animated: true)
        } else {
            if let selectAspectsViewController = AspectsRouter.createModule(isFromSettings: false) {
                self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
                self.navigationController?.navigationBar.shadowImage = UIImage()
                self.navigationController?.navigationBar.isTranslucent = true
                self.navigationController?.view.backgroundColor = .clear
                self.navigationController?.pushViewController(selectAspectsViewController, animated: true)
            }
        }
    }
    //to update next button title
    func updateNextButtonTitle() {
        if introPlayStatus != IntroScreenOption.replay { // no need to remember the screen number if user is replaying the intro screens.
            UserDefaults.standard.set(currentPage, forKey: UserDefaultsKey.introPageNum)
        }
        if currentPage == 2 {
            nextButton.setTitle("Enter lighf", for: .normal)
        } else {
            nextButton.setTitle("Next", for: .normal)
        }
    }
}
extension IntroViewController: IntroPresenterToViewProtocol {
}
