//
//  IntroProcotols.swift
//  Lighf
//
//  Created by L.
//  Copyright © 2019 L. All rights reserved.
//

import Foundation
import UIKit

protocol IntroViewToPresenterProtocol: class {
}
protocol IntroPresenterToViewProtocol: BasePresenterToViewProtocol {
}
protocol IntroPresenterToRouterProtocol: class {
    //
}
