//
//  IntroPresenter.swift
//  Lighf
//
//  Created by L.
//  Copyright © 2019 L. All rights reserved.
//

import Foundation
import UIKit

class IntroPresenter: NSObject {
    weak var view: IntroPresenterToViewProtocol?
    var router: IntroPresenterToRouterProtocol?
}
extension IntroPresenter: IntroViewToPresenterProtocol {
}
