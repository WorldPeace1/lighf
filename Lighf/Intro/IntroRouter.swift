//
//  IntroRouter.swift
//  Lighf
//
//  Created by L.
//  Copyright © 2019 L. All rights reserved.
//

import Foundation
import UIKit
class IntroRouter: NSObject {
    weak var presentedViewController: UIViewController?
    static let viewIdentifier = "Intro"
    static func createModule(withScreenOption: IntroScreenOption, toPage: Int) -> IntroViewController? {
        let view = UIStoryboard(name: viewIdentifier, bundle: Bundle.main).instantiateViewController(withIdentifier: viewIdentifier) as? IntroViewController
        let presenter = IntroPresenter()
        let router = IntroRouter()
        //setting view delegates
        view?.presenter = presenter
        view?.introPlayStatus = withScreenOption
        view?.currentPage = toPage
        //setting presenter delegates
        presenter.view = view
        presenter.router = router
        //setting router presentedViewController
        router.presentedViewController = view
        return view
    }
}
extension IntroRouter: IntroPresenterToRouterProtocol {
}
