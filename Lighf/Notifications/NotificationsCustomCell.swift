//
//  NotificationsCustomCell.swift
//  Lighf
//
//  Created by L.
//  Copyright © 2019 L. All rights reserved.
//

import Foundation
import UIKit
class NotificationsCustomCell: UITableViewCell {
    @IBOutlet weak var mainTextView: UITextView!
    @IBOutlet weak var detailTextView: UITextView!
    @IBOutlet weak var bottomDistance: NSLayoutConstraint!
}
