//
//  NotificationsViewController.swift
//  Lighf
//
//  Created by L.
//  Copyright © 2019 L. All rights reserved.
//

import Foundation
import UIKit
import AttributedStringBuilder
import MatomoTracker
class NotificationsViewController: BaseViewController {
    var presenter: NotificationsViewToPresenterProtocol?
    var notificationsTableData: [Any] = []
    @IBOutlet weak var baseView: UIView!
    @IBOutlet weak var notificationsTable: UITableView!
    var isAllNotificationsReceived: Bool = false
    var removeActivityIndicator: Bool = false
    var postOffset = 0
    let numberOfPostsPerAPI = 15
    var selectedNotificationIndex = -1
    var refreshControl = UIRefreshControl()
    override func viewDidLoad() {
        super.viewDidLoad()
        showActivityIndicator()
        presenter?.getNotifications(from: postOffset, to: numberOfPostsPerAPI, unReadOnly: false)
        refreshControl.addTarget(self, action: #selector(refreshFeeds), for: UIControl.Event.valueChanged)
        notificationsTable.refreshControl = refreshControl
        notificationsTable.addSubview(refreshControl) // not required when using UITableViewController
        notificationsTable.estimatedRowHeight = 45
        notificationsTable.rowHeight = UITableView.automaticDimension
        notificationsTable.reloadData()
        MatomoTracker.shared.track(view: ["Notifications view"])
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        configureHeaderView()
    }
    func configureHeaderView() {
        navigationController?.resetBottomBarLine()
        navigationController?.navigationBar.isTranslucent = false
        navigationController?.view.backgroundColor = UIColor.appGreenColor
        navigationController?.setBackGroundColor(UIColor.appGreenColor)
        //
        let barView = UIView.init(frame: CGRect(x: 0, y: 0, width: self.view.frame.width - 150, height: 40))
        let notificationImage =  UIImageView.init(image: UIImage.init(named: "notification"))
        let verticalConstraint = NSLayoutConstraint(item: notificationImage, attribute: NSLayoutConstraint.Attribute.centerX, relatedBy: NSLayoutConstraint.Relation.equal, toItem: barView, attribute: NSLayoutConstraint.Attribute.centerX, multiplier: 1, constant: 0)
        let horizontalConstraint = NSLayoutConstraint(item: notificationImage, attribute: NSLayoutConstraint.Attribute.centerY, relatedBy: NSLayoutConstraint.Relation.equal, toItem: barView, attribute: NSLayoutConstraint.Attribute.centerY, multiplier: 1, constant: 0)
        barView.addSubview(notificationImage)
        barView.backgroundColor = .clear
        NSLayoutConstraint.activate([verticalConstraint, horizontalConstraint])
        navigationItem.titleView = barView
    }
}

extension NotificationsViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if notificationsTableData.isEmpty && isAllNotificationsReceived {
            return 0
        } else if notificationsTableData.isEmpty && !isAllNotificationsReceived {
            return 1
        } else if isAllNotificationsReceived || removeActivityIndicator {
            removeActivityIndicator = false
            return notificationsTableData.count
        }
        return notificationsTableData.count+1
    }
    // configuring custom cell
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row < self.notificationsTableData.count {
            let cellIdentifier = "NotificationsCustomCell"
            if let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as? NotificationsCustomCell, let notificationObject = notificationsTableData[indexPath.row] as? NotificationModel {
                cell.mainTextView.text = notificationObject.notificationText
                if notificationObject.notificationReadStatus == "unread" {
                    cell.mainTextView.font = UIFont.init(name: Font.sfProDisplayBold.rawValue, size: 16)
                } else {
                    cell.mainTextView.font = UIFont.init(name: Font.sfProDisplayLight.rawValue, size: 16)
                }
                cell.selectionStyle = .default
                return cell
            }
        } else if (indexPath.row == notificationsTableData.count && !notificationsTableData.isEmpty) {
            return activityIndicatorCell(for: tableView, indexPath: indexPath)
        }
        return UITableViewCell()
    }
    //method to display activity indicator for lazy loading.
    private func activityIndicatorCell(for tableView: UITableView, indexPath: IndexPath) -> UITableViewCell {
        let loadingCell = UITableViewCell.init(frame: CGRect.init(x: 0, y: 0, width: self.notificationsTable.frame.size.width, height: 20))
        let activityIndicator = UIActivityIndicatorView.init(style: .gray)
        if !isAllNotificationsReceived {
            activityIndicator.startAnimating()
        }
        activityIndicator.translatesAutoresizingMaskIntoConstraints = false
        let horizontalConstraint = NSLayoutConstraint(item: activityIndicator, attribute: NSLayoutConstraint.Attribute.centerX, relatedBy: NSLayoutConstraint.Relation.equal, toItem: loadingCell, attribute: NSLayoutConstraint.Attribute.centerX, multiplier: 1, constant: 0)
        let verticalConstraint = NSLayoutConstraint(item: activityIndicator, attribute: NSLayoutConstraint.Attribute.centerY, relatedBy: NSLayoutConstraint.Relation.equal, toItem: loadingCell, attribute: NSLayoutConstraint.Attribute.centerY, multiplier: 1, constant: 0)
        if notificationsTableData.count % numberOfPostsPerAPI == 0 {
            loadingCell.addSubview(activityIndicator)
            NSLayoutConstraint.activate([verticalConstraint, horizontalConstraint])
        }
        return loadingCell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let notificationObject = notificationsTableData[indexPath.row] as? NotificationModel, selectedNotificationIndex == -1 {
            selectedNotificationIndex = indexPath.row
            presenter?.selectedNotification(notificationObj: notificationObject)
        }
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 30
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerViewHeight: CGFloat = 20.0
        let borderLineHeight: CGFloat = 1.0
        let headerViewframe = CGRect(x: 0, y: 0, width: notificationsTable.frame.size.width, height: headerViewHeight)
        let headerView = UIView.init(frame: headerViewframe)
        headerView.backgroundColor = UIColor.white
        let headerName = UILabel()
        headerName.text = "RECENT"
        headerName.frame = CGRect(x: 15, y: 5, width: notificationsTable.frame.size.width - 30, height: headerViewHeight)
        headerName.textAlignment = .left
        headerName.textColor = UIColor.appBlueColor
        headerName.font = UIFont(name: "SFProText-Regular", size: 13)
        headerView.addSubview(headerName)
        //
        let borderLineView = UIView.newAutoLayout()
        borderLineView.frame = CGRect(x: 0, y: headerViewHeight - borderLineHeight, width: notificationsTable.frame.size.width, height: borderLineHeight)
        borderLineView.backgroundColor = UIColor.seperatorColor
        headerView.addSubview(borderLineView)
        //
        borderLineView.autoPinEdge(toSuperviewEdge: .leading)
        borderLineView.autoPinEdge(toSuperviewEdge: .trailing)
        borderLineView.autoPinEdge(toSuperviewEdge: .bottom)
        borderLineView.autoSetDimension(.height, toSize: borderLineHeight)
        //
        return headerView
    }
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if indexPath.row == self.notificationsTableData.count  && tableView.contentOffset.y != 0 && !isAllNotificationsReceived {
            presenter?.getNotifications(from: postOffset, to: numberOfPostsPerAPI, unReadOnly: false)
        }
    }
}
extension NotificationsViewController: NotificationsPresenterToViewProtocol {
    func navigateToHome(_ success: Bool, error: String?) {
        var alertMessage = error
        if error?.lowercased().contains(Message.requestTimeOut) ?? false || error?.lowercased().contains(Message.messageNotFound) ?? false {
            alertMessage = Message.connectionLost
        } else {
            alertMessage = error
        }
        let alert = UIAlertController(title: "", message: alertMessage, preferredStyle: .alert)
        let alertAction = UIAlertAction(title: "Go to Home", style: .cancel) { (_) in
            self.navigationController?.popViewController(animated: true)
        }
        alert.addAction(alertAction)
        self.present(alert, animated: true, completion: nil)
    }
    // to process notification read api response.
    func readAPIResponse(_ success: Bool, error: String?) {
        if var notificationObj = notificationsTableData[selectedNotificationIndex] as? NotificationModel {
            if success {
                notificationObj.notificationReadStatus = "read"
            }
            notificationsTableData[selectedNotificationIndex] = notificationObj
            self.notificationsTable.reloadRows(at: [NSIndexPath.init(row: selectedNotificationIndex, section: 0) as IndexPath], with: UITableView.RowAnimation.none)
            selectedNotificationIndex = -1
            if notificationObj.postID != 0 {
                presenter?.navigateToPost(withNotificationObj: notificationObj)
            }
        }
    }
    // display alert
    func showAlert(_ message: String) {
        if self.notificationsTableData.isEmpty {
            notificationsTable.isHidden = true
        } else {
            notificationsTable.isHidden = false
        }
        removeActivityIndicator = true
        notificationsTable.reloadData()
        let alert = UIAlertController.alert(with: message)
        self.present(alert, animated: true, completion: nil)
    }
    // Show notification feeds
    func showFeeds(_ feeds: [Any]) {
        let prevOffset = self.notificationsTableData.count
        if prevOffset == 0 && feeds.isEmpty {
            notificationsTable.isHidden = true
        } else {
            notificationsTable.isHidden = false
            postOffset += feeds.count
            if (postOffset != 0 && feeds.isEmpty) || feeds.count % numberOfPostsPerAPI != 0 {
                isAllNotificationsReceived = true
            } else {
                isAllNotificationsReceived = false
            }
            // Remove duplicates from the feeds
            if let posts = feeds as? [NotificationModel], var currentPosts = self.notificationsTableData as? [NotificationModel] {
                for post in posts {
                    let isDuplicate = currentPosts.contains {$0.notificationID == post.notificationID}
                    if isDuplicate == false {
                        currentPosts.append(post)
                    }
                }
                self.notificationsTableData = currentPosts
            } else {
                // Handle activities here
            }
            if refreshControl.isRefreshing {
                refreshControl.endRefreshing()
            }
            notificationsTable.reloadData()
            dismissActivityIndicator()
            if prevOffset != 0 {
                notificationsTable.scrollToRow(at: IndexPath.init(row: prevOffset - 1, section: 0), at: .bottom, animated: false)
            }
        }
        let defaults  = UserDefaults.standard
        defaults.set(0, forKey: UserDefaultsKey.newNotificationsAvailable)
    }
    @objc func refreshFeeds() {
        notificationsTableData.removeAll()
        postOffset = 0
        isAllNotificationsReceived = false
        presenter?.getNotifications(from: postOffset, to: numberOfPostsPerAPI, unReadOnly: false)
    }
}
