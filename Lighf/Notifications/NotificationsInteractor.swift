//
//  NotificationsInteractor.swift
//  Lighf
//
//  Created by L.
//  Copyright © 2019 L. All rights reserved.
//

import Foundation
import UIKit

class NotificationsInteractor: NSObject {
    weak var presenter: NotificationsInteractorToPresenterProtocol?
}
extension NotificationsInteractor: NotificationsPresenterToInteractorProtocol {
    func invokeNotificationReadAPI(notificationObj: NotificationModel) {
        let defaults = UserDefaults.standard
        var parameters: [String: Any] = [:]
        parameters[APIParams.userID] = defaults.value(forKey: UserDefaultsKey.userID)
        parameters[APIParams.notificationID] = notificationObj.notificationID
        parameters[APIParams.notificationReadAction] = "read"
        var responseFlag = false
        APIManager.post(endPoint: EndPoint.notificationMarkRead, parameters: parameters) { (response, error) in
            guard error == nil, let response = response else {
                self.presenter?.apiFailed(true, error: error?.message)
                return
            }
            let status = response[APIResponse.apiResponse]["status"].stringValue
            let responseStatus = response[APIResponse.apiResponse][APIResponse.apiResponseStatus].stringValue
            if status == "read" && responseStatus == "ok" {
                responseFlag = true
            } else {
                responseFlag = false
            }
            self.presenter?.handleReadAPI(responseFlag, error: Constants.emptyString)
        }
    }
    func invokeNotificationsAPI(from index: Int, to count: Int, unReadOnly: Bool) {
        let defaults = UserDefaults.standard
        var parameters: [String: Any] = [:]
        parameters[APIParams.userID] = defaults.value(forKey: UserDefaultsKey.userID)
        parameters[APIParams.numberOfNotifications] = count
        parameters[APIParams.postOffset] = index
        parameters[APIParams.unReadNotifications] = unReadOnly
        APIManager.post(endPoint: EndPoint.getNotifications, parameters: parameters) { (response, error) in
            guard error == nil, let response = response else {
                self.presenter?.apiFailed(true, error: error?.message)
                return
            }
            let posts = response[APIResponse.apiResponse][APIResponse.apiNotifications].array
            let message = response[APIResponse.apiResponse][APIResponse.apiResponseMessage].stringValue
            self.presenter?.handleFeeds(posts ?? [], error: message)
        }
    }
}
