//
//  NotificationsPresenter.swift
//  Lighf
//
//  Created by L.
//  Copyright © 2019 L. All rights reserved.
//

import Foundation
import UIKit
import SwiftyJSON
class NotificationsPresenter: NSObject {
    weak var view: NotificationsPresenterToViewProtocol?
    var interactor: NotificationsPresenterToInteractorProtocol?
    var router: NotificationsPresenterToRouterProtocol?
}
extension NotificationsPresenter: NotificationsInteractorToPresenterProtocol {
    func apiFailed(_ success: Bool, error: String?) {
        view?.navigateToHome(success, error: error)
    }
    func handleReadAPI(_ success: Bool, error: String?) {
        view?.readAPIResponse(success, error: error)
    }
    func handleFeeds(_ feeds: [JSON], error: String?) {
        view?.dismissActivityIndicator()
        var responseArray: [Any] = []
        for loopVar in 0..<feeds.count {
            let postDict = feeds[loopVar]
            responseArray.append(NotificationModel(notificationObj: postDict))
        }
        guard error == Constants.emptyString else {
            view?.showAlert(error ?? Constants.emptyString)
            return
        }
        view?.showFeeds(responseArray)
    }
}

extension NotificationsPresenter: NotificationsViewToPresenterProtocol {
    func navigateToPost(withNotificationObj: NotificationModel) {
        router?.navigateToDetails(detailObject: withNotificationObj)
    }
    func getNotifications(from index: Int, to count: Int, unReadOnly: Bool) {
        interactor?.invokeNotificationsAPI(from: index, to: count, unReadOnly: unReadOnly)
    }
    func selectedNotification(notificationObj: NotificationModel) {
        interactor?.invokeNotificationReadAPI(notificationObj: notificationObj)
    }
}
extension NotificationsPresenter: NotificationsRouterToPresenterProtocol {
        //
}
