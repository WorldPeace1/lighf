//
//  NotificationsProtocols.swift
//  Lighf
//
//  Created by L.
//  Copyright © 2019 L. All rights reserved.
//

import Foundation
import UIKit
import SwiftyJSON

protocol NotificationsViewToPresenterProtocol: class {
    func selectedNotification(notificationObj: NotificationModel)
    func getNotifications(from index: Int, to count: Int, unReadOnly: Bool)
    func navigateToPost(withNotificationObj: NotificationModel)
}
protocol NotificationsPresenterToViewProtocol: BasePresenterToViewProtocol {
    func showAlert(_ message: String)
    func showFeeds(_ feeds: [Any])
    func readAPIResponse(_ success: Bool, error: String?)
    func navigateToHome(_ success: Bool, error: String?)
}
protocol NotificationsPresenterToInteractorProtocol: class {
    func invokeNotificationsAPI(from index: Int, to count: Int, unReadOnly: Bool)
    func invokeNotificationReadAPI(notificationObj: NotificationModel)
}
protocol NotificationsInteractorToPresenterProtocol: class {
    func handleFeeds(_ feeds: [JSON], error: String?)
    func handleReadAPI(_ success: Bool, error: String?)
    func apiFailed(_ success: Bool, error: String?)
}
protocol NotificationsPresenterToRouterProtocol: class {
    func navigateToDetails(detailObject: NotificationModel)
}
protocol NotificationsRouterToPresenterProtocol: class { }
