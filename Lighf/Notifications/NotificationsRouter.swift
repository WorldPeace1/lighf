//
//  NotificationsRouter.swift
//  Lighf
//
//  Created by L.
//  Copyright © 2019 L. All rights reserved.
//

import Foundation
import UIKit
class NotificationsRouter: NSObject {
    weak var presentedViewController: UIViewController?
    static let viewIdentifier = "Notifications"
    static func createModule() -> NotificationsViewController? {
        let view = UIStoryboard(name: viewIdentifier, bundle: Bundle.main).instantiateViewController(withIdentifier: viewIdentifier) as? NotificationsViewController
        let presenter = NotificationsPresenter()
        let router = NotificationsRouter()
        let interactor = NotificationsInteractor()
        //setting view delegates
        view?.presenter = presenter
        //setting presenter delegates
        presenter.view = view
        presenter.router = router
        presenter.interactor = interactor
        //setting interactor delegates
        interactor.presenter = presenter
        //setting router presentedViewController
        router.presentedViewController = view
        return view
    }
}
extension NotificationsRouter: NotificationsPresenterToRouterProtocol {
    func navigateToDetails(detailObject: NotificationModel) {
        //code to navigate to details page.
        if let postDetailsViewController = PostDetailsRouter.createModule(with: nil) {
            presentedViewController?.navigationItem.setHidesBackButton(false, animated: true)
            postDetailsViewController.postType = PostType.story
            var post = Story.init(with: NSAttributedString.init(string: Constants.emptyString))
            post.postId = detailObject.postID
            postDetailsViewController.post = post
            postDetailsViewController.commentIDFromNotification = detailObject.commentID
            postDetailsViewController.isFromNotificationScreen = true
            postDetailsViewController.viewType = .detailView
            postDetailsViewController.forceUpdate = true
            presentedViewController?.navigationController?.pushViewController(postDetailsViewController, animated: true)
        }
    }
}
