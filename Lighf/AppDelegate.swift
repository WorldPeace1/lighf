//
//  AppDelegate.swift
//  Lighf
//
//  Created by L.
//  Copyright © 2019 L. All rights reserved.
//

import UIKit
import Kingfisher

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        
        self.generateDeviceId()
        
        self.window     = UIWindow(frame: UIScreen.main.bounds)
        let defaults    = UserDefaults.standard
        let accessToken = defaults.object(forKey: UserDefaultsKey.accessToken) as? String
        
        // MARK: Choose Base Controller
        
        var baseVC: UIViewController?
        if let introPageNum = defaults.object(forKey: UserDefaultsKey.introPageNum) as? Int, introPageNum != -1 { //navigate to intro screen if value not -1
            baseVC = IntroRouter.createModule(withScreenOption: IntroScreenOption.playFromPausedState, toPage: introPageNum)
        } else if (accessToken != nil) && accessToken != Constants.emptyString { //navigate to home feed if access token is available.
            baseVC = HomeRouter.createModule()
        } else {
            baseVC = SignInRouter.createModule()
        }
        
        // MARK: Create Navigaion Controller
        
        let navigationController = UINavigationController(rootViewController: baseVC!)
        navigationController.setTransparent()
        navigationController.titleColor(UIColor.white, font: Font.robotoBold, size: 22.0)
        
        // MARK: Kingfisher image cache settings
        
        let cache = ImageCache.default
        cache.memoryStorage.config.totalCostLimit = 50 * 1024 * 1024 // Set maximum caache size to 50 MB
        cache.memoryStorage.config.countLimit = 50 // Set the maximum number of images in cache to 50
        cache.memoryStorage.config.expiration = .seconds(200)
        
        self.window?.rootViewController = navigationController
        self.window?.makeKeyAndVisible()
        
        return true
    }

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state.
        //This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or
        //when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and
        //store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }
}

// MARK: - Device Id

extension AppDelegate {
    func generateDeviceId() {
        guard UserDefaults.standard.value(forKey: UserDefaultsKey.deviceId) == nil else {
            return
        }
        
        guard let vendorIdentifier = UIDevice.current.identifierForVendor?.uuidString else {
            fatalError("Could not generate vendor identifier")
        }

        let identifier = DeviceIdentifier().generateDeviceId(vendorIdentifier: vendorIdentifier)
        UserDefaults.standard.set(identifier, forKey: UserDefaultsKey.deviceId)
    }
}

// Universal Links

extension AppDelegate {
    func application(_ application: UIApplication, continue userActivity: NSUserActivity, restorationHandler: @escaping ([UIUserActivityRestoring]?) -> Void) -> Bool {
      
        guard userActivity.activityType == NSUserActivityTypeBrowsingWeb,
            let incomingURL = userActivity.webpageURL,
            let components = NSURLComponents(url: incomingURL, resolvingAgainstBaseURL: true),
            let path = components.path,
            let params = components.queryItems
        else {
            print("Unrecognized link!")
            return false
        }
        
        if path == "/\(EndPoint.confirm)/" {
            guard let action = params.first(where: { $0.name == "action" })?.value,
                action == "confirm_signup"
            else {
                print("action is missing")
                return false
            }
            
            guard let userId = params.first(where: { $0.name == "user_id" })?.value else {
                print("user_id is missing")
                return false
            }
            
            guard let key = params.first(where: { $0.name == "key" })?.value else {
                print("key is missing")
                return false
            }
            
            guard let hash = params.first(where: { $0.name == "hash" })?.value else {
                print("hash is missing")
                return false
            }
            
            self.handleConfirmationLink(action: action, userId: userId, key: key, hash: hash)
            return true
        }
        
        return false
    }
    
    func showWindowAlert(message: String) {
        let alertController = UIAlertController.alert(with: message)
        self.window?.rootViewController?.present(alertController, animated: true, completion: nil)
    }
}
