//
//  SearchRouter.swift
//  Lighf
//
//  Created by L.
//  Copyright © 2019 L. All rights reserved.
//

import Foundation
import UIKit
class SearchRouter: NSObject {
    weak var presentedViewController: UIViewController?
    static let viewIdentifier = "Search"
    static func createModule(with interface: PostOperationsProtocol? = nil) -> SearchViewController? {
        let view = UIStoryboard(name: viewIdentifier, bundle: Bundle.main).instantiateViewController(withIdentifier: viewIdentifier) as? SearchViewController
        let presenter = SearchPresenter()
        let router = SearchRouter()
        let interactor = SearchInteractor()
        let feedsInteractor = GetFeedsInteractor()
        let leafInteractor = GiveLeafInteractor()
        let toggleNotificationInteractor = ToggleNotificationInteractor()
        //setting view delegates
        view?.presenter = presenter
        //setting presenter delegates
        presenter.view = view
        presenter.router = router
        presenter.interactor = interactor
        presenter.getFeedsInteractor = feedsInteractor
        presenter.homeInterface = interface
        presenter.leafInteractor = leafInteractor
        presenter.toggleNotificationInteractor = toggleNotificationInteractor
        //setting interactor delegates
        interactor.presenter = presenter
        //setting router presentedViewController
        router.presentedViewController = view
        feedsInteractor.searchPresenter = presenter
        leafInteractor.searchPresenter = presenter
        toggleNotificationInteractor.searchPresenter = presenter
        return view
    }
}
extension SearchRouter: SearchPresenterToRouterProtocol {
    func alertActionTapped(selectedOption: FlagOptions, postID: Post) {
        print(selectedOption)
        //write logic to navigate to new screen to input data.
        if let commentsViewController = CommentsViewRouter.createModule() {
            commentsViewController.flagOption = selectedOption
            commentsViewController.post = postID
            presentedViewController?.navigationController?.setTransparent()
            presentedViewController?.navigationItem.setHidesBackButton(false, animated: true)
            presentedViewController?.navigationController?.pushViewController(commentsViewController, animated: true)
        }
    }
    func viewPost(_ post: Post, type: PostType, interface: SearchModuleInterface) {
        if let postDetailsViewController = PostDetailsRouter.createModule(with: interface) {
            presentedViewController?.navigationItem.setHidesBackButton(false, animated: true)
            postDetailsViewController.postType = type
            postDetailsViewController.post = post
            postDetailsViewController.viewType = .detailView
            postDetailsViewController.forceUpdate = true
            presentedViewController?.navigationController?.pushViewController(postDetailsViewController, animated: true)
        }
    }
    func moveToEditPostView(with post: Post, type: PostType) {
//        if let createPostView = CreatePostRouter.createModule(for: type, post: post) {
//            self.presentedViewController?.navigationController?.pushViewController(createPostView, animated: true)
//        }
        let useAlternatePostCreationOption = UserDefaults.standard.value(forKey: UserDefaultsKey.showAlternatePostCreationOption) as? Bool ?? true
        if useAlternatePostCreationOption {
            if let createPostView = CreatePostRouter.createAlternateModule(for: type, post: post) {
                self.presentedViewController?.navigationController?.pushViewController(createPostView, animated: true)
            }
        } else {
            if let createPostView = CreatePostRouter.createDefaultModule(for: type, post: post) {
                self.presentedViewController?.navigationController?.pushViewController(createPostView, animated: true)
            }
        }
    }
}
