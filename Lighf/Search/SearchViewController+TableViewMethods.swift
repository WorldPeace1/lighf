//
//  SearchViewController+TableViewMethods.swift
//  Lighf
//
//  Created by L.
//  Copyright © 2019 L. All rights reserved.
//

import Foundation
import UIKit

extension SearchViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let post = feeds[indexPath.row]
        switch post {
        case let story as Story:
            presenter?.viewPost(story, type: .story)
        case let article as Article:
            presenter?.viewPost(article, type: .article)
        case let window as Window:
            presenter?.viewPost(window, type: .window)
        default:
            break
        }
        tableView.deselectRow(at: indexPath, animated: false)
    }
    // method to set swipe actions
    func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        if let post = feeds[indexPath.row] as? Post {
            if post.isOwnPost {
                let editAction = editPost(atIndex: indexPath, for: post)
                let deleteAction = deletePost(atIndex: indexPath, for: post)
                let notifications = toggleNotification(atIndex: indexPath, for: post)
                var actionArray: [UIContextualAction] = []
                actionArray.append(notifications)
                if post.isEditable {
                    actionArray.append(deleteAction)
                    actionArray.append(editAction)
                }
                let swipeConfig = UISwipeActionsConfiguration(actions: actionArray)
                swipeConfig.performsFirstActionWithFullSwipe = false
                return swipeConfig
            }
            var actionArray: [UIContextualAction] = []
            let notifications = toggleNotification(atIndex: indexPath, for: post)
            actionArray.append(notifications)
            let flagAction = flagComment(atIndex: indexPath, for: post)
            actionArray.append(flagAction)
            if post.type == .story || post.type == .statement {
                let giveLeafAction = giveLeafToComment(atIndex: indexPath, for: post)
                actionArray.append(giveLeafAction)
            }
            let swipeConfig = UISwipeActionsConfiguration(actions: actionArray)
            swipeConfig.performsFirstActionWithFullSwipe = false
            return swipeConfig
        }
        return nil
    }
    //method to handle flagging
    func flagComment(atIndex indexPath: IndexPath, for post: Post) -> UIContextualAction {
        let action = createFlagAction(post: post, atIndex: indexPath)
        if let cgImage = UIImage(named: "FlagGrey")?.cgImage {
            action.image = ImageWithoutRender(cgImage: cgImage, scale: UIScreen.main.nativeScale, orientation: .up)
        }
//        action.title = "Flag"
        return action
    }
    //method to handle giving a leaf
    func giveLeafToComment(atIndex indexPath: IndexPath, for post: Post) -> UIContextualAction {
        let action = createLeafAction(post: post, atIndex: indexPath)
        if let cgImage = UIImage(named: "leaf_white")?.cgImage {
            action.image = ImageWithoutRender(cgImage: cgImage, scale: UIScreen.main.nativeScale, orientation: .up)
        }
//        action.title = "Give Leaf"
        return action
    }
    //
    func editPost(atIndex indexPath: IndexPath, for post: Post) -> UIContextualAction {
        let action = createEditAction(post: post, atIndex: indexPath)
        action.title = "Edit"
        return action
    }
    //
    func deletePost(atIndex indexPath: IndexPath, for post: Post) -> UIContextualAction {
        let action = createDeleteAction(post: post, atIndex: indexPath)
        action.title = "Delete"
        return action
    }
    func toggleNotification(atIndex indexPath: IndexPath, for post: Post) -> UIContextualAction {
        let action = createNotificationAction(post: post, atIndex: indexPath)
//        action.title = "Notification"
        return action
    }
    // to send a leaf to a post
    func createLeafAction(post: Post, atIndex indexPath: IndexPath) -> UIContextualAction {
        //
        var isLeafGiven = false
        switch post {
        case var article as Article:
            isLeafGiven = article.leafGiven()
        case var story as Story:
            isLeafGiven = story.leafGiven()
        case var window as Window:
            isLeafGiven = window.leafGiven()
        default: break
        }
        //
        let action = UIContextualAction(style: .destructive, title: Constants.emptyString) { (_ contextAction: UIContextualAction, _ sourceView: UIView, completionHandler: (Bool) -> Void) in
            if isLeafGiven {
                //invoke an API to give leaf
                //
                // Need to update the flow to send the comment related data to send leaf for this particular comment
                self.presenter?.leafButtonClicked(for: post)
            }
            // Completion handler always returns false. This is a workaround to fix swipe issue in tableview
            completionHandler(false)
        }
        action.backgroundColor = UIColor.giveLeafBackgroundColor
        return action
    }
    // to a flag to a post
    func createFlagAction(post: Post, atIndex indexPath: IndexPath) -> UIContextualAction {
        //
        var isFlagged = false
        switch post {
        case var article as Article:
            isFlagged = article.toggleFlaggedFlag()
        case var story as Story:
            isFlagged = story.toggleFlaggedFlag()
        case var window as Window:
            isFlagged = window.toggleFlaggedFlag()
        default: break
        }
        //
        let action = UIContextualAction(style: .destructive, title: Constants.emptyString) { (_ contextAction: UIContextualAction, _ sourceView: UIView, completionHandler: (Bool) -> Void) in
            if isFlagged {
                //invoke an API to flag this comment
                self.flagOptionsButtonClicked(sender: post)
            }
            // Completion handler always returns false. This is a workaround to fix swipe issue in tableview
            completionHandler(false)
        }
        action.backgroundColor = UIColor.giveFlagBackgroundColor
        return action
    }
    //
    func createDeleteAction(post: Post, atIndex indexPath: IndexPath) -> UIContextualAction {
        //
        var isDeletable = false
        switch post {
        case let article as Article:
            isDeletable = article.isDeleteable
        case let story as Story:
            isDeletable = story.isDeleteable
        case let window as Window:
            isDeletable = window.isDeletable
        default: break
        }
        //
        let action = UIContextualAction(style: .destructive, title: Constants.emptyString) { (_ contextAction: UIContextualAction, _ sourceView: UIView, completionHandler: (Bool) -> Void) in
            if isDeletable {
                //invoke an API to delete this post
                let alert = UIAlertController(title: "", message: Message.deleteConfirmation, preferredStyle: .alert)
                let confirmAction = UIAlertAction(title: "Yes", style: .default, handler: { (_) in
                    self.presenter?.deletePost(post)
                })
                let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
                alert.addAction(confirmAction)
                alert.addAction(cancelAction)
                self.present(alert, animated: true, completion: nil)
            }
            // Completion handler always returns false. This is a workaround to fix swipe issue in tableview
            completionHandler(false)
        }
        return action
    }
    // to edit to a post
    func createEditAction(post: Post, atIndex indexPath: IndexPath) -> UIContextualAction {
        //
        var isEditable = false
        var postType: PostType?
        switch post {
        case let article as Article:
            isEditable = article.isEditable
            postType = PostType.article
        case let story as Story:
            isEditable = story.isEditable
            postType = PostType.story
        default: break
        }
        let action = UIContextualAction(style: .destructive, title: Constants.emptyString) { (_ contextAction: UIContextualAction, _ sourceView: UIView, completionHandler: (Bool) -> Void) in
            if isEditable {
                //invoke an API to flag this comment
                print("edit tapped")
                self.showActivityIndicator(onFullScreen: true)
                self.presenter?.editPost(post, type: postType ?? PostType.story)
            }
            // Completion handler always returns false. This is a workaround to fix swipe issue in tableview
            completionHandler(false)
        }
        action.backgroundColor = UIColor.lightGray
        return action
    }
    func createNotificationAction(post: Post, atIndex indexPath: IndexPath) -> UIContextualAction {
        //
        var isNotificationEnabled = false
        switch post {
        case let article as Article:
            isNotificationEnabled = article.isNotificationEnabled
        case let story as Story:
            isNotificationEnabled = story.isNotificationEnabled
        case let window as Window:
            isNotificationEnabled = window.isNotificationEnabled
        default: break
        }
        //
        let action = UIContextualAction(style: .destructive, title: Constants.emptyString) { (_ contextAction: UIContextualAction, _ sourceView: UIView, completionHandler: (Bool) -> Void) in
            // Completion handler always returns false. This is a workaround to fix swipe issue in tableview
            var toggleTo: NotificationState = NotificationState.turnOn
            if isNotificationEnabled {
                toggleTo = NotificationState.turnOff
            } else {
                toggleTo = NotificationState.turnOn
            }
            self.presenter?.toggleNotification(forItem: post, toState: toggleTo)
            completionHandler(false)
        }
        if isNotificationEnabled {
            action.image = UIImage.init(named: "notification")
        } else {
            action.image = UIImage.init(named: "notification_off")
        }
        action.backgroundColor = UIColor.toggleNotificationBackgroundColor
        return action
    }
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if indexPath.row == self.feeds.count  && !searchString.isEmpty && tableView.contentOffset.y != 0 && !isAllFeedReceived {//}&& postOffset > 0  {
            showActivityIndicator()
            presenter?.invokeSearchWithText(from: postOffset, to: numberOfPostsPerAPI, searchWith: searchString, filterWith: paramDict)
        }
    }
}

extension SearchViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if feeds.isEmpty && isSearchMode {
            return 0
        } else if isAllFeedReceived {
            return feeds.count
        }
        return feeds.count+1
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row < feeds.count {
            let post = feeds[indexPath.row]
            //
            switch post {
            case let article as Article:
                return articleCell(for: tableView, indexPath: indexPath, article: article)
            case let window as Window:
                return windowCell(for: tableView, indexPath: indexPath, window: window)
            case let story as Story:
                return storyCell(for: tableView, indexPath: indexPath, story: story)
            default:
                break
            }
        } else if (indexPath.row == feeds.count && !feeds.isEmpty) {
            return activityIndicatorCell(for: tableView, indexPath: indexPath)
        }
        return UITableViewCell()
    }
    //method to create article cell
    private func articleCell(for tableView: UITableView, indexPath: IndexPath, article: Article) -> UITableViewCell {
        if let articleCell = tableView.dequeueReusableCell(withIdentifier: ArticleCell.identifier, for: indexPath) as? ArticleCell {
            articleCell.prepareCell(with: article, atIndex: indexPath)
            articleCell.delegate = self
            setUpSwipeGestures(cell: articleCell)
            return articleCell
        }
        return UITableViewCell()
    }
    //method to create window cell
    private func windowCell(for tableView: UITableView, indexPath: IndexPath, window: Window) -> UITableViewCell {
        if let windowCell = tableView.dequeueReusableCell(withIdentifier: WindowCell.identifier, for: indexPath) as? WindowCell {
            windowCell.prepareCell(with: window, atIndex: indexPath)
            windowCell.delegate = self
            setUpSwipeGestures(cell: windowCell)
            return windowCell
        }
        return UITableViewCell()
    }
    //method to create story cell
    private func storyCell(for tableView: UITableView, indexPath: IndexPath, story: Story) -> UITableViewCell {
        if let storyCell = tableView.dequeueReusableCell(withIdentifier: StoryCell.identifier, for: indexPath) as? StoryCell {
            storyCell.prepareCell(with: story, atIndex: indexPath)
            storyCell.delegate = self
            setUpSwipeGestures(cell: storyCell)
            return storyCell
        }
        return UITableViewCell()
    }
    //method to display activity indicator for lazy loading.
    private func activityIndicatorCell(for tableView: UITableView, indexPath: IndexPath) -> UITableViewCell {
        let loadingCell = UITableViewCell.init(frame: CGRect.init(x: 0, y: 0, width: self.searchResultsTable.frame.size.width, height: 20))
        return loadingCell
    }
    // add swipe gestures
    func setUpSwipeGestures(cell: UITableViewCell) {
        let swipeRight = UISwipeGestureRecognizer(target: self, action: #selector(respondToSwipeGesture(gesture:)))
        swipeRight.direction = UISwipeGestureRecognizer.Direction.right
        swipeRight.numberOfTouchesRequired = 2
        cell.addGestureRecognizer(swipeRight)
        let swipeLeft = UISwipeGestureRecognizer(target: self, action: #selector(respondToSwipeGesture(gesture:)))
        swipeLeft.direction = UISwipeGestureRecognizer.Direction.left
        swipeLeft.numberOfTouchesRequired = 2
        cell.addGestureRecognizer(swipeLeft)
    }
    // add swipe responses
    @objc func respondToSwipeGesture(gesture: UIGestureRecognizer) {
        if let swipeGesture = gesture as? UISwipeGestureRecognizer {
            switch swipeGesture.direction {
            case UISwipeGestureRecognizer.Direction.right:
                print("Swiped right")// logic for previous post should come here
            case UISwipeGestureRecognizer.Direction.left:
                print("Swiped left")// logic for next post should come here
            default:
                break
            }
        }
    }

}

extension SearchViewController: StoryCellDelegate {
    func storyCell(_ cell: StoryCell, didClickRelateButtonFor post: Post, markAsRelate relate: Bool) {
        if let posts = feeds as? [Post], let index = posts.firstIndex(where: {$0.postId == post.postId}) {
//            if relate {
//                cell.relateButton.backgroundColor = UIColor.relatableGreenColor
//            } else {
//                cell.relateButton.backgroundColor = UIColor.appGreenColor
//            }
            presenter?.relateButtonClicked(for: feeds[index], markAsRelate: relate)
        }
    }
    func storyCell(_ cell: StoryCell, didClickBackpackButtonFor post: Post, addToBackPack: Bool) {
        if let posts = feeds as? [Post], let index = posts.firstIndex(where: {$0.postId == post.postId}) {
            presenter?.backpackButtonClicked(for: feeds[index], addToBackPack: addToBackPack)
        }
    }
    func storyCell(_ cell: StoryCell, didClickLeafButtonFor post: Post) {
        presenter?.leafButtonClicked(for: post)
    }
}

extension SearchViewController: ArticleCellDelegate {
    func articleCell(_ cell: ArticleCell, didClickBackpackButtonFor post: Post, addToBackpack: Bool) {
        if let posts = feeds as? [Post], let index = posts.firstIndex(where: {$0.postId == post.postId}) {
            presenter?.backpackButtonClicked(for: feeds[index], addToBackPack: addToBackpack)
        }
    }
}

extension SearchViewController: WindowCellDelegate {
    func windowCell(_ cell: WindowCell, didClickOnRelateButtonFor post: Post, markAsRelate relate: Bool) {
        if let posts = feeds as? [Post], let index = posts.firstIndex(where: {$0.postId == post.postId}) {
            presenter?.relateButtonClicked(for: feeds[index], markAsRelate: relate)
        }
    }
    func windowCell(_ cell: WindowCell, didClickBackpackButtonFor post: Post, addToBackPack: Bool) {
        if let posts = feeds as? [Post], let index = posts.firstIndex(where: {$0.postId == post.postId}) {
            presenter?.backpackButtonClicked(for: feeds[index], addToBackPack: addToBackPack)
        }
    }
}
