//
//  SearchPresenter.swift
//  Lighf
//
//  Created by L.
//  Copyright © 2019 L. All rights reserved.
//

import Foundation
import UIKit
import SwiftyJSON

class SearchPresenter: NSObject {
    var router: SearchPresenterToRouterProtocol?
    var interactor: SearchPresenterToInteractorProtocol?
    weak var view: SearchPresenterToViewProtocol?
    var getFeedsInteractor: SearchPresenterToGetFeedsInteractorProtocol?
    weak var homeInterface: PostOperationsProtocol?
    var leafInteractor: SearchPresenterToGiveLeafInteractorProtocol?
    var toggleNotificationInteractor: SearchPresenterToToggleNotificationProtocol?
}
extension SearchPresenter: SearchViewToPresenterProtocol {
    func toggleNotification(forItem: Any, toState: NotificationState) {
        view?.showActivityIndicator(onFullScreen: true)
        toggleNotificationInteractor?.invokeToggleNotification(forItem: forItem, toState: toState)
    }
    func deletePost(_ post: Post) {
        view?.showActivityIndicator()
        interactor?.deletePost(post)
    }
    //
    func backpackButtonClicked(for post: Any, addToBackPack: Bool) {
        view?.showActivityIndicator()
        interactor?.backPackPost(post, addToBackPack: addToBackPack)
    }
    func relateButtonClicked(for post: Any, markAsRelate: Bool) {
        view?.showActivityIndicator()
        interactor?.markPost(post, asRelate: markAsRelate)
    }
    func leafButtonClicked(for post: Any) {
        if let selectedPost = post as? Post {
            view?.showActivityIndicator()
            leafInteractor?.invokeGiveLeaf(forPost: selectedPost, leafCount: 1)
        }
    }
    func flagButtonClicked(selectedFlagOption: FlagOptions, for postID: Post) {
        router?.alertActionTapped(selectedOption: selectedFlagOption, postID: postID)
    }

    func viewPost(_ post: Post, type: PostType) {
        router?.viewPost(post, type: type, interface: self)
    }
    // invoke method to search.
    func invokeSearchWithText(from index: Int, to count: Int, searchWith searchString: String, filterWith filterParam: NSMutableDictionary) {
        getFeedsInteractor?.fetchSearchFeeds(from: index, to: count, searchWith: searchString, filterWith: filterParam)
    }
    func editPost(_ post: Post, type: PostType) {
        let postDetailsInteractorObject = PostDetailsInteractor()
        postDetailsInteractorObject.searchPresenter = self
        postDetailsInteractorObject.getDetails(for: post, type: type)
    }
    func getSuggestionTags() {
        view?.showActivityIndicator()
        interactor?.getSearchTags()
    }
}

extension SearchPresenter: SearchModuleInterface {
    func toggleNotification(with postId: Int, isEnabled: NotificationState) {
        view?.updateNotificationStatus(postID: postId, commentID: nil, notificationStatus: isEnabled)
    }
    func updateFeedForRelate(with postId: Int, toRelate addToRelates: Bool) {
        view?.markPost(with: postId, asRelated: addToRelates)
        homeInterface?.markPost(with: postId, asRelated: addToRelates)
    }
    func removePostFromFeed(with postId: Int) {
        view?.removePost(with: postId)
        homeInterface?.removePostFromFeed(with: postId)
    }
    func addPost(with postId: Int, toBackPack addToBackPack: Bool) {
        view?.addPost(with: postId, toBackPack: addToBackPack)
        homeInterface?.addPost(with: postId, toBackPack: addToBackPack)
    }
    func markPost(with postId: Int, asRelated asRelate: Bool) {
        view?.markPost(with: postId, asRelated: asRelate)
        homeInterface?.markPost(with: postId, asRelated: asRelate)
    }
    func markPost(with postId: Int, asRead markAsRead: Bool) {
        view?.markPost(with: postId, asRead: markAsRead)
        homeInterface?.markPost(with: postId, asRead: markAsRead)
    }
}

extension SearchPresenter: SearchRouterToPresenterProtocol {
    //
}

extension SearchPresenter: SearchInteractorToPresenterProtocol {
    func handleTags(_ activeTags: [Tag], userTags: [Tag], error: String?) {
        view?.dismissActivityIndicator()
        if error == nil {
            view?.showTags(activeTags, userTags: userTags)
        } else {
            view?.showAlert(error!)
        }
    }
    //
    func deletedPost(_ post: Post, with message: String) {
        view?.dismissActivityIndicator()
        if let postId = post.postId {
            view?.removePost(with: postId)
            homeInterface?.removePostFromFeed(with: postId)
        }
        view?.showToast(with: message, completion: {})
    }
    //
    func deletePost(_ post: Post, failedWithMessage message: String) {
        view?.dismissActivityIndicator()
        view?.showAlert(message == Constants.emptyString ? Message.removePost : message)
        // If the post is deleted by the author or backend, remove it from the feed
        if post.isDeleted, let postId = post.postId {
            view?.removePost(with: postId)
        }
    }
    //
    func performedOperation(_ operation: PostOperation, onPost post: Post, with error: String?) {
        view?.dismissActivityIndicator()
        if error == nil, let postId = post.postId {
            var toastMessage = ""
            switch operation {
            case .addBackPack:
                toastMessage = Message.addToBackpack
                view?.addPost(with: postId, toBackPack: true)
                homeInterface?.addPost(with: postId, toBackPack: true)
            case .removeBackPack:
                toastMessage = Message.removeFromBackPack
                view?.addPost(with: postId, toBackPack: false)
                homeInterface?.addPost(with: postId, toBackPack: false)
            case .related:
                toastMessage = Message.markAsRelate
                view?.markPost(with: postId, asRelated: true)
                homeInterface?.markPost(with: postId, asRelated: true)
            case .unRelated:
                toastMessage = Message.markAsNotRelated
                view?.markPost(with: postId, asRelated: false)
                homeInterface?.markPost(with: postId, asRelated: false)
            default: break
            }
            if toastMessage != Constants.emptyString {
                view?.showToast(with: toastMessage, completion: {})
            }
        } else {
            // If the post is deleted by the author or backend, remove it from the feed
            if post.isDeleted, let postId = post.postId {
                view?.removePost(with: postId)
                homeInterface?.removePostFromFeed(with: postId)
                view?.showToast(with: error! == Constants.emptyString ? Message.removePost : error!, completion: {})
            } else {
                 view?.showAlert(error!)
            }
        }
        let defaults  = UserDefaults.standard
        defaults.set("true", forKey: UserDefaultsKey.updateFromSearch)
    }
}
extension SearchPresenter: GetFeedsInteractorToSearchPresenterProtocol {
    func handleFeeds(_ feeds: [JSON], error: String?) {
        view?.dismissActivityIndicator()
        var responseArray: [Any] = []
        for loopVar in 0..<feeds.count {
            let postDict = feeds[loopVar]
            if let postType = PostType(rawValue: postDict["post_type"].stringValue) {
                switch postType {
                case .story, .statement:
                    responseArray.append(Story(storyObj: postDict))
                case .article:
                    responseArray.append(Article(articleObj: postDict))
                case .window:
                    responseArray.append(Window(windowObj: postDict))
                case .admin_post:
                    responseArray.append(AdminPost(adminPostObj: postDict))
                }
            }
        }
        guard error == Constants.emptyString else {
            view?.showAlert(error ?? Constants.emptyString)
            return
        }
        view?.showFeeds(responseArray)
    }
}

extension SearchPresenter: PostDetailsInteractorToSearchPresenterProtocol {
    // to handle edit
    func handle(post: Post, type: PostType) {
        view?.dismissActivityIndicator()
        router?.moveToEditPostView(with: post, type: type)
    }
    // to handle error scenarios
    func detailFetchFailed(for post: Post, with message: String) {
        view?.dismissActivityIndicator()
        view?.showAlert(message == Constants.emptyString ? Message.removePost : message)
        // If the post is deleted by the author or backend, remove it from the feed
        if post.isDeleted, let postId = post.postId {
            view?.removePost(with: postId)
        }
    }
}
extension SearchPresenter: GiveLeafInteractorToSearchPresenterProtocol {
    func giveLeafAPIResponse(_ status: String, error: String?) {
        view?.dismissActivityIndicator()
        if status != "ok" {
            view?.showAlert(error ?? Constants.emptyString)
        } else {
            view?.showToast(with: error ?? Constants.emptyString, completion: {})
        }
    }
}
extension SearchPresenter: ToggleNotificationToSearchPresenterProtocol {
    func toggleNotificationAPIResponse(_ itemID: Int, itemType: String, notificationStatus: NotificationState) {
        view?.dismissActivityIndicator()
        if itemType == "post" {
            view?.updateNotificationStatus(postID: itemID, commentID: nil, notificationStatus: notificationStatus)
        } else {
            view?.updateNotificationStatus(postID: nil, commentID: itemID, notificationStatus: notificationStatus)
        }

    }
    func toggleNotificationAPIResponseFailure(_ status: String, error: String?) {
        view?.dismissActivityIndicator()
        if status != "ok" {
            view?.showAlert(error ?? Constants.emptyString)
        } else {
            view?.showToast(with: error ?? Constants.emptyString, completion: {})
        }
    }
}
