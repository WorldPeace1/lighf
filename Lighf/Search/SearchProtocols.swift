//
//  SearchProtocols.swift
//  Lighf
//
//  Created by L.
//  Copyright © 2019 L. All rights reserved.
//

import Foundation
import UIKit
import SwiftyJSON

protocol SearchViewToPresenterProtocol: class {
    func invokeSearchWithText(from index: Int, to count: Int, searchWith searchString: String, filterWith filterParam: NSMutableDictionary)
    func viewPost(_ post: Post, type: PostType)
    func leafButtonClicked(for post: Any)
    func flagButtonClicked(selectedFlagOption: FlagOptions, for postID: Post)
    func backpackButtonClicked(for post: Any, addToBackPack: Bool)
    func relateButtonClicked(for post: Any, markAsRelate: Bool)
    func editPost(_ post: Post, type: PostType)
    func deletePost(_ post: Post)
    func getSuggestionTags()
    func toggleNotification(forItem: Any, toState: NotificationState)
}
protocol SearchPresenterToViewProtocol: BasePresenterToViewProtocol {
    func showFeeds(_ feeds: [Any])
    func showAlert(_ message: String)
    func addPost(with postId: Int, toBackPack addToBackPack: Bool)
    func markPost(with postId: Int, asRelated asRelate: Bool)
    func markPost(with postId: Int, asRead markAsRead: Bool)
//    func updateBackpackedStatus(with postId: Int, toBackPack addToBackPack: Bool)
//    func updateRelateStatus(_ relate: Bool, forPostWith postId: Int)
    func removePost(with postId: Int)
    func showTags(_ activeTags: [Tag], userTags: [Tag])
    func updateNotificationStatus(postID: Int?, commentID: Int?, notificationStatus: NotificationState)
}
protocol SearchPresenterToInteractorProtocol: class {
    func invokeSearchAPI(string: String)
    func markPost(_ post: Any, asRelate: Bool)
    func backPackPost(_ post: Any, addToBackPack: Bool)
    func deletePost(_ post: Post)
    func getSearchTags()
}
protocol SearchInteractorToPresenterProtocol: class {
    func performedOperation(_ operation: PostOperation, onPost post: Post, with error: String?)
    func deletedPost(_ post: Post, with message: String)
    func deletePost(_ post: Post, failedWithMessage message: String)
    func handleTags(_ activeTags: [Tag], userTags: [Tag], error: String?)
}
protocol SearchPresenterToRouterProtocol: class {
    func viewPost(_ post: Post, type: PostType, interface: SearchModuleInterface)
    func alertActionTapped(selectedOption: FlagOptions, postID: Post)
    func moveToEditPostView(with post: Post, type: PostType)
}
protocol SearchRouterToPresenterProtocol: class {
}
protocol SearchPresenterToGetFeedsInteractorProtocol {
    func fetchSearchFeeds(from index: Int, to count: Int, searchWith searchString: String, filterWith filterParam: NSMutableDictionary)
}
protocol GetFeedsInteractorToSearchPresenterProtocol {
    func handleFeeds(_ feeds: [JSON], error: String?)
}

protocol SearchModuleInterface: PostOperationsProtocol {
}
protocol PostDetailsInteractorToSearchPresenterProtocol: class {
    func handle(post: Post, type: PostType)
    func detailFetchFailed(for post: Post, with message: String)
}
protocol SearchPresenterToGiveLeafInteractorProtocol: class {
    func invokeGiveLeaf(forPost: Post, leafCount: Int)
}
protocol GiveLeafInteractorToSearchPresenterProtocol: class {
    func giveLeafAPIResponse(_ status: String, error: String?)
}

protocol SearchPresenterToToggleNotificationProtocol: class {
    func invokeToggleNotification(forItem: Any, toState: NotificationState)
}
protocol ToggleNotificationToSearchPresenterProtocol: class {
    func toggleNotificationAPIResponse(_ itemID: Int, itemType: String, notificationStatus: NotificationState)
    func toggleNotificationAPIResponseFailure(_ status: String, error: String?)
}
