//
//  SearchInteractor.swift
//  Lighf
//
//  Created by L.
//  Copyright © 2019 L. All rights reserved.
//

import Foundation
import UIKit
class SearchInteractor: PostOperationsInteractor, TagOperationProtoccol {
    weak var presenter: SearchInteractorToPresenterProtocol?
    //
    override func performedOperation(_ operation: PostOperation, onPost post: Post, with error: String?) {
        presenter?.performedOperation(operation, onPost: post, with: error)
    }
}
extension SearchInteractor: SearchPresenterToInteractorProtocol {
    func markPost(_ post: Any, asRelate: Bool) {
        if let selectedPost = post as? Post {
            mark(selectedPost, asRelated: asRelate)
        }
    }
    func backPackPost(_ post: Any, addToBackPack: Bool) {
        if let selectedPost = post as? Post {
            add(selectedPost, toBackPack: addToBackPack)
        }
    }
    func invokeSearchAPI(string: String) {
        //logic to invoke api
    }
    func deletePost(_ post: Post) {
        let defaults = UserDefaults.standard
        var parameters: [String: Any] = [:]
        parameters["id"] = defaults.value(forKey: UserDefaultsKey.userID)
        parameters["post_id"] = post.postId
        //
        APIManager.post(endPoint: EndPoint.deletePost, parameters: parameters) { (response, error) in
            guard error == nil, let response = response else {
                self.presenter?.deletePost(post, failedWithMessage: error!.message)
                return
            }
            let message = response[APIResponse.apiResponse][APIResponse.apiResponseMessage].stringValue
            let status = response[APIResponse.apiResponse][APIResponse.apiResponseStatus].stringValue
            if status == Constants.success {
                self.presenter?.deletedPost(post, with: message)
            } else {
                var currentPost = post
                currentPost.isDeleted = response[APIResponse.apiResponse]["is_deleted"].boolValue
                self.presenter?.deletePost(currentPost, failedWithMessage: message)
            }
        }
    }
    func getSearchTags() {
        let defaults = UserDefaults.standard
        var parameters: [String: Any] = [:]
        parameters["num_tags"] = 5
        parameters["offset"] = 0
        parameters["user_id"] = defaults.value(forKey: UserDefaultsKey.userID)
        parameters["filter"] = "profile_tags"
       //
        getSearchTags(properties: parameters) { (activeTags, forYouTags, error) in
            self.presenter?.handleTags(activeTags, userTags: forYouTags, error: error)
        }
    }
}
