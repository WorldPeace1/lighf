//
//  SearchViewController.swift
//  Lighf
//
//  Created by L.
//  Copyright © 2019 L. All rights reserved.
//

import Foundation
import UIKit
import MatomoTracker
class SearchViewController: BaseViewController {
    var presenter: SearchViewToPresenterProtocol?
    @IBOutlet weak var baseView: UIView!
    @IBOutlet weak var searchBarView: UIView!
    @IBOutlet weak var activeStack: UIStackView!
    @IBOutlet weak var forYouStack: UIStackView!
    @IBOutlet weak var activeStackLabel: UILabel!
    @IBOutlet weak var forYourLabel: UILabel!
    @IBOutlet weak var searchBarViewHeight: NSLayoutConstraint!
    @IBOutlet weak var searchResultsTable: UITableView!
    @IBOutlet weak var lighfSearchBar: UISearchBar!
    @IBOutlet weak var filterButton: UIButton!
    var filterView = FilterView()
    var isSearchMode = false
    var postOffset = 0
    let numberOfPostsPerAPI = 15
    var isAllFeedReceived: Bool = false
    var feeds: [Any] = []
    var searchString: String = Constants.emptyString
    var refreshControl = UIRefreshControl()
    var paramDict = NSMutableDictionary()
    var isFilterActive: Bool = false
    var isFilterVisible: Bool = false
    var isTableScrolled: Bool = false
    override func viewDidLoad() {
        super.viewDidLoad()
//        configureActiveStack()
//        configureForYouStack()
        presenter?.getSuggestionTags()
        configureViewVisibility()
        MatomoTracker.shared.track(view: ["Search view"])
        refreshControl.addTarget(self, action: #selector(refreshFeeds), for: UIControl.Event.valueChanged)
        self.searchResultsTable.addSubview(refreshControl) // not required when using UITableViewController
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.navigationBar.tintColor = UIColor.white
        configureHeaderView()
        UserDefaults.standard.set([], forKey: UserDefaultsKey.postHistoryArray) // to remove all swipe history.
    }
    func configureViewVisibility() {
        if isSearchMode {
            searchResultsTable.isHidden = false
            forYouStack.isHidden = true
            forYourLabel.isHidden = true
            activeStack.isHidden = true
            activeStackLabel.isHidden = true
        } else {
            searchResultsTable.isHidden = true
            forYouStack.isHidden = false
            forYourLabel.isHidden = false
            activeStack.isHidden = false
            activeStackLabel.isHidden = false
        }
    }
    //
    func configureHeaderView() {
        // Change search bar cancel button color
        UIBarButtonItem.appearance(whenContainedInInstancesOf: [UISearchBar.self]).setTitleTextAttributes([NSAttributedString.Key.foregroundColor: UIColor.black], for: .normal)
        let barView = UIView.init(frame: CGRect(x: 0, y: 0, width: self.view.frame.width - 150, height: 40))
        let notificationImage =  UIImageView.init(image: UIImage.init(named: "Search"))
        let verticalConstraint = NSLayoutConstraint(item: notificationImage, attribute: NSLayoutConstraint.Attribute.centerX, relatedBy: NSLayoutConstraint.Relation.equal, toItem: barView, attribute: NSLayoutConstraint.Attribute.centerX, multiplier: 1, constant: 0)
        let horizontalConstraint = NSLayoutConstraint(item: notificationImage, attribute: NSLayoutConstraint.Attribute.centerY, relatedBy: NSLayoutConstraint.Relation.equal, toItem: barView, attribute: NSLayoutConstraint.Attribute.centerY, multiplier: 1, constant: 0)
        barView.addSubview(notificationImage)
        barView.backgroundColor = .clear
        baseView.backgroundColor = UIColor.appGreenColor
        NSLayoutConstraint.activate([verticalConstraint, horizontalConstraint])
        navigationItem.titleView = barView
        navigationController?.setBackGroundColor(UIColor.appGreenColor)
    }
    func configureActiveStack() {
        let buttonOne = UIButton.init(frame: CGRect.init(x: 0, y: 60, width: activeStack.frame.size.width, height: 30))
        buttonOne.setTitle("one", for: .normal)
        buttonOne.setTitleColor(UIColor.appBlueColor, for: .normal)
        buttonOne.titleLabel?.font = UIFont.init(name: Font.sfProTextLight.rawValue, size: 24.0)
        buttonOne.addTarget(self, action: #selector(stackButtonTapped(sender:)), for: .touchDown)
        let buttonTwo = UIButton.init(frame: CGRect.init(x: 0, y: 60, width: activeStack.frame.size.width, height: 30))
        buttonTwo.setTitle("two", for: .normal)
        buttonTwo.setTitleColor(UIColor.appBlueColor, for: .normal)
        buttonTwo.titleLabel?.font = UIFont.init(name: Font.sfProTextLight.rawValue, size: 24.0)
        buttonTwo.addTarget(self, action: #selector(stackButtonTapped(sender:)), for: .touchDown)
        let buttonThree = UIButton.init(frame: CGRect.init(x: 0, y: 60, width: activeStack.frame.size.width, height: 30))
        buttonThree.setTitle("three", for: .normal)
        buttonThree.setTitleColor(UIColor.appBlueColor, for: .normal)
        buttonThree.titleLabel?.font = UIFont.init(name: Font.sfProTextLight.rawValue, size: 24.0)
        buttonThree.addTarget(self, action: #selector(stackButtonTapped(sender:)), for: .touchDown)
        let buttonFour = UIButton.init(frame: CGRect.init(x: 0, y: 60, width: activeStack.frame.size.width, height: 30))
        buttonFour.setTitle("four", for: .normal)
        buttonFour.setTitleColor(UIColor.appBlueColor, for: .normal)
        buttonFour.titleLabel?.font = UIFont.init(name: Font.sfProTextLight.rawValue, size: 24.0)
        buttonFour.addTarget(self, action: #selector(stackButtonTapped(sender:)), for: .touchDown)
        let buttonFive = UIButton.init(frame: CGRect.init(x: 0, y: 60, width: activeStack.frame.size.width, height: 30))
        buttonFive.setTitle("five", for: .normal)
        buttonFive.setTitleColor(UIColor.appBlueColor, for: .normal)
        buttonFive.titleLabel?.font = UIFont.init(name: Font.sfProTextLight.rawValue, size: 24.0)
        buttonFive.addTarget(self, action: #selector(stackButtonTapped(sender:)), for: .touchDown)
        activeStack.addArrangedSubview(buttonOne)
        activeStack.addArrangedSubview(buttonTwo)
        activeStack.addArrangedSubview(buttonThree)
        activeStack.addArrangedSubview(buttonFour)
        activeStack.addArrangedSubview(buttonFive)
    }
    func configureForYouStack() {
        let buttonOne = UIButton.init(frame: CGRect.init(x: 0, y: 60, width: activeStack.frame.size.width, height: 30))
        buttonOne.setTitle("one", for: .normal)
        buttonOne.setTitleColor(UIColor.appBlueColor, for: .normal)
        buttonOne.titleLabel?.font = UIFont.init(name: Font.sfProTextLight.rawValue, size: 24.0)
        buttonOne.addTarget(self, action: #selector(stackButtonTapped(sender:)), for: .touchDown)
        let buttonTwo = UIButton.init(frame: CGRect.init(x: 0, y: 60, width: activeStack.frame.size.width, height: 30))
        buttonTwo.setTitle("two", for: .normal)
        buttonTwo.setTitleColor(UIColor.appBlueColor, for: .normal)
        buttonTwo.titleLabel?.font = UIFont.init(name: Font.sfProTextLight.rawValue, size: 24.0)
        buttonTwo.addTarget(self, action: #selector(stackButtonTapped(sender:)), for: .touchDown)
        let buttonThree = UIButton.init(frame: CGRect.init(x: 0, y: 60, width: activeStack.frame.size.width, height: 30))
        buttonThree.setTitle("three", for: .normal)
        buttonThree.setTitleColor(UIColor.appBlueColor, for: .normal)
        buttonThree.titleLabel?.font = UIFont.init(name: Font.sfProTextLight.rawValue, size: 24.0)
        buttonThree.addTarget(self, action: #selector(stackButtonTapped(sender:)), for: .touchDown)
        let buttonFour = UIButton.init(frame: CGRect.init(x: 0, y: 60, width: activeStack.frame.size.width, height: 30))
        buttonFour.setTitle("four", for: .normal)
        buttonFour.setTitleColor(UIColor.appBlueColor, for: .normal)
        buttonFour.titleLabel?.font = UIFont.init(name: Font.sfProTextLight.rawValue, size: 24.0)
        buttonFour.addTarget(self, action: #selector(stackButtonTapped(sender:)), for: .touchDown)
        let buttonFive = UIButton.init(frame: CGRect.init(x: 0, y: 60, width: activeStack.frame.size.width, height: 30))
        buttonFive.setTitle("five", for: .normal)
        buttonFive.setTitleColor(UIColor.appBlueColor, for: .normal)
        buttonFive.titleLabel?.font = UIFont.init(name: Font.sfProTextLight.rawValue, size: 24.0)
        buttonFive.addTarget(self, action: #selector(stackButtonTapped(sender:)), for: .touchDown)
        forYouStack.addArrangedSubview(buttonOne)
        forYouStack.addArrangedSubview(buttonTwo)
        forYouStack.addArrangedSubview(buttonThree)
        forYouStack.addArrangedSubview(buttonFour)
        forYouStack.addArrangedSubview(buttonFive)
    }
    @objc func stackButtonTapped(sender: UIButton) {
        guard let title = sender.titleLabel?.text else {
            return
        }
        print(title as String)
        feeds.removeAll()
        searchString = title
        postOffset = 0
        self.view.endEditing(true)
        lighfSearchBar.text = title
        showActivityIndicator()
        presenter?.invokeSearchWithText(from: postOffset, to: numberOfPostsPerAPI, searchWith: searchString, filterWith: paramDict)
        isSearchMode = true
        configureViewVisibility()
    }
    @IBAction func filterButtonTapped(sender: UIButton) {
        if isFilterVisible { // logic to disable filter
            sender.isSelected = false
            filterView.removeFromSuperview()
            searchBarViewHeight.constant = 56
            isFilterVisible = false
            isFilterActive = false
            paramDict.removeObject(forKey: GetFeedOptions.feedTypeKey.rawValue)
            paramDict.removeObject(forKey: GetFeedOptions.feedFilterKey.rawValue)
            removeDataFromTable()
            if searchString != Constants.emptyString {
                // Do not reload the table if text is not entered in search box
                refreshFeeds()
            }
        } else if isFilterActive && !isFilterVisible { //logic to show filter without changing anything
            searchBarViewHeight.constant = 108
            searchBarView.addSubview(filterView)
            sender.isSelected = true
            isFilterVisible = true
        } else { // logic to implement filter
            createFilter(sender: sender)
        }
    }
    func createFilter(sender: UIButton) {
        if sender.isSelected {
            sender.isSelected = false
            filterView.removeFromSuperview()
            searchBarViewHeight.constant = 56
        } else {
            filterView = FilterView(frame: CGRect(x: 0, y: 56, width: view.frame.width, height: 52), values: [Utility.getAttributtedString(string: "lighfStory", fontName: Font.sfProDisplayLight.rawValue, size: 16.0), Utility.getAttributtedString(string: "Window", fontName: Font.sfProDisplayLight.rawValue, size: 16.0), Utility.getAttributtedString(string: "Article", fontName: Font.sfProDisplayLight.rawValue, size: 16.0)], images: nil, includeFilter: false)
            filterView.segmentControl.selectedSegmentIndex = 0
            filterView.view = self
            searchBarViewHeight.constant = 108
            searchBarView.addSubview(filterView)
            sender.isSelected = true
            isFilterVisible = true
            isFilterActive = true
            paramDict[GetFeedOptions.feedTypeKey.rawValue] = [GetFeedOptions.feedPostTypeFilterKey.rawValue]
            paramDict[GetFeedOptions.feedFilterKey.rawValue] = [PostType.story.rawValue]
            if searchResultsTable.contentOffset.y > 0 {
                isTableScrolled = true
            } else {
                isTableScrolled = false
            }
        }
        if !feeds.isEmpty {
            searchResultsTable.scrollToRow(at: IndexPath.init(row: 0, section: 0), at: .top, animated: true)
        }
        removeDataFromTable()
        if searchString != Constants.emptyString {
            refreshFeeds()
        }
    }
    // flagging options
    func flagOptionsButtonClicked(sender: Post) {
        let optionMenu = UIAlertController(title: nil, message: FlagOptions.header.rawValue, preferredStyle: .actionSheet)
        let notRelatedAction = UIAlertAction(title: FlagOptions.notRelated.rawValue, style: .default, handler: { (_ action) -> Void in
            self.alertActionSelected(selectedOption: FlagOptions.notRelated, withPost: sender)
        })
        let spamAction = UIAlertAction(title: FlagOptions.spam.rawValue, style: .default, handler: { (_ action) -> Void in
            self.alertActionSelected(selectedOption: FlagOptions.spam, withPost: sender)
        })
        let abuseAction = UIAlertAction(title: FlagOptions.abuse.rawValue, style: .default, handler: { (_ action) -> Void in
            self.alertActionSelected(selectedOption: FlagOptions.abuse, withPost: sender)
        })
        let copyRightAction = UIAlertAction(title: FlagOptions.copyright.rawValue, style: .default, handler: { (_ action) -> Void in
            self.alertActionSelected(selectedOption: FlagOptions.copyright, withPost: sender)
        })
        /* let otherAction = UIAlertAction(title: FlagOptions.other.rawValue, style: .default, handler: { (_ action) -> Void in
            self.alertActionSelected(selectedOption: FlagOptions.other, withPost: sender)
        })
        */
        let dontSeeThisAction = UIAlertAction(title: FlagOptions.dontSeeThis.rawValue, style: .default, handler: { (_ action) -> Void in
            self.alertActionSelected(selectedOption: FlagOptions.dontSeeThis, withPost: sender)
        })
        let dontSeeLikeThisAction = UIAlertAction(title: FlagOptions.dontSeeLikeThis.rawValue, style: .default, handler: { (_ action) -> Void in
            self.alertActionSelected(selectedOption: FlagOptions.dontSeeLikeThis, withPost: sender)
        })
        let turnOffNotificationAction = UIAlertAction(title: FlagOptions.turnOffNotification.rawValue, style: .default, handler: { (_ action) -> Void in
            //            self.alertActionSelected(selectedOption: FlagOptions.turnOffNotification, withPost: sender)
        })
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel)
        optionMenu.addAction(notRelatedAction)
        optionMenu.addAction(spamAction)
        optionMenu.addAction(abuseAction)
        optionMenu.addAction(copyRightAction)
        optionMenu.addAction(dontSeeThisAction)
        optionMenu.addAction(dontSeeLikeThisAction)
        optionMenu.addAction(turnOffNotificationAction)
        optionMenu.addAction(cancelAction)
        self.present(optionMenu, animated: true, completion: nil)
    }
    //flag option selected.
    func alertActionSelected(selectedOption: FlagOptions, withPost: Post) {
        presenter?.flagButtonClicked(selectedFlagOption: selectedOption, for: withPost)
    }
    //
    private func stackButton(with text: String) -> UIButton {
        let button = UIButton(frame: CGRect.init(x: 0, y: 60, width: activeStack.frame.size.width, height: 30))
        button.setTitle(text, for: .normal)
        button.setTitleColor(UIColor.appBlueColor, for: .normal)
        button.titleLabel?.font = UIFont.init(name: Font.sfProTextLight.rawValue, size: 24.0)
        button.titleLabel?.lineBreakMode = .byTruncatingTail
        button.addTarget(self, action: #selector(stackButtonTapped(sender:)), for: .touchDown)
        return button
    }
}
extension SearchViewController: SearchPresenterToViewProtocol {
    func updateNotificationStatus(postID: Int?, commentID: Int?, notificationStatus: NotificationState) {
        if postID != nil, let posts = self.feeds as? [Post] {
            let postIndex = posts.firstIndex { (post) -> Bool in
                return post.postId == postID
            }
            if let indexToEdit = postIndex, var updatedPost = self.feeds[indexToEdit] as? Post {
                if notificationStatus == NotificationState.turnOn {
                    updatedPost.isNotificationEnabled = true
                } else {
                    updatedPost.isNotificationEnabled = false
                }
                self.feeds[indexToEdit] = updatedPost
                self.searchResultsTable.reloadRows(at: [IndexPath.init(row: indexToEdit, section: 0)], with: .none)
            }
        } else {
            print("Error!")
        }

    }
    func showTags(_ activeTags: [Tag], userTags: [Tag]) {
        activeTags.forEach { (activeTag) in
            activeStack.addArrangedSubview(stackButton(with: activeTag.name))
        }
        //
        userTags.forEach { (userTag) in
            forYouStack.addArrangedSubview(stackButton(with: userTag.name))
        }
    }
    //
    func addPost(with postId: Int, toBackPack addToBackPack: Bool) {
        if var post = getPost(with: postId) {
            post.0.isBackPacked = addToBackPack
            feeds[post.1] = post.0
            UIView.performWithoutAnimation {
                searchResultsTable.reloadRows(at: [IndexPath(row: post.1, section: 0)], with: .none)
            }
        }
    }
    //
//    func updateBackpackedStatus(with postId: Int, toBackPack addToBackPack: Bool) {
//        if var post = getPost(with: postId) {
//            post.0.isBackPacked = addToBackPack
//            feeds[post.1] = post.0
//        }
//    }
    //
    func markPost(with postId: Int, asRelated asRelate: Bool) {
        if var post = getPost(with: postId) {
            post.0.isRelated = asRelate
            feeds[post.1] = post.0
            UIView.performWithoutAnimation {
                searchResultsTable.reloadRows(at: [IndexPath(row: post.1, section: 0)], with: .none)
            }
        }
    }
    //
//    func updateRelateStatus(_ relate: Bool, forPostWith postId: Int) {
//        if var post = getPost(with: postId) {
//            post.0.isRelated = relate
//            feeds[post.1] = post.0
//            searchResultsTable.reloadRows(at: [IndexPath(row: post.1, section: 0)], with: .none)
//        }
//    }
    func markPost(with postId: Int, asRead markAsRead: Bool) {
        if var post = getPost(with: postId) {
            post.0.isRead = markAsRead
            feeds[post.1] = post.0
            searchResultsTable.reloadRows(at: [IndexPath(row: post.1, section: 0)], with: .none)
        }
    }
    //
    func removePost(with postId: Int) {
        if let post = getPost(with: postId) {
            feeds.remove(at: post.1)
            UIView.performWithoutAnimation {
                searchResultsTable.deleteRows(at: [IndexPath(row: post.1, section: 0)], with: .none)
            }
        }
    }
    //
    func getPost(with postId: Int) -> (Post, Int)? {
        if let posts = feeds as? [Post] {
            let post = posts.filter {$0.postId == postId}.first
            let postIndex = posts.firstIndex { (post) -> Bool in
                return post.postId == postId
            }
            if let index = postIndex, let value = post {
                return (value, index)
            }
        }
        return nil
    }
    func showAlert(_ message: String) {
        removeActivityIndicatorCell()
        isAllFeedReceived = true
        if feeds.isEmpty {
            isSearchMode = false
            configureViewVisibility()
        }
        if self.navigationController?.viewControllers.contains(self) == true {
            let alert = UIAlertController.alert(with: message)
            self.present(alert, animated: true, completion: nil)
        }
    }
    func removeActivityIndicatorCell() {
        if !feeds.isEmpty && !isAllFeedReceived {
            let activityCell = searchResultsTable.cellForRow(at: IndexPath.init(row: feeds.count, section: 0))
            for view in activityCell?.subviews ?? [] where view is UIActivityIndicatorView {
                let activityIndicator = view as? UIActivityIndicatorView
                activityIndicator?.stopAnimating()
                UIView.performWithoutAnimation {
                     searchResultsTable.reloadRows(at: [IndexPath.init(row: feeds.count - 1, section: 0)], with: .none)
                }
            }
        }
    }
    //display feeds
    func showFeeds(_ feeds: [Any]) {
        let prevOffset = self.feeds.count
        postOffset += feeds.count
        if (postOffset != 0 && feeds.isEmpty) || feeds.count % numberOfPostsPerAPI != 0 {
            isAllFeedReceived = true
        } else {
            isAllFeedReceived = false
        }
        // Remove duplicates from the feeds
        if let posts = feeds as? [Post], var currentPosts = self.feeds as? [Post] {
            for post in posts {
                let isDuplicate = currentPosts.contains {$0.postId == post.postId}
                if isDuplicate == false {
                    currentPosts.append(post)
                }
            }
            self.feeds = currentPosts
        }
        if refreshControl.isRefreshing {
            refreshControl.endRefreshing()
        }
        searchResultsTable.reloadData()
        if prevOffset != 0 {
            searchResultsTable.scrollToRow(at: IndexPath.init(row: prevOffset - 1, section: 0), at: .bottom, animated: false)
        }
         isTableScrolled = false
    }
    @objc func refreshFeeds() {
        feeds.removeAll()
        postOffset = 0
        presenter?.invokeSearchWithText(from: postOffset, to: numberOfPostsPerAPI, searchWith: searchString, filterWith: paramDict)
    }
    //to customize pull to refresh drag threshold.
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if scrollView.contentOffset.y < 50 && isFilterActive && !isFilterVisible {
            searchBarViewHeight.constant = 108
            searchBarView.addSubview(filterView)
            isFilterVisible = true
        } else if scrollView.contentOffset.y > 50  && isFilterActive && !isTableScrolled && isFilterVisible {
            filterView.removeFromSuperview()
            searchBarViewHeight.constant = 56
            isFilterVisible = false
        }
    }
    func removeDataFromTable() {
        postOffset = 0
        isAllFeedReceived = false
        feeds.removeAll()
        searchResultsTable.reloadData()
//        showActivityIndicator()
    }
}
extension SearchViewController: FilterToViewProtocol {
    func segmentButtonClicked(sender: ScrollableSegmentedControl) {
        paramDict[GetFeedOptions.feedTypeKey.rawValue] = [GetFeedOptions.feedPostTypeFilterKey.rawValue]
        if sender.selectedSegmentIndex == 0 {
            paramDict[GetFeedOptions.feedFilterKey.rawValue] = [PostType.story.rawValue]
        } else if sender.selectedSegmentIndex == 1 {
            paramDict[GetFeedOptions.feedFilterKey.rawValue] = [PostType.window.rawValue]
        } else {
            paramDict[GetFeedOptions.feedFilterKey.rawValue] = [PostType.article.rawValue]
        }
        removeDataFromTable()
        if searchString != Constants.emptyString {
            refreshFeeds()
        }
    }
}
extension SearchViewController: UISearchBarDelegate {
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        searchBar.showsCancelButton = true
    }
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchBar.showsCancelButton = false
        searchBar.text = Constants.emptyString
        searchBar.endEditing(true)
        isSearchMode = false
        configureViewVisibility()
        // Reset the filter status when search is cancelled
        searchString = Constants.emptyString
        isFilterVisible = true // This flag is set to remove filter irrestpective of it's state
        filterButtonTapped(sender: filterButton)
    }
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        if searchBar.text?.removeWhiteSpace().isEmpty ?? true {
            showAlert(Message.invalidSearchText)
        } else {
            searchBar.endEditing(true)
            searchBar.showsCancelButton = false
            feeds.removeAll()
            postOffset = 0
            searchResultsTable.reloadData()
            isSearchMode = true
            configureViewVisibility()
            guard let searchText = searchBar.text else {
                return
            }
            searchString = searchText
            showActivityIndicator()
            presenter?.invokeSearchWithText(from: postOffset, to: numberOfPostsPerAPI, searchWith: searchText, filterWith: paramDict)

        }
    }
}
