//
//  SettingsRouter.swift
//  Lighf
//
//  Created by L.
//  Copyright © 2019 L. All rights reserved.
//

import Foundation
import UIKit

class SettingsRouter: NSObject {
    weak var presentedViewController: GeneralSettingsViewController?
    static let viewIdentifier = "Settings"
    static let webViewIdentifier = "SettingsWeb"
    static func createModule() -> SettingsViewController? {
        let view = UIStoryboard.settings.instantiateViewController(withIdentifier: viewIdentifier) as? SettingsViewController
        let presenter = SettingsPresenter()
        let router = SettingsRouter()
        let interactor = SettingsInteractor()
        //setting view delegates
        view?.presenter = presenter
        //setting presenter delegates
        presenter.view = view
        presenter.router = router
        presenter.interactor = interactor
        //setting interactor delegates
        interactor.presenter = presenter
        //setting router presentedViewController
        router.presentedViewController = view
        return view
    }
    static func createWebModule() -> SettingsWebViewController? {
        let view = UIStoryboard.settings.instantiateViewController(withIdentifier: webViewIdentifier) as? SettingsWebViewController
        return view
    }
    func createChangePasswordView() -> ChangePasswordViewController? {
        let view = UIStoryboard.settings.instantiateViewController(withIdentifier: ChangePasswordViewController.storyboardID) as? ChangePasswordViewController
        return view
    }
    //
    func createChangeEmailView() -> ChangeEmailViewController? {
        let view = UIStoryboard.settings.instantiateViewController(withIdentifier: ChangeEmailViewController.storyboardId) as? ChangeEmailViewController
        return view
    }
    //
    func updatePresentedView() {
        guard presentedViewController == nil else {
            return
        }
        if let navigationController = UIApplication.shared.keyWindow?.rootViewController as? UINavigationController {
            presentedViewController = navigationController.visibleViewController as? GeneralSettingsViewController
        }
    }
    //
    func updateView() {
        updatePresentedView()
        let presenter = presentedViewController?.presenter as? SettingsPresenter
        guard presenter?.view == nil else {
            return
        }
        presenter?.view = presentedViewController
    }
}

extension SettingsRouter: SettingsPresenterToRouterProtocol {
    func navigateToTutorials() {
        if let introViewController = IntroRouter.createModule(withScreenOption: IntroScreenOption.replay, toPage: 0) {
            presentedViewController?.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
            presentedViewController?.navigationController?.navigationBar.isHidden = true
            presentedViewController?.navigationController?.pushViewController(introViewController, animated: true)
        }
    }
    func moveToRootViewController() {
        updatePresentedView()
        if let signInVC = SignInRouter.createModule(), let viewsInStack = presentedViewController?.navigationController?.viewControllers {
            //Add signinVC as the base view controller
            presentedViewController?.navigationController?.viewControllers = [signInVC] + viewsInStack
        }
        presentedViewController?.navigationController?.setTransparent()
        presentedViewController?.navigationController?.popToRootViewController(animated: true)
    }
    func navigateToTagsView() {
        if let tagsViewController = TagsRouter.createModule() {
            presentedViewController?.navigationController?.view.backgroundColor = UIColor.appGreenColor
            presentedViewController?.navigationItem.setHidesBackButton(false, animated: true)
            presentedViewController?.navigationController?.pushViewController(tagsViewController, animated: true)
        }
    }
    func moveToWebView(with option: WebViewOptions) {
        updatePresentedView()
        if let view = SettingsRouter.createWebModule() {
            view.viewType = option
            let backItem = UIBarButtonItem()
            backItem.title = "Settings"
            presentedViewController?.navigationItem.backBarButtonItem = backItem
            presentedViewController?.navigationController?.pushViewController(view, animated: true)
        }
    }
    //
    func moveToChangePasswordView() {
        updatePresentedView()
        if let changePasswordView = createChangePasswordView() {
            presentedViewController?.navigationItem.backBarButtonItem = nil
            let presenter = presentedViewController?.presenter as? SettingsPresenter
            presenter?.view = changePasswordView
            changePasswordView.presenter = presenter
            presentedViewController?.navigationController?.pushViewController(changePasswordView, animated: true)
            presentedViewController = changePasswordView
        }
    }
    //
    func moveToChangeEmailView() {
        updatePresentedView()
        if let changeEmailView = createChangeEmailView() {
            presentedViewController?.navigationItem.backBarButtonItem = nil
            let presenter = presentedViewController?.presenter as? SettingsPresenter
            presenter?.view = changeEmailView
            changeEmailView.presenter = presenter
            presentedViewController?.navigationController?.pushViewController(changeEmailView, animated: true)
            presentedViewController = changeEmailView
        }
    }
    //
    func dismissView() {
        presentedViewController?.navigationController?.popViewController(animated: true)
    }
    func navigateToAspectsScreen() {
        if let selectAspectsViewController = AspectsRouter.createModule(isFromSettings: true) {
            presentedViewController?.navigationController?.view.backgroundColor = UIColor.appGreenColor
            presentedViewController?.navigationItem.setHidesBackButton(true, animated: true)
            presentedViewController?.navigationController?.navigationBar.tintColor = UIColor.white
            presentedViewController?.navigationController?.pushViewController(selectAspectsViewController, animated: true)
        }
    }
}
