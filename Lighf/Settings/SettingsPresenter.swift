//
//  SettingsPresenter.swift
//  Lighf
//
//  Created by L.
//  Copyright © 2019 L. All rights reserved.
//

import Foundation
import UIKit

class SettingsPresenter: NSObject {
    weak var view: SettingsPresenterToViewProtocol?
    var interactor: SettingsPresenterToInteractorProtocol?
    var router: SettingsPresenterToRouterProtocol?
}

extension SettingsPresenter: SettingsInteractorToPresenterProtocol {
    func changeEmail(with message: String, error: String?) {
        view?.dismissActivityIndicator()
        if error == nil {
            view?.showToast(with: message != Constants.emptyString ? message : Message.changeEmail, completion: {})
            router?.dismissView()
        } else {
            view?.showAlert(error!)
        }
    }
    //
    func changePassword(with message: String, error: String?) {
        view?.dismissActivityIndicator()
        if error == nil {
            view?.showToast(with: message != Constants.emptyString ? message : Message.changePassword, completion: {})
            router?.dismissView()
        } else {
            if error!.contains(Message.invalidOldPassword) {
                (view as? SettingsPresenterToChangePasswordViewProtocol)?.showInCorrectOldPasswordError()
            } else {
                view?.showAlert(error!)
            }
        }
    }
    //
    func showUserDetatails(_ user: User?, error: String?) {
        router?.updateView()
        view?.dismissActivityIndicator()
        if error == nil, let user = user {
            (view as? SettingsPresenterToProfileViewProtocol)?.showUserData(user)
        } else {
            view?.showAlert(error!)
        }
    }
    func signOut(with message: String, error: String?) {
        view?.dismissActivityIndicator()
        if error == nil {
            //Successful signout
            let defaults  = UserDefaults.standard
            defaults.set(Constants.emptyString, forKey: UserDefaultsKey.accessToken)
            UserDefaults.standard.set(-1, forKey: UserDefaultsKey.introPageNum)
            router?.moveToRootViewController()
        } else {
            let defaults  = UserDefaults.standard
            defaults.set(Constants.emptyString, forKey: UserDefaultsKey.accessToken)
            UserDefaults.standard.set(-1, forKey: UserDefaultsKey.introPageNum)
            router?.moveToRootViewController()
            view?.showAlert(error!)
        }
    }
}
extension SettingsPresenter: SettingsViewToPresenterProtocol {
    func changeEmail(with newEmail: String, mailOption: EmailOptions, password: String) {
        view?.showActivityIndicator()
        interactor?.changeEmail(with: newEmail, mailOption: mailOption, password: password)
    }
    func showAspectScreen() {
        router?.navigateToAspectsScreen()
    }
    func showTutorial() {
        router?.navigateToTutorials()
    }
    func changePassword(with oldPassword: String, newPassword: String) {
        view?.showActivityIndicator()
        interactor?.changePassword(with: oldPassword, newPassword: newPassword)
    }
    func changePasswordButtonClicked() {
        router?.moveToChangePasswordView()
    }
    //
    func changeEmailButtonClicked() {
        router?.moveToChangeEmailView()
    }
    //
    func useGuidelinesButtonClicked() {
        router?.moveToWebView(with: .useGuidelines)
    }
    //
    func getHelpButtonClicked() {
        router?.moveToWebView(with: .getHelp)
    }
    //
    func showUserDetails() {
        interactor?.getUserProfile()
    }
    func preferencesTapped() {
        router?.navigateToTagsView()
    }
    func signoutTapped() {
        view?.showActivityIndicator()
        interactor?.invokeSignout()
    }
}
extension SettingsPresenter: SettingsRouterToPresenterProtocol {
}
