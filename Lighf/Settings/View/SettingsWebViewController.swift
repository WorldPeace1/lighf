//
//  SettingsWebViewController.swift
//  Lighf
//
//  Created by L.
//  Copyright © 2019 L. All rights reserved.
//

import Foundation
import UIKit
import  WebKit
class SettingsWebViewController: BaseViewController, WKUIDelegate, WKNavigationDelegate {
    @IBOutlet weak var webView: WKWebView!
    var viewType: WebViewOptions = WebViewOptions.terms
    override func viewDidLoad() {
        super.viewDidLoad()
        configureHeaderView()
        self.showActivityIndicator()
        webView.navigationDelegate = self
        if viewType == WebViewOptions.useGuidelines {
            webView.load(URLRequest.init(url: URL.init(string: Constants.kUseGuidelinesURL)!))
        } else {
            webView.load(URLRequest.init(url: URL.init(string: Constants.kGetHelpURL)!))
        }
    }
    //to configure header
    func configureHeaderView() {
        let barView = UIView.init(frame: CGRect(x: 0, y: 0, width: self.view.frame.width - 200, height: 40))
        let headerLabel =  UILabel.init(frame: barView.frame)
        headerLabel.text = viewType.rawValue
        headerLabel.font = UIFont.init(name: Font.robotoBold.rawValue, size: 22.0)
        headerLabel.textColor = UIColor.white
        headerLabel.textAlignment = .center
        barView.addSubview(headerLabel)
        barView.backgroundColor = .clear
        view.backgroundColor = UIColor.white
        navigationItem.titleView = barView
    }
    // to dismiss activity indicator
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        self.dismissActivityIndicator()
    }
}
