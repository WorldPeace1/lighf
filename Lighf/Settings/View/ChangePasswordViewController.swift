//
//  ChangePasswordViewController.swift
//  Lighf
//
//  Created by L.
//  Copyright © 2019 L. All rights reserved.
//

import UIKit
import SkyFloatingLabelTextField

class ChangePasswordViewController: GeneralSettingsViewController {
    //
    @IBOutlet weak var oldPasswordTextField: SkyFloatingLabelTextField!
    @IBOutlet weak var newPasswordTextField: SkyFloatingLabelTextField!
    @IBOutlet weak var saveButton: UIButton!
    
    var shouldResetTextFields: Bool = false
    var shouldResetOldPassword: Bool = false
    
    var passwordValidator = PasswordValidator()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationItem.title = "Change Password"
        
        self.keyboardDismissalRegistrer.registerControls([self.oldPasswordTextField, self.newPasswordTextField])
        
        saveButton.disable()
        self.setupPasswordFields()
    }
    
    private func setupPasswordFields() {
        self.oldPasswordTextField.modifyTitleFormatter()
        self.oldPasswordTextField.supportShowHide(tintColor: .black)
        
        self.newPasswordTextField.modifyTitleFormatter()
        self.newPasswordTextField.supportShowHide(tintColor: .black)
        
    }
    
    static var storyboardID: String {
        return "changePasswordVC"
    }
    //
    @IBAction func saveButtonClicked(_ sender: UIButton) {
        // Check if new password and confirm password are same
        if let oldPassword = oldPasswordTextField.text, let newPassword = newPasswordTextField.text {
            if !newPassword.isEmpty {
                // Passeords are same. Call API to change password
                presenter?.changePassword(with: oldPassword, newPassword: newPassword)
            } else {
                // Modify textfields
                // This is a work around to change the title of textfields
                newPasswordTextField.titleColor = UIColor.red
                newPasswordTextField.selectedTitleColor = UIColor.red
                newPasswordTextField.title = "Passwords don’t match! (New Password)"
                newPasswordTextField.selectedTitle = "Passwords don’t match! (New Password)"
                //
                shouldResetTextFields = true
            }
        }
    }
    
    private func resetOldPasswordTextField() {
        oldPasswordTextField.title = "Enter Old Password"
        oldPasswordTextField.selectedTitle = "Enter Old Password"
        oldPasswordTextField.titleColor = UIColor.textFieldTitleColor
        oldPasswordTextField.selectedTitleColor = UIColor.textFieldTitleColor
    }
    
    func validatePassword() {
        guard
            let password = newPasswordTextField.text else
        {
                saveButton.disable()
                return
        }
        
        let passwordIsEmpty = password.removeWhiteSpaceAndNewLine().isEmpty

        guard !passwordIsEmpty else {
            saveButton.disable()
            return
        }
        
        let passwordIsValid = self.passwordValidator.validate(password)
        
        guard passwordIsValid else {
            saveButton.disable()
            return
        }
        
        saveButton.enable()
    }
    
    private func resetTextFields() {
        newPasswordTextField.title = "New Password"
        newPasswordTextField.selectedTitle = "New Password"
        //
        newPasswordTextField.titleColor = UIColor.textFieldTitleColor
        newPasswordTextField.selectedTitleColor = UIColor.textFieldTitleColor
        //
    }
    
    @IBAction func textFieldDidChange(_ sender: SkyFloatingLabelTextField) {
        if let text = sender.text, text.isEmpty {
            if shouldResetTextFields {
                resetTextFields()
                shouldResetTextFields = false
            }
            
            if shouldResetOldPassword && sender == oldPasswordTextField {
                resetOldPasswordTextField()
                shouldResetOldPassword = false
            }
        }
        
        self.validatePassword()
    }
}

extension ChangePasswordViewController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        switch textField {
        case oldPasswordTextField:
            newPasswordTextField.becomeFirstResponder()
        default:
            view.endEditing(true)
        }
        return true
    }
}

//extension ChangePasswordViewController: SettingsPresenterToViewProtocol {
//    func showAlert(_ message: String) {
//        let alert = UIAlertController.alert(with: message)
//        self.present(alert, animated: true, completion: nil)
//    }
//}

extension ChangePasswordViewController: SettingsPresenterToChangePasswordViewProtocol {
    func showInCorrectOldPasswordError() {
        oldPasswordTextField.title = "Incorrect Old Password!"
        oldPasswordTextField.selectedTitle = "Incorrect Old Password!"
        oldPasswordTextField.titleColor = UIColor.red
        oldPasswordTextField.selectedTitleColor = UIColor.red
        shouldResetOldPassword = true
    }
}
