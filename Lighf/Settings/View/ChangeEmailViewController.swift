//
//  ChangeEmailViewController.swift
//  Lighf
//
//  Created by L.
//  Copyright © 2019 L. All rights reserved.
//

import UIKit
import SkyFloatingLabelTextField
import PopupDialog

class ChangeEmailViewController: GeneralSettingsViewController {
    static var storyboardId: String {
        return "ChangeEmailVC"
    }
    @IBOutlet weak var topView: UIView!
    @IBOutlet weak var infoMessageLabel: UILabel!
    @IBOutlet weak var confirmEmailTextField: SkyFloatingLabelTextField!
    @IBOutlet weak var emailTextField: SkyFloatingLabelTextField!
    @IBOutlet weak var passwordTextField: SkyFloatingLabelTextField!
    @IBOutlet weak var emailStackView: UIStackView!
    @IBOutlet weak var saveButton: UIButton!
    @IBOutlet weak var viewTopConstraint: NSLayoutConstraint!
    //
    @IBOutlet weak var infoButton: UIButton!
    @IBOutlet weak var emailOptionSlider: UISlider!
    @IBOutlet weak var emailContainerView: UIView!
    @IBOutlet weak var confirmMailContainerView: UIView!
    @IBOutlet weak var passwordContainerView: UIView!
    @IBOutlet weak var emailStackViewTopConstraint: NSLayoutConstraint!
    //
    var selectedMailOption: EmailOptions = .none
    var shouldResetTextFields: Bool = false
    let topOffset: CGFloat = 20.0
    let emailStackViewDefaultTopOffset: CGFloat = 30
    
    let sliderMinValue: Float = 0.0
    let sliderMiddleValue: Float = 0.5
    let sliderMaxValue: Float = 1.0
    let lowerThreshold: Float = 0.25
    let upperThreshold: Float = 0.75
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.keyboardDismissalRegistrer.registerControls([self.confirmEmailTextField, self.emailTextField, self.passwordTextField])
        
        self.navigationItem.title  = "Manage Email Settings"
        self.setInfoButtonTintColor()
        self.setupSecureFields()
        saveButton.disable()
        self.setupInitialSliderState()
    }
    
    private func setupInitialSliderState() {
        self.emailOptionSlider.isContinuous = false
        self.infoMessageLabel.text = "Default level of privacy.  Get all notification types including email, push, and in-app.\nYour email address stored with no additional encryption"
    }

    private func setInfoButtonTintColor() {
        guard let image = self.infoButton.currentImage else { return }
        self.infoButton.setImage(image, for: .disabled)
        self.infoButton.setImage(image.withRenderingMode(.alwaysTemplate), for: .normal)
        self.infoButton.tintColor = UIColor.appGreenColor
    }
    
    private func setupSecureFields() {
        self.emailTextField.modifyTitleFormatter()
        self.emailTextField.supportShowHide(tintColor: .black)
        
        self.confirmEmailTextField.modifyTitleFormatter()
        self.confirmEmailTextField.supportShowHide(tintColor: .black)
        
        self.passwordTextField.modifyTitleFormatter()
        self.passwordTextField.supportShowHide(tintColor: .black)
    }
    
    @objc
    func valueChanged(_ sender: StepSlider) {
        guard let selectedOption = EmailOptions(rawValue: Int(sender.index)) else {
            return
        }
        // Modify UI for Email-less option
        if selectedOption == .emailLess {
            emailContainerView.isHidden = true
            emailStackView.removeArrangedSubview(confirmMailContainerView)
            emailStackViewTopConstraint.constant = emailStackViewDefaultTopOffset * 3.0
        } else {
            emailContainerView.isHidden = false
            emailStackView.insertArrangedSubview(confirmMailContainerView, at: 1)
            emailStackViewTopConstraint.constant = emailStackViewDefaultTopOffset
        }
        switch selectedOption {
        case .none:
            infoMessageLabel.text = "Get Email & Phone Push notifications.\nYour email address stored as Is (No Encryption)"
            infoMessageLabel.textAlignment = .left
        case .hashed:
            infoMessageLabel.text = "Phone Push Notifications No Emails. (To change email address must choose default option)"
            infoMessageLabel.textAlignment = .center
        case .emailLess:
            infoMessageLabel.text = "No Emails. No PW Reset"
            infoMessageLabel.textAlignment = .right
        }
        selectedMailOption = selectedOption
        setSaveButtonState()
    }
    
    @IBAction func valueChanged(sender: UISlider) {
        let sliderValue = sender.value
        let selectedOption = self.determineEmailOptionFromSliderValue(value: sliderValue)
        
        self.updateSliderValue(value: sliderValue)
        self.updateEmailContainerStateFor(emailOption: selectedOption)
        self.updateInfoMessageFor(emailOption: selectedOption)
        
        self.selectedMailOption = selectedOption
        self.setSaveButtonState()
    }
    
    private func updateEmailContainerStateFor(emailOption: EmailOptions) {
        if emailOption == .emailLess {
            self.emailContainerView.isHidden = true
            self.emailStackView.removeArrangedSubview(self.confirmMailContainerView)
            self.emailStackViewTopConstraint.constant = self.emailStackViewDefaultTopOffset * 3.0
        } else {
            self.emailContainerView.isHidden = false
            self.emailStackView.insertArrangedSubview(self.confirmMailContainerView, at: 1)
            self.emailStackViewTopConstraint.constant = self.emailStackViewDefaultTopOffset
        }
    }
    
    private func updateInfoMessageFor(emailOption: EmailOptions) {
        switch emailOption {
        case .none:
            self.infoMessageLabel.text = "Store email address with basic encryption. Ability to get all Alert types (Push, Email and In-App)."
        case .hashed:
            self.infoMessageLabel.text = "Email address only used for registration then double encrypted until password recovery. No Email Alerts."
        case .emailLess:
            self.infoMessageLabel.text = "Most secure. No email address tied to lighf. Lose ability to get Email Alerts or recover password."
        }
    }
    
    private func determineEmailOptionFromSliderValue(value: Float) -> EmailOptions {
        switch value {
        case sliderMinValue..<lowerThreshold:
            return .none
        case lowerThreshold...sliderMiddleValue:
            return .hashed
        case sliderMiddleValue..<upperThreshold:
            return .hashed
        case upperThreshold...sliderMaxValue:
            return .emailLess
        default:
            return .none
        }
    }
    
    private func updateSliderValue(value: Float) {
        switch value {
        case sliderMinValue..<lowerThreshold:
            self.emailOptionSlider.setValue(sliderMinValue, animated: true)
        case lowerThreshold...sliderMiddleValue:
            self.emailOptionSlider.setValue(sliderMiddleValue, animated: true)
        case sliderMiddleValue..<upperThreshold:
            self.emailOptionSlider.setValue(sliderMiddleValue, animated: true)
        case upperThreshold...sliderMaxValue:
            self.emailOptionSlider.setValue(sliderMaxValue, animated: true)
        default:
            break
        }
    }
    
    private func showEmailMismatchError() {
        emailTextField.titleColor = UIColor.red
        emailTextField.selectedTitleColor = UIColor.red
        emailTextField.title = "Emails don’t match! (New Email)"
        emailTextField.selectedTitle = "Emails don’t match! (New Email)"
        //
        confirmEmailTextField.titleColor = UIColor.red
        confirmEmailTextField.selectedTitleColor = UIColor.red
        confirmEmailTextField.title = "Emails don’t match! (Confirm New Email)"
        confirmEmailTextField.selectedTitle = "Emails don’t match! (Confirm New Email)"
        shouldResetTextFields = true
    }
    //
    private func resetTextFields() {
        emailTextField.title = "Enter New Email"
        emailTextField.selectedTitle = "Enter New Email"
        confirmEmailTextField.title = "Confirm New Email"
        confirmEmailTextField.selectedTitle = "Confirm New Email"
        //
        emailTextField.titleColor = UIColor.textFieldTitleColor
        emailTextField.selectedTitleColor = UIColor.textFieldTitleColor
        //
        confirmEmailTextField.titleColor = UIColor.textFieldTitleColor
        confirmEmailTextField.selectedTitleColor = UIColor.textFieldTitleColor
    }
    //
    private func setSaveButtonState() {
        if selectedMailOption == .emailLess && (passwordTextField.text?.removeWhiteSpaceAndNewLine().count ?? 0) > 0 {
            saveButton.enable()
        } else {
            // Check the email fields contain minimum number of characters
            if (emailTextField.text?.count ?? 0) > 0 &&
                (confirmEmailTextField.text?.count ?? 0) > 0 &&
                (passwordTextField.text?.count ?? 0) > 0 {
                saveButton.enable()
            } else {
                saveButton.disable()
            }
        }
    }
    //
    @IBAction func textFieldDidChange(_ sender: SkyFloatingLabelTextField) {
        if let text = sender.text, text.isEmpty {
            if shouldResetTextFields {
                resetTextFields()
                shouldResetTextFields = false
            }
        }
        setSaveButtonState()
    }
    //
    @IBAction func infoButtonClicked(_ sender: UIButton) {
        self.showInfoModal(type: .email)
    }
    @IBAction func saveButtonClicked(_ sender: UIButton) {
        if let email = emailTextField.text, let confirmMail = confirmEmailTextField.text, let password = passwordTextField.text {
            if email == confirmMail {
                presenter?.changeEmail(with: email, mailOption: selectedMailOption, password: password)
            } else {
                showEmailMismatchError()
            }
        }
    }
}

extension ChangeEmailViewController: UITextFieldDelegate {
    //
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == emailTextField {
            confirmEmailTextField.becomeFirstResponder()
        } else if textField == confirmEmailTextField {
            passwordTextField.becomeFirstResponder()
        } else {
            viewTopConstraint.constant = topOffset
            textField.resignFirstResponder()
            if saveButton.isEnabled {
                saveButtonClicked(saveButton)
            }
        }
        return true
    }
    //
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField == confirmEmailTextField {
            UIView.animate(withDuration: 0.5) {
                self.viewTopConstraint.constant = -50
                self.view.layoutIfNeeded()
            }
        } else if textField == passwordTextField {
            UIView.animate(withDuration: 0.5) {
                self.viewTopConstraint.constant = -120
                self.view.layoutIfNeeded()
            }
        } else {
            UIView.animate(withDuration: 0.5) {
                self.viewTopConstraint.constant = self.topOffset
                self.view.layoutIfNeeded()
            }
        }
    }
}

// MARK: - EmailOptionInfoViewControllerDelegate

extension ChangeEmailViewController: EmailOptionInfoViewControllerDelegate {
    func requestedDismissal(viewController: EmailOptionInfoViewController) {
        self.hideBlurredBackground()
        self.hideOverlay()
        
        for childViewController in self.children where childViewController is EmailOptionInfoViewController {
            childViewController.view.removeFromSuperview()
            childViewController.removeFromParent()
        }
    }
}

// MARK: - Modal

extension ChangeEmailViewController {
    func showInfoModal(type: InfoContentMode) {
        self.showOverlay()
        self.showBlurredBackground(style: .regular)
        
        let emailInfoViewController = EmailOptionInfoViewController.instantiate()
        emailInfoViewController.delegate = self
        self.addChild(emailInfoViewController)
        emailInfoViewController.view.addCenteredInParentView(self.view, size: CGSize(width: self.view.bounds.width - 80.0, height: type == .email ? 420.0 : 280.0))
        self.view.bringSubviewToFront(emailInfoViewController.view)
        emailInfoViewController.infoContentMode = type
    }
}
