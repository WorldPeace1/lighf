//
//  GeneralSettingsViewController.swift
//  Lighf
//
//  Created by L.
//  Copyright © 2019 L. All rights reserved.
//

import UIKit

class GeneralSettingsViewController: BaseViewController {
    //
    var presenter: SettingsViewToPresenterProtocol?
    //
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    //
    @IBAction func logoutButtonClicked(_ sender: UIButton) {
        let signOutAlert = UIAlertController(title: "", message: Message.signOut, preferredStyle: .alert)
        let confirmAction = UIAlertAction(title: "Jump Out", style: .default) { (_) in
            self.presenter?.signoutTapped()
        }
        let cancelAction = UIAlertAction(title: "Go Back", style: .cancel, handler: nil)
        signOutAlert.addAction(confirmAction)
        signOutAlert.addAction(cancelAction)
        self.present(signOutAlert, animated: true, completion: nil)
    }
    //
    @IBAction func useGuidelinesButtonClicked(_ sender: UIButton) {
        presenter?.useGuidelinesButtonClicked()
    }
    //
    @IBAction func getHelpButtonClicked(_ sender: UIButton) {
        presenter?.getHelpButtonClicked()
    }
}

extension GeneralSettingsViewController: SettingsPresenterToViewProtocol {
    func showAlert(_ message: String) {
        let alert = UIAlertController.alert(with: message)
        self.present(alert, animated: true, completion: nil)
    }
}
