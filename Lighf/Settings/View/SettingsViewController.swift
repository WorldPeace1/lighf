//
//  SettingsViewController.swift
//  Lighf
//
//
//  Copyright © 2020 L. All rights reserved.
//

import Foundation
import UIKit
import SkyFloatingLabelTextField
class SettingsViewController: GeneralSettingsViewController {
    @IBOutlet weak var userName: SkyFloatingLabelTextField!
    @IBOutlet weak var email: SkyFloatingLabelTextField!
    @IBOutlet weak var password: SkyFloatingLabelTextField!
    @IBOutlet weak var changePasswordButton: UIButton!
    @IBOutlet weak var versionLabel: UILabel!
    @IBOutlet weak var popOverSwitch: UISwitch!
    @IBOutlet weak var alertnatePostCreationSwitch: UISwitch!
    @IBOutlet weak var changeEmailButton: UIButton!
    @IBOutlet weak var changeEmailButtonTopConstraint: NSLayoutConstraint!
    @IBOutlet weak var changeEmailButtonLeadingConstraint: NSLayoutConstraint!
    //
    let changeEmailButtonMinTopOffset: CGFloat = -6
    let changeEmailButtonMaxTopOffsset: CGFloat = 18.0
    let changeEmailButtonMinLeftOffset: CGFloat = 35.0
    let changeEmailButtonMaxLeftOffset: CGFloat = 45.0
    //
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.keyboardDismissalRegistrer.registerControls([self.userName, self.email, self.password])
        
        userName.modifyTitleFormatter()
        email.modifyTitleFormatter()
        password.modifyTitleFormatter()
        // Initially hide the change password button
        changePasswordButton.isHidden = true
        changeEmailButton.isHidden = true
        versionLabel.text = Utility.appVersion()
        let isPopUpRequired =  UserDefaults.standard.value(forKey: UserDefaultsKey.showPopUp) as? Bool ?? true
        if isPopUpRequired {
            popOverSwitch.isOn = true
        } else {
            popOverSwitch.isOn = false
        }
        //
        let useAlternatePostCreation =  UserDefaults.standard.value(forKey: UserDefaultsKey.showAlternatePostCreationOption) as? Bool ?? true
        if useAlternatePostCreation {
            alertnatePostCreationSwitch.isOn = true
        } else {
            alertnatePostCreationSwitch.isOn = false
        }
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationItem.setHidesBackButton(false, animated: true)
        // Fetch user details from server
        presenter?.showUserDetails()
        self.navigationController?.resetBottomBarLine()
        configureHeaderView()
    }
    func configureHeaderView() {
        let barView = UIView.init(frame: CGRect(x: 0, y: 0, width: self.view.frame.width - 150, height: 40))
        let notificationImage =  UIImageView.init(image: UIImage.init(named: "Settings"))
        let verticalConstraint = NSLayoutConstraint(item: notificationImage, attribute: NSLayoutConstraint.Attribute.centerX, relatedBy: NSLayoutConstraint.Relation.equal, toItem: barView, attribute: NSLayoutConstraint.Attribute.centerX, multiplier: 1, constant: 0)
        let horizontalConstraint = NSLayoutConstraint(item: notificationImage, attribute: NSLayoutConstraint.Attribute.centerY, relatedBy: NSLayoutConstraint.Relation.equal, toItem: barView, attribute: NSLayoutConstraint.Attribute.centerY, multiplier: 1, constant: 0)
        barView.addSubview(notificationImage)
        barView.backgroundColor = UIColor.clear
        view.backgroundColor = UIColor.white
        NSLayoutConstraint.activate([verticalConstraint, horizontalConstraint])
        navigationItem.titleView = barView
        self.navigationController?.navigationBar.isHidden = false
        self.navigationController?.view.backgroundColor = UIColor.appGreenColor
        self.navigationController?.setBackGroundColor(UIColor.appGreenColor)
    }
    //
    @IBAction func preferencesButtonTapped() {
        presenter?.preferencesTapped()
    }
    @IBAction func changePasswordButtonClicked(_ sender: Any) {
        presenter?.changePasswordButtonClicked()
    }
    @IBAction func changeEmailButtonClicked(_ sender: UIButton) {
        presenter?.changeEmailButtonClicked()
    }
    @IBAction func popoverSwitchValuedChanged(_ sender: UISwitch) {
        if sender.isOn {
            UserDefaults.standard.set(true, forKey: UserDefaultsKey.showPopUp)
        } else {
            UserDefaults.standard.set(false, forKey: UserDefaultsKey.showPopUp)
        }
    }
    //
    @IBAction func postCreationOptionSwitchValueChanged(_ sender: UISwitch) {
        if sender.isOn {
            UserDefaults.standard.set(true, forKey: UserDefaultsKey.showAlternatePostCreationOption)
        } else {
            UserDefaults.standard.set(false, forKey: UserDefaultsKey.showAlternatePostCreationOption)
        }
    }
    //
    @IBAction func showTutorialsButtonClicked() {
        presenter?.showTutorial()
    }
    @IBAction func showSelectAspectsScreen() {
        presenter?.showAspectScreen()
    }
}
//extension SettingsViewController: SettingsPresenterToViewProtocol {
//    func showAlert(_ message: String) {
//        let alert = UIAlertController.alert(with: message)
//        self.present(alert, animated: true, completion: nil)
//    }
//}
//
extension SettingsViewController: SettingsPresenterToProfileViewProtocol {
    //
    func showUserData(_ user: User) {
        userName.text = user.userName
//        email.text = user.email
        //
//        if user.email == Constants.emptyString {
//            changeEmailButtonTopConstraint.constant = changeEmailButtonMaxTopOffsset
//            changeEmailButtonLeadingConstraint.constant = changeEmailButtonMaxLeftOffset
//        } else {
//            changeEmailButtonTopConstraint.constant = changeEmailButtonMinTopOffset
//            changeEmailButtonLeadingConstraint.constant = changeEmailButtonMinLeftOffset
//        }
        changeEmailButtonTopConstraint.constant = changeEmailButtonMinTopOffset
        changeEmailButtonLeadingConstraint.constant = changeEmailButtonMinLeftOffset
        //
        switch user.mailOption {
        case .none:
            email.text = user.email
        case .hashed:
            email.text = "You chose to encrypt"
        case .emailLess:
             email.text = "You chose Email-less"
        }
        //
        if user.password != Constants.emptyString {
            password.text = user.password
        } else {
            password.text = "*******" // Fake data to show password
        }
        changePasswordButton.isHidden = false
        changeEmailButton.isHidden = false
    }
}
//
extension SettingsViewController: UITextFieldDelegate {
    func textFieldDidBeginEditing(_ textField: UITextField) {
        textField.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
    }
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        guard let contentText = textField.text as NSString? else {
            return true
        }
        let newText = contentText.replacingCharacters(in: range, with: string)
        //
        switch textField {
        case userName:
            return newText.count <= Constants.maxCharacterCount
        default: break
        }
        return true
    }
    @objc func textFieldDidChange(_ sender: UITextField) {
        changePasswordButton.isHidden = password.isEmpty()
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        textField.removeTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
        textField.resignFirstResponder()
    }
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        return true
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}
