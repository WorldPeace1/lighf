//
//  SettingsProtocols.swift
//  Lighf
//
//  
//  Copyright © 2020 L. All rights reserved.
//

import Foundation
import UIKit

protocol SettingsViewToPresenterProtocol: class {
    func signoutTapped()
    func preferencesTapped()
    func showUserDetails()
    ///
    func useGuidelinesButtonClicked()
    func getHelpButtonClicked()
    func changePasswordButtonClicked()
    func changeEmailButtonClicked()
    func changePassword(with oldPassword: String, newPassword: String)
    func showTutorial()
    func showAspectScreen()
    func changeEmail(with newEmail: String, mailOption: EmailOptions, password: String)
}
protocol SettingsPresenterToViewProtocol: BasePresenterToViewProtocol {
    func showAlert(_ message: String)
}
protocol SettingsPresenterToProfileViewProtocol: SettingsPresenterToViewProtocol {
    func showUserData(_ user: User)
}
protocol SettingsPresenterToChangePasswordViewProtocol: SettingsPresenterToViewProtocol {
    func showInCorrectOldPasswordError()
}
protocol SettingsPresenterToInteractorProtocol: class {
    func invokeSignout()
    func getUserProfile()
    func changePassword(with oldPassword: String, newPassword: String)
    func changeEmail(with newEmail: String, mailOption: EmailOptions, password: String)
}
protocol SettingsInteractorToPresenterProtocol: class {
    func signOut(with message: String, error: String?)
    func showUserDetatails(_ user: User?, error: String?)
    func changePassword(with message: String, error: String?)
    func changeEmail(with message: String, error: String?)
}
protocol SettingsPresenterToRouterProtocol: class {
    func navigateToTagsView()
    func moveToRootViewController()
    func moveToWebView(with option: WebViewOptions)
    func moveToChangePasswordView()
    func moveToChangeEmailView()
    func dismissView()
    func navigateToTutorials()
    func navigateToAspectsScreen()
    func updateView()
}
protocol SettingsRouterToPresenterProtocol: class {
}
