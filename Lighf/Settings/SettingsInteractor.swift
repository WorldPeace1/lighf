//
//  SettingsInteractor.swift
//  Lighf
//
//  Created by LDEVE.
//  Copyright © 2019 L. All rights reserved.
//

import Foundation
import UIKit

class SettingsInteractor: NSObject {
    weak var presenter: SettingsInteractorToPresenterProtocol?
}
extension SettingsInteractor: SettingsPresenterToInteractorProtocol {
    //
    func changeEmail(with newEmail: String, mailOption: EmailOptions, password: String) {
        let defaults = UserDefaults.standard
        let userId = defaults.value(forKey: UserDefaultsKey.userID) as Any
        var parameters = [String: Any]()
        parameters["id"] = userId
        parameters["email"] = newEmail
        parameters["email_choice"] = mailOption.keyValue
        parameters["password"] = password
        //
        APIManager.post(endPoint: EndPoint.changeEmail, parameters: parameters) { (response, error) in
            guard let response = response, error == nil else {
                self.presenter?.changePassword(with: "", error: error!.message)
                return
            }
            let message = response[APIResponse.apiResponse][APIResponse.apiResponseMessage].stringValue
            let responseStatus = response[APIResponse.apiResponse][APIResponse.apiResponseStatus].stringValue
            if responseStatus == Constants.success {
                self.presenter?.changeEmail(with: message, error: nil)
            } else {
                self.presenter?.changeEmail(with: "", error: message)
            }
        }
    }
    //
    func changePassword(with oldPassword: String, newPassword: String) {
        let defaults = UserDefaults.standard
        let userId = defaults.value(forKey: UserDefaultsKey.userID) as Any
        var parameters = [String: Any]()
        parameters["id"] = userId
        parameters["old_password"] = oldPassword
        parameters["password"] = newPassword
        parameters["password_confirmation"] = newPassword
        //
        APIManager.post(endPoint: EndPoint.changePassword, parameters: parameters) { (response, error) in
            guard let response = response, error == nil else {
                self.presenter?.changePassword(with: "", error: error!.message)
                return
            }
            let message = response[APIResponse.apiResponse][APIResponse.apiResponseMessage].stringValue
            let responseStatus = response[APIResponse.apiResponse][APIResponse.apiResponseStatus].stringValue
            if responseStatus == Constants.success {
                self.presenter?.changePassword(with: message, error: nil)
            } else {
                self.presenter?.changePassword(with: "", error: message)
            }
        }
    }
    //
    func invokeTagsView() {
    }
    func invokeSignout() {
        let defaults = UserDefaults.standard
        let userId = defaults.value(forKey: UserDefaultsKey.userID) as Any
        // invoke api
        APIManager.post(endPoint: EndPoint.signOut, parameters: ["id": userId]) { (json, error) in
            guard error == nil, let response = json else {
                self.presenter?.signOut(with: "", error: error?.message)
                return
            }
            let message = response[APIResponse.apiResponse][APIResponse.apiResponseMessage].stringValue
            let responseStatus = response[APIResponse.apiResponse][APIResponse.apiResponseStatus].stringValue
            if responseStatus == Constants.success {
                self.presenter?.signOut(with: message, error: nil)
            } else {
                self.presenter?.signOut(with: "", error: message)
            }
        }
    }
    //
    func getUserProfile() {
        let defaults = UserDefaults.standard
        let userId = defaults.value(forKey: UserDefaultsKey.userID) as Any
        // Invoke API
        APIManager.post(endPoint: EndPoint.userProfile, parameters: ["id": userId]) { (response, error) in
            guard let response = response, error == nil else {
                self.presenter?.showUserDetatails(nil, error: error?.message)
                return
            }
            let status = response["api_response"]["response_status"].stringValue
            if status == Constants.success {
                var user = User(name: "", inviteCode: "")
                user.userName = response["api_response"]["username"].stringValue
                user.email = response["api_response"]["email"].stringValue
                //
                let emailChoice = response["api_response"]["email_choice"].stringValue
                if let option = EmailOptions.option(for: emailChoice) {
                    user.mailOption = option
                }
                //
                self.presenter?.showUserDetatails(user, error: nil)
            } else {
                let message = response["api_response"]["message"].stringValue
                self.presenter?.showUserDetatails(nil, error: message)
            }
        }
    }
}
