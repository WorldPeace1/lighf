//
//  CommentsViewProtocols.swift
//  Lighf
//
//  Created by L.
//  Copyright © 2019 L. All rights reserved.
//

import Foundation
import UIKit

protocol CommentsViewToPresenterProtocol: class {
    func addComment(_ comment: String, type: CommentType, for post: Post, as reply: PostComment?)
    func addFlag(_ option: FlagOptions, comment: String?, for itemID: Int, itemType: String)
    func cancelButtonPressed()
    func editComment(_ comment: PostComment)
}
protocol CommentsPresenterToViewProtocol: BasePresenterToViewProtocol {
    func showAlert(with message: String)
}
protocol CommentsPresenterToInteractorProtocol: class {
    func addComment(_ comment: String, type: CommentType, for post: Post, as reply: PostComment?)
    func addFlag(_ option: FlagOptions, comment: String?, for itemID: Int, itemType: String)
    func invokeEditCommentAPI(_ comment: PostComment)
}
protocol CommentsInteractorToPresenterProtocol: class {
    func commentAdded(with error: String?, with type: CommentType)
    func flagAdded(with error: String?, for itemID: Int)
    func commentEditResponse(with error: String?, updatedComment: PostComment?)
}
protocol CommentsPresenterToRouterProtocol: class {
    func dismissView()
}
