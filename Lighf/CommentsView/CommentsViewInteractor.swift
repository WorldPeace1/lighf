//
//  CommentsViewInteractor.swift
//  Lighf
//
//  Created by L.
//  Copyright © 2019 L. All rights reserved.
//

import Foundation
import UIKit

class CommentsViewInteractor: NSObject {
    weak var presenter: CommentsInteractorToPresenterProtocol?
}
extension CommentsViewInteractor: CommentsPresenterToInteractorProtocol {
    func invokeEditCommentAPI(_ comment: PostComment) {
        // API call
        let defaults = UserDefaults.standard
        var parameters: [String: Any] = [:]
        parameters["id"] = defaults.value(forKey: UserDefaultsKey.userID)
        parameters["comment_id"] = comment.commentID
        parameters["comment_body"] = comment.commentText
        parameters["comment_type"] = comment.commentType.rawValue
        print(parameters)
        APIManager.post(endPoint: EndPoint.editComment, parameters: parameters) { (response, error) in
            if error == nil, let responseParam = response {
                let responseStatus = responseParam[APIResponse.apiResponse][APIResponse.apiResponseStatus].stringValue
                let responseMessage = responseParam[APIResponse.apiResponse][APIResponse.apiResponseMessage].stringValue
                if responseStatus == "ok" {
                    let updatedComment = PostComment(from: responseParam[APIResponse.apiResponse])
                    self.presenter?.commentEditResponse(with: responseMessage, updatedComment: updatedComment)
                } else {
                    self.presenter?.commentEditResponse(with: responseMessage, updatedComment: nil)
                }
            } else {
                self.presenter?.commentEditResponse(with: error?.message, updatedComment: nil)
            }
        }
    }
    //method to add a comment
    func addComment(_ comment: String, type: CommentType, for post: Post, as reply: PostComment?) {
        // API call
        let defaults = UserDefaults.standard
        var parameters: [String: Any] = [:]
        parameters["id"] = defaults.value(forKey: UserDefaultsKey.userID)

        if let reply = reply {
            parameters["comment_parent_id"] = reply.parentCommentID != 0 ? reply.parentCommentID : reply.commentID
        }
        parameters["post_id"] = post.postId
        parameters["comment_body"] = comment
        parameters["comment_type"] = type.rawValue
        print(parameters)
        APIManager.post(endPoint: EndPoint.addComment, parameters: parameters) { (response, error) in
            if error == nil, let responseParam = response {
                let responseStatus = responseParam[APIResponse.apiResponse][APIResponse.apiResponseStatus].stringValue
                let parentID = responseParam[APIResponse.apiResponse]["comment_parent_id"].intValue
                let commentID = responseParam[APIResponse.apiResponse]["comment_id"].intValue
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "scrollToComment"), object: commentID)
                if responseStatus == "ok" && parentID != 0 {
                    self.presenter?.commentAdded(with: nil, with: CommentType.reply)
                } else if responseStatus == "ok" {
                    self.presenter?.commentAdded(with: nil, with: type)
                } else {
                    let responseMessage = responseParam[APIResponse.apiResponse][APIResponse.apiResponseMessage].stringValue
                    self.presenter?.commentAdded(with: responseMessage, with: type)
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "removeComment"), object: nil)
                }
            } else {
                self.presenter?.commentAdded(with: error?.message, with: type)
            }
        }
    }
    //method to add a flag
    func addFlag(_ option: FlagOptions, comment: String?, for itemID: Int, itemType: String) {
        // API call
        let defaults = UserDefaults.standard
        var parameters: [String: Any] = [:]
        parameters["id"] = defaults.value(forKey: UserDefaultsKey.userID)
        parameters["item_id"] = itemID
        parameters["item_type"] = itemType //“post” or “comment”
        parameters["reason"] = option.message
        parameters["reason_details"] = comment
        //
        APIManager.post(endPoint: EndPoint.flagPost, parameters: parameters) { (response, error) in
            guard let response = response, error == nil else {
                self.presenter?.flagAdded(with: error?.message, for: itemID)
                return
            }
            let message = response[APIResponse.apiResponse][APIResponse.apiResponseMessage].stringValue
            let status = response[APIResponse.apiResponse][APIResponse.apiResponseStatus].stringValue
            if status == Constants.success {
                // On successful completion
                self.presenter?.flagAdded(with: Message.feedbackSuccess, for: itemID)
            } else {
                self.presenter?.flagAdded(with: message, for: itemID)
            }
        }
    }
}
