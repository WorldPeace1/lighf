//
//  AccessoryView.swift
//  Lighf
//
//  Copyright © 2020 L. All rights reserved.
//

import UIKit

/* Custom keyboard shortcut icons including quotes and bullets.   Notice in AccessoryView there is commented out code related to showing these shortcut icons live on the app.  Currently they are turned off. */
protocol AccessoryViewToParentViewProtocol: class {
    func insertQuote(at position: Int, with text: NSAttributedString)
    func insertBullet(at position: Int, with text: NSAttributedString)
}
class AccessoryView: UIView {
    //
    weak var parentTextView: UITextView?
    weak var parentViewController: UIViewController?
    weak var view: AccessoryViewToParentViewProtocol?
    var currentFontSize = FontSize.medium
    var bulletPointsActive = false
    var returnButtonTapped = false
    var maxCount = 0
//    let fontButton: UIButton = {
//        let fontButton = UIButton.newAutoLayout()
//        fontButton.setImage(UIImage.init(imageLiteralResourceName: "FontSize"), for: .normal)
//        fontButton.setTitleColor(UIColor.red, for: .normal)
//        fontButton.frame = CGRect.init(x: 0, y: 0, width: 16, height: 16)
//        fontButton.addTarget(self, action:
//            #selector(fontSizeButtonTapped(sender:)), for: .touchUpInside)
//        fontButton.showsTouchWhenHighlighted = true
//        return fontButton
//    }()
//    let quotesButton: UIButton = {
//        let quotesButton = UIButton.newAutoLayout()
//        quotesButton.setImage(UIImage.init(imageLiteralResourceName: "Quotes"), for: .normal)
//        quotesButton.setTitleColor(UIColor.red, for: .normal)
//        quotesButton.frame = CGRect.init(x: 0, y: 0, width: 16, height: 16)
//        quotesButton.addTarget(self, action:
//            #selector(quotesButtonTapped(sender:)), for: .touchUpInside)
//        quotesButton.showsTouchWhenHighlighted = true
//        return quotesButton
//    }()
    let bulletsButton: UIButton = {
        let bulletsButton = UIButton.newAutoLayout()
        bulletsButton.setImage(UIImage.init(imageLiteralResourceName: "BulletPoints"), for: .normal)
        bulletsButton.setTitleColor(UIColor.red, for: .normal)
        bulletsButton.frame = CGRect.init(x: 0, y: 0, width: 16, height: 16)
        bulletsButton.addTarget(self, action:
            #selector(bulletPointButtonTapped(sender:)), for: .touchUpInside)
        bulletsButton.showsTouchWhenHighlighted = true
        return bulletsButton
    }()
    //
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    init(frame: CGRect, parentTextView: UITextView, parentViewController: UIViewController) {
        super.init(frame: frame)
        self.parentTextView = parentTextView
        self.parentViewController = parentViewController
        configure()
    }
    func configure() {
        self.backgroundColor = UIColor.white
        self.translatesAutoresizingMaskIntoConstraints = false
        //
        // Add autolayout for the buttons
//        fontButton.autoSetDimensions(to: CGSize(width: 26.0, height: 26.0))
//        quotesButton.autoSetDimensions(to: CGSize(width: 26.0, height: 26.0))
        bulletsButton.autoSetDimensions(to: CGSize(width: 26.0, height: 26.0))
        //
        let stackView = UIStackView.newAutoLayout()
        stackView.axis = .horizontal
        stackView.spacing = 30.0
//        stackView.addArrangedSubview(fontButton)
//        stackView.addArrangedSubview(quotesButton)
        stackView.addArrangedSubview(bulletsButton)
        self.addSubview(stackView)
        //
        stackView.autoAlignAxis(.horizontal, toSameAxisOf: self)
        stackView.autoPinEdge(.leading, to: .leading, of: self, withOffset: 30.0)
    }
    // MARK: Button Actions
    @objc
    func fontSizeButtonTapped(sender: UIButton) {
        let optionMenu = UIAlertController(title: nil, message: "Choose Option", preferredStyle: .actionSheet)
        let smallAction = UIAlertAction(title: FontSize.small.rawValue, style: .default, handler: {(_ action) -> Void in
            self.alertActionSelected(selectedOption: FontSize.small)
        })
        let mediumAction = UIAlertAction(title: FontSize.medium.rawValue, style: .default, handler: { (_ action) -> Void in
            self.alertActionSelected(selectedOption: FontSize.medium)
        })
        let largeAction = UIAlertAction(title: FontSize.large.rawValue, style: .default, handler: { (_ action) -> Void in
            self.alertActionSelected(selectedOption: FontSize.large)
        })
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel)
        optionMenu.addAction(smallAction)
        optionMenu.addAction(mediumAction)
        optionMenu.addAction(largeAction)
        optionMenu.addAction(cancelAction)
        parentViewController?.present(optionMenu, animated: true, completion: nil)
    }
    private func alertActionSelected(selectedOption: FontSize) {
        currentFontSize = selectedOption
        var font = UIFont(name: Font.sfProDisplayRegular.rawValue, size: 22)!
        if let textViewFont = parentTextView?.font {
            font = textViewFont
        }
        if currentFontSize == FontSize.large {
            parentTextView?.typingAttributes = [NSAttributedString.Key.font: UIFont(name: font.fontName, size: 22)!]
        } else if currentFontSize == FontSize.medium {
            parentTextView?.typingAttributes = [NSAttributedString.Key.font: UIFont(name: font.fontName, size: 16)!]
        } else {
            parentTextView?.typingAttributes = [NSAttributedString.Key.font: UIFont(name: font.fontName, size: 10)!]
        }
    }
    @objc
    func quotesButtonTapped(sender: UIButton) {
        if let selectedRange = parentTextView?.selectedTextRange, let textView = parentTextView, textView.text.count < maxCount {
            let cursorPosition = textView.offset(from: textView.beginningOfDocument, to: selectedRange.start)
            let quoteString = NSAttributedString.init(string: "\"", attributes: textView.typingAttributes)
            view?.insertQuote(at: cursorPosition, with: quoteString)
            setCursorPosition(textView: textView, cursorPos: cursorPosition)
        }
    }
    // to set cursor to desired position
    public func setCursorPosition(textView: UITextView, cursorPos: Int) {
        if let newPosition = textView.position(from: textView.beginningOfDocument, offset: cursorPos+1) {
            textView.selectedTextRange = textView.textRange(from: newPosition, to: newPosition)
        }
    }
    public func setCursorPositionAtEnd(textView: UITextView, cursorPos: Int) {
        for loopVar1 in stride(from: cursorPos, to: textView.text.count, by: +1) {
            let subStr = textView.text.subString(indexPosition: loopVar1)
            if subStr == "\n" {
                if let newPosition = textView.position(from: textView.beginningOfDocument, offset: loopVar1) {
                    textView.selectedTextRange = textView.textRange(from: newPosition, to: newPosition)
                }
                break
            }
        }
    }
    @objc
    func bulletPointButtonTapped(sender: UIButton) {
        var bulletAdded = false
        if let selectedRange = parentTextView?.selectedTextRange, let textView = parentTextView {
            // Bullets can only be added if the text count is less than the max count by two(". "). 
            if textView.text.count+2 > maxCount {
                if bulletPointsActive == false {
                    return
                }
            }
            let cursorPosition = textView.offset(from: textView.beginningOfDocument, to: selectedRange.start)
            let tempText = textView.text
            var prevSubStr: Character?
            for loopVar1 in stride(from: cursorPosition-1, to: -1, by: -1) {
                let subStr = tempText?.subString(indexPosition: loopVar1)
                if subStr == "•" && prevSubStr == Character.init(" ") {
                    let quoteString = NSAttributedString.init(string: Constants.emptyString, attributes: textView.typingAttributes)
                    view?.insertBullet(at: loopVar1, with: quoteString)
                    bulletAdded = true
                    bulletPointsActive = !bulletPointsActive
                    setCursorPositionAtEnd(textView: textView, cursorPos: cursorPosition-2)
                    break
                } else if subStr == "\n" && !bulletPointsActive {
                    let quoteString = NSAttributedString.init(string: "\(Constants.bulletCharacter) ", attributes: textView.typingAttributes)
                    view?.insertBullet(at: loopVar1+1, with: quoteString)
                    bulletAdded = true
                    bulletPointsActive = !bulletPointsActive
                    setCursorPositionAtEnd(textView: textView, cursorPos: cursorPosition)
                    break
                }
                prevSubStr = subStr
            }
            if !bulletAdded {
                let quoteString = NSAttributedString.init(string: "\(Constants.bulletCharacter) ", attributes: textView.typingAttributes)
                view?.insertBullet(at: 0, with: quoteString)
                bulletPointsActive = !bulletPointsActive
                setCursorPositionAtEnd(textView: textView, cursorPos: cursorPosition)
            }
        }
    }
    public func textViewDidChange() {
        //inserting bullet character
        if returnButtonTapped && bulletPointsActive && (parentTextView?.text.count ?? 0 + 3 < maxCount), let selectedRange = parentTextView?.selectedTextRange, let textView = parentTextView {
            let cursorPosition = textView.offset(from: textView.beginningOfDocument, to: selectedRange.start)
            let quoteString = NSAttributedString.init(string: "\(Constants.bulletCharacter) ", attributes: textView.typingAttributes)
            view?.insertBullet(at: cursorPosition, with: quoteString)
            setCursorPosition(textView: textView, cursorPos: cursorPosition+1)
        }
    }
    public func textViewDidModify(text: NSString, in range: NSRange, replacementText newText: String) {
        //using returnButtonTapped to insert bullet point if required
        if newText == "\n" {
            returnButtonTapped = true
        } else {
            returnButtonTapped = false
        }
        //using bulletPointsActive to switch off active mode if user chooses to use backspace.
        let removedChar = text.substring(with: range)
        if removedChar == Constants.bulletCharacter {
            bulletPointsActive = false
        }
    }
}
