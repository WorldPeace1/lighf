//
//  CommentsViewController.swift
//  Lighf
//
//  Created by L.
//  Copyright © 2019 L. All rights reserved.
//

import Foundation
import  UIKit
import KMPlaceholderTextView
import AttributedStringBuilder

class CommentsViewController: BaseViewController, UITextViewDelegate {
    @IBOutlet weak var textViewToBottomConstraint: NSLayoutConstraint!
    @IBOutlet weak var characterCountLabel: UILabel!
    @IBOutlet weak var textView: KMPlaceholderTextView!
    @IBOutlet weak var textViewHeightConstraint: NSLayoutConstraint!

    var keyboardHeight: CGFloat = 0.0
    let topMargin: CGFloat = 30
    let bottomMargin: CGFloat = 50
    var maximumCharacterCount: Int = 1000
    //
    var presenter: CommentsViewToPresenterProtocol?
    var titleLabel: CommentsViewTitle = CommentsViewTitle.comment
    var accessoryView: AccessoryView?
    var commentToEdit: PostComment?
    public var commentType: CommentType?
    public var flagOption: FlagOptions? {
        didSet {
            switch flagOption {
            case .abuse?:
                self.titleLabel = CommentsViewTitle.abuse
            case .copyright?:
                self.titleLabel = CommentsViewTitle.copyright
            /* case .other?:
                self.titleLabel = CommentsViewTitle.other
            */
            case .dontSeeThis?:
                self.titleLabel = CommentsViewTitle.dontSeeThis
            case .dontSeeLikeThis?:
                self.titleLabel = CommentsViewTitle.dontSeeLikeThis
            case .notRelated?:
                self.titleLabel = CommentsViewTitle.notRelated
            default:
                self.titleLabel = CommentsViewTitle.spam
            }
        }
    }
    public var post: Post?
    public var parentComment: PostComment?
    var accessoryActionActive: Bool = false
    var textViewMaxHeight: Bool = false
    var doneButton: UIButton?
    //
    override func viewDidLoad() {
        super.viewDidLoad()
        configureHeaderView()
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow(_:)), name: UIResponder.keyboardWillShowNotification, object: nil)
        setPlaceHolderForTextView()
        setUpForEditing()
        configureAccessoryView()
        self.keyboardDismissalRegistrer.registerControls([self.textView])
        textView.typingAttributes = [NSAttributedString.Key.font: UIFont(name: Font.sfProDisplayRegular.rawValue, size: 16)!]
        textView.becomeFirstResponder()
        navigationItem.rightBarButtonItem?.isEnabled = flagOption == nil ? false : true
    }
    func setUpForEditing() {
        if let comment = commentToEdit {
            let textToEdit = NSAttributedString.init(string: comment.commentText, attributes: [NSAttributedString.Key.font: UIFont(name: Font.sfProDisplayRegular.rawValue, size: 16)!])
            characterCountLabel.text = " \(comment.commentText.count)/\(maximumCharacterCount)"
            textView.attributedText = textToEdit
        } else {
            characterCountLabel.text = " 0/\(maximumCharacterCount)"
        }
    }
    func setPlaceHolderForTextView() {
        if titleLabel == CommentsViewTitle.comment || titleLabel == CommentsViewTitle.advice || titleLabel == CommentsViewTitle.question || titleLabel == CommentsViewTitle.reply {
            maximumCharacterCount = 365
        }
        if parentComment != nil {
            textView.placeholder = "Add your reply"
        } else {
            switch titleLabel {
            case CommentsViewTitle.comment:
                textView.placeholder = PlaceHolder.shortStory
            case CommentsViewTitle.question:
                textView.placeholder = PlaceHolder.question
            case CommentsViewTitle.advice:
                textView.placeholder = PlaceHolder.advice
            default:
                textView.placeholder = PlaceHolder.flagBody
            }
        }
    }
    func configureHeaderView() {
        doneButton = UIButton.customNavigationRightBarButton(withTitle: "Done")
        doneButton?.addTarget(self, action: #selector(doneButtonClicked), for: .touchUpInside)
        let backPackButton = UIBarButtonItem(customView: doneButton!)
        self.navigationItem.rightBarButtonItem = backPackButton
        let cancelButton = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(cancelButtonClicked))
        self.navigationItem.leftBarButtonItem = cancelButton
        self.navigationItem.leftBarButtonItem?.tintColor = UIColor.black
        let barView = UIView.init(frame: CGRect(x: 0, y: 0, width: self.view.frame.width - 150, height: 40))
        let typeLabel = UILabel(frame: CGRect(x: 0, y: 0, width: self.view.frame.width - 150, height: 40))
        typeLabel.textColor = UIColor.black
        typeLabel.textAlignment = .center
        typeLabel.font = UIFont.init(name: Font.sfProDisplayMedium.rawValue, size: 18.0)
        barView.backgroundColor = UIColor.clear
        typeLabel.text = titleLabel.rawValue
        barView.addSubview(typeLabel)
        navigationItem.titleView = barView
    }
    @objc func doneButtonClicked(sender: UIButton) {
        view.endEditing(true)
        doneButton?.isEnabled = false
        if var comment = commentToEdit {
            comment.commentText = textView.text
           presenter?.editComment(comment)
        } else if let commentType = commentType, let post = post {
            presenter?.addComment(textView.text, type: commentType, for: post, as: parentComment)
        } else if let flagOption = flagOption, let comment = parentComment {
            print("flagging")
            presenter?.addFlag(FlagOptions(rawValue: flagOption.rawValue) ?? FlagOptions(rawValue: FlagOptions.abuse.rawValue)!, comment: textView.text, for: comment.commentID, itemType: "comment")
        } else if let flagOption = flagOption, let post = post {
            presenter?.addFlag(FlagOptions(rawValue: flagOption.rawValue) ?? FlagOptions(rawValue: FlagOptions.abuse.rawValue)!, comment: textView.text, for: post.postId ?? 0, itemType: "post")
        } else {
        }
    }
    @objc func cancelButtonClicked(sender: UIButton) {
        presenter?.cancelButtonPressed()
    }
    @objc func keyboardWillShow(_ notification: Notification) {
        if let keyboardFrame: NSValue = notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue {
            let keyboardRectangle = keyboardFrame.cgRectValue
            keyboardHeight = keyboardRectangle.height
            textViewToBottomConstraint.constant = keyboardHeight + bottomMargin
        }
    }
    // to configure accessory view.
    func configureAccessoryView() {
        accessoryView = AccessoryView(frame: CGRect(x: 0, y: 0, width: view.frame.width, height: 45), parentTextView: textView, parentViewController: self)
        accessoryView?.maxCount = maximumCharacterCount
        accessoryView?.view = self
        textView.inputAccessoryView = accessoryView
    }
    func textViewDidChange(_ textView: UITextView) { //Handle the text changes here
        let contentSize = textView.sizeThatFits(CGSize(width: view.frame.width, height: CGFloat.greatestFiniteMagnitude))
        let maxPossibleHeight = view.frame.height - (topMargin + bottomMargin + Utility.getSafeAreaInsets() + self.navigationController!.navigationBar.frame.height + keyboardHeight)
        let textViewContentHeight = contentSize.height
        //
        if textViewContentHeight >= maxPossibleHeight {
            // Make the text view to scroll if it's content size is larger than the parent view size
            if !textViewMaxHeight {
                var frame = textView.frame
                frame.size.height = maxPossibleHeight
                textView.frame = frame
                textViewMaxHeight = true
                textViewHeightConstraint.constant = maxPossibleHeight
            }
            textView.isScrollEnabled = true
        } else {
            textView.isScrollEnabled = false
            textViewMaxHeight = false
        }
        if textView.text.count > maximumCharacterCount {
            textView.text = textView.text.substring(to: maximumCharacterCount - 1)
            accessoryView?.setCursorPosition(textView: textView, cursorPos: textView.text.count - 2)
        }
        accessoryView?.textViewDidChange()
    }
    //text view delegate.
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        guard let contentText = textView.text as NSString? else {
            return true
        }
        let textToAdd = text
//        if (textView.text.count + text.count > maximumCharacterCount) && ((maximumCharacterCount - textView.text.count - 1) <= text.count && !text.isEmpty) {
//            textToAdd = text.substring(to: maximumCharacterCount - textView.text.count - 1)
//        }
        let newText = contentText.replacingCharacters(in: range, with: textToAdd)
        self.navigationItem.rightBarButtonItem?.isEnabled = flagOption != nil ? true : !newText.removeWhiteSpaceAndNewLine().isEmpty
        if newText.count > maximumCharacterCount {
            let textTobeAdded = newText.substring(to: maximumCharacterCount-1)
            textView.attributedText = NSAttributedString(string: textTobeAdded, attributes: textView.typingAttributes)
            characterCountLabel.text = "\(textTobeAdded.count)/\(maximumCharacterCount)"
            return false
        }
        characterCountLabel.text = "\((newText.count))/\(maximumCharacterCount)"
        accessoryView?.textViewDidModify(text: contentText, in: range, replacementText: textToAdd)
        if (textToAdd == "\"" || textToAdd == "\(Constants.bulletCharacter) " || textToAdd == Constants.emptyString) && accessoryActionActive {
            let attrText  = NSMutableAttributedString.init(string: textToAdd, attributes: textView.typingAttributes)
            let mainText = textView.attributedText.mutableCopy() as? NSMutableAttributedString ?? NSMutableAttributedString.init(string: textView.text, attributes: textView.typingAttributes)
            if textToAdd == Constants.emptyString {
                mainText.replaceCharacters(in: range, with: Constants.emptyString)
            } else {
                mainText.insert(attrText, at: range.location)
            }
            textView.attributedText = mainText
            accessoryActionActive = false
        }
        return true
    }
}

extension CommentsViewController: CommentsPresenterToViewProtocol {
    func showAlert(with message: String) {
        doneButton?.isEnabled = true
        let alert = UIAlertController.alert(with: message)
        self.present(alert, animated: true, completion: nil)
    }
}
extension CommentsViewController: AccessoryViewToParentViewProtocol {
    //adding quote
    func insertQuote(at position: Int, with text: NSAttributedString) {
        accessoryActionActive = true
        _ = self.textView(textView, shouldChangeTextIn: NSRange.init(location: position, length: 0), replacementText: text.string)
    }
    //bullet click
    func insertBullet(at position: Int, with text: NSAttributedString) {
        accessoryActionActive = true
        var range = NSRange.init(location: position, length: 0)
        if text.string == Constants.emptyString {
            range.length = 2
        }
        _ = self.textView(textView, shouldChangeTextIn: range, replacementText: text.string)
    }

}
