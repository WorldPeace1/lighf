//
//  CommentsViewRouter.swift
//  Lighf
//
//  Created by L.
//  Copyright © 2019 L. All rights reserved.
//

import Foundation
import UIKit

class CommentsViewRouter: NSObject {
    weak var presentedViewController: UIViewController?
    static let viewIdentifier = "CommentsView"
    static func createModule(with interface: PostDetailsInterface? = nil) -> CommentsViewController? {
        let view = UIStoryboard(name: viewIdentifier, bundle: Bundle.main).instantiateViewController(withIdentifier: viewIdentifier) as? CommentsViewController
        let presenter = CommentsViewPresenter()
        let router = CommentsViewRouter()
        let interactor = CommentsViewInteractor()
        //setting view delegates
        view?.presenter = presenter
        //setting presenter delegates
        presenter.view = view
        presenter.router = router
        presenter.interactor = interactor
        presenter.postDetailInterface = interface
        //setting interactor delegates
        interactor.presenter = presenter
        //setting router presentedViewController
        router.presentedViewController = view
        return view
    }
}
extension CommentsViewRouter: CommentsPresenterToRouterProtocol {
    func dismissView() {
        presentedViewController?.navigationController?.popViewController(animated: true)
    }
}
