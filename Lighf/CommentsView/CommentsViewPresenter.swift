//
//  CommentsViewPresenter.swift
//  Lighf
//
//  Created by L.
//  Copyright © 2019 L. All rights reserved.
//

import Foundation
import UIKit
class CommentsViewPresenter: NSObject {
    weak var view: CommentsPresenterToViewProtocol?
    var interactor: CommentsPresenterToInteractorProtocol?
    var router: CommentsPresenterToRouterProtocol?
    weak var postDetailInterface: PostDetailsInterface?
}
extension CommentsViewPresenter: CommentsInteractorToPresenterProtocol {
    func commentEditResponse(with error: String?, updatedComment: PostComment?) {
        if let comment = updatedComment {
            postDetailInterface?.updateComments(comment)
            router?.dismissView()
        } else {
            view?.showAlert(with: error ?? Constants.emptyString)
        }
    }
    func commentAdded(with error: String?, with type: CommentType) {
        guard error == nil else {
            view?.showAlert(with: error ?? Constants.emptyString)
            router?.dismissView()
            return
        }
        // Show a toast to acknowledge user that comment is added successfully
        var message = ""
        switch type {
        case .advice:
            message = Message.addAdvice
        case .question:
            message = Message.addQuestion
        case .comment:
            message = Message.addComment
        case .reply:
            message = Message.addReply
        }
        view?.showToast(with: message, completion: {})
        postDetailInterface?.updateComments(true)
        router?.dismissView()
    }
    func flagAdded(with error: String?, for itemID: Int) {
        guard error == nil else {
            view?.showToast(with: error!, completion: {})
            router?.dismissView()
            return
        }
        view?.showToast(with: Message.addFlag, completion: {})
        postDetailInterface?.updateComments(false)
        router?.dismissView()
    }
}
extension CommentsViewPresenter: CommentsViewToPresenterProtocol {
    func editComment(_ comment: PostComment) {
        interactor?.invokeEditCommentAPI(comment)
    }
    func addComment(_ comment: String, type: CommentType, for post: Post, as reply: PostComment?) {
        interactor?.addComment(comment, type: type, for: post, as: reply)
    }
    //
    func addFlag(_ option: FlagOptions, comment: String?, for itemID: Int, itemType: String) {
        interactor?.addFlag(option, comment: comment, for: itemID, itemType: itemType)
    }
    func cancelButtonPressed() {
        postDetailInterface?.updateComments(false)
        router?.dismissView()
    }
}
