//
//  HomePresenter.swift
//  Lighf
//
//  Created by L.
//  Copyright © 2019 L. All rights reserved.
//

import UIKit
import MobileCoreServices
import SwiftyJSON
class HomePresenter: NSObject {

    var interactor: HomePresenterToInteractorProtocol?
    var router: HomePresenterToRouterProtocol?
    var feedsInteractor: HomePresenterToGetFeedsInteractorProtocol?
    weak var view: HomePresenterToViewProtocol?
    var postDetailsInteractor: HomePresenterToPostDetailsInteractorProtocol?
    var isHeaderImageDeletion: Bool = false
    var leafInteractor: HomePresenterToGiveLeafProtocol?
    var toggleNotificationInteractor: HomePresenterToToggleNotificationProtocol?
}

extension HomePresenter: HomeViewToPresenterProtocol {
    func toggleNotification(forItem: Any, toState: NotificationState) {
        view?.showActivityIndicator(onFullScreen: true)
        toggleNotificationInteractor?.invokeToggleNotification(forItem: forItem, toState: toState)
    }
    func getSavedAspects() {
        view?.showActivityIndicator(onFullScreen: true)
        interactor?.invokeGetSavedAspects()
    }
    func setImageUrl(_ url: String?) {
        isHeaderImageDeletion = url == nil
        interactor?.setImageUrl(url)
    }
    func setImage(image: UIImage?) {
        isHeaderImageDeletion = false
        interactor?.setImage(image: image ?? nil)
    }
    func uploadImageButtonClicked(sourceType: UIImagePickerController.SourceType) {
        let media = kUTTypeImage as String
        view?.showGallery(for: sourceType, mediaTypes: [media])
    }
    func promptUserToAuthorizeCameraInSettings() {
        Utility().presentTwoActionAlerController(titleString: "Authorization Required", messageString: "Please allow Lighf to access the camera from your phone Settings.", rightButtonTitle: "Go to Settings", leftButtonTitle: "Use Photo Gallery") { index in
            if index == 1 {
                if #available(iOS 10.0, *) {
                    UIApplication.shared.openURL(URL(string: UIApplication.openSettingsURLString)!)
                } else {
                    UIApplication.shared.open(URL(string: UIApplication.openSettingsURLString)!)
                }
            } else if index == 2 {
                self.uploadImageButtonClicked(sourceType: .photoLibrary)
            }
        }
    }
    func getUpdatedFeedItems(count: Int, withParams: NSDictionary) {
        feedsInteractor?.fetchUpdatedFeedItems(count: count, withParams: withParams)
    }
    func flagButtonClicked(selectedFlagOption: FlagOptions, for postID: Post) {
        router?.alertActionTapped(selectedOption: selectedFlagOption, postID: postID)
    }
    func updateFeeds(from index: Int, to count: Int, withParams: NSDictionary) {
        feedsInteractor?.fetchFeeds(from: index, to: count, withParams: withParams)
    }
    // method to invoke api to fetch user points
    func getUserPoints() {
        interactor?.fetchUserPoints()
    }
    func viewPost(_ post: Post, type: PostType) {
        router?.viewPost(post, type: type, interface: self)
    }
    func selectAspect() {
        router?.navigateToSelectAspects()
    }
    func viewWindow() {
        router?.viewWindow()
        view?.dismissActivityIndicator()
    }
    func viewStory() {
        router?.viewStory()
        view?.dismissActivityIndicator()
    }
    func viewArticle() {
        router?.viewArticle()
        view?.dismissActivityIndicator()
    }
    //
    func createPost(type: PostType) {
        router?.moveToCreatePostView(type)
    }
    //
    func settingsButtonClicked() {
        router?.navigateToSettings()
    }
    func searchButtonClicked() {
        router?.navigateToSearchScreen(with: self)
    }
    func notificationButtonClicked() {
        router?.navigateToNotificationsScreen()
    }
    //
    func createButtonClicked() {
        view?.showCreateOptionsPopup()
    }
    //
    func backpackButtonClicked(for post: Any, addToBackPack: Bool) {
        view?.showActivityIndicator()
        interactor?.backPackPost(post, addToBackPack: addToBackPack)
    }
    func relateButtonClicked(for post: Any, markAsRelate: Bool) {
        view?.showActivityIndicator()
        interactor?.markPost(post, asRelate: markAsRelate)
    }
    func leafButtonClicked(for post: Any) {
        if let selectedPost = post as? Post {
            view?.showActivityIndicator()
            leafInteractor?.invokeGiveLeaf(forPost: selectedPost, leafCount: 1)
        }
    }
    func imageFetchFailed(withError: Error) {
    }
    func activityButtonClicked() {
        interactor?.fetchActivities()
    }
    func backpackButtonClicked(from index: Int, to count: Int, withParams: NSDictionary) {
        feedsInteractor?.fetchFeeds(from: index, to: count, withParams: withParams)
    }
    func myStoryButtonClicked(from index: Int, to count: Int, withParams: NSDictionary) {
        feedsInteractor?.fetchFeeds(from: index, to: count, withParams: withParams)
    }
    func fetchMyPosts(from index: Int, to count: Int) {
        feedsInteractor?.fetchMyPosts(from: index, to: count)
    }
    func deletePost(_ post: Post) {
        view?.showActivityIndicator()
        interactor?.deletePost(post)
    }
    func editPost(_ post: Post, type: PostType) {
        let postDetailsInteractorObject = PostDetailsInteractor()
        postDetailsInteractorObject.homePresenter = self
        postDetailsInteractorObject.getDetails(for: post, type: type)
    }
    func getProfileHeaderImage() {
        interactor?.getProfileHeaderImage()
    }
}

extension HomePresenter: HomeModuleInterface {
    func toggleNotification(with postId: Int, isEnabled: NotificationState) {
        view?.updateNotificationStatus(postID: postId, commentID: nil, notificationStatus: isEnabled)
    }
    func updateFeedForRelate(with postId: Int, toRelate addToRelates: Bool) {
        view?.updateFeedForRelate(with: postId, toRelate: addToRelates)
    }
    func removePostFromFeed(with postId: Int) {
        view?.removePost(with: postId)
    }
    func markPost(with postId: Int, asRead markAsRead: Bool) {
        view?.markPost(with: postId, asRead: markAsRead)
    }
    func markPost(with postId: Int, asRelated asRelate: Bool) {
        view?.updateRelateStatus(asRelate, forPostWith: postId)
    }
    func addPost(with postId: Int, toBackPack addToBackPack: Bool) {
        view?.addPost(with: postId, toBackPack: addToBackPack)
    }
}

extension HomePresenter: HomeInteractorToPresenterProtocol {
    func updateListOfSavedAspects(aspectsArray: [JSON], error: String) {
        view?.dismissActivityIndicator()
        var responseArray: [Aspect] = []
        for loopVar in 0..<aspectsArray.count {
            let aspectDict = aspectsArray[loopVar]
            let aspectID = aspectDict["id"].intValue
            let aspectName = aspectDict["name"].stringValue
            let aspectSlug = aspectDict["slug"].stringValue
            let aspectsObject = Aspect(aspectId: aspectID, name: aspectName, slug: aspectSlug)
            responseArray.append(aspectsObject)
        }
        view?.listOfSavedAspects(responseArray: responseArray)
    }
    // handle image upload response.
    func handleImageUpload(_ imageURL: String, error: String?) {
        if isHeaderImageDeletion {
            if error ==  nil {
                view?.didResetHeaderImage(status: true)
            } else {
                view?.didResetHeaderImage(status: false)
                view?.showAlert(error!)
            }
        } else {
            if error == nil {
                view?.showAlert("Image uploaded successfully")
                view?.didSetHeaderImage(status: true)
            } else {
                view?.showAlert(error ?? Constants.emptyString)
                view?.didSetHeaderImage(status: false)
            }
        }
    }
    //
    func performedOperation(_ operation: PostOperation, onPost post: Post, with error: String?) {
        view?.dismissActivityIndicator()
        if error == nil, let postId = post.postId {
            var toastMessage = ""
            switch operation {
            case .addBackPack:
                toastMessage = Message.addToBackpack
                view?.updateBackpackedStatus(with: postId, toBackPack: true)
            case .removeBackPack:
                toastMessage = Message.removeFromBackPack
                view?.updateBackpackedStatus(with: postId, toBackPack: false)
            case .related:
                toastMessage = Message.markAsRelate
                view?.updateRelateStatus(true, forPostWith: postId)
            case .unRelated:
                toastMessage = Message.markAsNotRelated
                view?.updateRelateStatus(false, forPostWith: postId)
            default: break
            }
            if toastMessage != Constants.emptyString {
                view?.showToast(with: toastMessage, completion: {})
            }
        } else {
            // If the post is deleted by the author or backend, remove it from the feed
            if post.isDeleted, let postId = post.postId {
                view?.removePost(with: postId)
                view?.showToast(with: error! == Constants.emptyString ? Message.removePost : error!, completion: {})
            } else {
                view?.showAlert(error!)
            }
        }
    }
    // invoke method to display points
    func handleUserPoints(_ points: Int, error: String?) {
        view?.showUserPoints(points)
    }
    //
    func handleActivities(_ activities: [Activity], error: Error?) {
        guard error == nil else {
            view?.showAlert(error!.message)
            return
        }
        view?.showFeeds(activities)
    }
    func handleBackPackItems(_ items: [Post], error: Error?) {
        guard error == nil else {
            view?.showAlert(error!.message)
            return
        }
        view?.showFeeds(items)
    }
    func handleMyStories(_ posts: [Post], error: Error?) {
        guard error == nil else {
            view?.showAlert(error!.message)
            return
        }
        view?.showFeeds(posts)
    }
    func deletedPost(_ post: Post, with message: String) {
        // With the successfull deletion of a post, the feeds list should be refreshed
        view?.dismissActivityIndicator()
        // delete post
        if let postId = post.postId {
            view?.removePost(with: postId)
        }
        view?.showToast(with: message, completion: {})
        view?.popToFeed()
    }
    func deletePost(_ post: Post, failedWithMessage message: String) {
        view?.dismissActivityIndicator()
        view?.showAlert( message == Constants.emptyString ? Message.removePost : message)
        //
        if post.isDeleted, let postId = post.postId {
            view?.removePost(with: postId)
        }
    }
    func flagAdded(with error: String?, for itemID: Int) {
        guard error == nil else {
            view?.showToast(with: error!, completion: {})
            router?.dismissView()
            return
        }
        view?.showToast(with: Message.addFlag, completion: {})
        router?.dismissView()
    }
    func handleProfileImage(url: String?, error: String?) {
        if let imageUrl = url {
            view?.setProfileHeaderImage(url: imageUrl)
        } else {
            view?.setProfileHeaderImage(url: nil)
            if let alertMessage = error {
                view?.showToast(with: alertMessage, completion: {})
            }
        }
    }
}
extension HomePresenter: GetFeedsInteractorToHomePresenterProtocol {
    func handleUpdatedItems(_ items: [JSON], error: String?) {
        view?.dismissActivityIndicator()
        var responseArray: [Any] = []
        for loopVar in 0..<items.count {
            let postDict = items[loopVar]
            let postType = postDict["post_type"].stringValue
            switch postType {
            case PostType.story.rawValue:
                responseArray.append(Story(storyObj: postDict))
            case PostType.article.rawValue:
                responseArray.append(Article(articleObj: postDict))
            default:
                responseArray.append(Window(windowObj: postDict))
            }
        }
        guard error == Constants.emptyString else {
            view?.showAlert(error ?? Constants.emptyString)
            return
        }
        view?.updateRefreshedItems(responseArray)

    }
    //
    func handleFeeds(_ feeds: [JSON], error: String?) {
        view?.dismissActivityIndicator()
        var responseArray: [Any] = []
        for loopVar in 0..<feeds.count {
            let postDict = feeds[loopVar]
            let postType = postDict["post_type"].stringValue
            switch postType {
            case PostType.story.rawValue:
                responseArray.append(Story(storyObj: postDict))
            case PostType.article.rawValue:
                responseArray.append(Article(articleObj: postDict))
            case PostType.window.rawValue:
                responseArray.append(Window(windowObj: postDict))
            case PostType.admin_post.rawValue:
                print(postDict)
                responseArray.append(AdminPost(adminPostObj: postDict))
            default:
                responseArray.append(Story(storyObj: postDict))
            }
        }
        guard error == Constants.emptyString else {
            view?.showAlert(error ?? Constants.emptyString)
            return
        }
        view?.showFeeds(responseArray)
    }
}

extension HomePresenter: PostDetailsInteractorToHomePresenterProtocol {
// to handle edit
    func handle(post: Post, type: PostType) {
        router?.moveToEditPostView(with: post, type: type)
        view?.dismissActivityIndicator()
    }
// to handle error scenarios
    func detailFetchFailed(for post: Post, with message: String) {
        view?.dismissActivityIndicator()
        view?.showAlert(message == Constants.emptyString ? Message.removePost : message)
        // If the post is deleted by the author or backend, remove it from the feed
        if post.isDeleted, let postId = post.postId {
            view?.removePost(with: postId)
        }
    }
}

extension HomePresenter: GiveLeafToHomePresenterProtocol {
    func giveLeafAPIResponse(_ status: String, error: String?) {
        view?.dismissActivityIndicator()
        if status != "ok" {
            view?.showAlert(error ?? Constants.emptyString)
        } else {
            view?.showToast(with: error ?? Constants.emptyString, completion: {})
            view?.updateUserLeafCount()
        }
    }
}
extension HomePresenter: ToggleNotificationToHomePresenterProtocol {
    func toggleNotificationAPIResponse(_ itemID: Int, itemType: String, notificationStatus: NotificationState) {
        view?.dismissActivityIndicator()
        if itemType == "post" {
            view?.updateNotificationStatus(postID: itemID, commentID: nil, notificationStatus: notificationStatus)
        } else {
            view?.updateNotificationStatus(postID: nil, commentID: itemID, notificationStatus: notificationStatus)
        }
    }
    func toggleNotificationAPIResponseFailure(_ status: String, error: String?) {
        view?.dismissActivityIndicator()
        if status != "ok" {
            view?.showAlert(error ?? Constants.emptyString)
        } else {
            view?.showToast(with: error ?? Constants.emptyString, completion: {})
        }
    }
}
