//
//  HomeProtocols.swift
//  Lighf
//
//  Created by L.
//  Copyright © 2019 L. All rights reserved.
//

import UIKit
import SwiftyJSON

protocol HomeViewToPresenterProtocol: class {
    func createPost(type: PostType)
    //
    func settingsButtonClicked()
    func searchButtonClicked()
    func notificationButtonClicked()
    func createButtonClicked()
    //
    func backpackButtonClicked(for post: Any, addToBackPack: Bool)
    func relateButtonClicked(for post: Any, markAsRelate: Bool)
    func leafButtonClicked(for post: Any)
    func uploadImageButtonClicked(sourceType: UIImagePickerController.SourceType)
    func promptUserToAuthorizeCameraInSettings()
    func imageFetchFailed(withError: Error)
    func flagButtonClicked(selectedFlagOption: FlagOptions, for postID: Post)
    // My lighf button actions
    func activityButtonClicked()
    func backpackButtonClicked(from index: Int, to count: Int, withParams: NSDictionary)
    func myStoryButtonClicked(from index: Int, to count: Int, withParams: NSDictionary)
    func getUserPoints()
    //
    func updateFeeds(from index: Int, to count: Int, withParams: NSDictionary)
    func getUpdatedFeedItems(count: Int, withParams: NSDictionary)
    func viewPost(_ post: Post, type: PostType)
    func deletePost(_ post: Post)
    func editPost(_ post: Post, type: PostType)
    func fetchMyPosts(from index: Int, to count: Int)
    func setImageUrl(_ url: String?)
    func setImage(image: UIImage?)
    //
    func getProfileHeaderImage()
    func getSavedAspects()
    func toggleNotification(forItem: Any, toState: NotificationState)
}

protocol HomePresenterToInteractorProtocol: class {
    func fetchActivities()
    func fetchBackPackItems(from index: Int, to count: Int, withParams: NSDictionary)
    func fetchMyStories()
    func deletePost(_ post: Post)
    func fetchUserPoints()
    // Post operations
    func markPost(_ post: Any, asRelate: Bool)
    func backPackPost(_ post: Any, addToBackPack: Bool)
    func addFlag(_ option: FlagOptions, comment: String?, for itemID: Int, itemType: String)
    func setImageUrl(_ url: String?)
    func setImage(image: UIImage?)
    //
    func getProfileHeaderImage()
    func invokeGetSavedAspects()
}
protocol HomePresenterToGiveLeafProtocol: class {
    func invokeGiveLeaf(forPost: Post, leafCount: Int)
}
protocol GiveLeafToHomePresenterProtocol: class {
    func giveLeafAPIResponse(_ status: String, error: String?)
}

protocol HomeInteractorToPresenterProtocol: class {
    func handleActivities(_ activities: [Activity], error: Error?)
    func handleBackPackItems(_ items: [Post], error: Error?)
    func handleMyStories(_ posts: [Post], error: Error?)
    func deletedPost(_ post: Post, with message: String)
    func deletePost(_ post: Post, failedWithMessage message: String)
    func handleUserPoints(_ points: Int, error: String?)
    func performedOperation(_ operation: PostOperation, onPost post: Post, with error: String?)
    func flagAdded(with error: String?, for itemID: Int)
    func handleImageUpload(_ imageURL: String, error: String?)
    func handleProfileImage(url: String?, error: String?)
    func updateListOfSavedAspects(aspectsArray: [JSON], error: String)
}

protocol HomePresenterToViewProtocol: BasePresenterToViewProtocol {
    func showCreateOptionsPopup()
    func showFeeds(_ feeds: [Any])
    func showGallery(for sourceType: UIImagePickerController.SourceType, mediaTypes: [String])
    func showAlert(_ message: String)
    func refreshFeeds()
    func showUserPoints(_ points: Int)
    func addPost(with postId: Int, toBackPack addToBackPack: Bool)
//    func markPost(with postId: Int, asRelated asRelate: Bool)
    func markPost(with postId: Int, asRead markAsRead: Bool)
    func popToFeed()
    func updateBackpackedStatus(with postId: Int, toBackPack addToBackPack: Bool)
    func updateRelateStatus(_ relate: Bool, forPostWith postId: Int)
    func removePost(with postId: Int)
    func updateFeedForRelate(with postId: Int, toRelate addToRelates: Bool)
    func updateRefreshedItems(_ feeds: [Any])
    func setProfileHeaderImage(url: String?)
    func didSetHeaderImage(status: Bool)
    func didResetHeaderImage(status: Bool)
    func listOfSavedAspects(responseArray: [Aspect])
    func updateNotificationStatus(postID: Int?, commentID: Int?, notificationStatus: NotificationState)
    func updateUserLeafCount()
}

protocol HomePresenterToRouterProtocol: class {
    func moveToCreatePostView(_ type: PostType)
    func viewWindow()
    func viewStory()
    func viewArticle()
    func navigateToSelectAspects()
    func navigateToNotificationsScreen()
    func navigateToSearchScreen(with interface: HomeModuleInterface)
    func navigateToSettings()
    func viewPost(_ post: Post, type: PostType, interface: HomeModuleInterface)
    func dismissView()
    func alertActionTapped(selectedOption: FlagOptions, postID: Post)
    func moveToEditPostView(with post: Post, type: PostType)
}

protocol HomePresenterToGetFeedsInteractorProtocol {
    func fetchFeeds(from index: Int, to count: Int, withParams: NSDictionary)
    func fetchMyPosts(from index: Int, to count: Int)
    func fetchUpdatedFeedItems(count: Int, withParams: NSDictionary)
}
protocol GetFeedsInteractorToHomePresenterProtocol {
    func handleFeeds(_ feeds: [JSON], error: String?)
    func handleUpdatedItems(_ items: [JSON], error: String?)
}

protocol PostOperationsProtocol: class {
    func addPost(with postId: Int, toBackPack addToBackPack: Bool)
    func updateFeedForRelate(with postId: Int, toRelate addToRelates: Bool)
    func markPost(with postId: Int, asRelated asRelate: Bool)
    func markPost(with postId: Int, asRead markAsRead: Bool)
    func removePostFromFeed(with postId: Int)
    func toggleNotification(with postId: Int, isEnabled: NotificationState)
}

protocol HomeModuleInterface: PostOperationsProtocol {
}

protocol PostDetailsInteractorToHomePresenterProtocol: class {
    func handle(post: Post, type: PostType)
    func detailFetchFailed(for post: Post, with message: String)
}

protocol HomePresenterToToggleNotificationProtocol: class {
    func invokeToggleNotification(forItem: Any, toState: NotificationState)
}
protocol ToggleNotificationToHomePresenterProtocol: class {
    func toggleNotificationAPIResponse(_ itemID: Int, itemType: String, notificationStatus: NotificationState)
    func toggleNotificationAPIResponseFailure(_ status: String, error: String?)

}
