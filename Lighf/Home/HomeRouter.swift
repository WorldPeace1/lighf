//
//  HomeRouter.swift
//  Lighf
//
//
//  Copyright © 2012 L. All rights reserved.
//

import UIKit

class HomeRouter: NSObject {

    weak var presentedViewController: UIViewController?
    // Identifier need to be modified
    static let viewIdentifier = "HomeScreen"
    //
    static func createModule() -> HomeViewController? {

        let view = UIStoryboard.main.instantiateViewController(withIdentifier: viewIdentifier) as? HomeViewController
        //
        let presenter = HomePresenter()
        let interactor = HomeInteractor()
        let router = HomeRouter()
        let getFeedsInteractor = GetFeedsInteractor()
        let giveLeafInteractor = GiveLeafInteractor()
        let toggleNotificationInteractor = ToggleNotificationInteractor()
        //
        view?.presenter = presenter
        //
        presenter.view = view
        presenter.interactor = interactor
        presenter.router = router
        presenter.feedsInteractor = getFeedsInteractor
        presenter.leafInteractor = giveLeafInteractor
        presenter.toggleNotificationInteractor = toggleNotificationInteractor
        //
        interactor.presenter = presenter
        //
        router.presentedViewController = view
        //
        getFeedsInteractor.homePresenter = presenter
        giveLeafInteractor.homePresenter = presenter
        toggleNotificationInteractor.homePresenter = presenter
        return view
    }
}

extension HomeRouter: HomePresenterToRouterProtocol {
    func navigateToSettings() {
        if let settingsViewController = SettingsRouter.createModule() {
            presentedViewController?.navigationController?.view.backgroundColor = UIColor.appGreenColor
            presentedViewController?.navigationItem.setHidesBackButton(false, animated: true)
            presentedViewController?.navigationController?.navigationBar.tintColor = UIColor.white
            presentedViewController?.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
            presentedViewController?.navigationController?.pushViewController(settingsViewController, animated: true)
        }
    }
    func navigateToSearchScreen(with interface: HomeModuleInterface) {
        if let searchViewController = SearchRouter.createModule(with: interface) {
            presentedViewController?.navigationController?.view.backgroundColor = UIColor.appGreenColor
            presentedViewController?.navigationItem.setHidesBackButton(false, animated: true)
            presentedViewController?.navigationController?.pushViewController(searchViewController, animated: true)
        }
    }
    func navigateToNotificationsScreen() {
        if let notificationsViewController = NotificationsRouter.createModule() {
            presentedViewController?.navigationController?.view.backgroundColor = UIColor.appGreenColor
            presentedViewController?.navigationItem.setHidesBackButton(false, animated: true)
            presentedViewController?.navigationController?.pushViewController(notificationsViewController, animated: true)
        }
    }
    func viewPost(_ post: Post, type: PostType, interface: HomeModuleInterface) {
        if let postDetailsViewController = PostDetailsRouter.createModule(with: interface) {
            presentedViewController?.navigationItem.setHidesBackButton(false, animated: true)
            postDetailsViewController.postType = type
            postDetailsViewController.post = post
            postDetailsViewController.viewType = .detailView
            presentedViewController?.navigationController?.pushViewController(postDetailsViewController, animated: true)
        }
    }
    func viewWindow() {
        if let postDetailsViewController = PostDetailsRouter.createModule() {
            presentedViewController?.navigationController?.view.backgroundColor = UIColor.appBlueColor
            presentedViewController?.navigationItem.setHidesBackButton(false, animated: true)
            postDetailsViewController.postType = PostType.window
            presentedViewController?.navigationController?.pushViewController(postDetailsViewController, animated: true)
        }
    }
    func viewStory() {
        if let postDetailsViewController = PostDetailsRouter.createModule() {
            presentedViewController?.navigationController?.view.backgroundColor = UIColor.appGreenColor
            presentedViewController?.navigationItem.setHidesBackButton(false, animated: true)
            postDetailsViewController.postType = PostType.story
            presentedViewController?.navigationController?.pushViewController(postDetailsViewController, animated: true)
        }
    }
    func viewArticle() {
        if let postDetailsViewController = PostDetailsRouter.createModule() {
            presentedViewController?.navigationController?.view.backgroundColor = .black
            presentedViewController?.navigationItem.setHidesBackButton(false, animated: true)
            postDetailsViewController.postType = PostType.article
            presentedViewController?.navigationController?.pushViewController(postDetailsViewController, animated: true)
        }
    }
    func navigateToSelectAspects() {
        if let selectAspectsViewController = AspectsRouter.createModule(isFromSettings: false) {
            presentedViewController?.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
            presentedViewController?.navigationController?.navigationBar.shadowImage = UIImage()
            presentedViewController?.navigationController?.navigationBar.isTranslucent = true
            presentedViewController?.navigationController?.view.backgroundColor = .clear
            presentedViewController?.navigationController?.pushViewController(selectAspectsViewController, animated: true)
        }
    }
    //
    func moveToCreatePostView(_ type: PostType) {
        let useAlternatePostCreationOption = UserDefaults.standard.value(forKey: UserDefaultsKey.showAlternatePostCreationOption) as? Bool ?? true
        if useAlternatePostCreationOption {
            if let createPostView = CreatePostRouter.createAlternateModule(for: type, post: nil) {
                self.presentedViewController?.navigationController?.pushViewController(createPostView, animated: true)
            }
        } else {
            if let createPostView = CreatePostRouter.createDefaultModule(for: type, post: nil) {
                self.presentedViewController?.navigationController?.pushViewController(createPostView, animated: true)
            }
        }
    }
    func dismissView() {
        presentedViewController?.navigationController?.popViewController(animated: true)
    }
    func alertActionTapped(selectedOption: FlagOptions, postID: Post) {
        print(selectedOption)
        //write logic to navigate to new screen to input data.
        if let commentsViewController = CommentsViewRouter.createModule() {
            commentsViewController.flagOption = selectedOption
            commentsViewController.post = postID
            presentedViewController?.navigationController?.setTransparent()
            presentedViewController?.navigationItem.setHidesBackButton(false, animated: true)
            presentedViewController?.navigationController?.pushViewController(commentsViewController, animated: true)
        }
    }
    func moveToEditPostView(with post: Post, type: PostType) {
//        if let createPostView = CreatePostRouter.createModule(for: type, post: post) {
//            self.presentedViewController?.navigationController?.pushViewController(createPostView, animated: true)
//        }
        let useAlternatePostCreationOption = UserDefaults.standard.value(forKey: UserDefaultsKey.showAlternatePostCreationOption) as? Bool ?? true
        if useAlternatePostCreationOption {
            if let createPostView = CreatePostRouter.createAlternateModule(for: type, post: post) {
                self.presentedViewController?.navigationController?.pushViewController(createPostView, animated: true)
            }
        } else {
            if let createPostView = CreatePostRouter.createDefaultModule(for: type, post: post) {
                self.presentedViewController?.navigationController?.pushViewController(createPostView, animated: true)
            }
        }
    }
}
