//
//  HomeInteractor.swift
//  Lighf
//
//  Created by L.
//  Copyright © 2019 L. All rights reserved.
//

import UIKit

class HomeInteractor: PostOperationsInteractor {

    weak var presenter: HomeInteractorToPresenterProtocol?
    //
    override func performedOperation(_ operation: PostOperation, onPost post: Post, with error: String?) {
        presenter?.performedOperation(operation, onPost: post, with: error)
    }
}

extension HomeInteractor: HomePresenterToInteractorProtocol {
    //get list of saved aspects.
    func invokeGetSavedAspects() {
        let apiEndPoint = EndPoint.getSavedAspectList
        let defaults = UserDefaults.standard
        if let userID = defaults.object(forKey: UserDefaultsKey.userID) as? Int {
            let params = ["id": userID]
            APIManager.post(endPoint: apiEndPoint, parameters: params as [String: Any]) { (response, error) in
                guard let response = response, error == nil else {
                    self.presenter?.updateListOfSavedAspects(aspectsArray: [], error: error?.message ?? Constants.emptyString)
                    return
                }
                let responseArray = response["api_response"]["aspects"].array
                let message = response["api_status"]["message"].stringValue
                self.presenter?.updateListOfSavedAspects(aspectsArray: responseArray ?? [], error: message)
            }
        }
    }
    //upload image via gallery
    func setImageUrl(_ url: String?) {
        guard let urlString = url else {
            uploadUserHeaderImage(encodedImage: Constants.emptyString)
            return
        }
        //
        if url != Constants.emptyString, let imageURL = URL(string: urlString), let imageData = try? Data(contentsOf: imageURL) {
            let image: UIImage = UIImage(data: imageData) ?? UIImage()
            let jpegData = image.jpegData(compressionQuality: 0.5)
            let encodedImage = jpegData?.base64EncodedString()
            uploadUserHeaderImage(encodedImage: encodedImage ?? Constants.emptyString)
        }
    }
    //upload image via phone
    func setImage(image: UIImage?) {
        if image != nil, let imageData = image?.jpegData(compressionQuality: 0.5) {
            let encodedImage = imageData.base64EncodedString()
            uploadUserHeaderImage(encodedImage: encodedImage)
        }
    }
    //upload image api
    func uploadUserHeaderImage(encodedImage: String) {
        let defaults = UserDefaults.standard
        var parameters: [String: Any] = [:]
        parameters["id"] = defaults.value(forKey: UserDefaultsKey.userID)
        parameters["image"] = encodedImage
        APIManager.post(endPoint: EndPoint.changeHeaderImage, parameters: parameters) {(response, error) in
            guard error == nil, let response = response else {
                self.presenter?.handleImageUpload(Constants.emptyString, error: error?.message)
                return
            }
            let imageURL = response[APIResponse.apiResponse]["image_urls"]["full"].stringValue
            let status = response[APIResponse.apiResponse][APIResponse.apiResponseStatus].stringValue
            if status == Constants.success {
                let imageId = response[APIResponse.apiResponse]["image_id"].intValue
                UserDefaults.standard.set(imageId, forKey: UserDefaultsKey.imageId)
                self.presenter?.handleImageUpload(imageURL, error: nil)
            } else {
                self.presenter?.handleImageUpload(Constants.emptyString, error: error?.message)
            }
        }
    }
    func markPost(_ post: Any, asRelate: Bool) {
        if let selectedPost = post as? Post {
            mark(selectedPost, asRelated: asRelate)
        }
    }
    //
    func backPackPost(_ post: Any, addToBackPack: Bool) {
        if let selectedPost = post as? Post {
            add(selectedPost, toBackPack: addToBackPack)
        }
    }
    //invoke api to fetch user points
    func fetchUserPoints() {
        let defaults = UserDefaults.standard
        var parameters: [String: Any] = [:]
        parameters["id"] = defaults.value(forKey: UserDefaultsKey.userID)
        APIManager.post(endPoint: EndPoint.getUserPoints, parameters: parameters) {(response, error) in
            guard error == nil, let response = response else {
                self.presenter?.handleUserPoints(0, error: error?.message)
                return
            }
            let points = response[APIResponse.apiResponse][APIResponse.apiUserPoints].intValue
            UserDefaults.standard.set(points, forKey: UserDefaultsKey.leafCount)
            self.presenter?.handleUserPoints(points, error: error?.message)
        }
    }
    func addFlag(_ option: FlagOptions, comment: String?, for itemID: Int, itemType: String) {
        // API call
        let defaults = UserDefaults.standard
        var parameters: [String: Any] = [:]
        parameters["id"] = defaults.value(forKey: UserDefaultsKey.userID)
        parameters["item_id"] = itemID
        parameters["item_type"] = itemType //“post” or “comment”
        parameters["reason"] = option.message
        parameters["reason_details"] = comment
        //
        APIManager.post(endPoint: EndPoint.flagPost, parameters: parameters) { (response, error) in
            guard let response = response, error == nil else {
                self.presenter?.flagAdded(with: error?.message, for: itemID)
                return
            }
            let message = response[APIResponse.apiResponse][APIResponse.apiResponseMessage].stringValue
            let status = response[APIResponse.apiResponse][APIResponse.apiResponseStatus].stringValue
            if status == Constants.success {
                // On successful completion
                self.presenter?.flagAdded(with: Message.feedbackSuccess, for: itemID)
            } else {
                self.presenter?.flagAdded(with: message, for: itemID)
            }
        }
    }
    //
    func fetchActivities() {
         // API call
    }
    func fetchBackPackItems(from index: Int, to count: Int, withParams: NSDictionary) {
         // API call
    }
    func fetchMyStories() {
         // API call
    }
    //
    func getProfileHeaderImage() {
        let defaults = UserDefaults.standard
        var parameters: [String: Any] = [:]
        parameters["image_id"] = defaults.value(forKey: UserDefaultsKey.imageId)
        //
        APIManager.post(endPoint: EndPoint.headerImage, parameters: parameters) { (response, error) in
            guard let response = response, error == nil else {
                self.presenter?.handleProfileImage(url: nil, error: error?.message)
                return
            }
            let message = response[APIResponse.apiResponse][APIResponse.apiResponseMessage].stringValue
            let status = response[APIResponse.apiResponse][APIResponse.apiResponseStatus].stringValue
            if status == Constants.success {
                let url = response[APIResponse.apiResponse]["image_urls"]["full"].stringValue
                self.presenter?.handleProfileImage(url: url, error: nil)
            } else {
                self.presenter?.handleProfileImage(url: nil, error: message)
            }
        }
    }
    /// Delete post
    ///
    /// - Parameter post: The post that should be deleted
    func deletePost(_ post: Post) {
        let defaults = UserDefaults.standard
        var parameters: [String: Any] = [:]
        parameters["id"] = defaults.value(forKey: UserDefaultsKey.userID)
        parameters["post_id"] = post.postId
        //
        APIManager.post(endPoint: EndPoint.deletePost, parameters: parameters) { (response, error) in
            guard error == nil, let response = response else {
                self.presenter?.deletePost(post, failedWithMessage: error!.message)
                return
            }
            let message = response[APIResponse.apiResponse][APIResponse.apiResponseMessage].stringValue
            let status = response[APIResponse.apiResponse][APIResponse.apiResponseStatus].stringValue
            if status == Constants.success {
                self.presenter?.deletedPost(post, with: message)
            } else {
                var currentPost = post
                currentPost.isDeleted = response[APIResponse.apiResponse]["is_deleted"].boolValue
                self.presenter?.deletePost(currentPost, failedWithMessage: message)
            }
        }
    }
}
