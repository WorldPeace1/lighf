//
//  FilterView.swift
//  Lighf
//
//  Created by L.
//  Copyright © 2019 L. All rights reserved.
//

import UIKit

@objc
protocol FilterToViewProtocol: class {
    func segmentButtonClicked(sender: ScrollableSegmentedControl)
    @objc optional func filterButtonClicked(_ sender: UIButton)
}

class FilterView: UIView {

    @IBOutlet var contentView: UIView!
    @IBOutlet weak var segmentedControlLeadingConstraint: NSLayoutConstraint!
    @IBOutlet weak var segmentControl: ScrollableSegmentedControl!
    @IBOutlet weak var filterButton: UIButton!
    weak var view: FilterToViewProtocol?
    //
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    //
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    //
    init(frame: CGRect, values: [NSAttributedString], images: [String]?, includeFilter: Bool) {
        super.init(frame: frame)
        Bundle.main.loadNibNamed("FilterView", owner: self, options: nil)
        self.addSubview(contentView)
        contentView.frame = self.bounds
        configureSegmentControl(values: values, images: images, includeFilter: includeFilter)
        segmentControl.addTarget(self, action: #selector(segmentControlDidChange(sender:)), for: .valueChanged)
    }
    //
    private func configureSegmentControl(values: [NSAttributedString], images: [String]?, includeFilter: Bool) {
        //
        if includeFilter == false {
            segmentedControlLeadingConstraint.constant = 0.0
        }
        //
        segmentControl.underlineSelected = true
        segmentControl.tintColor = UIColor(red: 21/255.0, green: 146/255.0, blue: 230/255.0, alpha: 1.0)
        //
        segmentControl.segmentContentColor = UIColor.black
        segmentControl.selectedSegmentContentColor = UIColor.black
        segmentControl.imageTintColor = UIColor.appBlueColor
        segmentControl.backgroundColor = UIColor.white
        //
        if let images = images {
            segmentControl.segmentStyle = .imageOnLeft
            for (index, value) in values.enumerated() {
                var image: UIImage?
                image = index >= images.count ? nil : UIImage(named: images[index])
                segmentControl.insertSegment(withTitle: value, image: image, at: index)
            }
        } else {
            segmentControl.segmentStyle = .textOnly
            for (index, value) in values.enumerated() {
                segmentControl.insertSegment(withTitle: value, at: index)
            }
        }
        segmentControl.fixedSegmentWidth = true
    }
    @objc func segmentControlDidChange(sender: ScrollableSegmentedControl) {
        view?.segmentButtonClicked(sender: sender)
    }
    //
    @IBAction func filterButtonClicked(_ sender: UIButton) {
        view?.filterButtonClicked?(sender)
    }
}
