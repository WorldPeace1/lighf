//
//  HomeViewController+FilterView.swift
//  Lighf
//
//  Created by L.
//  Copyright © 2019 L. All rights reserved.
//

import Foundation
import UIKit
extension HomeViewController: FilterToViewProtocol {
    ///
    /// Delegate method called when an option in any filter is selected
    ///
    /// - Parameter sender: filter
    func segmentButtonClicked(sender: ScrollableSegmentedControl) {
        if sender.tag == 0 && sender.selectedSegmentIndex == 1 {
            isMyLighfSelected = true
            if isFilterVisible {
                isFilterVisible = false
            }
            presenter?.getUserPoints()
            showPrimaryFilter()
            primaryFilter?.filterButton.isHidden = true
            myLighfViewHeightConstraint.constant = myLighfViewHeight
            myRelatesButtonClicked(relatedPostsButton)
        } else if sender.tag == 0 && sender.selectedSegmentIndex == 0 {
            isMyLighfSelected = false
            primaryFilter?.filterButton.isHidden = false
            myLighfViewHeightConstraint.constant = 0.0
            if isFilterActive {
                primaryFilter?.filterButton.isSelected = true
                if isFilterVisible {
                    showAllFilters()
                } else {
                    showPrimaryFilter()
                }
                apiParams[GetFeedOptions.feedTypeKey.rawValue] = [GetFeedOptions.feedAspectFilterKey.rawValue]
                apiParams[GetFeedOptions.feedFilterKey.rawValue] = [selectedFilterAspectID]
            } else {
                apiParams.removeObject(forKey: GetFeedOptions.feedTypeKey.rawValue)
                apiParams.removeObject(forKey: GetFeedOptions.feedFilterKey.rawValue)
            }
            removeDataFromTable()
            refreshFeeds()
        } else if sender.tag == 1 {
            selectedAspectsSegment(sender: sender)
        } else {
            selectedPostTypeSegment(sender: sender)
        }
    }
    //Aspects selection
    func selectedAspectsSegment(sender: ScrollableSegmentedControl) {
        let keysArray = Array(aspectsObjectsDict.keys)
        if let paramsArray = apiParams[GetFeedOptions.feedTypeKey.rawValue] as? [String], let index = paramsArray.firstIndex(of: GetFeedOptions.feedAspectFilterKey.rawValue) {
            var filterKeyArray = apiParams[GetFeedOptions.feedFilterKey.rawValue] as? [Any]
            if filterKeyArray?.count ?? 0 > index {
                filterKeyArray?[index] = keysArray[sender.selectedSegmentIndex]
                selectedFilterAspectID = keysArray[sender.selectedSegmentIndex]
                apiParams[GetFeedOptions.feedFilterKey.rawValue] = filterKeyArray
            }
        } else {
         // add GetFeedOptions.feedAspectFilterKey.rawValue and aspect ID
            var paramsArray = apiParams[GetFeedOptions.feedTypeKey.rawValue] as? [String]
            paramsArray?.append(GetFeedOptions.feedAspectFilterKey.rawValue)
            apiParams[GetFeedOptions.feedTypeKey.rawValue] = paramsArray
            var filterKeyArray = apiParams[GetFeedOptions.feedFilterKey.rawValue] as? [Any]
            filterKeyArray?.append(keysArray[sender.selectedSegmentIndex] as Int)
            apiParams[GetFeedOptions.feedFilterKey.rawValue] = filterKeyArray
        }
        removeDataFromTable()
        refreshFeeds()
    }
    //Post type selections
    func selectedPostTypeSegment(sender: ScrollableSegmentedControl) {
       var postTypeParam = Constants.emptyString
        if sender.selectedSegmentIndex == 0 {
            postTypeParam = PostType.story.rawValue
        } else if sender.selectedSegmentIndex == 1 {
            postTypeParam = PostType.window.rawValue
        } else {
            postTypeParam = PostType.article.rawValue
        }
        if let paramsArray = apiParams[GetFeedOptions.feedTypeKey.rawValue] as? [String], let index = paramsArray.firstIndex(of: GetFeedOptions.feedPostTypeFilterKey.rawValue) {
            var filterKeyArray = apiParams[GetFeedOptions.feedFilterKey.rawValue] as? [Any]
            if filterKeyArray?.count ?? 0 > index {
                filterKeyArray?[index] = postTypeParam
                apiParams[GetFeedOptions.feedFilterKey.rawValue] = filterKeyArray
            }
        } else {
            // add GetFeedOptions.feedPostTypeFilterKey.rawValue and postTypeParam
            var paramsArray = apiParams[GetFeedOptions.feedTypeKey.rawValue] as? [String]
            paramsArray?.append(GetFeedOptions.feedPostTypeFilterKey.rawValue)
            apiParams[GetFeedOptions.feedTypeKey.rawValue] = paramsArray
            var filterKeyArray = apiParams[GetFeedOptions.feedFilterKey.rawValue] as? [Any]
            filterKeyArray?.append(postTypeParam)
            apiParams[GetFeedOptions.feedFilterKey.rawValue] = filterKeyArray
        }
        removeDataFromTable()
        refreshFeeds()

    }
    /// Show just the primary filter
    func showPrimaryFilter() {
        for index in stride(from: Int(topViewHeightConstraint.constant), to: Int(filterViewHeight), by: -1) {
            topViewHeightConstraint.constant = CGFloat(index)
        }
        isFilterVisible = false
        filterStackView.removeArrangedSubview(secondaryFilter!)
        filterStackView.removeArrangedSubview(postTypeFilter!)
        isTableScrolled = false
    }
    /// Show all the available filters
    func showAllFilters() {
        filterStackView.addArrangedSubview(secondaryFilter!)
        filterStackView.addArrangedSubview(postTypeFilter!)
        topViewHeightConstraint.constant = 3 * filterViewHeight
    }
    /// Method that shows and hides the secondary and postType selection filters
    ///
    /// - Parameter sender: Filter button
    func filterButtonClicked(_ sender: UIButton) {
        if isFilterVisible { // logic to disable filter
            showPrimaryFilter()
            sender.isSelected = false
            apiParams.removeObject(forKey: GetFeedOptions.feedTypeKey.rawValue)
            apiParams.removeObject(forKey: GetFeedOptions.feedFilterKey.rawValue)
            isFilterActive = false
            isFilterVisible = false
            sender.isSelected = false
            removeDataFromTable()
            refreshFeeds()
        } else if isFilterActive && !isFilterVisible { //logic to show filter without changing anything
            showAllFilters()
            sender.isSelected = true
            isFilterVisible = true
        } else { // logic to implement filter
            if sender.isSelected == false {
                showAllFilters()
                if feedTableView.contentOffset.y > 0 {
                    isTableScrolled = true
                } else {
                    isTableScrolled = false
                }
            } else {
                showPrimaryFilter()
            }
            sender.isSelected = !sender.isSelected
            if sender.isSelected {
                let keysArray = Array(aspectsObjectsDict.keys)
                apiParams[GetFeedOptions.feedTypeKey.rawValue] = [GetFeedOptions.feedAspectFilterKey.rawValue, GetFeedOptions.feedPostTypeFilterKey.rawValue]
                if selectedFilterAspectID == -1 && !keysArray.isEmpty {
                    selectedFilterAspectID = keysArray[0]
                }
                apiParams[GetFeedOptions.feedFilterKey.rawValue] = [selectedFilterAspectID, PostType.story.rawValue]
            } else {
                apiParams.removeObject(forKey: GetFeedOptions.feedTypeKey.rawValue)
                apiParams.removeObject(forKey: GetFeedOptions.feedFilterKey.rawValue)
            }
            isFilterActive = sender.isSelected
            isFilterVisible = sender.isSelected
            removeDataFromTable()
            refreshFeeds()
        }
    }
}
