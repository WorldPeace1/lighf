//
//  HomeViewController+ButtonActions.swift
//  Lighf
//
//  
//  Copyright © 2020 L. All rights reserved.
//

import UIKit
import AVKit
import MBProgressHUD

extension HomeViewController {
    // MARK: - Navigation header view button actions
    //
    @objc
    func settingsButtonClicked(sender: UIButton) {
        presenter?.settingsButtonClicked()
    }
    //
    @objc
    func searchButtonClicked(sender: UIButton) {
        presenter?.searchButtonClicked()
    }
    //
    @objc
    func notificationButtonClicked(sender: UIButton) {
        presenter?.notificationButtonClicked()
    }
    //
    @objc
    func createStoryButtonClicked(sender: UIButton) {
        if let createOptions = UserDefaults.standard.value(forKey: UserDefaultsKey.createOptions) as? [String], createOptions.count == 1 {
            let value = createOptions[0] as String
            if value == PostType.story.rawValue {
                presenter?.createPost(type: PostType.story)
            } else if value == PostType.article.rawValue {
                presenter?.createPost(type: PostType.article)
            }
        } else {
            presenter?.createButtonClicked()
        }
    }
    // MARK: - My lighf button actions
    //
    @IBAction func activityButtonClicked(_ sender: UIButton) {
        handleMyLighfButton(sender)
        presenter?.activityButtonClicked()
    }
    //
    @IBAction func backpackButtonClicked(_ sender: UIButton) {
        handleMyLighfButton(sender)
        isCreatedList = false
        isBackpackedList = true
        isRelatesList = false
        apiParams[GetFeedOptions.feedTypeKey.rawValue] = [GetFeedOptions.feedBackpackedFilterKey.rawValue]
        apiParams[GetFeedOptions.feedFilterKey.rawValue] = []
        removeDataFromTable()
        presenter?.backpackButtonClicked(from: postOffset, to: numberOfPostsPerAPI, withParams: apiParams)
    }
    //
    @IBAction func myStoryButtonClicked(_ sender: UIButton) {
        handleMyLighfButton(sender)
        isCreatedList = true
        isBackpackedList = false
        isRelatesList = false
        apiParams[GetFeedOptions.feedTypeKey.rawValue] = [GetFeedOptions.feedCreatedPostFilterKey.rawValue]
        apiParams[GetFeedOptions.feedFilterKey.rawValue] = []
        removeDataFromTable()
        presenter?.myStoryButtonClicked(from: postOffset, to: numberOfPostsPerAPI, withParams: apiParams)
    }
    @IBAction func myRelatesButtonClicked(_ sender: UIButton) {
        handleMyLighfButton(sender)
        isCreatedList = false
        isBackpackedList = false
        isRelatesList = true
        apiParams[GetFeedOptions.feedTypeKey.rawValue] = [GetFeedOptions.feedRelatedPostFilterKey.rawValue]
        apiParams[GetFeedOptions.feedFilterKey.rawValue] = []
        removeDataFromTable()
        presenter?.myStoryButtonClicked(from: postOffset, to: numberOfPostsPerAPI, withParams: apiParams)
    }
    func removeDataFromTable() {
        postOffset = 0 // reset offset to zero to get initial data.
        isAllFeedReceived = false // reset flag to enable lazy loading
        feeds.removeAll() // removing all data from table source.
        feedTableView.reloadData()
        showActivityIndicator()
    }
    //get feed related post details for a single post.
    func getPostDetails(withPostID: Int) {
        apiParams[GetFeedOptions.feedTypeKey.rawValue] = [GetFeedOptions.feedPostsByIDsFilterKey.rawValue]
        // need to get the post IDs that need to be updated.
        apiParams[GetFeedOptions.feedFilterKey.rawValue] = [[withPostID]]
        presenter?.getUpdatedFeedItems(count: 1, withParams: apiParams)
    }
    //
    private func handleMyLighfButton(_ sender: UIButton) {
        emptyMessageLabel.isHidden = true
        guard sender.isSelected == false else {
            return
        }
        sender.isSelected = true
        sender.backgroundColor = UIColor.appBlueColor
        //
        myLighfButtonCollection.forEach {
            if $0 != sender {
                $0.backgroundColor = UIColor.white
                $0.isSelected = false
            }
        }
    }
    // give user option to choose from either gallery or camera.
    @IBAction func uploadImageButtonClicked(_ sender: UIButton) {
        Utility().presentTwoActionAlerController(titleString: "Choose image source", messageString: "", rightButtonTitle: "Gallery", leftButtonTitle: "Camera") { updateButtonTapped in
            if updateButtonTapped == 1 {
                DispatchQueue.main.async {
                    self.presenter?.uploadImageButtonClicked(sourceType: UIImagePickerController.SourceType.photoLibrary)
                }
            } else if updateButtonTapped == 2 {
                self.captureImageViaCamera()
            }
        }
    }
    func captureImageViaCamera() {
        let cameraMediaType = AVMediaType.video
        let cameraAuthorizationStatus = AVCaptureDevice.authorizationStatus(for: cameraMediaType)
        switch cameraAuthorizationStatus {
        case .denied:
            presenter?.promptUserToAuthorizeCameraInSettings()
        case .authorized:
            presenter?.uploadImageButtonClicked(sourceType: UIImagePickerController.SourceType.camera)
        case .restricted: break
        presenter?.uploadImageButtonClicked(sourceType: UIImagePickerController.SourceType.photoLibrary)
        case .notDetermined:
            // Prompting user for the permission to use the camera.
            AVCaptureDevice.requestAccess(for: cameraMediaType) { granted in
                if granted {
                    print("Granted access to \(cameraMediaType)")
                    DispatchQueue.main.async {
                        self.presenter?.uploadImageButtonClicked(sourceType: UIImagePickerController.SourceType.camera)
                    }
                } else {
                    print("Denied access to \(cameraMediaType)")
                    DispatchQueue.main.async {
                        self.presenter?.uploadImageButtonClicked(sourceType: UIImagePickerController.SourceType.photoLibrary)
                    }
                }
            }
        }
    }
    // flagging options
    func flagOptionsButtonClicked(sender: Post) {
        let optionMenu = UIAlertController(title: nil, message: FlagOptions.header.rawValue, preferredStyle: .actionSheet)
        let notRelatedAction = UIAlertAction(title: FlagOptions.notRelated.rawValue, style: .default, handler: { (_ action) -> Void in
            self.alertActionSelected(selectedOption: FlagOptions.notRelated, withPost: sender)
        })
        optionMenu.addAction(notRelatedAction)
        let spamAction = UIAlertAction(title: FlagOptions.spam.rawValue, style: .default, handler: { (_ action) -> Void in
            self.alertActionSelected(selectedOption: FlagOptions.spam, withPost: sender)
        })
        optionMenu.addAction(spamAction)
        let abuseAction = UIAlertAction(title: FlagOptions.abuse.rawValue, style: .default, handler: { (_ action) -> Void in
            self.alertActionSelected(selectedOption: FlagOptions.abuse, withPost: sender)
        })
        optionMenu.addAction(abuseAction)
       let copyRightAction = UIAlertAction(title: FlagOptions.copyright.rawValue, style: .default, handler: { (_ action) -> Void in
            self.alertActionSelected(selectedOption: FlagOptions.copyright, withPost: sender)
        })
        optionMenu.addAction(copyRightAction)
       let dontSeeThisAction = UIAlertAction(title: FlagOptions.dontSeeThis.rawValue, style: .default, handler: { (_ action) -> Void in
            self.alertActionSelected(selectedOption: FlagOptions.dontSeeThis, withPost: sender)
        })
        optionMenu.addAction(dontSeeThisAction)
        let dontSeeLikeThisAction = UIAlertAction(title: FlagOptions.dontSeeLikeThis.rawValue, style: .default, handler: { (_ action) -> Void in
            self.alertActionSelected(selectedOption: FlagOptions.dontSeeLikeThis, withPost: sender)
        })
        /*optionMenu.addAction(otherThisAction)
             let otherThisAction = UIAlertAction(title: FlagOptions.otherThis.rawValue, style: .default, handler: { (_ action) -> Void in
                  self.alertActionSelected(selectedOption: FlagOptions.otherThis, withPost: sender)
              })
        */
        optionMenu.addAction(dontSeeLikeThisAction)
        if sender.isNotificationEnabled {
            let turnOffNotificationAction = UIAlertAction(title: FlagOptions.turnOffNotification.rawValue, style: .default, handler: { (_ action) -> Void in
                self.alertActionSelected(selectedOption: FlagOptions.turnOffNotification, withPost: sender)
            })
            optionMenu.addAction(turnOffNotificationAction)
        } else {
            let turnOnNotificationAction = UIAlertAction(title: FlagOptions.turnOnNotification.rawValue, style: .default, handler: { (_ action) -> Void in
                self.alertActionSelected(selectedOption: FlagOptions.turnOnNotification, withPost: sender)
            })
            optionMenu.addAction(turnOnNotificationAction)
        }
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel)
        optionMenu.addAction(cancelAction)
        self.present(optionMenu, animated: true, completion: nil)
    }
    //flag option selected.
    func alertActionSelected(selectedOption: FlagOptions, withPost: Post) {
        if selectedOption != FlagOptions.turnOffNotification, selectedOption != FlagOptions.turnOnNotification {
            presenter?.flagButtonClicked(selectedFlagOption: selectedOption, for: withPost)
        } else if selectedOption == FlagOptions.turnOffNotification || selectedOption == FlagOptions.turnOnNotification {
            print("turn off notifications")
            if withPost.isNotificationEnabled {
                presenter?.toggleNotification(forItem: withPost, toState: NotificationState.turnOff)
            } else {
                presenter?.toggleNotification(forItem: withPost, toState: NotificationState.turnOn)
            }
        }
    }

    // prompt to delete an image.
    @IBAction func removeImageButtonClicked(_ sender: UIButton) {
        let removeImageAlert = UIAlertController(title: "", message: Message.deleteConfirmation, preferredStyle: .alert)
        let deleteAction = UIAlertAction(title: "Delete", style: .default) { (_) in
            MBProgressHUD.showAdded(to: self.headerImageContainerView, animated: true)
            self.presenter?.setImageUrl(nil)
        }
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        removeImageAlert.addAction(deleteAction)
        removeImageAlert.addAction(cancelAction)
        self.present(removeImageAlert, animated: true, completion: nil)
    }
}
