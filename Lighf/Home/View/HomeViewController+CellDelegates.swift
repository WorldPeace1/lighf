//
//  HomeViewController+CellDelegates.swift
//  Lighf
//
//  Created by L.
//  Copyright © 2019 L. All rights reserved.
//

import UIKit

extension HomeViewController: ArticleCellDelegate {
    func articleCell(_ cell: ArticleCell, didClickBackpackButtonFor post: Post, addToBackpack: Bool) {
        if let posts = feeds as? [Post], let index = posts.firstIndex(where: {$0.postId == post.postId}) {
            presenter?.backpackButtonClicked(for: feeds[index], addToBackPack: addToBackpack)
        }
    }
}

extension HomeViewController: WindowCellDelegate {
    func windowCell(_ cell: WindowCell, didClickOnRelateButtonFor post: Post, markAsRelate relate: Bool) {
        if let posts = feeds as? [Post], let index = posts.firstIndex(where: {$0.postId == post.postId}) {
            if relate {
                cell.relateButton.backgroundColor = UIColor.relatableGreenColor
            } else {
                cell.relateButton.backgroundColor = UIColor.appGreenColor
            }
            presenter?.relateButtonClicked(for: feeds[index], markAsRelate: relate)
        }
    }
    //
    func windowCell(_ cell: WindowCell, didClickBackpackButtonFor post: Post, addToBackPack: Bool) {
        if let posts = feeds as? [Post], let index = posts.firstIndex(where: {$0.postId == post.postId}) {
            presenter?.backpackButtonClicked(for: feeds[index], addToBackPack: addToBackPack)
        }
    }
}

extension HomeViewController: StoryCellDelegate {
    func storyCell(_ cell: StoryCell, didClickRelateButtonFor post: Post, markAsRelate relate: Bool) {
        if let posts = feeds as? [Post], let index = posts.firstIndex(where: {$0.postId == post.postId}) {
//            if relate {
//                cell.relateButton.backgroundColor = UIColor.relatableGreenColor
//            } else {
//                cell.relateButton.backgroundColor = UIColor.appGreenColor
//            }
            presenter?.relateButtonClicked(for: feeds[index], markAsRelate: relate)
        }
    }
    //
    func storyCell(_ cell: StoryCell, didClickBackpackButtonFor post: Post, addToBackPack: Bool) {
        if let posts = feeds as? [Post], let index = posts.firstIndex(where: {$0.postId == post.postId}) {
            presenter?.backpackButtonClicked(for: feeds[index], addToBackPack: addToBackPack)
        }
    }
    //
    func storyCell(_ cell: StoryCell, didClickLeafButtonFor post: Post) {
        if let posts = feeds as? [Post], let index = posts.firstIndex(where: {$0.postId == post.postId}) {
             presenter?.leafButtonClicked(for: feeds[index])
        }
    }
}

extension HomeViewController: AdminPostCellDelegate {
    func adminPostCell(_ cell: AdminPostCell, didClickBackpackButtonFor post: Post, addToBackPack: Bool) {
        if let posts = feeds as? [Post], let index = posts.firstIndex(where: {$0.postId == post.postId}) {
            presenter?.backpackButtonClicked(for: feeds[index], addToBackPack: addToBackPack)
        }
    }
}
