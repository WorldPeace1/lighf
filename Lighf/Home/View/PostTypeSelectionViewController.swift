//
//  PostTypeSelectionViewController.swift
//  Lighf
//
//  Created by L.
//  Copyright © 2019 L. All rights reserved.
//

import UIKit
import PopupDialog

protocol PostTypeSelectionProtocol: class {
    func postTypeSelectionViewController(_ selectionVC: PostTypeSelectionViewController, didSelectType type: PostType)
}

class PostTypeSelectionViewController: UIViewController {

    weak var delegate: PostTypeSelectionProtocol?
    var parentPopUp: PopupDialog?
    //
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    //
    static var storyBoardId: String {
        return "PostTypeSelectionVC"
    }
    //
    @IBAction func createStoryButtonClicked(sender: UIButton) {
        dismiss()
        delegate?.postTypeSelectionViewController(self, didSelectType: .story)
    }
    //
    @IBAction func publishArticleButtonClicked(sender: UIButton) {
        dismiss()
        delegate?.postTypeSelectionViewController(self, didSelectType: .article)
    }
    //
    @IBAction func closeButtonClicked(sender: UIButton) {
        dismiss()
    }
    //
    func dismiss() {
        guard let parentPopUp = parentPopUp else {
            return
        }
        parentPopUp.dismiss()
    }
}
