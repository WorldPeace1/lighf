//
//  WindowCell.swift
//  Lighf
//
//  Created by L.
//  Copyright © 2019 L. All rights reserved.
//

import UIKit
import Kingfisher

protocol WindowCellDelegate: class {
    func windowCell(_ cell: WindowCell, didClickOnRelateButtonFor post: Post, markAsRelate relate: Bool)
    func windowCell(_ cell: WindowCell, didClickBackpackButtonFor post: Post, addToBackPack: Bool)
}

class WindowCell: UITableViewCell {

    static var identifier: String {
        return "WindowCell"
    }
    var windowPost: Window?
    weak var delegate: WindowCellDelegate?
    //
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var statementLabel: UILabel!
    @IBOutlet weak var backpackButton: UIButton!
    @IBOutlet weak var relateButton: UIButton!
    @IBOutlet weak var creditsLabel: UILabel!
    @IBOutlet weak var windowImageView: UIImageView!
    //
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    //
    @IBAction func backpackButtonClicked(sender: UIButton) {
        if let post = windowPost {
            delegate?.windowCell(self, didClickBackpackButtonFor: post, addToBackPack: !sender.isSelected)
//            sender.isSelected = !sender.isSelected
        }
    }
    //
    @IBAction func relateButtonClicked(sender: UIButton) {
        if let post = windowPost {
            delegate?.windowCell(self, didClickOnRelateButtonFor: post, markAsRelate: !sender.isSelected)
            sender.isSelected = !sender.isSelected
        }
    }
    //
    public func prepareCell(with window: Window?, atIndex indexPath: IndexPath) {
        statementLabel.text = window?.title.string 
        creditsLabel.text = window?.authorName
        backpackButton.isSelected = window?.isBackPacked ?? false
        relateButton.isSelected = window?.isRelated ?? false
        if window?.isRelated ?? false {
            relateButton.backgroundColor = UIColor.relatableGreenColor
        } else {
            relateButton.backgroundColor = UIColor.appGreenColor
        }
        // If the content is already read by the user, then add a fade effect to show that it is already read.
        containerView.alpha = window?.isRead == true ? 0.75 : 1.0
        //
        if let imageUrl = window?.imageUrl, imageUrl != Constants.emptyString {
            windowImageView.kf.indicatorType = .activity
            
            if let url = URL(string: imageUrl) {
                let processor = BlurImageProcessor(blurRadius: 40.0)
                windowImageView.kf.setImage(with: url, placeholder: nil, options: [.processor(processor)], progressBlock: nil) { (result) in
                    switch result {
                    case .success(let value):
                        print("Image: \(value.image). Got from: \(value.cacheType)")
                    case .failure(let error):
                        print("Error: \(error)")
                    }
                }
            }
        } else {
            windowImageView.image = nil
        }
        self.windowPost = window
    }
}
