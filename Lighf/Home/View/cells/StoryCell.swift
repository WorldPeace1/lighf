//
//  StoryCell.swift
//  Lighf
//
//  Created by L.
//  Copyright © 2019 L. All rights reserved.
//

import UIKit
import Kingfisher

protocol StoryCellDelegate: class {
    func storyCell(_ cell: StoryCell, didClickRelateButtonFor post: Post, markAsRelate relate: Bool)
    func storyCell(_ cell: StoryCell, didClickBackpackButtonFor post: Post, addToBackPack: Bool)
    func storyCell(_ cell: StoryCell, didClickLeafButtonFor post: Post)
}

class StoryCell: UITableViewCell {
    //
    static var identifier: String {
        return "StoryCell"
    }
    let imageViewHeight: CGFloat = 230.0
    var story: Story?
    weak var delegate: StoryCellDelegate?
    var isBackPackedScreen: Bool = false
    //
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var statementLabel: UILabel!
    @IBOutlet weak var backpackButton: UIButton!
    @IBOutlet weak var relateButton: UIButton!
    @IBOutlet weak var storyImageView: UIImageView!
    @IBOutlet weak var imageViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var titleToSuperViewTrailingWidth: NSLayoutConstraint!
    @IBOutlet weak var leafbutton: UIButton!
    //
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    //
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    //
    public func prepareCell(with story: Story?, atIndex indexPath: IndexPath) {
        guard let story = story else {
            return
        }
        self.story = story
        statementLabel.text = story.shortStory.string
        backpackButton.isSelected = story.isBackPacked
        backpackButton.isEnabled = true
        relateButton.isSelected = story.isRelated
        // If the content is already read by the user, then add a fade effect to show that it is already read.
        containerView.alpha = story.isRead == true ? 0.75 : 1.0
        if story.imageUrl.isEmpty {
            imageViewHeightConstraint.constant = 0.0
            storyImageView.image = nil
            titleToSuperViewTrailingWidth.constant = 52.0
        } else {
            imageViewHeightConstraint.constant = imageViewHeight
            storyImageView.kf.indicatorType = .activity
            storyImageView.kf.setImage(with: URL(string: story.imageUrl))
            titleToSuperViewTrailingWidth.constant = 10.0
        }
        if story.isRelated {
            relateButton.backgroundColor = UIColor.relatableGreenColor
        } else {
            relateButton.backgroundColor = UIColor.appGreenColor
        }
        leafbutton.isHidden = story.isOwnPost
        relateButton.isHidden = story.isOwnPost
        backpackButton.isHidden = story.isOwnPost
    }
    //
    @IBAction func relateButtonClicked(_ sender: UIButton) {
        if let post = story {
            delegate?.storyCell(self, didClickRelateButtonFor: post, markAsRelate: !sender.isSelected)
//            sender.isSelected = !sender.isSelected
        }
    }
    //
    @IBAction func backpackButtonClicked(_ sender: UIButton) {
        if let post = story {
//            if isBackPackedScreen && sender.isSelected {
//                sender.isEnabled = false
//            } else {
//                sender.isEnabled = true
//            }
            delegate?.storyCell(self, didClickBackpackButtonFor: post, addToBackPack: !sender.isSelected)
//            sender.isSelected = !sender.isSelected
        }
    }
    //
    @IBAction func leafButtonClicked(_ sender: UIButton) {
        if let post = story {
            delegate?.storyCell(self, didClickLeafButtonFor: post)
        }
    }
}
