//
//  ActivityCell.swift
//  Lighf
//
//  Created by L.
//  Copyright © 2019 L. All rights reserved.
//

import UIKit

class ActivityCell: UITableViewCell {

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var detailLabel: UILabel!
    //
    static var identifier: String {
        return "activityCell"
    }
    //
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    //
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    //
    public func prepareCell(with title: String, detailText: String) {
        titleLabel.text = title
        detailLabel.text = detailText
    }
}
