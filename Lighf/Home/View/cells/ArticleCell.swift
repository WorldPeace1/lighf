//
//  ArticleCell.swift
//  Lighf
//
//  Created by L.
//  Copyright © 2019 L. All rights reserved.
//

import UIKit
import TTTAttributedLabel

protocol ArticleCellDelegate: class {
    func articleCell(_ cell: ArticleCell, didClickBackpackButtonFor post: Post, addToBackpack: Bool)
}

class ArticleCell: UITableViewCell {
    //
    weak var delegate: ArticleCellDelegate?
    static var identifier: String {
        return "ArticleCell"
    }
    let bodyLabelHeight: CGFloat = 100.0
    var readMoreRect: CGRect = CGRect.zero
    let margin: CGFloat = 10.0
    private var article: Post?
    var isBackPackedScreen: Bool = false

    //
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var bodyLabel: TTTAttributedLabel!
    @IBOutlet weak var contentImageView: UIImageView!
    @IBOutlet weak var backpackButton: UIButton!
    //
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    //
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    //
    @IBAction func backpakButtonPressed(_ sender: UIButton) {
        if let post = article {
//            if isBackPackedScreen && sender.isSelected {
//                sender.isEnabled = false
//            } else {
//                sender.isEnabled = true
//            }
            delegate?.articleCell(self, didClickBackpackButtonFor: post, addToBackpack: !sender.isSelected)
//            sender.isSelected = !sender.isSelected
        }
    }
    //
    public func prepareCell(with article: Article?, atIndex indexPath: IndexPath) {
        guard let article = article else {
            return
        }
        self.article = article
        //
        titleLabel.text = article.title.string
        bodyLabel.text = article.body.string
        backpackButton.isSelected = article.isBackPacked
        backpackButton.isEnabled = true
        backpackButton.isHidden = article.isOwnPost
        // If the content is already read by the user, then add a fade effect to show that it is already read.
        containerView.alpha = article.isRead == true ? 0.75 : 1.0
        if article.imageUrl != Constants.emptyString {
            contentImageView.kf.indicatorType = .activity
            contentImageView.kf.setImage(with: URL(string: article.imageUrl))
        } else {
            contentImageView.image = nil
        }
        //
        let possibleHeight = UILabel.height(for: article.body.string, width: bodyLabel.frame.width, font: UIFont(name: Font.sfProTextSemiBold.rawValue, size: 14.0))
        // Add read more at the end of label if article body content is very large
        if possibleHeight > bodyLabelHeight {
            let truncationStr = "... Read More"
            let truncation = NSMutableAttributedString(string: truncationStr)
            truncation.addAttribute(NSAttributedString.Key.font, value: UIFont(name: Font.sfProTextSemiBold.rawValue, size: 14.0)!, range: NSRange(location: 0, length: truncationStr.count))
            truncation.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor.appBlueColor, range: NSRange(location: 0, length: truncationStr.count))
            bodyLabel.attributedTruncationToken = truncation
        }
    }
}
