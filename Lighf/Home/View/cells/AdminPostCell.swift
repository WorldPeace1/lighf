//
//  StoryCell.swift
//  Lighf
//
//  Created by L.
//  Copyright © 2019 L. All rights reserved.
//

import UIKit
import Kingfisher

protocol AdminPostCellDelegate: class {
    func adminPostCell(_ cell: AdminPostCell, didClickBackpackButtonFor post: Post, addToBackPack: Bool)
}

class AdminPostCell: UITableViewCell {
    //
    static var identifier: String {
        return "AdminPostCell"
    }
    let imageViewHeight: CGFloat = 230.0
    var adminPost: AdminPost?
    weak var delegate: AdminPostCellDelegate?
    var isBackPackedScreen: Bool = false
    //
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var statementLabel: UILabel!
    @IBOutlet weak var backpackButton: UIButton!
    @IBOutlet weak var storyImageView: UIImageView!
    @IBOutlet weak var imageViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var titleToSuperViewTrailingWidth: NSLayoutConstraint!
    
    public func prepareCell(with adminPost: AdminPost?, atIndex indexPath: IndexPath) {
        guard let adminPost = adminPost else {
            return
        }
        self.adminPost = adminPost
        statementLabel.text = adminPost.shortStory.string
        
        backpackButton.isSelected = adminPost.isBackPacked
        backpackButton.isEnabled = true
        containerView.alpha = adminPost.isRead == true ? 0.75 : 1.0
        
        if adminPost.imageUrl.isEmpty {
            storyImageView.image = nil
        } else {
            storyImageView.kf.indicatorType = .activity
            storyImageView.kf.setImage(with: URL(string: adminPost.imageUrl))
        }
        
        if adminPost.imageUrl.isEmpty {
            imageViewHeightConstraint.constant = 0.0
            storyImageView.image = nil
            titleToSuperViewTrailingWidth.constant = 52.0
        } else {
            imageViewHeightConstraint.constant = imageViewHeight
            storyImageView.kf.indicatorType = .activity
            storyImageView.kf.setImage(with: URL(string: adminPost.imageUrl))
            titleToSuperViewTrailingWidth.constant = 10.0
        }
        
        backpackButton.isHidden = adminPost.isOwnPost
    }
    
    //
    @IBAction func backpackButtonClicked(_ sender: UIButton) {
        if let post = adminPost {
            delegate?.adminPostCell(self, didClickBackpackButtonFor: post, addToBackPack: !sender.isSelected)
        }
    }
}
