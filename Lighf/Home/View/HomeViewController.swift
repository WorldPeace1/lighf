//
//  HomeViewController.swift
//  Lighf
//
//  Created by L.
//  Copyright © 2019 L. All rights reserved.
//

import UIKit
import PopupDialog
import MatomoTracker
import MBProgressHUD

class HomeViewController: BaseViewController, UINavigationControllerDelegate, UIGestureRecognizerDelegate {

    var presenter: HomeViewToPresenterProtocol?
    let filterViewHeight: CGFloat = 50.0
    let myLighfViewHeight: CGFloat = 185.0
    var feeds: [Any] = []
    var isMyLighfSelected: Bool = false
    var refreshControl = UIRefreshControl()
    var isAllFeedReceived: Bool = false
    var apiParams = NSMutableDictionary.init()
    var refreshMyPosts: Bool = false
    var aspectsObjectsDict: [Int: String] =  [:]
    var editedPostID: Int = 0
    var isTableScrolled: Bool = false
    var selectedFilterAspectID: Int = -1
    //
    @IBOutlet weak var topViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var filterStackView: UIStackView!
    @IBOutlet weak var feedTableView: UITableView!
    @IBOutlet weak var headerImageView: UIImageView!
    // My Lighf view
    @IBOutlet weak var myLighfViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet var myLighfButtonCollection: [UIButton]!
    @IBOutlet weak var activityButton: UIButton!
    @IBOutlet weak var userPoints: UILabel!
    @IBOutlet weak var backPackButton: UIButton!
    @IBOutlet weak var createdPostsButton: UIButton!
    @IBOutlet weak var relatedPostsButton: UIButton!
    @IBOutlet weak var changeImage: UIButton!
    @IBOutlet weak var headerImageContainerView: UIView!
    @IBOutlet weak var imageRemoveButton: UIButton!
    @IBOutlet weak var emptyMessageLabel: UILabel!
    // FIlter views
    var primaryFilter: FilterView?
    var secondaryFilter: FilterView?
    var postTypeFilter: FilterView?
    var isFilterActive: Bool = false
    var isFilterVisible: Bool = false
    //
    var postOffset = 0
    let numberOfPostsPerAPI = 15
    var isBackpackedList: Bool = false
    var isCreatedList: Bool = false
    var isRelatesList: Bool = false
    var imagePicketSource: UIImagePickerController.SourceType?
    var notificationButton: UIButton?
    var imageViewController: ImageViewController?
    override func viewDidLoad() {
        super.viewDidLoad()
        configureView()
        showActivityIndicator(onFullScreen: true)
        presenter?.updateFeeds(from: postOffset, to: numberOfPostsPerAPI, withParams: apiParams)
        presenter?.getUserPoints()
        MatomoTracker.shared.track(view: ["Home feed"])
        refreshControl.addTarget(self, action: #selector(refreshFeeds), for: UIControl.Event.valueChanged)
        self.feedTableView.refreshControl = refreshControl
        self.feedTableView.addSubview(refreshControl) // not required when using UITableViewController
        NotificationCenter.default.addObserver(self, selector: #selector(updateMyPosts(notification:)), name: NSNotification.Name(rawValue: "updateMyPosts"), object: nil)
        imageRemoveButton.isHidden = true
        // get profile header image
        if let imageId = UserDefaults.standard.value(forKey: UserDefaultsKey.imageId) as? Int, imageId != 0 {
            headerImageView.image = nil
            MBProgressHUD.showAdded(to: headerImageContainerView, animated: true)
            presenter?.getProfileHeaderImage()
        }
        if headerImageView != nil {
            let longPress = UILongPressGestureRecognizer(target: self, action: #selector(longPressed(sender:)))
            longPress.minimumPressDuration = 0.5
            longPress.delaysTouchesBegan = true
            longPress.delegate = self
            headerImageView.isUserInteractionEnabled = true
            headerImageView.addGestureRecognizer(longPress)
        }
    }
    @objc func longPressed(sender: UILongPressGestureRecognizer) {
        if sender.state == UIGestureRecognizer.State.began {
            imageViewController = UIStoryboard.postDetails.instantiateViewController(withIdentifier: ImageViewController.storyboardId) as? ImageViewController
            imageViewController?.image = headerImageView.image
            if let imageview = imageViewController?.view {
                self.view.addSubview(imageview)
            }
        } else if sender.state == UIGestureRecognizer.State.ended {
            if let imageview = imageViewController?.view {
                imageview.removeFromSuperview()
            }
        }
    }
    //
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        emptyMessageLabel.isHidden = true
        modifyHeaderView()
        updateNotificationImage()
        refreshMyOwnPosts()
        self.navigationController?.navigationBar.isHidden = false
        if isFilterVisible {
            showAllFilters()
        } else {
            showPrimaryFilter()
        }
        updateUserLeafCount()
        UserDefaults.standard.set([], forKey: UserDefaultsKey.postHistoryArray) // to remove all swipe history.
    }
    //
    override func viewWillDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        isFilterVisible = false
    }
    //configuring view
    private func configureView() {
        if traitCollection.forceTouchCapability == .available {
            registerForPreviewing(with: self, sourceView: view)
        }
        
        self.feedTableView.rowHeight = UITableView.automaticDimension
        
        myLighfViewHeightConstraint.constant = 0.0
        configureFilterView()
    }
    //
    private func refreshMyOwnPosts() {
        let isMyPostsScreen = myLighfButtonCollection[2].isSelected && isMyLighfSelected
        let defaults  = UserDefaults.standard
        let needUpdate = defaults.object(forKey: UserDefaultsKey.updateFromSearch) as? String
        if refreshMyPosts && isMyPostsScreen {
            refreshMyPosts = false
            removeDataFromTable()
            presenter?.fetchMyPosts(from: postOffset, to: numberOfPostsPerAPI)
        } else if needUpdate == "true" {
            if !feeds.isEmpty {
                feedTableView.scrollToRow(at: IndexPath.init(row: 0, section: 0), at: .top, animated: true)
            }
            refreshFeeds()
            defaults.set("false", forKey: UserDefaultsKey.updateFromSearch)
        }
    }
    //
    @objc
    func updateMyPosts(notification: NSNotification) {
        editedPostID = notification.object as? Int ?? 0
        getPostDetails(withPostID: editedPostID)
        // Update My posts screen
        let isMyPostsScreen = myLighfButtonCollection[2].isSelected && isMyLighfSelected
        if isMyPostsScreen {
            refreshMyPosts = true
        }
    }
    @IBAction func createButtonClicked(sender: UIButton) {
        presenter?.createButtonClicked()
    }
    /// Create the UI for the filters in home screen
    private func configureFilterView() {
        primaryFilter = createFilterView(with: ["Feed", "My lighf"], images: ["Feed"], tag: 0, includeFilter: true)
        if let data = UserDefaults.standard.object(forKey: UserDefaultsKey.aspects) as? Data {
            do {
                if let array = try NSKeyedUnarchiver.unarchiveTopLevelObjectWithData(data) as? [Int: String] {
                    if array.isEmpty {
                        presenter?.getSavedAspects()
                    } else {
                        aspectsObjectsDict = array
                    }
                }
            } catch {
                presenter?.getSavedAspects()
            }
        } else {
            presenter?.getSavedAspects()
        }
        var aspectArray: [String] = []
        print(aspectsObjectsDict)
        for keyVal in aspectsObjectsDict.keys {
            aspectArray.append(aspectsObjectsDict[keyVal] ?? Constants.emptyString)
        }
        secondaryFilter = createFilterView(with: aspectArray, images: nil, tag: 1)
        postTypeFilter = createFilterView(with: ["lighfStory", "Window", "Article"], images: nil, tag: 3)
        //
        topViewHeightConstraint.constant = filterViewHeight
        filterStackView.addArrangedSubview(primaryFilter!)
    }
    /// Create filterView based on the parameters received
    ///
    /// - Parameters:
    ///   - options: the options the should be available in the filter
    ///   - images: images to be attached with the options
    ///   - tag: filterview tag
    ///   - includeFilter: The parameter which determines if the filter button should be shown
    /// - Returns: returns object of FilterView
    func createFilterView(with options: [String], images: [String]?, tag: Int, includeFilter: Bool = false) -> FilterView {
        let values = options.map {Utility.getAttributtedString(string: $0, fontName: Font.sfProDisplayLight.rawValue, size: 16.0)}
        let filterView = FilterView(frame: CGRect(x: 0, y: 0, width: view.frame.width, height: filterViewHeight), values: values, images: images, includeFilter: includeFilter)
        filterView.segmentControl.selectedSegmentIndex = 0
        filterView.view = self
        filterView.segmentControl.tag = tag
        return filterView
    }
    //
    private func prepareDummyData() {
        var article = Article(title: NSAttributedString(string: "Title of article"))
        article.body = NSAttributedString(string: "This is a sample article body. Above is the current function I use to determine the height but it is not working. I would greatly appreciate any help I can get. I would perfer the answer in Swift and not Objective C.")
        feeds.append(article)
        //
        let window = Window(title: NSAttributedString(string: "This is a short story"))
        feeds.append(window)
        //
        let story = Story(with: NSAttributedString(string: "This is a short story"))
        feeds.append(story)

        let adminPost = AdminPost(with: NSAttributedString(string: "This is a regular admin post"))
        feeds.append(adminPost)
    }
    // MARK: - Navigation controller modification
    private func modifyHeaderView() {
        // Configure background color
        navigationController?.resetBottomBarLine()
        navigationController?.setBackGroundColor(UIColor.appGreenColor)
        // configure title image
        let logoImage = UIImage(named: "lighf_logo")
        let logoButton = UIButton.init()
        logoButton.setImage(logoImage, for: .normal)
        logoButton.addTarget(self, action: #selector(logoTapped), for: .touchUpInside)
        self.navigationItem.titleView = logoButton
        //
        let rightSpace: UIBarButtonItem = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.fixedSpace, target: nil, action: nil)
        rightSpace.width = 25.0
        // Configure right bar buttons
        notificationButton = UIButton.button(with: "notification")
        notificationButton!.addTarget(self, action: #selector(notificationButtonClicked(sender:)), for: .touchUpInside)
        let notificationBarButtonItem = UIBarButtonItem(customView: notificationButton!)
        //
        let createStoryButton = UIButton.button(with: "Create")
        createStoryButton.addTarget(self, action: #selector(createStoryButtonClicked(sender:)), for: .touchUpInside)
        let createStoryBarButtonItem = UIBarButtonItem(customView: createStoryButton)
        navigationItem.rightBarButtonItems = [createStoryBarButtonItem, rightSpace, notificationBarButtonItem]
        //
        // Configure left bar button items
        let searchButton = UIButton.button(with: "Search")
        searchButton.addTarget(self, action: #selector(searchButtonClicked(sender:)), for: .touchUpInside)
        let searchBarButtonItem = UIBarButtonItem(customView: searchButton)
        //
        let settingsButton = UIButton.button(with: "Settings")
        settingsButton.addTarget(self, action: #selector(settingsButtonClicked(sender:)), for: .touchUpInside)
        settingsButton.alpha = 0.5
        let settingsBarButtonItem = UIBarButtonItem(customView: settingsButton)
        //
        let leftSpace: UIBarButtonItem = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.fixedSpace, target: nil, action: nil)
        leftSpace.width = 20.0
        navigationItem.leftBarButtonItems = [settingsBarButtonItem, leftSpace, searchBarButtonItem]
    }
    @objc func logoTapped() {
        feedTableView.scrollToTop(animated: true)
        showActivityIndicator(onFullScreen: true)
        self.perform(#selector(refreshFeeds), with: self, afterDelay: 1.0)
    }
}
//
extension HomeViewController: UIImagePickerControllerDelegate {
    //
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey: Any]) {
        //
        if imagePicketSource == UIImagePickerController.SourceType.photoLibrary {
            guard let image = info[.originalImage] as? UIImage, let imagePath = info[.imageURL] as? URL else {
                presenter?.imageFetchFailed(withError: ImagePickerError.unknown)
                return
            }
            headerImageView.image = image
            //
            MBProgressHUD.showAdded(to: headerImageContainerView, animated: true)
            //
            presenter?.setImageUrl(imagePath.absoluteString)
            dismiss(animated: true, completion: nil)
        } else {
            guard let image = info[.originalImage] as? UIImage else {
                presenter?.imageFetchFailed(withError: ImagePickerError.unknown)
                dismiss(animated: true, completion: nil)
                return
            }
            let imageWithFixedOrientation = image.fixOrientation()
            headerImageView.image = imageWithFixedOrientation
            MBProgressHUD.showAdded(to: headerImageContainerView, animated: true)
            presenter?.setImage(image: imageWithFixedOrientation)
            UIImageWriteToSavedPhotosAlbum(imageWithFixedOrientation, nil, nil, nil)
            dismiss(animated: true, completion: nil)
        }
        //
        guard let image = info[.originalImage] as? UIImage, let imagePath = info[.imageURL] as? URL else {
            presenter?.imageFetchFailed(withError: ImagePickerError.unknown)
            return
        }
        print("imagepath = \(imagePath)")
        headerImageView.image = image
        UIImageWriteToSavedPhotosAlbum(image, nil, nil, nil)
        dismiss(animated: true, completion: nil)
    }
    //
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }
}

extension HomeViewController: PostTypeSelectionProtocol {
    //
    func postTypeSelectionViewController(_ selectionVC: PostTypeSelectionViewController, didSelectType type: PostType) {
        presenter?.createPost(type: type)
    }
}

extension HomeViewController: UIViewControllerPreviewingDelegate {
    func previewingContext(_ previewingContext: UIViewControllerPreviewing, commit viewControllerToCommit: UIViewController) {
    }
    func previewingContext(_ previewingContext: UIViewControllerPreviewing, viewControllerForLocation location: CGPoint) -> UIViewController? {
        let relativeLocation = view.convert(location, to: headerImageView)
        if headerImageView.bounds.contains(relativeLocation), headerImageView.image != nil {
            if let imageViewController = UIStoryboard.postDetails.instantiateViewController(withIdentifier: ImageViewController.storyboardId) as? ImageViewController {
                imageViewController.image = headerImageView.image
                previewingContext.sourceRect = headerImageView.frame
                return imageViewController
            }
            return UIViewController()
        }
        return nil
    }
}
