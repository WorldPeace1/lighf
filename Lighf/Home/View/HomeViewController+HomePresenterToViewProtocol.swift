//
//  HomeViewController+HomePresenterToViewProtocol.swift
//  Lighf
//
//  Created by L.
//  Copyright © 2019 L. All rights reserved.
//

import Foundation
import UIKit
import MBProgressHUD
import PopupDialog
extension HomeViewController: HomePresenterToViewProtocol {
    func updateUserLeafCount() {
        if let points = UserDefaults.standard.object(forKey: UserDefaultsKey.leafCount) as? Int {
            self.userPoints.text = "\(String(describing: points))"
        }
    }
    func updateNotificationStatus(postID: Int?, commentID: Int?, notificationStatus: NotificationState) {
        if postID != nil, let posts = self.feeds as? [Post] {
            let postIndex = posts.firstIndex { (post) -> Bool in
                return post.postId == postID
            }
            if let indexToEdit = postIndex, var updatedPost = self.feeds[indexToEdit] as? Post {
                if notificationStatus == NotificationState.turnOn {
                    updatedPost.isNotificationEnabled = true
                } else {
                    updatedPost.isNotificationEnabled = false
                }
                self.feeds[indexToEdit] = updatedPost
                self.feedTableView.reloadRows(at: [IndexPath.init(row: indexToEdit, section: 0)], with: .none)
            }
        } else {
            print("Error!")
        }
    }
    func listOfSavedAspects(responseArray: [Aspect]) {
        dismissActivityIndicator()
        if !responseArray.isEmpty {
            let responseDict: NSMutableDictionary = [:]
            for loopVar in 0..<responseArray.count {
                let aspectDict = responseArray[loopVar]
                let aspectID = aspectDict.aspectID
                let aspectName = aspectDict.aspectName
                responseDict[aspectID] = aspectName
            }
            do {
                let defaults  = UserDefaults.standard
                let data = try NSKeyedArchiver.archivedData(withRootObject: responseDict, requiringSecureCoding: false)
                defaults.set(data, forKey: UserDefaultsKey.aspects)
                if let data = UserDefaults.standard.object(forKey: UserDefaultsKey.aspects) as? Data {
                    do {
                        if let array = try NSKeyedUnarchiver.unarchiveTopLevelObjectWithData(data) as? [Int: String] {
                            if array.isEmpty {
                                print("Couldn't write file")
                            } else {
                                aspectsObjectsDict = array
                                var aspectArray: [String] = []
                                for keyVal in aspectsObjectsDict.keys {
                                    aspectArray.append(aspectsObjectsDict[keyVal] ?? Constants.emptyString)
                                }
                                secondaryFilter = createFilterView(with: aspectArray, images: nil, tag: 1)
                            }
                        }
                    } catch {
                        print("Couldn't write file")
                    }
                }
            } catch {
                print("Couldn't write file")
            }
        }
    }
    //update entry.
    func updateRefreshedItems(_ feeds: [Any]) {
        if let posts = self.feeds as? [Post], !self.feeds.isEmpty, !feeds.isEmpty {
            let postIndex = posts.firstIndex { (post) -> Bool in
                return post.postId == editedPostID
            }
            if let indexToReplace: Int = postIndex, let postToReplace = feeds[0] as? Post {
                self.feeds[indexToReplace] = postToReplace
                UIView.performWithoutAnimation {
                    feedTableView.reloadRows(at: [IndexPath(row: indexToReplace, section: 0)], with: .none)
                }
            }
        }
        if isMyLighfSelected && isBackpackedList {
            apiParams[GetFeedOptions.feedTypeKey.rawValue] = [GetFeedOptions.feedBackpackedFilterKey.rawValue]
            apiParams[GetFeedOptions.feedFilterKey.rawValue] = []
        } else if isMyLighfSelected && isRelatesList {
            apiParams[GetFeedOptions.feedTypeKey.rawValue] = [GetFeedOptions.feedRelatedPostFilterKey.rawValue]
            apiParams[GetFeedOptions.feedFilterKey.rawValue] = []
        } else if isMyLighfSelected && isCreatedList {
            apiParams[GetFeedOptions.feedTypeKey.rawValue] = [GetFeedOptions.feedCreatedPostFilterKey.rawValue]
            apiParams[GetFeedOptions.feedFilterKey.rawValue] = []
        } else {
            apiParams[GetFeedOptions.feedTypeKey.rawValue] = []
            apiParams[GetFeedOptions.feedFilterKey.rawValue] = []
        }
    }
    //
    func popToFeed() {
        self.navigationController?.popViewController(animated: true)
    }
    func addPost(with postId: Int, toBackPack addToBackPack: Bool) {
        if let posts = feeds as? [Post] {
            var post = posts.filter {$0.postId == postId}.first
            post?.isBackPacked = addToBackPack
            let postIndex = posts.firstIndex { (post) -> Bool in
                return post.postId == postId
            }
            let isBackPackScreen = backPackButton.isSelected && isMyLighfSelected
            // Remove the post if the post is removed from backpack. This is only for backpack screen
            if let index = postIndex, let value = post {
                if isBackPackScreen {
                    // if the post is unbackpacked from the backpack screen, it is removed from the feeds
                    feeds.remove(at: index)
                    updatePostOffSet(incrementWith: -1)
                    if feeds.isEmpty {
                        feedTableView.reloadData()
                        showActivityIndicator(onFullScreen: true)
                        postOffset = 0
                        presenter?.updateFeeds(from: postOffset, to: numberOfPostsPerAPI, withParams: apiParams)
                    } else {
                        let indexPath = IndexPath(row: index, section: 0)
                        UIView.performWithoutAnimation {
                            feedTableView.deleteRows(at: [indexPath], with: .none)
                        }
                    }
                } else {
                    feeds[index] = value
                    UIView.performWithoutAnimation {
                        feedTableView.reloadRows(at: [IndexPath(row: index, section: 0)], with: .none)
                    }
                }
            }
        }
    }
    func updateFeedForRelate(with postId: Int, toRelate addToRelates: Bool) {
        if let posts = feeds as? [Post] {
            var post = posts.filter {$0.postId == postId}.first
            post?.isRelated = addToRelates
            let postIndex = posts.firstIndex { (post) -> Bool in
                return post.postId == postId
            }
            let isRelatedScreen = relatedPostsButton.isSelected && isMyLighfSelected
            // Remove the post if the post is removed from backpack. This is only for backpack screen
            if let index = postIndex, let value = post {
                if isRelatedScreen {
                    // if the post is unbackpacked from the backpack screen, it is removed from the feeds
                    feeds.remove(at: index)
                    updatePostOffSet(incrementWith: -1)
                    if feeds.isEmpty {
                        feedTableView.reloadData()
                        showActivityIndicator(onFullScreen: true)
                        postOffset = 0
                        presenter?.updateFeeds(from: postOffset, to: numberOfPostsPerAPI, withParams: apiParams)
                    } else {
                        let indexPath = IndexPath(row: index, section: 0)
                        UIView.performWithoutAnimation {
                            feedTableView.deleteRows(at: [indexPath], with: .none)
                        }
                    }
                } else {
                    feeds[index] = value
                    UIView.performWithoutAnimation {
                        feedTableView.reloadRows(at: [IndexPath(row: index, section: 0)], with: .none)
                    }
                }
            }
        }
    }
    //to update source array
    func updateBackpackedStatus(with postId: Int, toBackPack addToBackPack: Bool) {
        if isMyLighfSelected && isBackpackedList {
            addPost(with: postId, toBackPack: addToBackPack)
        } else {
            if var post = getPost(with: postId) {
                post.0.isBackPacked = addToBackPack
                feeds[post.1] = post.0
                UIView.performWithoutAnimation {
                    feedTableView.reloadRows(at: [IndexPath(row: post.1, section: 0)], with: .none)
                }
            }
        }
    }
    func updatePostOffSet(incrementWith: Int) {
        if isMyLighfSelected {
            postOffset += incrementWith
        }
    }
    //
    func updateRelateStatus(_ relate: Bool, forPostWith postId: Int) {
        if isMyLighfSelected && isRelatesList {
            updateFeedForRelate(with: postId, toRelate: relate)
        } else {
            if var post = getPost(with: postId) {
                post.0.isRelated = relate
                feeds[post.1] = post.0
                UIView.performWithoutAnimation {
                    feedTableView.reloadRows(at: [IndexPath(row: post.1, section: 0)], with: .none)
                }
            }
        }
    }
    func markPost(with postId: Int, asRead markAsRead: Bool) {
        if var post = getPost(with: postId) {
            post.0.isRead = markAsRead
            feeds[post.1] = post.0
            feedTableView.reloadRows(at: [IndexPath(row: post.1, section: 0)], with: .none)
        }
    }
    //
    func removePost(with postId: Int) {
        if let post = getPost(with: postId) {
            feeds.remove(at: post.1)
            updatePostOffSet(incrementWith: -1)
            if feeds.isEmpty == false {
                UIView.performWithoutAnimation {
                    feedTableView.deleteRows(at: [IndexPath(row: post.1, section: 0)], with: .none)
                }
            }
            if feeds.isEmpty {
                feedTableView.reloadData()
                showActivityIndicator(onFullScreen: true)
                postOffset = 0
                presenter?.updateFeeds(from: postOffset, to: numberOfPostsPerAPI, withParams: apiParams)
            }
        }
    }
    //
    func getPost(with postId: Int) -> (Post, Int)? {
        if let posts = feeds as? [Post] {
            let post = posts.filter {$0.postId == postId}.first
            let postIndex = posts.firstIndex { (post) -> Bool in
                return post.postId == postId
            }
            if let index = postIndex, let value = post {
                return (value, index)
            }
        }
        return nil
    }
    //
    func didSetHeaderImage(status: Bool) {
        MBProgressHUD.hide(for: headerImageContainerView, animated: true)
        //
        if status == false {
            headerImageView.image = UIImage(named: "default_header")
        } else {
            imageRemoveButton.isHidden = false
        }
    }
    //
    func didResetHeaderImage(status: Bool) {
        MBProgressHUD.hide(for: headerImageContainerView, animated: true)
        //
        if status {
            imageRemoveButton.isHidden = true
            headerImageView.image = UIImage(named: "default_header")
        }
    }
    //display points accumulated by user
    func showUserPoints(_ points: Int) {
        self.userPoints.text = "\(points)"
    }
    //display alert
    func showAlert(_ message: String) {
        //        removeActivityIndicatorCell()
        isAllFeedReceived = true
        refreshControl.endRefreshing()
        if self.viewIfLoaded?.window != nil {
            //
            if feeds.isEmpty {
                emptyMessageLabel.isHidden = false
                if message.lowercased().contains(Message.requestTimeOut) || message.lowercased().contains(Message.messageNotFound) {
                    emptyMessageLabel.text = Message.connectionLost
                } else {
                    emptyMessageLabel.text = message
                }
                feedTableView.reloadData()
            } else {
                let alert = UIAlertController.alert(with: message)
                self.present(alert, animated: true, completion: nil)
            }
        }
        self.view.isUserInteractionEnabled = true
    }
    //    func removeActivityIndicatorCell() {
    //        if !feeds.isEmpty && !isAllFeedReceived {
    //            let activityCell = feedTableView.cellForRow(at: IndexPath.init(row: feeds.count, section: 0))
    //            for view in activityCell?.subviews ?? [] where view is UIActivityIndicatorView {
    //                let activityIndicator = view as? UIActivityIndicatorView
    //                activityIndicator?.stopAnimating()
    //                UIView.performWithoutAnimation {
    //                    feedTableView.reloadRows(at: [IndexPath.init(row: feeds.count - 1, section: 0)], with: .none)
    //                }
    //            }
    //        }
    //    }
    //
    func showFeeds(_ feeds: [Any]) {
        // Show feeds
        let prevOffset = self.feeds.count
        postOffset += feeds.count
        if (postOffset != 0 && feeds.isEmpty) || feeds.count % numberOfPostsPerAPI != 0 {
            isAllFeedReceived = true
        } else {
            isAllFeedReceived = false
        }
        // Remove duplicates from the feeds
        if let posts = feeds as? [Post], var currentPosts = self.feeds as? [Post] {
            for post in posts {
                let isDuplicate = currentPosts.contains {$0.postId == post.postId}
                if isDuplicate == false {
                    currentPosts.append(post)
                }
            }
            self.feeds = currentPosts
        } else {
            // Handle activities here
        }
        if refreshControl.isRefreshing {
            refreshControl.endRefreshing()
        }
        dismissActivityIndicator()
        feedTableView.reloadData()
        if prevOffset != 0 {
            feedTableView.scrollToRow(at: IndexPath.init(row: prevOffset - 1, section: 0), at: .bottom, animated: false)
        }
        isTableScrolled = false
        self.view.isUserInteractionEnabled = true
        updateNotificationImage()
    }
    func updateNotificationImage() {
        let notificationCount = UserDefaults.standard.object(forKey: UserDefaultsKey.newNotificationsAvailable) as? Int
        if notificationCount ?? 0 > 0 {
            notificationButton?.setImage(UIImage.init(named: "notification_enabled"), for: .normal)
        } else {
            notificationButton?.setImage(UIImage.init(named: "notification"), for: .normal)
        }
    }
    //
    @objc func refreshFeeds() {
        emptyMessageLabel.isHidden = true
        feeds.removeAll()
        feedTableView.reloadData()
        postOffset = 0
        isAllFeedReceived = false
        self.view.isUserInteractionEnabled = false
        presenter?.updateFeeds(from: postOffset, to: numberOfPostsPerAPI, withParams: apiParams)
        presenter?.getUserPoints()
    }
    //
    func showCreateOptionsPopup() {
        guard let optionsViewController = UIStoryboard.main.instantiateViewController(withIdentifier: PostTypeSelectionViewController.storyBoardId) as? PostTypeSelectionViewController else {
            return
        }
        optionsViewController.delegate = self
        let popUp = PopupDialog(viewController: optionsViewController,
                                buttonAlignment: .vertical,
                                transitionStyle: .zoomIn,
                                tapGestureDismissal: true,
                                panGestureDismissal: false,
                                hideStatusBar: false)
        optionsViewController.parentPopUp = popUp
        present(popUp, animated: false, completion: nil)
    }
    //
    func showGallery(for sourceType: UIImagePickerController.SourceType, mediaTypes: [String]) {
        let imagePicker = UIImagePickerController()
        imagePicker.delegate = self
        imagePicker.allowsEditing = false
        imagePicketSource = sourceType
        //
        if UIImagePickerController.isSourceTypeAvailable(sourceType) {
            imagePicker.sourceType = sourceType
            imagePicker.mediaTypes = mediaTypes
            self.present(imagePicker, animated: true, completion: nil)
        } else {
            presenter?.imageFetchFailed(withError: ImagePickerError.sourceTypeNotAvailable)
        }
    }
    func setProfileHeaderImage(url: String?) {
        if let imageUrl = url {
            headerImageView.kf.setImage(with: URL(string: imageUrl), placeholder: nil, options: nil, progressBlock: nil) { (_) in
                MBProgressHUD.hide(for: self.headerImageContainerView, animated: true)
                self.imageRemoveButton.isHidden = false
            }
        } else {
            MBProgressHUD.hide(for: headerImageContainerView, animated: true)
            headerImageView.image = UIImage(named: "default_header")
        }
    }
}
