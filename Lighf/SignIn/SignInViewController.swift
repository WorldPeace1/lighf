//
//  SignInViewController.swift
//  Lighf
//
//  Created by L.
//  Copyright © 2019 L. All rights reserved.
//

import UIKit
import SkyFloatingLabelTextField
import MatomoTracker

class SignInViewController: BaseViewController {
    var presenter: SignInViewToPresenterProtocol?
    
    @IBOutlet weak var usernameTextField: SkyFloatingLabelTextField!
    @IBOutlet weak var passwordTextField: SkyFloatingLabelTextField!
    @IBOutlet weak var signInButton: UIButton!
    @IBOutlet weak var versionLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        usernameTextField.modifyTitleFormatter()
        passwordTextField.modifyTitleFormatter()
        versionLabel.text = Utility.appVersion()
        
        self.keyboardDismissalRegistrer.registerControls([self.usernameTextField, self.passwordTextField])
        self.passwordTextField.supportShowHide()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        resetTextFields()
        updateStateOfLoginButton()
        MatomoTracker.shared.track(view: ["SignIn Screen"])
    }
    
    // Reset username and password textfields
    private func resetTextFields() {
        let defaults = UserDefaults.standard
        let userName = defaults.object(forKey: UserDefaultsKey.userName) as? String
        usernameTextField.text = userName ?? Constants.emptyString
        passwordTextField.text = Constants.emptyString
    }
    //update the state of the login button.
    @objc func updateStateOfLoginButton() {
        if usernameTextField.isEmpty() || passwordTextField.isEmpty() {
            signInButton.disable()
        } else if usernameTextField.text?.removeWhiteSpaceAndNewLine().count ?? 0 >= 1 && passwordTextField.text?.removeWhiteSpaceAndNewLine().count ?? 0 >= 1 {
            signInButton.enable()
        } else {
            signInButton.disable()
        }
    }
    //
    @IBAction func signInButtonClicked(sender: UIButton) {
        guard let username = usernameTextField.text, let password = passwordTextField.text else {
            return
        }
        dismissKeyBoard()
        invokeSignIn(with: username, password: password)
    }
    //
    @IBAction func forgotPasswordButtonClicked(sender: UIButton) {
        let matomoEvent = Event.init(tracker: MatomoTracker.shared, action: ["SignIn screen", "Clicked on forgot password button"])
        MatomoTracker.shared.track(matomoEvent)
        dismissKeyBoard()
        presenter?.forgotPasswordButtonClicked()
    }

    @IBAction func signUpButtonClicked(sender: UIButton) {
        let matomoEvent = Event.init(tracker: MatomoTracker.shared, action: ["SignIn screen", "Clicked on create new user button"])
        MatomoTracker.shared.track(matomoEvent)
        dismissKeyBoard()
        presenter?.createNewUserButtonClicked()
    }
    //
    @IBAction func textDidChange(_ sender: UITextField) {
        updateStateOfLoginButton()
    }
    
    func invokeSignIn(with username: String, password: String) {
        let matomoEvent = Event.init(tracker: MatomoTracker.shared, action: ["SignIn screen", "invoked SignIn API"])
        MatomoTracker.shared.track(matomoEvent)
        
        dismissKeyBoard()
        
        guard
            let deviceId = UserDefaults.standard.value(forKey: UserDefaultsKey.deviceId) as? String,
            !deviceId.isEmpty
        else {
            Utility().showAlert(Constants.kErrorKey, message: Message.deviceIdNotFound, delegate: self)
            return
        }
        
        if Reachability.isConnectedToNetwork() {
            presenter?.signIn(with: username, password: password.removeWhiteSpace(), deviceId: "deviceId")
        } else {
            Utility().showAlert(Constants.kErrorKey, message: Message.networkUnavailable, delegate: self)
        }
    }
}

extension SignInViewController: SignInPresenterToViewProtocol {
    func showLoginErrorAlert(_ message: String) {
        let alert = UIAlertController.alert(with: message)
        self.present(alert, animated: true, completion: nil)
    }
}

extension SignInViewController: UITextFieldDelegate {
    //
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == passwordTextField && signInButton.isEnabled {
            invokeSignIn(with: usernameTextField.text ?? Constants.emptyString, password: passwordTextField.text ?? Constants.emptyString)
            textField.resignFirstResponder()
        } else if textField == usernameTextField {
            passwordTextField.becomeFirstResponder()
        } else {
            textField.resignFirstResponder()
            moveScreenDown()
        }
        return true
    }
    //
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        guard let text = textField.text as NSString? else {
            return true
        }
        let newText = text.replacingCharacters(in: range, with: string)
        // disallow user to enter characters beyond a particular range
        if textField == usernameTextField {
            if newText.count > Constants.maxCharacterCount {
                return false
            }
        }
        return true
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        moveScreenUp()
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        moveScreenDown()
    }
}
