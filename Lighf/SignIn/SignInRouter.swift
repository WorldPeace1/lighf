//
//  SignInRouter.swift
//  Lighf
//
//  Created by L.
//  Copyright © 2019 L. All rights reserved.
//

import UIKit

class SignInRouter: NSObject {

    weak var presentedViewController: UIViewController?
    static let viewIdentifier = "SignIn"
    //
    static func createModule() -> SignInViewController? {
        let view = UIStoryboard(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: viewIdentifier) as? SignInViewController
        //
        let presenter = SignInPresenter()
        let interactor = SignInInteractor()
        let router = SignInRouter()
        //
        view?.presenter = presenter
        //
        presenter.view = view
        presenter.interactor = interactor
        presenter.router = router
        //
        interactor.presenter = presenter
        //
        router.presentedViewController = view
        //
        return view
    }
    func navigateToForgotPasswordScreen() {
    }
}

extension SignInRouter: SignInPresenterToRouterProtocol {
    func moveToHomeScreen() {
        if let homePageViewController = HomeRouter.createModule() {
            presentedViewController?.navigationController?.pushViewController(homePageViewController, animated: true)
            presentedViewController?.navigationController?.viewControllers = [homePageViewController]
        }
    }
    //
    func navigateToForgotPassword() {
        if let forgotPasswordViewController = ForgotPasswordRouter.createModule() {
            presentedViewController?.navigationController?.pushViewController(forgotPasswordViewController, animated: true)
        }
    }
    func navigateToCreateNewUser() {
        if let createNewUserViewController = SignUpRouter.createModule() {
            presentedViewController?.navigationController?.pushViewController(createNewUserViewController, animated: true)
        }
    }
// navigate to intro screens and then to select aspect screen.
    func navigateToSelectAspects() {
        if let introViewController = IntroRouter.createModule(withScreenOption: IntroScreenOption.initialPlay, toPage: 0) {
            presentedViewController?.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
            presentedViewController?.navigationController?.navigationBar.isHidden = true
            presentedViewController?.navigationController?.pushViewController(introViewController, animated: true)
        }
    }
    func navigateToPostDetails() {
        if let postDetailsViewController = PostDetailsRouter.createModule() {
            presentedViewController?.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
            presentedViewController?.navigationController?.navigationBar.shadowImage = UIImage()
            presentedViewController?.navigationController?.navigationBar.isTranslucent = true
            presentedViewController?.navigationController?.view.backgroundColor = .clear
            presentedViewController?.navigationItem.setHidesBackButton(false, animated: true)
            postDetailsViewController.postType = PostType.story
            presentedViewController?.navigationController?.pushViewController(postDetailsViewController, animated: true)
        }
    }
}
