//
//  SignInPresenter.swift
//  Lighf
//
//  Created by L.
//  Copyright © 2019 L. All rights reserved.
//

import UIKit

class SignInPresenter: NSObject {

    var interactor: SignInPresenterToInteractorProtocol?
    var router: SignInPresenterToRouterProtocol?
    weak var view: SignInPresenterToViewProtocol?
}

extension SignInPresenter: SignInViewToPresenterProtocol {
    //navigation to forgot password screen.
    func forgotPasswordButtonClicked() {
        router?.navigateToForgotPassword()
    }
    //navigation to user sign up
    func createNewUserButtonClicked() {
     router?.navigateToCreateNewUser()
    }
    //invoked interactor to trigger API
    func signIn(with username: String, password: String, deviceId: String) {
        view?.showActivityIndicator()
        interactor?.signIn(with: username, password: password, deviceId: deviceId)
    }
}

extension SignInPresenter: SignInInteractorToPresenterProtocol {
    //delegate method to handle API response
    func signInStatus(_ success: Bool, aspectSelected: Bool, error: String) {
        view?.dismissActivityIndicator()
        if success && aspectSelected {
            router?.moveToHomeScreen()
        } else if success && !aspectSelected {
            router?.navigateToSelectAspects()
        } else {
            view?.showLoginErrorAlert(error)
        }
    }
}
