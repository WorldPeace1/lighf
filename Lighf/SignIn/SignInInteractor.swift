//
//  SignInInteractor.swift
//  Lighf
//
//  Created by L.
//  Copyright © 2019 L. All rights reserved.
//

import UIKit
import SwiftyJSON

class SignInInteractor: NSObject {
    weak var presenter: SignInInteractorToPresenterProtocol?

}

extension SignInInteractor: SignInPresenterToInteractorProtocol {
    func signIn(with username: String, password: String, deviceId: String) {
        let params = [
            "username": username,
            "password": password,
            "device_id": deviceId
        ]
        
        APIManager.post(endPoint: EndPoint.signIn, parameters: params) {(response, error) in
            guard error == nil, let response = response else {
                self.presenter?.signInStatus(false, aspectSelected: false, error: error?.message ?? Constants.emptyString)
                return
            }
            
            let responseMessage = response["api_response"]["message"].stringValue
            let status = response[APIResponse.apiResponse][APIResponse.apiResponseStatus].stringValue
            if status != Constants.success {
                self.presenter?.signInStatus(false, aspectSelected: false, error: responseMessage)
            } else {
                let defaults  = UserDefaults.standard
                
                guard let userName = response["api_response"]["username"].string else {
                    return
                }
                
                print(userName as Any)
                defaults.set(userName, forKey: UserDefaultsKey.userName)
                
                guard let accessToken = response["api_response"]["token"].string else {
                    return
                }
                
                defaults.set(accessToken, forKey: UserDefaultsKey.accessToken)
                
                guard let userID = response["api_response"]["id"].int else {
                    return
                }
                
                defaults.set(userID, forKey: UserDefaultsKey.userID)

                if let imageId = response[APIResponse.apiResponse]["image_id"].int {
                    defaults.set(imageId, forKey: UserDefaultsKey.imageId)
                }
                
                self.saveEditOptions(response: response)
                self.processAspects(response: response)
            }
        }
    }
    func saveEditOptions(response: JSON) {
        let editOptions = response["api_response"]["can_write"].arrayValue
        print(editOptions)
        var editOptionsValues: [String] = []
        for loopVar in 0..<editOptions.count {
            if let value = editOptions[loopVar].rawValue as? String {
                editOptionsValues.append(value)
            }
        }
        UserDefaults.standard.set(editOptionsValues, forKey: UserDefaultsKey.createOptions)
    }
    func processAspects(response: JSON) {
        var aspectsAdded = false
        guard let aspectsArray = response["api_response"]["aspects"].array else {
            return
        }
        if !aspectsArray.isEmpty {
            aspectsAdded = true
            let responseDict: NSMutableDictionary = [:]
            for loopVar in 0..<aspectsArray.count {
                let aspectDict = aspectsArray[loopVar]
                let aspectID = aspectDict["id"].intValue
                let aspectName = aspectDict["name"].stringValue
                responseDict[aspectID] = aspectName
            }
            do {
                let data = try NSKeyedArchiver.archivedData(withRootObject: responseDict, requiringSecureCoding: false)
                UserDefaults.standard.set(data, forKey: UserDefaultsKey.aspects)
            } catch {
                print("Couldn't write file")
            }
        }
        self.presenter?.signInStatus(true, aspectSelected: aspectsAdded, error: Constants.emptyString)
    }
}
