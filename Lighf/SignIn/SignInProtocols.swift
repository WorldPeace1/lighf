//
//  SignInProtocols.swift
//  Lighf
//
//  Created by L.
//  Copyright © 2019 L. All rights reserved.
//

import UIKit

protocol SignInViewToPresenterProtocol: class {
    func signIn(with username: String, password: String, deviceId: String)
    func forgotPasswordButtonClicked()
    func createNewUserButtonClicked()
}

protocol SignInPresenterToInteractorProtocol: class {
    func signIn(with username: String, password: String, deviceId: String)
}

protocol SignInInteractorToPresenterProtocol: class {
    func signInStatus(_ success: Bool, aspectSelected: Bool, error: String)
}

protocol SignInPresenterToViewProtocol: BasePresenterToViewProtocol {
    func showLoginErrorAlert(_ message: String)
}

protocol SignInPresenterToRouterProtocol: class {
    func moveToHomeScreen()
    func navigateToForgotPassword()
    func navigateToCreateNewUser()
    func navigateToSelectAspects()
    func navigateToPostDetails()
}
