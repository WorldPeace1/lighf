//
//  AppDelegate+EmailConfirmation.swift
//  Lighf
//
//  Created by L.
//  Copyright © 2020 L. All rights reserved.
//

import UIKit
import CommonCrypto

extension AppDelegate {
    func handleConfirmationLink(action: String, userId: String, key: String, hash: String) {
        guard
            let localHash = self.hashUserIdAndKey(userId: userId, key: key),
            localHash == hash
        else {
            self.showWindowAlert(message: Message.activationFailure)
            return
        }
        
        performSeamlessSignout { (status, message) in
            if status == Constants.success {
                guard let signInVC = SignInRouter.createModule() else {
                    fatalError("Could not get signInRouter")
                }
                
                let navigationController = UINavigationController(rootViewController: signInVC)
                navigationController.setTransparent()
                navigationController.titleColor(UIColor.white, font: Font.robotoBold, size: 22.0)
                self.window?.rootViewController = navigationController
                
                self.invokeAccountValidation(action: action, userId: userId, activationKey: key, hash: hash) { (status, message) in
                    
                    if status == Constants.success {
                        let successMessage = message == "Validation was successful" ? Message.activationSuccess : message
                        self.showWindowAlert(message: successMessage)
                    } else {
                        self.showWindowAlert(message: Message.activationFailure)
                    }
                }
            } else {
                self.showWindowAlert(message: message)
            }
        }
    }
    
    func performSeamlessSignout(completion: @escaping (String, String) -> Void) {
        let defaults = UserDefaults.standard
        let accessToken = defaults.object(forKey: UserDefaultsKey.accessToken) as? String
        
        if (accessToken != nil) && (accessToken != Constants.emptyString) {
            let userId = defaults.value(forKey: UserDefaultsKey.userID) as Any
            
            APIManager.post(endPoint: EndPoint.signOut, parameters: [APIParams.userID: userId]) { (json, error) in
                guard error == nil, let response = json else {
                    completion(Constants.failure, Message.activationFailure)
                    return
                }

                defaults.set(Constants.emptyString, forKey: UserDefaultsKey.accessToken)
                UserDefaults.standard.set(-1, forKey: UserDefaultsKey.introPageNum)
                
                let message = response[APIResponse.apiResponse][APIResponse.apiResponseMessage].stringValue
                let responseStatus = response[APIResponse.apiResponse][APIResponse.apiResponseStatus].stringValue
                
                completion(responseStatus, message)
                return
            }
        }

        defaults.set(Constants.emptyString, forKey: UserDefaultsKey.accessToken)
        UserDefaults.standard.set(-1, forKey: UserDefaultsKey.introPageNum)
        
        completion(Constants.success, Constants.success)
    }
    
    func invokeAccountValidation(action: String, userId: String, activationKey: String, hash: String, completion: @escaping (String, String) -> Void) {
        guard let userIdAsInteger = Int(userId) else {
            completion(Constants.failure, Message.activationFailure)
            return
        }
        
        let parameters: [String: Any] = [
            APIParams.userID: userIdAsInteger,
            APIParams.key: activationKey,
            APIParams.confirmationAtion: "confirm_signup"
        ]
        
        APIManager.post(endPoint: EndPoint.validateUserKey, parameters: parameters) { (json, error) in
            guard error == nil, let response = json else {
                completion(Constants.failure, Message.activationFailure)
                return
            }
            let message = response[APIResponse.apiResponse][APIResponse.apiResponseMessage].stringValue
            let responseStatus = response[APIResponse.apiResponse][APIResponse.apiResponseStatus].stringValue
            let keyValidated = response[APIResponse.apiResponse][APIResponse.keyValidated].stringValue
                
            print(#function, responseStatus, message, keyValidated)
            completion(responseStatus, message)
        }
    }
    
    private func hashUserIdAndKey(userId: String, key: String) -> String? {
        guard let data = "\(userId)_\(key)".data(using: .utf8) else { fatalError("Could not convert combo to data") }
        
        var digest = [UInt8](repeating: 0, count: Int(CC_SHA256_DIGEST_LENGTH))
        _ = data.withUnsafeBytes { CC_SHA256($0.baseAddress, UInt32(data.count), &digest) }
        
        var comboAsSha256 = ""
        for byte in digest {
            comboAsSha256 += String(format: "%02x", UInt8(byte))
        }
        
        return comboAsSha256
    }
}
