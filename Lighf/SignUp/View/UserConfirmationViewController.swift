//
//  UserConfirmationViewController.swift
//  Lighf
//
//  Created by L.
//  Copyright © 2019 L. All rights reserved.
//

import UIKit

class UserConfirmationViewController: UserViewController, SignUpPresenterToUserConfirmationViewProtocol {
    func showSignUpAlertMessage(_ message: String) {
    }
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    //
    @IBAction func doneButtonClicked(sender: UIButton) {
        presenter?.doneButtonClicked(with: "")
    }
}
