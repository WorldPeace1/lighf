//
//  UserCredentialsViewController.swift
//  Lighf
//
//  Created by L.
//  Copyright © 2019 L. All rights reserved.
//

import UIKit
import SkyFloatingLabelTextField
import EmailValidator
import MatomoTracker
import PopupDialog

class UserCredentialsViewController: UserViewController, SignUpPresenterToViewProtocol {

    @IBOutlet weak var emailTextField: SkyFloatingLabelTextField!
    @IBOutlet weak var passwordTextField: SkyFloatingLabelTextField!
    @IBOutlet weak var signupButton: UIButton!
    @IBOutlet weak var emailOptionSlider: UISlider!
    @IBOutlet weak var infoMessageLabel: UILabel!
    
    var currentMailOption: EmailOptions = .none
    let passwordValidator = PasswordValidator()
    
    var isEmailLess = false
    let sliderMinValue: Float = 0.0
    let sliderMiddleValue: Float = 0.5
    let sliderMaxValue: Float = 1.0
    let lowerThreshold: Float = 0.25
    let upperThreshold: Float = 0.75
    let screenShiftValue: Float = 50.0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.keyboardDismissalRegistrer.registerControls([self.emailTextField, self.passwordTextField])
        
        // Do any additional setup after loading the view.
        self.navigationItem.title = "Create Account"
        modifyTextFieldTitleProperties()
        updateStateOfSignUpButton()
        emailTextField.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
        passwordTextField.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
        
        MatomoTracker.shared.track(view: ["SignUpScreen"])
        emailOptionSlider.isContinuous = false
        
        self.passwordTextField.supportShowHide()
    }
    //
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        dismissKeyBoard()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        dismissKeyBoard()
    }
    //
    //next button click event
    @IBAction func nextButtonClicked(sender: UIButton) {
        dismissKeyBoard()
        var isEmailVaild = EmailValidator.validate(email: emailTextField.text as String? ?? Constants.emptyString)
        if isEmailLess {
            isEmailVaild = true
        }
        let isPasswordValid = passwordTextField.text?.isValidPassword() ?? false
        if isEmailVaild && isPasswordValid {
            invokeUserRegistration(with: emailTextField.text ?? Constants.emptyString, password: passwordTextField.text ?? Constants.emptyString)
        } else if !isEmailVaild {
            displayAlert(message: SignUpMessage.invalidEmail)
        } else if !isPasswordValid {
            displayAlert(message: SignUpMessage.invalidPassword)
        }
    }
    
    @IBAction func passwordInfoButtonClicked(_ sender: UIButton) {
        self.showInfoModal(type: .password)
    }
    
    @IBAction func valueChanged(_ sender: UISlider) {
        let sliderValue = sender.value
        var emailOption: EmailOptions = .none
        // Position the slider properly on the 3 available options
        switch sliderValue {
        case sliderMinValue..<lowerThreshold:
            sender.setValue(sliderMinValue, animated: true)
            emailOption = .none
        case lowerThreshold...sliderMiddleValue:
            sender.setValue(sliderMiddleValue, animated: true)
            emailOption = .hashed
        case sliderMiddleValue..<upperThreshold:
            sender.setValue(sliderMiddleValue, animated: true)
            emailOption = .hashed
        case upperThreshold...sliderMaxValue:
            sender.setValue(sliderMaxValue, animated: true)
            emailOption = .emailLess
        default:
            break
        }
        handleEmailSelection(emailOption)
    }
    // modify UI based on user email selection
    private func handleEmailSelection(_ option: EmailOptions) {
        currentMailOption = option
        switch option {
        case .none:
            isEmailLess = false
            emailTextField.placeholder = "Email"
            emailTextField.isEnabled = true
        case .hashed:
            isEmailLess = false
            emailTextField.placeholder = "Email"
            emailTextField.isEnabled = true
        case .emailLess:
            isEmailLess = true
            emailTextField.placeholder = "No Email Required"
            emailTextField.isEnabled = false
            emailTextField.text = Constants.emptyString
            self.view.endEditing(true)
            moveScreenDown()
        }
        updateStateOfSignUpButton()
        self.updateInfoMessageFor(emailOption: option)
    }
    //
    func moveToSignUp() {
        if signupButton.isEnabled {
            nextButtonClicked(sender: UIButton.init())
        }
        moveScreenDown()
    }
    //to display alert
    func displayAlert(message: String) {
        Utility().showAlert(Constants.emptyString, message: message, delegate: self)
    }
    //to navigate user to login screen
    @IBAction func loginButtonClicked(sender: UIButton) {
        dismissKeyBoard()
        presenter?.loginButtonClicked()
    }
    
    @IBAction func infoButtonClicked(_ sender: UIButton) {
        self.showInfoModal(type: .email)
    }
    
    //email less switch action
//    @IBAction func emailLessSwitchStateChanged(_ sender: UISwitch) {
//        if sender.isOn {
//            isEmailLess = true
//            emailTextField.placeholder = "No Email Required"
//            emailTextField.isEnabled = false
//            emailTextField.text = Constants.emptyString
//            self.view.endEditing(true)
//            moveScreenDown()
//        } else {
//            isEmailLess = false
//            emailTextField.placeholder = "Email"
//            emailTextField.isEnabled = true
//        }
//        updateStateOfSignUpButton()
//    }
    //
    /// A method to modify SkyFloatingLabelTextField's title property
    func modifyTextFieldTitleProperties() {
        emailTextField.modifyTitleFormatter()
        passwordTextField.modifyTitleFormatter()
    }
    // initiate registration flow
    func invokeUserRegistration(with email: String, password: String) {
        let matomoEvent = Event.init(tracker: MatomoTracker.shared, action: ["Create new user screen", "Clicked on create new user button"])
        MatomoTracker.shared.track(matomoEvent)

        if Reachability.isConnectedToNetwork() {
            presenter?.nextButtonClicked(with: email, password: password.removeWhiteSpace(), emailOption: currentMailOption)
        } else {
            Utility().showAlert(Constants.kErrorKey, message: Message.networkUnavailable, delegate: self)
        }
    }
    func showSignUpAlertMessage(_ message: String) {
        Utility().showAlert(Constants.emptyString, message: message, delegate: self)
    }
    @objc func updateStateOfSignUpButton() {
        guard
            let email = emailTextField.text,
            let password = passwordTextField.text else
        {
                signupButton.disable()
                return
        }
        
        let passwordIsEmpty = password.removeWhiteSpaceAndNewLine().isEmpty

        guard !passwordIsEmpty else {
            signupButton.disable()
            return
        }
        
        let passwordIsValid = self.passwordValidator.validate(password)
        
        guard passwordIsValid else {
            signupButton.disable()
            return
        }
        
        let notEmailLessAndEmailIsInvalid = email.isEmpty && !isEmailLess

        guard !notEmailLessAndEmailIsInvalid else {
            signupButton.disable()
            return
        }
        
        signupButton.enable()
    }
    
    private func updateInfoMessageFor(emailOption: EmailOptions) {
        switch emailOption {
        case .none:
            self.infoMessageLabel.text = "Store email address with basic encryption. Ability to get all Alert types (Push, Email and In-App)."
        case .hashed:
            self.infoMessageLabel.text = "Email address only used for registration then double encrypted until password recovery. No Email Alerts."
        case .emailLess:
            self.infoMessageLabel.text = "Most secure. No email address tied to lighf. Lose ability to get Email Alerts or recover password."
        }
    }
}

extension UserCredentialsViewController: UITextFieldDelegate {
    //
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        updateStateOfSignUpButton()
        if textField == passwordTextField {
            if emailTextField.isEnabled {
                emailTextField.becomeFirstResponder()
            } else {
                moveToSignUp()
                textField.resignFirstResponder()
            }
        } else {
            moveToSignUp()
            textField.resignFirstResponder()
        }
        return true
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
       moveScreenUp()
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        moveScreenDown()
    }
    
    @objc func textFieldDidChange(_ textField: UITextField) {
        updateStateOfSignUpButton()
    }
}

// MARK: - EmailOptionInfoViewControllerDelegate

extension UserCredentialsViewController: EmailOptionInfoViewControllerDelegate {
    func requestedDismissal(viewController: EmailOptionInfoViewController) {
        self.hideBlurredBackground()
        
        for childViewController in self.children where childViewController is EmailOptionInfoViewController {
            childViewController.view.removeFromSuperview()
            childViewController.removeFromParent()
        }
    }
}

// MARK: - EmailOptionInfoViewController

extension UserCredentialsViewController {
    func showInfoModal(type: InfoContentMode) {
        self.showBlurredBackground(style: .dark)
        
        let emailInfoViewController = EmailOptionInfoViewController.instantiate()
        emailInfoViewController.delegate = self
        self.addChild(emailInfoViewController)
        emailInfoViewController.view.addCenteredInParentView(self.view, size: CGSize(width: self.view.bounds.width - 80.0, height: type == .email ? 420.0 : 280.0))
        self.view.bringSubviewToFront(emailInfoViewController.view)
        emailInfoViewController.infoContentMode = type
    }
}
