//
//  UserCreationViewController.swift
//  Lighf
//
//  Created by L.
//  Copyright © 2019 L. All rights reserved.
//

import UIKit
import SkyFloatingLabelTextField
import MatomoTracker

// MARK: - UserCreationViewController

class UserCreationViewController: UserViewController {
    @IBOutlet weak var usernameTextField: SkyFloatingLabelTextField!
    @IBOutlet weak var inviteCodeTextField: SkyFloatingLabelTextField!
    @IBOutlet weak var nextButton: UIButton!
}

// MARK: - View Lifecycle

extension UserCreationViewController {
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.keyboardDismissalRegistrer.registerControls([self.usernameTextField, self.inviteCodeTextField])
        
        self.navigationItem.title = "Create Account"
        
        self.usernameTextField.modifyTitleFormatter()
        self.inviteCodeTextField.modifyTitleFormatter()
        self.nextButton.disable()
        
        MatomoTracker.shared.track(view: ["Check username availablity screen"])
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        dismissKeyBoard()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        dismissKeyBoard()
    }
}

// MARK: - Actions

extension UserCreationViewController {
    @IBAction func nextButtonClicked(sender: UIButton) {
        dismissKeyBoard()
        
        guard let name = self.usernameTextField.text, !name.isEmpty else { return }
        guard let inviteCode = self.inviteCodeTextField.text, !inviteCode.isEmpty else { return }
        
        self.invokeCreateUserAPI(userName: name, inviteCode: inviteCode)
    }
    
    @IBAction func loginButtonClicked(sender: UIButton) {
        dismissKeyBoard()
        presenter?.loginButtonClicked()
    }
}

// MARK: - SignUpPresenterToUserCreationViewProtocol

extension UserCreationViewController: SignUpPresenterToUserCreationViewProtocol {
    func showSignUpAlertMessage(_ message: String) {
    }
    func showUsernameUnavailable() {
     // Modify label text or show Toast
    }
    //
    func showAlertMessage(_ message: String) {
        let alert = UIAlertController.alert(with: message)
        present(alert, animated: true, completion: nil)
    }
}

// MARK: - UITextFieldDelegate

extension UserCreationViewController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if nextButton.isEnabled {
            self.invokeCreateUserAPI(userName: self.usernameTextField.text!, inviteCode: self.inviteCodeTextField.text!)
        }
        
        moveScreenDown()
        textField.resignFirstResponder()
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        guard let text = textField.text as NSString? else {
            return true
        }
        
        let theOtherTextField = textField == self.usernameTextField ? self.inviteCodeTextField : self.usernameTextField
        let theOtherText = theOtherTextField?.text ?? ""
        let newText = text.replacingCharacters(in: range, with: string).removeWhiteSpace()
        
        if textField == self.usernameTextField && newText.count > Constants.maxCharacterCount {
            if theOtherText.isEmpty {
                self.nextButton.disable()
            }
            return false
        } else if textField == self.usernameTextField && newText.count < Constants.minCharacterCount {
            self.nextButton.disable()
        } else if textField == self.inviteCodeTextField && newText.isEmpty {
            self.nextButton.disable()
        } else {
            if theOtherText.isEmpty {
                self.nextButton.disable()
            } else {
                self.nextButton.enable()
            }
        }
        return true
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
       moveScreenUp()
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        moveScreenDown()
    }
}

// MARK: - Internals

extension UserCreationViewController {
    func invokeCreateUserAPI(userName: String, inviteCode: String) {
        let matomoEvent = Event.init(tracker: MatomoTracker.shared, action: ["Check username screen", "Clicked on check new username button"])
        MatomoTracker.shared.track(matomoEvent)
        if Reachability.isConnectedToNetwork() {
            presenter?.createUsername(username: userName.removeWhiteSpace(), inviteCode: inviteCode.removeWhiteSpace())
        } else {
            Utility().showAlert(Constants.kErrorKey, message: Message.networkUnavailable, delegate: self)
        }
    }
}
