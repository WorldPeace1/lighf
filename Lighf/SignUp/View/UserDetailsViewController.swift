//
//  UserDetailsViewController.swift
//  Lighf
//
//  Created by L.
//  Copyright © 2019 L. All rights reserved.
//

import UIKit

class UserDetailsViewController: UserViewController, SignUpPresenterToViewProtocol {
    func showSignUpAlertMessage(_ message: String) {
    }
    @IBOutlet weak var firstNameTextField: UITextField!
    @IBOutlet weak var lastNameTextField: UITextField!
    @IBOutlet weak var phoneNumberTextField: UITextField!
    //
    override func viewDidLoad() {
        super.viewDidLoad()

        self.keyboardDismissalRegistrer.registerControls([self.firstNameTextField, self.lastNameTextField, self.phoneNumberTextField])
    }
    //
    @IBAction func nextButtonClicked(sender: UIButton) {
//        presenter?.nextButtonClicked(with: "firstName", lastName: "lstName", phoneNumber: "phonenumber")
    }
    @IBAction func loginButtonClicked(sender: UIButton) {
        presenter?.loginButtonClicked()
    }
}
