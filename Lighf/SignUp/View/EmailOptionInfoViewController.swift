//
//  EmailOptionInfoViewController.swift
//  Lighf
//
//  Created by L.
//  Copyright © 2019 L. All rights reserved.
//

import UIKit

// MARK: EmailOptionInfoViewControllerDelegate

protocol EmailOptionInfoViewControllerDelegate: NSObjectProtocol {
    func requestedDismissal(viewController: EmailOptionInfoViewController)
}

// MARK: EmailOptionInfoViewController

class EmailOptionInfoViewController: UIViewController, Storyboardable {
    
    @IBOutlet var closeButton: UIButton!
    @IBOutlet var titleLabel: UILabel!
    @IBOutlet var stackView: UIStackView!
    @IBOutlet var contentView: UIView!
    
    // MARK: Properties
    
    weak var delegate: EmailOptionInfoViewControllerDelegate?
    
    var infoContentMode: InfoContentMode = .email {
        didSet {
            self.updateContent()
        }
    }
    var emailOptions: EmailOptions?
}

// MARK: - Content Presenter

extension EmailOptionInfoViewController {
    private func updateContent() {
        if self.infoContentMode == .email {
            self.addTitle("None")
            self.addDescription("If you chose not to add additional layers of email privacy you will get all alerts and notifications from L by default, I.e. emailed, push, and in-app. You can turn off notifications for (posts, tags, and general notifications) on the settings page.")
            self.addTitle("Want to Encrypt your email?")
            self.addDescription("When you choose to encrypt (or hash) your email address, it is used to complete validation of your account then immediately encrypted and stored on an independent server until a password reset or email address change is requested by you.  No email notifications with this option, like with the Email-less option. You can stay up to date with what's going on in lighf by checking your in-app and push notifications.")
            self.addTitle("Want to go Email-less?")
            self.addDescription("When you go Email-less, there is no email address associated with your account so you will not receive email notifications or be able to perform a password reset or email address change.\n\nYou can still, however get notifications that do not require an email address including in-app and push notifications.")
        } else if infoContentMode == .password {
            self.titleLabel.text = "Valid Password"
            self.addDescription("The password should have at least 7 characters, one digit, one upper case letter, one lower case letter and a special character.")
        }
    }
}

// MARK: Actions

extension EmailOptionInfoViewController {
    @IBAction func closeButtonAction(sender: UIButton) {
        if let delegate = self.delegate {
            delegate.requestedDismissal(viewController: self)
        }
    }
}

// MARK: - API

extension EmailOptionInfoViewController {
    func addTitle(_ title: String) {
        let label = UILabel(frame: CGRect.zero)
        label.text = title
        label.numberOfLines = 0
        label.lineBreakMode = .byWordWrapping
        label.backgroundColor = .clear
        label.font = UIFont(name: Font.sfProDisplayMedium.rawValue, size: 22.0)
        label.textColor = .white
        label.textAlignment = .center
        
        self.stackView.addArrangedSubview(label)
    }
    
    func addDescription(_ description: String) {
        let label = UILabel(frame: CGRect.zero)
        label.text = description
        label.numberOfLines = 0
        label.lineBreakMode = .byWordWrapping
        label.backgroundColor = .clear
        label.font = UIFont(name: Font.sfProTextRegular.rawValue, size: 13.0)
        label.textColor = .white
        label.textAlignment = .center
        
        self.stackView.addArrangedSubview(label)
    }
}
