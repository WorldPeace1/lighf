//
//  SignUpRouter.swift
//  Lighf
//
//  Created by L.
//  Copyright © 2019 L. All rights reserved.
//

import UIKit

class SignUpRouter: NSObject {

    weak var presentedViewController: UserViewController?
    static let viewIdentifier = "UserCreationVC"
    //
    static func createModule() -> UserCreationViewController? {
        let view = UIStoryboard.main.instantiateViewController(withIdentifier: viewIdentifier) as? UserCreationViewController
        //
        let presenter = SignUpPresenter()
        let interactor = SignUpInteractor()
        let router = SignUpRouter()
        //
        view?.presenter = presenter
        //
        presenter.view = view
        presenter.interactor = interactor
        presenter.router = router
        //
        interactor.presenter = presenter
        //
        router.presentedViewController = view
        //
        return view
    }
    //
    func updatePresentedView() {
        guard presentedViewController == nil else {
            return
        }
        if let navigationController = UIApplication.shared.keyWindow?.rootViewController as? UINavigationController {
            presentedViewController = navigationController.visibleViewController as? UserViewController
        }
    }
    //
    func updateView() {
        updatePresentedView()
        let presenter = presentedViewController?.presenter as? SignUpPresenter
        guard presenter?.view == nil else {
            return
        }
        presenter?.view = presentedViewController as? SignUpPresenterToViewProtocol
    }
    //
    func createUserCredentialsView() -> UserCredentialsViewController? {
        let viewIdentifier = "UserCredentialsVC"
        let view = UIStoryboard.main.instantiateViewController(withIdentifier: viewIdentifier) as? UserCredentialsViewController
        updatePresentedView()
        //
        let presenter = presentedViewController?.presenter as? SignUpPresenter
        view?.presenter = presenter
        presenter?.view = view
        //
        return view
    }
    //
    func createUserDetailsView() -> UserDetailsViewController? {
        let viewIdentifier = "UserDetailsVC"
        let view = UIStoryboard.main.instantiateViewController(withIdentifier: viewIdentifier) as? UserDetailsViewController
        updatePresentedView()
        //
        let presenter = presentedViewController?.presenter as? SignUpPresenter
        view?.presenter = presenter
        presenter?.view = view
        //
        return view
    }
    //
    func createUserConfirmationView() -> UserConfirmationViewController? {
        let viewIdentifier = "UserConfirmationVC"
        let view = UIStoryboard.main.instantiateViewController(withIdentifier: viewIdentifier) as? UserConfirmationViewController
        updatePresentedView()
        //
        let presenter = presentedViewController?.presenter as? SignUpPresenter
        view?.presenter = presenter
        presenter?.view = view
        //
        return view
    }
}

extension SignUpRouter: SignUpPresenterToRouterProtocol {
    //
    func moveToUserCredentialsView() {
        if let userCredentialView = createUserCredentialsView() {
            presentedViewController?.navigationController?.pushViewController(userCredentialView, animated: true)
            presentedViewController = userCredentialView
        }
    }
    //
    func moveToUserDetailsView() {
        if let userDetailsView = createUserDetailsView() {
            presentedViewController?.navigationController?.pushViewController(userDetailsView, animated: true)
            presentedViewController = userDetailsView
        }
    }
    //
    func moveToUserConfirmationView() {
        if let userConfirmationView = createUserConfirmationView() {
            presentedViewController?.navigationController?.pushViewController(userConfirmationView, animated: true)
            presentedViewController = userConfirmationView
        }
    }
    //
    func moveToSignInView() {
        updatePresentedView()
        presentedViewController?.navigationController?.popToRootViewController(animated: true)
    }
}
