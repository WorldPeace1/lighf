//
//  SignUpProtocols.swift
//  Lighf
//
//  Created by L.
//  Copyright © 2019 L. All rights reserved.
//

import UIKit

protocol SignUpViewToPresenterProtocol: class {
    func createUsername(username: String, inviteCode: String)
    func nextButtonClicked(with email: String, password: String, emailOption: EmailOptions)
    func doneButtonClicked(with code: String)
    func loginButtonClicked()
}

protocol SignUpPresenterToInteractorProtocol: class {
    func checkAvailability(for username: String, inviteCode: String)
    func registerUser(with email: String, password: String, emailOption: EmailOptions)
}

protocol SignUpInteractorToPresenterProtocol: class {
    func username(_ name: String, isAvailable: Bool, errorMessage: String)
    func userRegistration(success: Bool, errorMessage: String)
}

protocol SignUpPresenterToRouterProtocol: class {
    func moveToUserCredentialsView()
    func moveToUserDetailsView()
    func moveToSignInView()
    func updateView()
}

protocol SignUpPresenterToViewProtocol: BasePresenterToViewProtocol {
    func showSignUpAlertMessage(_ message: String)
}

protocol SignUpPresenterToUserCreationViewProtocol: SignUpPresenterToViewProtocol {
    func showUsernameUnavailable()
    func showAlertMessage(_ message: String)
}

protocol SignUpPresenterToUserConfirmationViewProtocol: SignUpPresenterToViewProtocol {
}
protocol SignUpPresenterToUserCredentialsViewProtocol: class {
}
