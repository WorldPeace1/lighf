//
//  SignUpPresenter.swift
//  Lighf
//
//  Created by L.
//  Copyright © 2019 L. All rights reserved.
//

import UIKit

class SignUpPresenter: NSObject {

    var interactor: SignUpPresenterToInteractorProtocol?
    var router: SignUpPresenterToRouterProtocol?
    weak var view: SignUpPresenterToViewProtocol?
}

extension SignUpPresenter: SignUpInteractorToPresenterProtocol {
    func userRegistration(success: Bool, errorMessage: String) {
        if success {
            router?.moveToSignInView()
            view?.showSignUpAlertMessage(Message.accountCreated)
        } else {
            view?.showSignUpAlertMessage(errorMessage)
        }
    }
    //
    func username(_ name: String, isAvailable: Bool, errorMessage: String) {
        view?.dismissActivityIndicator()
        //
        if isAvailable {
            router?.moveToUserCredentialsView()
        } else {
            (view as? SignUpPresenterToUserCreationViewProtocol)?.showAlertMessage(errorMessage != "" ? errorMessage : Message.usernameUnavailable)
        }
    }
}

extension SignUpPresenter: SignUpViewToPresenterProtocol {
    //invoke check username API
    func createUsername(username: String, inviteCode: String) {
        router?.updateView()
        if username.isValidUsername() {
            view?.showActivityIndicator()
            interactor?.checkAvailability(for: username, inviteCode: inviteCode)
        } else {
            (view as? SignUpPresenterToUserCreationViewProtocol)?.showAlertMessage(Message.invalidUsername)
        }
    }
    //invoke user registration API
    func nextButtonClicked(with email: String, password: String, emailOption: EmailOptions) {
        router?.updateView()
        interactor?.registerUser(with: email, password: password, emailOption: emailOption)
    }
    //
    func doneButtonClicked(with code: String) {
    }
    //navigates user to login screen
    func loginButtonClicked() {
        router?.moveToSignInView()
    }
    //
    func updateView() {
    }
}
