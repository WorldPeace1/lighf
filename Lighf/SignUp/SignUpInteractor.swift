//
//  SignUpInteractor.swift
//  Lighf
//
//  Created by L.
//  Copyright © 2019 L. All rights reserved.
//

import UIKit

class SignUpInteractor: NSObject {

    weak var presenter: SignUpInteractorToPresenterProtocol?
    fileprivate var user: User?
}

extension SignUpInteractor: SignUpPresenterToInteractorProtocol {
    //
    func checkAvailability(for username: String, inviteCode: String) {
        // API call
        APIManager.post(endPoint: EndPoint.username, parameters: ["username": username]) { (response, error) in
            //
            guard let response = response, error == nil else {
                self.presenter?.username(username, isAvailable: false, errorMessage: error!.message)
                return
            }
            let isAvailable = response["api_response"]["is_available"].boolValue
            let message = response["api_response"]["message"].stringValue
            self.user = isAvailable ? User(name: username, inviteCode: inviteCode) : nil
            self.presenter?.username(username, isAvailable: isAvailable, errorMessage: message)
        }
    }
    //Invokes API to create a user. First and last names are given as empty strings as there is no option to capture those details currently.
    func registerUser(with email: String, password: String, emailOption: EmailOptions) {
        self.user?.email = email
        self.user?.password = password
        
        var params = [
            "username": self.user!.userName,
            "email": email,
            "password": password,
            "first_name": Constants.emptyString,
            "last_name": Constants.emptyString,
            "invite_code": self.user!.inviteCode
        ]
        
        if emailOption != .emailLess {
            params["email_choice"] = emailOption.keyValue
        }
        
        print(params)
        
        APIManager.post(endPoint: EndPoint.signUpBeta, parameters: params as [String: Any]) {(response, error) in
            if error == nil, let response = response {
                let responseMessage = response["api_response"]["message"].string
                let status = response[APIResponse.apiResponse][APIResponse.apiResponseStatus].stringValue
                if status == Constants.success {
                    let defaults  = UserDefaults.standard
                    defaults.set(self.user!.userName, forKey: UserDefaultsKey.userName)
                    self.presenter?.userRegistration(success: true, errorMessage: Constants.emptyString)
                } else {
                    self.presenter?.userRegistration(success: false, errorMessage: responseMessage ?? Constants.emptyString)
                }
            } else {
                self.presenter?.userRegistration(success: false, errorMessage: error?.message ?? Constants.emptyString)
            }
        }
    }
}
