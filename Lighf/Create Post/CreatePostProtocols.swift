//
//  CreatePostProtocols.swift
//  Lighf
//
//  Created by L.
//  Copyright © 2019 L. All rights reserved.
//

import UIKit

protocol CreatePostViewToPresenterProtocol: class {
    func initializeView(position: ViewHierarchy)
    func nextButtonClicked(from viewController: PostCreationViewController)
    func addStatementButtonClicked()
    func doneButtonClicked(with statements: [RelatableStatement])
    func updateView(_ view: CreatePostPresenterToViewProtocol)
    func cancelButtonClicked()
    // Image upload
    func uploadButtonClicked(sourceType: UIImagePickerController.SourceType)
    func promptUserToAuthorizeCameraInSettings()
    func setImageUrl(_ url: String)
    func setImage(image: UIImage?)
    func imageFetchFailed(withError error: Error)
    func contentEditButtonClicked(from viewController: PostCreationViewController)
    //
    func setTextInput(_ text: NSAttributedString, from view: PostCreationViewController)
    func setTags(_ tags: [String])
    func setMediaLinks(_ media: Media)
    func setPhotoCredit(_ photoCredit: String)
    var isUpdated: Bool {get set}
    //
    func getTitleTextInput() -> NSAttributedString?
    func getBodyTextInput() -> NSAttributedString?
    func setTitle(_ title: NSAttributedString, body: NSAttributedString)
}

protocol CreatePostPresenterToInteractorProtocol: class {
    func createStory(with shortStory: NSAttributedString)
    func addLongStory(_ longStory: NSAttributedString)
    func createArticle(with title: NSAttributedString)
    func addArticleBody(_ body: NSAttributedString)
    func setStatements(_ statements: [RelatableStatement])
    func setTags(_ tags: [String])
    func setMedia(_ media: Media)
    func setImageUrl(_ url: String)
    func setImage(image: UIImage?)
    func setImageCredits(_ credits: String)
    // Get methods
    func getPost() -> Post?
    func getStatements() -> [RelatableStatement]
    func getTags() -> [String]
    func getMedia() -> Media
    func getImageUrl() -> String
    func getImage() -> UIImage?
    func getImageCredits() -> String
    func getInitialTextInput() -> NSAttributedString
    func getFinalTextInput() -> NSAttributedString
    func clearData()
}

protocol CreatePostInteractorToPresenterProtocol: class {
}

protocol CreatePostPresenterToViewProtocol: class {
    func showAlert(_ message: String)
}

protocol CreatePostPresenterToContentCreationViewProtocol: CreatePostPresenterToViewProtocol {
    var viewTitle: String { get set }
    var maximumCharacterCount: Int { get set }
    var placeHolder: String { get set }
    var postType: PostType { get set}
    var initialTextInput: NSAttributedString { get set }
    var finalTextInput: NSAttributedString { get set }
}

protocol CreatePostPresenterToMoreOptionsViewProtocol: CreatePostPresenterToViewProtocol {
    var showStatement: Bool { get set}
    func updateStatements(_ statements: [RelatableStatement])
    func updateTags(_ tags: [String])
    func updateMedia(_ media: Media)
    func updateImage(_ url: String)
    func updateImageWithImage(image: UIImage?)
    func updateImageCredits(_ credits: String)
    func showGallery(for sourceType: UIImagePickerController.SourceType, mediaTypes: [String])
}

protocol CreatePostPresenterToAddStatementViewProtocol: CreatePostPresenterToViewProtocol {
    var addedStatements: [RelatableStatement] { get set }
    var noOfStatementsAllowed: Int { get set }
    var initialTextInput: NSAttributedString { get set }
    var finalTextInput: NSAttributedString { get set }
}

protocol CreatePostPresenterToRouterProtocol: class {
    func moveToFinalContentCreationView()
    func moveToMoreOptionsView()
    func moveToAddStatementView()
    func popView()
    func updateView()
    func moveToPreviewScreen(with postType: PostType, post: Post)
    func popToContentCreationFinalView()
    var isUpdated: Bool {get set}
}
