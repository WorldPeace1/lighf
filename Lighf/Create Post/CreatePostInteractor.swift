//
//  CreatePostInteractor.swift
//  Lighf
//
//
//  Copyright © 2020 L. All rights reserved.
//

import UIKit

class CreatePostInteractor: NSObject {
    //
    weak var presenter: CreatePostInteractorToPresenterProtocol?
    var postType: PostType = .story
    var post: Post?
}

extension CreatePostInteractor: CreatePostPresenterToInteractorProtocol {
    // MARK: - Set methods
    func createStory(with shortStory: NSAttributedString) {
        postType = .story
        if post == nil {
            post = Story(with: shortStory)
        } else {
            var story = post as? Story
            story?.shortStory = shortStory
            post = story
        }
    }
    //
    func addLongStory(_ longStory: NSAttributedString) {
        var story = post as? Story
        story?.longStory = longStory
        post = story
    }
    //
    func createArticle(with title: NSAttributedString) {
        postType = .article
        if post == nil {
            post = Article(title: title)
        } else {
            var article = post as? Article
            article?.title = title
            post = article
        }
    }
    //
    func addArticleBody(_ body: NSAttributedString) {
        var article = post as? Article
        article?.body = body
        post = article
    }
    //
    func setStatements(_ statements: [RelatableStatement]) {
        var article = post as? Article
        article?.statements = statements
        post = article
    }
    //
    func setTags(_ tags: [String]) {
        post?.tags = tags
    }
    //
    func setMedia(_ media: Media) {
        post?.media = media
    }
    //
    func setImageUrl(_ url: String) {
        post?.imageUrl = url
    }
    func setImage(image: UIImage?) {
        // set image here.
        post?.postImage = image
    }
    //
    func setImageCredits(_ credits: String) {
        post?.photoCredit = credits
    }
    // MARK: - Get methods
    func getPost() -> Post? {
        return post
    }
    //
    func getStatements() -> [RelatableStatement] {
        return (post as? Article)?.statements ?? []
    }
    //
    func getTags() -> [String] {
        return post?.tags ?? []
    }
    //
    func getMedia() -> Media {
        return post?.media ?? Media()
    }
    //
    func getImageUrl() -> String {
        return post?.imageUrl ?? Constants.emptyString
    }
    func getImage() -> UIImage? {
        return post?.postImage
    }
    //
    func getImageCredits() -> String {
        return post?.photoCredit ?? Constants.emptyString
    }
    //
    func getInitialTextInput() -> NSAttributedString {
        if postType == .story {
            return (post as? Story)?.shortStory ?? NSAttributedString(string: Constants.emptyString)
        } else {
             return (post as? Article)?.title ?? NSAttributedString(string: Constants.emptyString)
        }
    }
    //
    func getFinalTextInput() -> NSAttributedString {
        if postType == .story {
            return (post as? Story)?.longStory ?? NSAttributedString(string: Constants.emptyString)
        } else {
            return (post as? Article)?.body ?? NSAttributedString(string: Constants.emptyString)
        }
    }
    //
    func clearData() {
        post = nil
    }
}
