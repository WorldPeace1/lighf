//
//  CreatePostPresenter.swift
//  Lighf
//
//  Created by L.
//  Copyright © 2019 L. All rights reserved.
//

import UIKit
import MobileCoreServices

enum ViewHierarchy {
    case initial
    case final
}

enum ImagePickerError: Error {
    case sourceTypeNotAvailable
    case unknown
}

class CreatePostPresenter: NSObject {
    //
    var interactor: CreatePostPresenterToInteractorProtocol?
    var router: CreatePostPresenterToRouterProtocol?
    weak var view: CreatePostPresenterToViewProtocol?
    //
    var postType: PostType = .story
    //
    func clearData() {
        interactor?.clearData()
    }
    var isUpdated: Bool = false
}

extension CreatePostPresenter: CreatePostViewToPresenterProtocol {
    func setTitle(_ title: NSAttributedString, body: NSAttributedString) {
        postType == .story ? interactor?.createStory(with: title) : interactor?.createArticle(with: title)
        postType == .story ? interactor?.addLongStory(body) : interactor?.addArticleBody(body)
    }
    //
    func getTitleTextInput() -> NSAttributedString? {
        return interactor?.getInitialTextInput()
    }
    func getBodyTextInput() -> NSAttributedString? {
        return interactor?.getFinalTextInput()
    }
    //
    func imageFetchFailed(withError error: Error) {
        print(error.message)
    }
    //
    func updateView(_ view: CreatePostPresenterToViewProtocol) {
        guard let moreOptionsView = view as? CreatePostPresenterToMoreOptionsViewProtocol, let interactor = interactor else {
            return
        }
        moreOptionsView.updateStatements(interactor.getStatements())
        moreOptionsView.updateTags(interactor.getTags())
        moreOptionsView.updateMedia(interactor.getMedia())
        if interactor.getImageUrl() == Constants.emptyString {
            moreOptionsView.updateImageWithImage(image: interactor.getImage())
        } else {
            moreOptionsView.updateImage(interactor.getImageUrl())
        }
        moreOptionsView.updateImageCredits(interactor.getImageCredits())
    }
    // Initialize view contents
    func initializeView(position: ViewHierarchy) {
        if let view = view as? CreatePostPresenterToContentCreationViewProtocol {
            initializeCreateContentView(view, position: position)
        } else if let view = view as? CreatePostPresenterToMoreOptionsViewProtocol {
            initializeMoreOptionsView(view, position: position)
        } else if let view = view as? CreatePostPresenterToAddStatementViewProtocol, let interactor = interactor {
            view.addedStatements = interactor.getStatements()
            view.noOfStatementsAllowed = Article.maxStatements - interactor.getStatements().count
            view.initialTextInput = interactor.getInitialTextInput()
            view.finalTextInput = interactor.getFinalTextInput()
        } else {
            return
        }
    }
    //Update initial and final content creation vc's title and maximum character count values
    private func initializeCreateContentView(_ view: CreatePostPresenterToContentCreationViewProtocol, position: ViewHierarchy) {
        guard let interactor = interactor else {
            return
        }
        view.postType = postType
        switch (postType, position) {
        case (.story, .initial):
            view.maximumCharacterCount = Story.shortStoryMaxCharCount
            view.viewTitle = Title.shortStory
            view.placeHolder = PlaceHolder.shortStory
            view.initialTextInput = interactor.getInitialTextInput()
        case (.story, .final):
            view.maximumCharacterCount = Story.longStoryMaxCharCount
            view.viewTitle = Title.longStory
            view.placeHolder = PlaceHolder.longStory
            view.initialTextInput = interactor.getInitialTextInput()
            view.finalTextInput = interactor.getFinalTextInput()
        case (.article, .initial):
            view.maximumCharacterCount = Article.titleMaxCharCount
            view.viewTitle = Title.articleHeader
            view.placeHolder = PlaceHolder.articleHeader
            view.initialTextInput = interactor.getInitialTextInput()
        case (.article, .final):
            view.maximumCharacterCount = Article.bodyMaxCharCount
            view.viewTitle = Title.articleBody
            view.placeHolder = PlaceHolder.articleBody
            view.initialTextInput = interactor.getInitialTextInput()
            view.finalTextInput = interactor.getFinalTextInput()
        default: break
        }
    }
    //
    private func initializeMoreOptionsView(_ view: CreatePostPresenterToMoreOptionsViewProtocol, position: ViewHierarchy) {
        if position == .initial || position == .final {
            view.showStatement = postType == .article
        }
    }
    // MARK: Handle Button clicks
    // Handle right bar button item click from various controllers
    func nextButtonClicked(from viewController: PostCreationViewController) {
        switch viewController {
        case is ContentCreationInitialViewController:
            router?.moveToFinalContentCreationView()
        case is ContentCreationFinalViewController:
            router?.moveToMoreOptionsView()
        case is SingleContentCreationViewController:
            router?.moveToMoreOptionsView()
        case is MoreOptionsViewController:
            if let post = interactor?.getPost() {
                router?.isUpdated = isUpdated
                router?.moveToPreviewScreen(with: postType, post: post)
            }
        default: break
        }
    }
    //
    func doneButtonClicked(with statements: [RelatableStatement]) {
        router?.popView()
        interactor?.setStatements(statements)
    }
    //
    func cancelButtonClicked() {
        router?.popView()
    }
    //
    func uploadButtonClicked(sourceType: UIImagePickerController.SourceType) {
        router?.updateView()
        let media = kUTTypeImage as String
        (view as? CreatePostPresenterToMoreOptionsViewProtocol)?.showGallery(for: sourceType, mediaTypes: [media])
    }
    
    func promptUserToAuthorizeCameraInSettings() {
        Utility().presentTwoActionAlerController(titleString: "Permission requested", messageString: "for lighf to use your camera.", rightButtonTitle: "Go to Settings", leftButtonTitle: "Use Photo Library") { index in
            if index == 1 {
                if #available(iOS 10.0, *) {
                    UIApplication.shared.openURL(URL(string: UIApplication.openSettingsURLString)!)
                } else {
                    UIApplication.shared.open(URL(string: UIApplication.openSettingsURLString)!)
                }
            } else if index == 2 {
                self.uploadButtonClicked(sourceType: .photoLibrary)
            }
        }
    }
    
    func addStatementButtonClicked() {
        router?.moveToAddStatementView()
    }
    //
    func contentEditButtonClicked(from viewController: PostCreationViewController) {
        switch viewController {
        case is ContentCreationFinalViewController:
            router?.popView()
        case is AddStatementViewController:
            router?.popToContentCreationFinalView()
        default:
            break
        }
    }
    // MARK: - Set Parameters
    //
    func setTextInput(_ text: NSAttributedString, from view: PostCreationViewController) {
        switch view {
        case is ContentCreationInitialViewController:
            postType == .story ? interactor?.createStory(with: text) : interactor?.createArticle(with: text)
        case is ContentCreationFinalViewController:
            postType == .story ? interactor?.addLongStory(text) : interactor?.addArticleBody(text)
        default:
            break
        }
    }
    //
    func setImageUrl(_ url: String) {
        interactor?.setImageUrl(url)
    }
    func setImage(image: UIImage?) {
        // set image here
        interactor?.setImage(image: image ?? nil)
    }
    //
    func setTags(_ tags: [String]) {
        interactor?.setTags(tags)
    }
    //
    func setMediaLinks(_ media: Media) {
        interactor?.setMedia(media)
    }
    //
    func setPhotoCredit(_ photoCredit: String) {
        interactor?.setImageCredits(photoCredit.removeWhiteSpace())
    }
}

extension CreatePostPresenter: CreatePostInteractorToPresenterProtocol {
}
