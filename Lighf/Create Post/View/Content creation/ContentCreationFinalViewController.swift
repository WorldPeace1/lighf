//
//  ContentCreationFinalViewController.swift
//  Lighf
//
//  Created by L.
//  Copyright © 2019 L. All rights reserved.
//

import UIKit
import KMPlaceholderTextView

class ContentCreationFinalViewController: ContentCreationViewController {
    @IBOutlet weak var initialContentView: UIView!
    @IBOutlet weak var initialContentLabel: UILabel!
    let singleLineTitleHeight: CGFloat = 50.0
    let bottomOffset: CGFloat = 50.0
    let initialContentViewTopOffset: CGFloat = 20.0
    //
    static var storyboardId: String {
        return "ContentCreaionFinalVC"
    }
    //
    override var bottomMargin: CGFloat {
        return initialContentViewTopOffset + bottomOffset + singleLineTitleHeight
    }
    //
    override func viewDidLoad() {
        super.viewDidLoad()
        viewHierarchy = .final
        textViewBottomConstraint.constant = bottomMargin
    }
    //
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        presenter?.initializeView(position: .final)
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        let mutableCopy = NSMutableAttributedString(attributedString: contentView.attributedText)
        presenter?.setTextInput(mutableCopy.trimCharactersInSet(charSet: CharacterSet.whitespacesAndNewlines), from: self)
    }
    //
    override var viewTitle: String {
        didSet {
            let subTitle = postType == .story ? Title.optional : Constants.emptyString
            self.navigationItem.setTitle(title: viewTitle, subTitle: subTitle)
        }
    }
    //
    override var initialTextInput: NSAttributedString {
        didSet {
            let mutableString = NSMutableAttributedString(attributedString: initialTextInput)
            mutableString.addAttribute(NSAttributedString.Key.font, value: UIFont(name: Font.sfProDisplayLight.rawValue, size: 16.0)!, range: NSRange(location: 0, length: initialTextInput.string.count))
            let paragraphStyle = NSMutableParagraphStyle()
            paragraphStyle.alignment = .left
            // Define attributed string attributes
            let attributes = [NSAttributedString.Key.paragraphStyle: paragraphStyle]
            mutableString.addAttributes(attributes, range: NSRange(location: 0, length: initialTextInput.string.count))
            initialContentLabel.attributedText = mutableString
        }
    }
    override var finalTextInput: NSAttributedString {
        didSet {
            let mutableCopy = NSMutableAttributedString(attributedString: finalTextInput)
            mutableCopy.addAttribute(NSAttributedString.Key.font, value: UIFont(name: Font.sfProDisplayLight.rawValue, size: 16.0)!, range: NSRange(location: 0, length: finalTextInput.string.count))
            contentView.attributedText = mutableCopy
            characterCountLabel.text = "\(finalTextInput.string.count)/\(maximumCharacterCount)"
            if postType == .article {
                navigationItem.rightBarButtonItem?.isEnabled = !finalTextInput.string.removeWhiteSpaceAndNewLine().isEmpty
            }
            contentView.layoutIfNeeded()
        }
    }
    override func keyboardWillShow(notification: Notification) {
        super.keyboardWillShow(notification: notification)
        textViewDidChange(contentView)
    }

    //
    @IBAction func initialContentEditButtonClicked(_ sender: Any) {
        presenter?.contentEditButtonClicked(from: self)
    }
}
