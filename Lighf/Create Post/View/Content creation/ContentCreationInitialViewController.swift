//
//  ContentCreaionInitialViewController.swift
//  Lighf
//
//  Created by L.
//  Copyright © 2019 L. All rights reserved.
//

import UIKit
import KMPlaceholderTextView

class ContentCreationInitialViewController: ContentCreationViewController {
    override func viewDidLoad() {
        super.viewDidLoad()
        textViewHeightConstraint.constant = 35.0
        addLeftBarButtonItem(withTitle: "Cancel")
    }
    //
    static var storyBoardId: String {
        return "ContentCreaionInitialVC"
    }
    //
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        presenter?.initializeView(position: .initial)
    }
    //
    override var initialTextInput: NSAttributedString {
        didSet {
            let mutableCopy = NSMutableAttributedString(attributedString: initialTextInput)
            mutableCopy.addAttribute(NSAttributedString.Key.font, value: UIFont(name: Font.sfProDisplayLight.rawValue, size: 16.0)!, range: NSRange(location: 0, length: initialTextInput.string.count))
            contentView.attributedText = mutableCopy
            characterCountLabel.text = "\(initialTextInput.string.count)/\(maximumCharacterCount)"
            navigationItem.rightBarButtonItem?.isEnabled = !initialTextInput.string.removeWhiteSpaceAndNewLine().isEmpty
            contentView.layoutIfNeeded()
        }
    }
}
