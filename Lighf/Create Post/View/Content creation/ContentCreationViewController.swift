//
//  ContentCreationViewController.swift
//  Lighf
//
//  Created by L.
//  Copyright © 2019 L. All rights reserved.
//

import UIKit
import KMPlaceholderTextView

class ContentCreationViewController: PostCreationViewController, CreatePostPresenterToContentCreationViewProtocol {
    //
    @IBOutlet weak var contentView: KMPlaceholderTextView!
    @IBOutlet weak var characterCountLabel: UILabel!
    @IBOutlet weak var textViewBottomConstraint: NSLayoutConstraint!
    @IBOutlet weak var textViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var verticalLineView: UIView!
    //
    let topMargin: CGFloat = 30
    var bottomMargin: CGFloat {
        return 50
    }
    var keyboardHeight: CGFloat = 0
    var viewHierarchy: ViewHierarchy = .initial
    var accessoryView: AccessoryView?
    var accessoryActionActive: Bool = false
    var textViewMaxHeight: Bool = false
    //
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationController?.titleColor(UIColor.black, font: Font.sfProDisplayMedium, size: 18.0)
        addRightBarButtonItem(withTitle: "Next")
        self.navigationItem.rightBarButtonItem?.isEnabled = false
        contentView.delegate = self
        navigationController?.resetBackgroundColor()
    }
    //
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow(notification:)),
                                               name: UIResponder.keyboardWillShowNotification, object: nil)
        contentView.becomeFirstResponder()
        configureAccessoryView()
    }
    //
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        view.endEditing(true)
        NotificationCenter.default.removeObserver(self,
                                                  name: UIResponder.keyboardWillShowNotification, object: nil)
    }
    //
    @objc
    func keyboardWillShow(notification: Notification) {
        if let keyboardFrame = notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue {
            let keyboardRect = keyboardFrame.cgRectValue
            keyboardHeight = keyboardRect.height
            // Reset textview bottom constraint
            textViewBottomConstraint.constant = keyboardHeight + bottomMargin
        }
    }
    private func configureAccessoryView() {
        if accessoryView == nil {
            accessoryView = AccessoryView(frame: CGRect(x: 0, y: 0, width: view.frame.width, height: 45), parentTextView: contentView, parentViewController: self)
        }
        accessoryView?.view = self
        contentView.inputAccessoryView = accessoryView
    }
    // MARK: - CreatePostPresentToContentCreationViewProtocol
    var maximumCharacterCount: Int = 0 {
        didSet {
            accessoryView?.maxCount = maximumCharacterCount
            characterCountLabel.text = "\(contentView.text.count)/\(maximumCharacterCount)"
        }
    }
    //
    var viewTitle: String = "" {
        didSet {
            self.navigationItem.title = viewTitle
        }
    }
    //
    var placeHolder: String = "" {
        didSet {
            contentView.placeholder = placeHolder
        }
    }
    //
    var postType: PostType = .story {
        didSet {
            if viewHierarchy == .final {
                navigationItem.rightBarButtonItem?.isEnabled = postType == .story
            }
        }
    }
    //
    var initialTextInput: NSAttributedString = NSAttributedString(string: "") {
        didSet {
            // override in subclass
        }
    }
    var finalTextInput: NSAttributedString = NSAttributedString(string: "") {
        didSet {
            // override in subclass
        }
    }
    override func nextButtonClicked(sender: UIBarButtonItem) {
        super.nextButtonClicked(sender: sender)
        let mutableCopy = NSMutableAttributedString(attributedString: contentView.attributedText)
        presenter?.setTextInput(mutableCopy.trimCharactersInSet(charSet: CharacterSet.whitespacesAndNewlines), from: self)
        textViewMaxHeight = false
    }
}

extension ContentCreationViewController: UITextViewDelegate {
    //
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        //
        guard let contentText = textView.text as NSString? else {
            return true
        }
        presenter?.isUpdated = true
        var newText = Constants.emptyString
        let textToAdd = text
//        if (textView.text.count + text.count > maximumCharacterCount) && ((maximumCharacterCount - textView.text.count - 1) <= text.count) {
//            textToAdd = text.substring(to: maximumCharacterCount - textView.text.count - 1)
//        }
        newText = contentText.replacingCharacters(in: range, with: textToAdd)
        //
        if viewHierarchy == .initial {
            navigationItem.rightBarButtonItem?.isEnabled = !newText.removeWhiteSpaceAndNewLine().isEmpty
        } else {
            navigationItem.rightBarButtonItem?.isEnabled = postType == .article ? !newText.removeWhiteSpaceAndNewLine().isEmpty : true
        }
        // Keep the character count in it's limit
        if newText.count > maximumCharacterCount {
            let textTobeAdded = newText.substring(to: maximumCharacterCount-1)
            textView.attributedText = NSAttributedString(string: textTobeAdded, attributes: textView.typingAttributes)
            characterCountLabel.text = "\(textTobeAdded.count)/\(maximumCharacterCount)"
            return false
        }
        characterCountLabel.text = "\(newText.count)/\(maximumCharacterCount)"
        accessoryView?.textViewDidModify(text: contentText, in: range, replacementText: textToAdd)
        if (textToAdd == "\"" || textToAdd == "\(Constants.bulletCharacter) " || textToAdd == Constants.emptyString) && accessoryActionActive {
            let attrText  = NSMutableAttributedString.init(string: textToAdd, attributes: textView.typingAttributes)
            let mainText = textView.attributedText.mutableCopy() as? NSMutableAttributedString ?? NSMutableAttributedString.init(string: textView.text, attributes: textView.typingAttributes)
            if textToAdd == Constants.emptyString {
                mainText.replaceCharacters(in: range, with: Constants.emptyString)

            } else {
                mainText.insert(attrText, at: range.location)
            }
            textView.attributedText = mainText
            accessoryActionActive = false
        }
        return true
    }
    //
    func textViewDidChange(_ textView: UITextView) {
        let contentSize = textView.sizeThatFits(CGSize(width: view.frame.width, height: CGFloat.greatestFiniteMagnitude))
        let maxPossibleHeight = view.frame.height - (topMargin + bottomMargin + Utility.getSafeAreaInsets() + self.navigationController!.navigationBar.frame.height + keyboardHeight)
        let textViewContentHeight = contentSize.height
        //
        if textViewContentHeight >= maxPossibleHeight {
            // Make the text view to scroll if it's content size is larger than the parent view size
            if !textViewMaxHeight {
                var frame = textView.frame
                frame.size.height = maxPossibleHeight
                textView.frame = frame
                textViewMaxHeight = true
                textViewHeightConstraint.constant = maxPossibleHeight
            }
            textView.isScrollEnabled = true
        } else {
            textView.isScrollEnabled = false
            textViewMaxHeight = false
        }
        if textView.text.count > maximumCharacterCount {
            textView.text = textView.text.substring(to: maximumCharacterCount - 1)
            accessoryView?.setCursorPosition(textView: textView, cursorPos: textView.text.count - 2)
        }
        accessoryView?.textViewDidChange()
    }
}

extension ContentCreationViewController: AccessoryViewToParentViewProtocol {
    //adding quote
    func insertQuote(at position: Int, with text: NSAttributedString) {
        accessoryActionActive = true
        _ = self.textView(contentView, shouldChangeTextIn: NSRange.init(location: position, length: 0), replacementText: text.string)
    }
    //bullet click
    func insertBullet(at position: Int, with text: NSAttributedString) {
        accessoryActionActive = true
        var range = NSRange.init(location: position, length: 0)
        if text.string == Constants.emptyString {
            range.length = 2
        }
        _ = self.textView(contentView, shouldChangeTextIn: range, replacementText: text.string)
    }
}
