//
//  MoreOptionsViewController.swift
//  Lighf
//
//
//  Copyright © 2020 L. All rights reserved.
//

import UIKit
import AVKit

class MoreOptionsViewController: PostCreationViewController, CreatePostPresenterToMoreOptionsViewProtocol, UINavigationControllerDelegate, UIGestureRecognizerDelegate {
    //
    @IBOutlet weak var mediaContainerViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var mediaContainerView: UIView!
    @IBOutlet weak var statementContainerView: UIView!
    @IBOutlet weak var statementContainerViewHeightConstraint: NSLayoutConstraint!
//    @IBOutlet weak var tagTextField: UITextField!
    @IBOutlet weak var contentScrollView: UIScrollView!
    @IBOutlet weak var selectedImageView: UIImageView!
    @IBOutlet weak var tagView: UIView!
    @IBOutlet weak var tagViewHeightConstraint: NSLayoutConstraint!
    //
    @IBOutlet weak var imageContainerView: UIView!
    @IBOutlet weak var photoCreditTextField: UITextField!
    @IBOutlet weak var tagViewBottomConstraint: NSLayoutConstraint!
    //
    var imagePicketSource: UIImagePickerController.SourceType?
    var showStatement: Bool = true {
        didSet {
            if showStatement {
                addStatementsView()
            } else {
                addEmptyView()
            }
        }
    }
    var moveToNextScreen: Bool = false
    weak var statementViewController: StatementViewController?
    weak var mediaViewController: MediaViewController?
    let minimumStatementViewHeight: CGFloat = 165.0
    let bottomMargin: CGFloat = 50.0
    let imageCreditMaximumCharacterCount: Int = 52
    let tagFieldView = TagField()
    let tagViewMinHeight: CGFloat = 52.0
    var imageViewController: ImageViewController?
    //
    static var storyboardId: String {
        return "MoreOptionsVC"
    }
    //
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.setTitle(title: Title.moreOptions, subTitle: Title.optional)
        addTagField()
        addMediaView()
        
        self.keyboardDismissalRegistrer.registerControls([self.tagFieldView.textField])
        
//        tagTextField.delegate = self
        addRightBarButtonItem(withTitle: "Preview")
        photoCreditTextField.attributedPlaceholder = NSAttributedString(string: "+ Add photo credit",
                                                                        attributes: [NSAttributedString.Key.foregroundColor: UIColor.white, NSAttributedString.Key.font: UIFont(name: Font.sfProDisplayLight.rawValue, size: 16)!])
        presenter?.initializeView(position: .initial)
        // Add 3d touch to show the full image
        if traitCollection.forceTouchCapability == .available {
            registerForPreviewing(with: self, sourceView: view)
        }
        if selectedImageView != nil {
            let longPress = UILongPressGestureRecognizer(target: self, action: #selector(longPressed(sender:)))
            longPress.minimumPressDuration = 0.5
            longPress.delaysTouchesBegan = true
            longPress.delegate = self
            selectedImageView.isUserInteractionEnabled = true
            selectedImageView.addGestureRecognizer(longPress)
        }
    }
    //
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        moveToNextScreen = false
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow(_:)),
                                               name: UIResponder.keyboardWillShowNotification, object: nil)
        presenter?.updateView(self)
    }
    //
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        setMediaLinks()
        moveToNextScreen = false
        self.view.endEditing(true)
        setTags()
        NotificationCenter.default.removeObserver(self,
                                                  name: UIResponder.keyboardWillShowNotification, object: nil)
    }
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        contentScrollView.contentOffset = CGPoint.zero
        tagViewBottomConstraint.constant = bottomMargin
    }
    //
    @objc
    func keyboardWillShow(_ notification: Notification) {
        if let keyboardFrame = notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue {
            let keyboardRect = keyboardFrame.cgRectValue
            let keyboardHeight = keyboardRect.height
            //
//            if tagTextField.isFirstResponder && tagViewBottomConstraint.constant == bottomMargin {
            if tagFieldView.textField.isFirstResponder && tagViewBottomConstraint.constant == bottomMargin {
                contentScrollView.contentOffset = CGPoint(x: 0, y: contentScrollView.contentSize.height - contentScrollView.frame.height + keyboardHeight)
                tagViewBottomConstraint.constant = bottomMargin + keyboardHeight
            }
        }
    }
    override func nextButtonClicked(sender: UIBarButtonItem) {
        self.view.endEditing(false)
        setTags()
        setMediaLinks()
        if let mediaVC = mediaViewController {
            if mediaVC.media.isValid() {
                super.nextButtonClicked(sender: sender)
            } else {
                moveToNextScreen = true
                mediaVC.checkMediaValidity()
            }
        }
    }
    // MARK: - Prepare view
    private func addTagField() {
        tagFieldView.frame = CGRect(x: 5.0, y: 5.0, width: tagView.frame.width - 10.0, height: tagView.frame.height - 10.0)
        tagFieldView.backgroundColor = UIColor.white
        tagFieldView.tagFieldDelegate = self
        //
        tagFieldView.textField.backgroundColor = UIColor.white
        tagFieldView.textField.textColor = UIColor.black
        tagView.addSubview(tagFieldView)
    }
    // Add view to enter the media links
    func addMediaView() {
        if let mediaVC = UIStoryboard.story.instantiateViewController(withIdentifier: MediaViewController.storyBoardId) as? MediaViewController {
            self.addChild(mediaVC)
            mediaVC.delegate = self
            mediaVC.view.translatesAutoresizingMaskIntoConstraints = false
            mediaContainerView.addSubview(mediaVC.view)
            mediaVC.view.autoPinEdgesToSuperviewEdges()
            // Set content view height with respect to the tableview height
            mediaVC.mediaTableView.layoutIfNeeded()
            mediaContainerViewHeightConstraint.constant = mediaVC.mediaTableView.contentSize.height
            mediaViewController = mediaVC
        }
    }
    // Add view to enter statements
    func addStatementsView() {
        if let statementVC = UIStoryboard.story.instantiateViewController(withIdentifier: StatementViewController.storyboardId) as? StatementViewController {
            self.addChild(statementVC)
            statementVC.delegate = self
            statementVC.view.translatesAutoresizingMaskIntoConstraints = false
            statementContainerView.addSubview(statementVC.view)
            statementVC.view.autoPinEdgesToSuperviewEdges()
            statementViewController = statementVC
        }
    }
    // Add empty view in case of story
    func addEmptyView() {
        let view = UIView.newAutoLayout()
        view.backgroundColor = UIColor.white
        statementContainerView.addSubview(view)
        view.autoPinEdgesToSuperviewEdges()
    }
    // MARK: - Set parameters to presenter
    func setTags() {
        if tagFieldView.isModified {
            presenter?.isUpdated = tagFieldView.isModified
        }
        presenter?.setTags(tagFieldView.tags)
    }
    func setMediaLinks() {
        if let mediaVC = mediaViewController {
            presenter?.setMediaLinks(mediaVC.media)
        }
    }
    // MARK: - Update View
    func updateStatements(_ statements: [RelatableStatement]) {
        guard let statementVC = statementViewController else {
            return
        }
        statementVC.statements = statements
        // Set content view height with respect to the tableview height
        statementVC.statementTableView.layoutIfNeeded()
        statementContainerViewHeightConstraint.constant = statementVC.contentHeight > minimumStatementViewHeight ? statementVC.contentHeight : minimumStatementViewHeight
    }
    //
    func updateTags(_ tags: [String]) {
        tagFieldView.removeAll()
        tagFieldView.isModified = false
        tagFieldView.tags = tags
    }
    //
    func updateMedia(_ media: Media) {
        guard let mediaVC = mediaViewController else {
            return
        }
        mediaVC.media = media
        mediaVC.reload()
    }
    //
    func updateImage(_ url: String) {
        guard url != Constants.emptyString, let fileUrl = URL(string: url) else {
            selectedImageView.image = nil
            imageContainerView.isHidden = true
            return
        }
        if fileUrl.isFileURL {
            do {
                let data = try Data(contentsOf: fileUrl)
                selectedImageView.image = UIImage(data: data)
                imageContainerView.isHidden = false
            } catch {
                imageContainerView.isHidden = true
            }
        } else {
            imageContainerView.isHidden = false
            selectedImageView.kf.setImage(with: fileUrl)
        }
    }
    func updateImageWithImage(image: UIImage?) {
        guard image != nil else {
            selectedImageView.image = nil
            imageContainerView.isHidden = true
            return
        }
        selectedImageView.image = image
        imageContainerView.isHidden = false
    }
    //
    func updateImageCredits(_ credits: String) {
        photoCreditTextField.text = credits
    }
    func showGallery(for sourceType: UIImagePickerController.SourceType, mediaTypes: [String]) {
        let imagePicker = UIImagePickerController()
        imagePicker.delegate = self
        imagePicker.allowsEditing = false
        //
        imagePicketSource = sourceType
        if UIImagePickerController.isSourceTypeAvailable(sourceType) {
            imagePicker.sourceType = sourceType
            imagePicker.mediaTypes = mediaTypes
            self.present(imagePicker, animated: true, completion: nil)
        } else {
            presenter?.imageFetchFailed(withError: ImagePickerError.sourceTypeNotAvailable)
        }
    }
    // MARK: Button clicks
    //
    func uploadButtonClicked() {
        let cameraMediaType = AVMediaType.video
        let cameraAuthorizationStatus = AVCaptureDevice.authorizationStatus(for: cameraMediaType)
        switch cameraAuthorizationStatus {
        case .denied:
            presenter?.promptUserToAuthorizeCameraInSettings()
        case .authorized:
            presenter?.uploadButtonClicked(sourceType: UIImagePickerController.SourceType.camera)
        case .restricted: break
            presenter?.uploadButtonClicked(sourceType: UIImagePickerController.SourceType.photoLibrary)
        case .notDetermined:
            // Prompting user for the permission to use the camera.
            AVCaptureDevice.requestAccess(for: cameraMediaType) { granted in
                if granted {
                    print("Granted access to \(cameraMediaType)")
                    DispatchQueue.main.async {
                        self.presenter?.uploadButtonClicked(sourceType: UIImagePickerController.SourceType.camera)
                    }
                } else {
                    print("Denied access to \(cameraMediaType)")
                    DispatchQueue.main.async {
                        self.presenter?.uploadButtonClicked(sourceType: UIImagePickerController.SourceType.photoLibrary)
                    }
                }
            }
        }
    }
    @IBAction func closeButtonClicked(_ sender: UIButton) {
        selectedImageView.image = nil
        imageContainerView.isHidden = true
        photoCreditTextField.text = ""
        presenter?.setImageUrl("")
        presenter?.setPhotoCredit("")
        presenter?.setImage(image: nil)
        presenter?.isUpdated = true
    }
    @IBAction func invokeImageSelectionChoice(_ sender: UIButton) {
        Utility().presentTwoActionAlerController(titleString: "Attach Photo", messageString: "", rightButtonTitle: "Existing", leftButtonTitle: "Take New Photo") { updateButtonTapped in
            if updateButtonTapped == 1 {
                DispatchQueue.main.async {
                    self.presenter?.uploadButtonClicked(sourceType: UIImagePickerController.SourceType.photoLibrary)
                }
            } else if updateButtonTapped == 2 {
                DispatchQueue.main.async {
                    self.uploadButtonClicked()
                }
            }
        }
    }
    @objc func longPressed(sender: UILongPressGestureRecognizer) {
        if sender.state == UIGestureRecognizer.State.began {
            imageViewController = UIStoryboard.postDetails.instantiateViewController(withIdentifier: ImageViewController.storyboardId) as? ImageViewController
            imageViewController?.image = selectedImageView.image
            if let imageview = imageViewController?.view {
                self.view.addSubview(imageview)
            }
        } else if sender.state == UIGestureRecognizer.State.ended {
            if let imageview = imageViewController?.view {
                imageview.removeFromSuperview()
            }
        }
    }
}

extension MoreOptionsViewController: StatementViewProtocol {
    func addNewStatement() {
        presenter?.addStatementButtonClicked()
    }
}

extension MoreOptionsViewController: MediaLinkListDelegate {
    func mediaReset() {
        presenter?.isUpdated = true
    }
    func mediaViewController(_ view: MediaViewController, didCompleteValidation status: Bool) {
        if status {
            presenter?.isUpdated = true
            if moveToNextScreen {
                presenter?.nextButtonClicked(from: self)
            }
        }
    }
}
extension MoreOptionsViewController: UITextFieldDelegate {
    //
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        guard let contentText = textField.text as NSString? else {
            return true
        }
        presenter?.isUpdated = true
        let newText = contentText.replacingCharacters(in: range, with: string)

//        if textField == tagTextField {
//            // Check for tags count. Maximum 5 tags are allowed.
//            let tags = newText.components(separatedBy: " ").filter {$0 != Constants.emptyString}
//            if tags.count >= 5 && string == Constants.singleSpace {
//                return false
//            }
//            return true
//        }
        // Limit character count in image credits field
        if textField == photoCreditTextField {
            if newText.count > imageCreditMaximumCharacterCount {
                return false
            } else {
                presenter?.isUpdated = true
                presenter?.setPhotoCredit(newText)
            }
        }
        return true
    }
    //
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}

extension MoreOptionsViewController: TagFieldDelegate {
    func textFieldDidEndEditing(_ textField: UITextField) {
        contentScrollView.contentOffset = CGPoint.zero
        tagViewBottomConstraint.constant = bottomMargin
    }
    //
    func didUpdateFrameSize(_ size: CGSize) {
        let previousHeight = tagViewHeightConstraint.constant
        let height: CGFloat = max(tagViewMinHeight, size.height+10)
        tagViewHeightConstraint.constant = height
        //
        let heightDifference: CGFloat = tagViewHeightConstraint.constant - previousHeight
        let currentOffset = contentScrollView.contentOffset
        contentScrollView.contentOffset = CGPoint(x: currentOffset.x, y: currentOffset.y + heightDifference)
    }
}

extension MoreOptionsViewController: UIImagePickerControllerDelegate {
    //
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey: Any]) {
        // picker repsonse comes here.
        if imagePicketSource == UIImagePickerController.SourceType.photoLibrary {
            guard let image = info[.originalImage] as? UIImage, let imagePath = info[.imageURL] as? URL else {
                  presenter?.imageFetchFailed(withError: ImagePickerError.unknown)
                  return
              }
              imageContainerView.isHidden = false
              selectedImageView.image = image
              presenter?.isUpdated = true
              presenter?.setImageUrl(imagePath.absoluteString)
            dismiss(animated: true, completion: nil)
        } else {
            guard let image = info[.originalImage] as? UIImage else {
                presenter?.imageFetchFailed(withError: ImagePickerError.unknown)
                dismiss(animated: true, completion: nil)
                return
            }
            let imageWithFixedOrientation = image.fixOrientation()
            imageContainerView.isHidden = false
            selectedImageView.image = imageWithFixedOrientation
            presenter?.isUpdated = true
            presenter?.setImage(image: imageWithFixedOrientation)
            
            UIImageWriteToSavedPhotosAlbum(imageWithFixedOrientation, nil, nil, nil)
            
            dismiss(animated: true, completion: nil)
        }
    }
    //
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }
}

extension MoreOptionsViewController: UIViewControllerPreviewingDelegate {
    func previewingContext(_ previewingContext: UIViewControllerPreviewing, viewControllerForLocation location: CGPoint) -> UIViewController? {
        let relativeLocation = view.convert(location, to: selectedImageView)
        if selectedImageView.bounds.contains(relativeLocation), selectedImageView.image != nil {
            if let imageViewController = UIStoryboard.postDetails.instantiateViewController(withIdentifier: ImageViewController.storyboardId) as? ImageViewController {
                imageViewController.image = selectedImageView.image
                previewingContext.sourceRect = selectedImageView.frame
                return imageViewController
            }
            return UIViewController()
        }
        return nil
    }
    //
    func previewingContext(_ previewingContext: UIViewControllerPreviewing, commit viewControllerToCommit: UIViewController) {
    }
}
