//
//  SingleContentCreationViewController.swift
//  Lighf
//
//
//  Copyright © 2020 L. All rights reserved.
//

import UIKit
import KMPlaceholderTextView

class SingleContentCreationViewController: PostCreationViewController {
    let maxStoryTitleCharCount = 365
    let maxArticleTitleCharCount = 365
    let maxStoryBodyCharCount = 610
    let maxArticleBodyCharCount = 10946
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var titleTextView: KMPlaceholderTextView!
    @IBOutlet weak var titleCharCountLabel: UILabel!
    @IBOutlet weak var bodyLabel: UILabel!
    @IBOutlet weak var bodyTextView: KMPlaceholderTextView!
    @IBOutlet weak var bodyCharCountLabel: UILabel!
    @IBOutlet weak var contentScrollView: UIScrollView!
    
    var postType: PostType = .story
    var titleAccessoryView: AccessoryView?
    var bodyAccessoryView: AccessoryView?
    var accessoryActionActive: Bool = false
    
    static var storyboardId: String {
        return "singleContentCreationVC"
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationController?.titleColor(UIColor.black, font: Font.sfProDisplayMedium, size: 18.0)
        addRightBarButtonItem(withTitle: "Next")
        navigationController?.resetBackgroundColor()
        addLeftBarButtonItem(withTitle: "Cancel")
        configureView(for: postType)
        configureAccessoryView()
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(handleTap(recognizer:)))
        self.view.addGestureRecognizer(tapGestureRecognizer)
        
        self.keyboardDismissalRegistrer.registerControls([self.titleTextView, self.bodyTextView])
    }
    @objc func handleTap(recognizer: UITapGestureRecognizer) {
        contentScrollView.contentOffset = CGPoint.zero
        self.view.endEditing(true)
    }
    //
    private func configureView(for postType: PostType) {
        switch postType {
        case .story:
            navigationItem.title = "Create Your Story"
            titleLabel.text = "Write Relatable lighfStory"
            bodyLabel.text = "Add The Longer Story"
            titleCharCountLabel.text = "0/\(maxStoryTitleCharCount)"
            bodyCharCountLabel.text = "0/\(maxStoryBodyCharCount)"
            //
            titleTextView.placeholder = "Share a lighfStory for others to relate to in 365 chars."
            bodyTextView.placeholder = "Need more room? Add the longer story here.  This part does not have to be relatable."
        case .article:
            navigationItem.title = "Create your Article"
            titleLabel.text = "Add Article Title"
            bodyLabel.text = "Add Article Body"
            titleCharCountLabel.text = "0/\(maxArticleTitleCharCount)"
            bodyCharCountLabel.text = "0/\(maxArticleBodyCharCount)"
            //
            titleTextView.placeholder = "Add your Article’s Title"
            bodyTextView.placeholder = "Add your Article’s Body"
        default:
            break
        }
        //
        navigationItem.rightBarButtonItem?.isEnabled = false
        if let title = presenter?.getTitleTextInput() {
            let mutableCopy = NSMutableAttributedString(attributedString: title)
            mutableCopy.addAttribute(NSAttributedString.Key.font, value: UIFont(name: Font.sfProDisplayLight.rawValue, size: 16.0)!, range: NSRange(location: 0, length: title.string.count))
            titleTextView.attributedText = mutableCopy
            let maxCount = postType == .story ? maxStoryTitleCharCount : maxArticleTitleCharCount
            titleCharCountLabel.text = "\(title.string.count)/\(maxCount)"
            navigationItem.rightBarButtonItem?.isEnabled = postType == .story ? !title.string.removeWhiteSpaceAndNewLine().isEmpty : false
        }
        //
        if let body = presenter?.getBodyTextInput() {
            let mutableCopy = NSMutableAttributedString(attributedString: body)
            mutableCopy.addAttribute(NSAttributedString.Key.font, value: UIFont(name: Font.sfProDisplayLight.rawValue, size: 16.0)!, range: NSRange(location: 0, length: body.string.count))
            bodyTextView.attributedText = mutableCopy
            let maxCount = postType == .story ? maxStoryBodyCharCount : maxArticleBodyCharCount
            bodyCharCountLabel.text = "\(body.string.count)/\(maxCount)"
            navigationItem.rightBarButtonItem?.isEnabled = !body.string.removeWhiteSpaceAndNewLine().isEmpty
        }
    }
    //
    private func configureAccessoryView() {
        if titleAccessoryView == nil {
            titleAccessoryView = AccessoryView(frame: CGRect(x: 0, y: 0, width: view.frame.width, height: 45), parentTextView: titleTextView, parentViewController: self)
        }
        titleAccessoryView?.view = self
        titleTextView.inputAccessoryView = titleAccessoryView
        titleAccessoryView?.maxCount = postType == .story ? maxStoryTitleCharCount : maxArticleTitleCharCount
        //
        if bodyAccessoryView == nil {
           bodyAccessoryView = AccessoryView(frame: CGRect(x: 0, y: 0, width: view.frame.width, height: 45), parentTextView: bodyTextView, parentViewController: self)
        }
        bodyAccessoryView?.view = self
        bodyTextView.inputAccessoryView = bodyAccessoryView
        bodyAccessoryView?.maxCount = postType == .story ? maxStoryBodyCharCount : maxArticleBodyCharCount
    }
    //
    override func nextButtonClicked(sender: UIBarButtonItem) {
        super.nextButtonClicked(sender: sender)
        let titleMutableCopy = NSMutableAttributedString(attributedString: titleTextView.attributedText)
        let bodyMutableCopy = NSMutableAttributedString(attributedString: bodyTextView.attributedText)
        presenter?.setTitle(titleMutableCopy.trimCharactersInSet(charSet: CharacterSet.whitespacesAndNewlines),
                            body: bodyMutableCopy.trimCharactersInSet(charSet: CharacterSet.whitespacesAndNewlines))
    }
}

extension SingleContentCreationViewController: UITextViewDelegate {
    func textViewDidBeginEditing(_ textView: UITextView) {
        accessoryActionActive = false
        if textView == bodyTextView {
            contentScrollView.contentOffset = CGPoint(x: 0.0, y: bodyTextView.frame.height + 100)
        } else {
            contentScrollView.contentOffset = CGPoint.zero
        }
    }
    //
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView == bodyTextView {
            contentScrollView.contentOffset = CGPoint.zero
        }
    }
    //
    func textViewDidChange(_ textView: UITextView) {
        if textView == titleTextView {
            titleAccessoryView?.textViewDidChange()
            contentScrollView.contentOffset = CGPoint.zero
        } else {
            bodyAccessoryView?.textViewDidChange()
            contentScrollView.contentOffset = CGPoint(x: 0.0, y: bodyTextView.frame.height + 100)
        }
    }
    //
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        //
        guard let contentText = textView.text as NSString? else {
            return true
        }
        presenter?.isUpdated = true
        let newText = contentText.replacingCharacters(in: range, with: text)
        if textView == titleTextView {
            //
            if postType == .story {
                navigationItem.rightBarButtonItem?.isEnabled = !newText.removeWhiteSpaceAndNewLine().isEmpty
            } else {
                navigationItem.rightBarButtonItem?.isEnabled = !newText.removeWhiteSpaceAndNewLine().isEmpty && !bodyTextView.text.removeWhiteSpaceAndNewLine().isEmpty
            }
            //
            let maxCount = postType == .story ? maxStoryTitleCharCount : maxArticleTitleCharCount
            if newText.count > maxCount {
                let maxCount = postType == .story ? maxStoryTitleCharCount : maxArticleTitleCharCount
                let textTobeAdded = newText.substring(to: maxCount-1)
                textView.attributedText = NSAttributedString(string: textTobeAdded, attributes: textView.typingAttributes)
                titleCharCountLabel.text = "\(textTobeAdded.count)/\(maxCount)"
                return false
            }
            titleCharCountLabel.text = "\(newText.count)/\(maxCount)"
            titleAccessoryView?.textViewDidModify(text: contentText, in: range, replacementText: text)
        } else {
            //
            if postType == .article {
                navigationItem.rightBarButtonItem?.isEnabled = !newText.removeWhiteSpaceAndNewLine().isEmpty && !titleTextView.text.removeWhiteSpaceAndNewLine().isEmpty
            }
            //
            let maxCount = postType == .story ? maxStoryBodyCharCount : maxArticleBodyCharCount
            if newText.count > maxCount {
                let maxCount = postType == .story ? maxStoryBodyCharCount : maxArticleBodyCharCount
                let textTobeAdded = newText.substring(to: maxCount-1)
                textView.attributedText = NSAttributedString(string: textTobeAdded, attributes: textView.typingAttributes)
                bodyCharCountLabel.text = "\(textTobeAdded.count)/\(maxCount)"
                return false
            }
            bodyCharCountLabel.text = "\(newText.count)/\(maxCount)"
            bodyAccessoryView?.textViewDidModify(text: contentText, in: range, replacementText: text)
        }
        //
        if (text == "\"" || text == "\(Constants.bulletCharacter) " || text == Constants.emptyString) && accessoryActionActive {
            let attrText  = NSMutableAttributedString.init(string: text, attributes: textView.typingAttributes)
            let mainText = textView.attributedText.mutableCopy() as? NSMutableAttributedString ?? NSMutableAttributedString.init(string: textView.text, attributes: textView.typingAttributes)
            if text == Constants.emptyString {
                mainText.replaceCharacters(in: range, with: Constants.emptyString)
            } else {
                mainText.insert(attrText, at: range.location)
            }
            textView.attributedText = mainText
            accessoryActionActive = false
        }
        //
        return true
    }
}

extension SingleContentCreationViewController: AccessoryViewToParentViewProtocol {
    //adding quote
    func insertQuote(at position: Int, with text: NSAttributedString) {
        accessoryActionActive = true
        _ = self.textView(titleTextView.isFirstResponder ? titleTextView : bodyTextView, shouldChangeTextIn: NSRange.init(location: position, length: 0), replacementText: text.string)
    }
    //bullet click
    func insertBullet(at position: Int, with text: NSAttributedString) {
        accessoryActionActive = true
        var range = NSRange.init(location: position, length: 0)
        if text.string == Constants.emptyString {
            range.length = 2
        }
        _ = self.textView(titleTextView.isFirstResponder ? titleTextView : bodyTextView, shouldChangeTextIn: range, replacementText: text.string)
    }
}
