//
//  PostCreationViewController.swift
//  Lighf
//
//  Created by L.
//  Copyright © 2019 L. All rights reserved.
//

import UIKit

class PostCreationViewController: UIViewController, CreatePostPresenterToViewProtocol {
    var presenter: CreatePostViewToPresenterProtocol?
    var keyboardDismissalRegistrer: KeyboardDismissalRegistrer!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.keyboardDismissalRegistrer = KeyboardDismissalRegistrer(view: self.view)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    //
    func addRightBarButtonItem(withTitle title: String) {
        let nextButton = UIButton.customNavigationRightBarButton(withTitle: title)
        nextButton.addTarget(self, action: #selector(nextButtonClicked(sender:)), for: .touchUpInside)
        let rightButton = UIBarButtonItem(customView: nextButton)
        self.navigationItem.rightBarButtonItem = rightButton
    }
    //
    @objc
    func nextButtonClicked(sender: UIBarButtonItem) {
        presenter?.nextButtonClicked(from: self)
    }
    // MARK: - CreatePostPresenterToViewProtocol
    func showAlert(_ message: String) {
        let alert = UIAlertController.alert(with: message)
        self.present(alert, animated: true, completion: nil)
    }
    //
    func addLeftBarButtonItem(withTitle title: String) {
        let cancelButton = UIButton(type: .custom)
        cancelButton.setTitle(title, for: .normal)
        cancelButton.setTitleColor(UIColor.black, for: .normal)
        cancelButton.titleLabel?.font = UIFont(name: Font.sfProTextRegular.rawValue, size: 17.0)
        cancelButton.addTarget(self, action: #selector(cancelButtonClicked(sender:)), for: .touchUpInside)
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(customView: cancelButton)
    }
    //
    @objc func cancelButtonClicked(sender: UIBarButtonItem) {
        presenter?.cancelButtonClicked()
    }
}
