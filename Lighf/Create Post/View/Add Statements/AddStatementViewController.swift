//
//  AddStatementViewController.swift
//  Lighf
//
//  Created by L.
//  Copyright © 2019 L. All rights reserved.
//

import UIKit

class AddStatementViewController: PostCreationViewController, CreatePostPresenterToAddStatementViewProtocol {
    //
    static var storyboardId: String {
        return "AddStatementVC"
    }
    let topMargin: CGFloat = 45
    let singleLineHeight: CGFloat = 50.0
    var bottomMargin: CGFloat {
        return 80 + singleLineHeight + (UIApplication.shared.delegate?.window??.safeAreaInsets.bottom ?? 0)
    }
    var keyBoardHeight: CGFloat = 0
    //
    var addedStatements: [RelatableStatement] = [] {
        didSet {
            newStatements = addedStatements
        }
    }
    var newStatements = [RelatableStatement]()
    var noOfStatementsAllowed: Int = Article.maxStatements {
        didSet {
            if noOfStatementsAllowed != 0 {
                newStatements.append(RelatableStatement(statementId: 0, text: "", related: false))
            }
            viewTitle.text = "Add up to \(Article.maxStatements) relatable statements to your article"
            addStatementTableView.reloadData()
            addStatementTableView.layoutIfNeeded()
            tableViewHeightConstraint.constant = tableViewHeight
        }
    }
    //
    var initialTextInput: NSAttributedString = NSAttributedString(string: "") {
        didSet {
            initialContentLabel.attributedText = initialTextInput
        }
    }
    //
    var finalTextInput: NSAttributedString = NSAttributedString(string: "") {
        didSet {
            finalContentLabel.attributedText = finalTextInput
        }
    }
    //
    @IBOutlet weak var tableViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var addStatementTableView: UITableView!
    @IBOutlet weak var viewTitle: UILabel!
    @IBOutlet weak var initialContentLabel: UILabel!
    @IBOutlet weak var finalContentLabel: UILabel!
    //
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.title = "Add Relatable Statements"
        addRightBarButtonItem(withTitle: "Done")
        addLeftBarButtonItem(withTitle: "Cancel")
        addStatementTableView.layoutIfNeeded()
        tableViewHeightConstraint.constant = tableViewHeight
        presenter?.initializeView(position: .initial)
        addStatementTableView.delegate = self
    }
    //
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    //
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(keyboardWillShow(notification:)),
                                               name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(keyboardWillHide(notification:)),
                                               name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    //
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    //
    @objc
    func keyboardWillShow(notification: Notification) {
        if let keyboardFrame = notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue {
            let keyboardRect = keyboardFrame.cgRectValue
            keyBoardHeight = keyboardRect.height
            updateTableViewHeight()
        }
    }
    //
    @objc
    func keyboardWillHide(notification: Notification) {
        keyBoardHeight = 0
        updateTableViewHeight()
    }
    //
    @IBAction func addButtonClicked(sender: UIButton) {
        if newStatements.count < Article.maxStatements {
            newStatements.append(RelatableStatement(statementId: 0, text: "", related: false))
            addStatementTableView.reloadData()
            addStatementTableView.layoutIfNeeded()
            updateTableViewHeight()
            setTableViewContentOffset()
            if addStatementTableView.isScrollEnabled == false {
                addStatementTableView.scrollToRow(at: IndexPath(item: 0, section: 0), at: .top, animated: true)
            }
        }
    }
    //
    @objc
    override func nextButtonClicked(sender: UIBarButtonItem) {
        presenter?.doneButtonClicked(with: newStatements.filter {$0.statementText.removeWhiteSpaceAndNewLine() != Constants.emptyString})
    }
    //
    @IBAction func contentEditButtonClicked(_ sender: UIButton) {
        presenter?.contentEditButtonClicked(from: self)
    }
}

extension AddStatementViewController: UITableViewDataSource {
    //
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    //
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return newStatements.count
    }
    //
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: AddStatementCell.cellIdentifier, for: indexPath) as? AddStatementCell {
            cell.statementTextView.text = newStatements[indexPath.row].statementText
            cell.delegate = self
            return cell
        }
        return UITableViewCell()
    }
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
}

extension AddStatementViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if indexPath.row == tableView.indexPathsForVisibleRows?.last?.row {
            updateTableViewHeight()
        }
        if let statementCell = cell as? AddStatementCell, indexPath.row == newStatements.count - 1 && !statementCell.statementTextView.isFirstResponder {
            print("becomeFirstResponder")
            statementCell.statementTextView.becomeFirstResponder()
        }
    }
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        if newStatements[indexPath.row].statementText != Constants.emptyString {
            return true
        }
        return false
    }
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            newStatements.remove(at: indexPath.row)
            if newStatements.isEmpty {
                newStatements.append(RelatableStatement(statementId: 0, text: "", related: false))
            }
            presenter?.isUpdated = true
            tableView.reloadData()
            addStatementTableView.setNeedsLayout()
            addStatementTableView.layoutIfNeeded()
            updateTableViewHeight()
            setTableViewContentOffset()
        }
    }
}

extension AddStatementViewController: AddStatementCellProtocol {
    //
    func addStatementCell(_ cell: AddStatementCell, didChangeTextView textview: UITextView) {
        //
        if let indexPath = addStatementTableView.indexPath(for: cell) {
            var statement = newStatements[indexPath.row]
            statement.statementText = textview.text
            newStatements[indexPath.row] = statement
        }
        presenter?.isUpdated = true
        UIView.performWithoutAnimation {
            addStatementTableView.beginUpdates()
            addStatementTableView.endUpdates()
        }
        //
        updateTableViewHeight()
        setTableViewContentOffset()
    }
    /// Modify table view height based on statement text view height.
    func updateTableViewHeight() {
        let navigationBarHeight = self.navigationController?.navigationBar.frame.height ?? 0.0
        let tableMaxPossibleHeight = view.frame.height - (navigationBarHeight + topMargin + bottomMargin + keyBoardHeight)
        let tableContentHeight = tableViewHeight
        // if the table view is larger than the view, then make it scroll
        if tableContentHeight <= tableMaxPossibleHeight {
            addStatementTableView.isScrollEnabled = false
            tableViewHeightConstraint.constant = tableViewHeight
        } else {
            addStatementTableView.isScrollEnabled = true
            tableViewHeightConstraint.constant = tableMaxPossibleHeight
        }
    }
    var tableViewHeight: CGFloat {
        return addStatementTableView.contentSize.height
    }
    /// Change tableview content offset in such a way that the editing in textview is always visible
    func setTableViewContentOffset() {
        addStatementTableView.layoutIfNeeded()
        let navigationBarHeight = self.navigationController?.navigationBar.frame.height ?? 0.0
        let tableMaxPossibleHeight = view.frame.height - (navigationBarHeight + topMargin + bottomMargin + keyBoardHeight)
        if addStatementTableView.isScrollEnabled {
            let tableContentHeight = tableViewHeight
            let contentOffset = CGPoint(x: 0, y: tableContentHeight - tableMaxPossibleHeight)
            addStatementTableView.setContentOffset(contentOffset, animated: false)
        }
    }
    // Reset tableview to it's natural position
    func resetTableViewContentOffset() {
        addStatementTableView.contentOffset = CGPoint.zero
    }
}
