//
//  AddStatementCell.swift
//  Lighf
//
//  Created by L.
//  Copyright © 2019 L. All rights reserved.
//

import UIKit
import KMPlaceholderTextView

protocol AddStatementCellProtocol: class {
    func addStatementCell(_ cell: AddStatementCell, didChangeTextView textview: UITextView)
}

class AddStatementCell: UITableViewCell {
    //
    static var cellIdentifier: String {
        return "AddStatementCell"
    }
    let maxCharacterCount = 365
    weak var delegate: AddStatementCellProtocol?
    @IBOutlet weak var statementTextView: KMPlaceholderTextView!
    //
    override func awakeFromNib() {
        super.awakeFromNib()
        statementTextView.delegate = self
    }
    //
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
}

extension AddStatementCell: UITextViewDelegate {
    func textViewDidChange(_ textView: UITextView) {
        if textView.text.count > maxCharacterCount {
            textView.text = textView.text.substring(to: maxCharacterCount - 1)
        }
        delegate?.addStatementCell(self, didChangeTextView: textView)
    }
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        guard let contentText = textView.text as NSString? else {
            return true
        }
        var textToAdd = text
        if (textView.text.count + text.count > maxCharacterCount) && ((maxCharacterCount - textView.text.count - 1) <= text.count) {
            textToAdd = text.substring(to: maxCharacterCount - textView.text.count - 1)
        }
        let newText = contentText.replacingCharacters(in: range, with: textToAdd)
        return newText.count <= maxCharacterCount
    }
}
