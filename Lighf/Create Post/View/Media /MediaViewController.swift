//
//  MediaViewController.swift
//  Lighf
//
//  Created by L.
//  Copyright © 2019 L. All rights reserved.
//

import UIKit

protocol MediaLinkListDelegate: class {
    func mediaViewController(_ view: MediaViewController, didCompleteValidation status: Bool)
    func mediaReset()
}

class MediaViewController: UIViewController {
    //
    @IBOutlet weak var mediaTableView: UITableView!
    //
    static var storyBoardId: String {
        return "MediaVC"
    }
    //
    var media = Media()
    var isValidityCheckInProgress: Bool = false
    weak var delegate: MediaLinkListDelegate?
    let medias = ["Youtube", "Music", "Podcast", "Article_link"]
    let mediasActive = ["Youtube_active", "Music_active", "Podcast_active", "Article_active"]
    let placeHolders = ["Paste Youtube Link", "Paste Soundcloud or Youtube Link", "Paste Soundcloud or Youtube Link", "Paste an Article Link"]
    var keyboardDismissalRegistrer: KeyboardDismissalRegistrer!
       
    //
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.keyboardDismissalRegistrer = KeyboardDismissalRegistrer(view: self.view)
    }
    //
    func reload() {
        mediaTableView.reloadData()
    }
    //
    func checkMediaValidity() {
        isValidityCheckInProgress = true
        reload()
    }
    //
    func resetCells() {
        mediaTableView.visibleCells.forEach {$0.isUserInteractionEnabled = true}
    }
}

extension MediaViewController: UITableViewDataSource {
    //
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    //
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return medias.count
    }
    //
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: MediaTableViewCell.cellIdentifier, for: indexPath) as? MediaTableViewCell {
            //
            cell.mediaTextField.placeholder = placeHolders[indexPath.row]
            let value = media.linkForType(MediaType(rawValue: indexPath.row))
            let image = value?.url.removeWhiteSpace() == Constants.emptyString ? UIImage(named: medias[indexPath.row]) : UIImage(named: mediasActive[indexPath.row])
            if media.isEmpty() {
                cell.isUserInteractionEnabled = true
            } else {
                if value?.url.removeWhiteSpace() != Constants.emptyString {
                    cell.isUserInteractionEnabled = true
                } else {
                    cell.isUserInteractionEnabled = false
                }
            }
            cell.prepareCell(with: value, image: image, indexPath: indexPath)
//            cell.borderView.isHidden = medias.count-1 == indexPath.row
            cell.delegate = self
            cell.parentView = self.view

            self.keyboardDismissalRegistrer.registerControl(cell.mediaTextField)

            return cell
        }
       return UITableViewCell()
    }
}

extension MediaViewController: MediaTableViewCellDelegate {
    func mediaReset() {
        delegate?.mediaReset()
    }
    //
    func mediaCell(_ cell: MediaTableViewCell, didChangeMedia mediaLink: Link?, for mediaType: MediaType) {
        //
        guard let link = mediaLink else {
            return
        }
        switch mediaType {
        case .youtube:
            media.youtubeLink = link
        case MediaType.music:
            media.musicLink = link
        case MediaType.podcast:
            media.podcastLink = link
        case MediaType.article:
            media.articleLink = link
        }
        cell.mediaImageView.image = link.url.removeWhiteSpace() == Constants.emptyString ? UIImage(named: medias[mediaType.rawValue]) : UIImage(named: mediasActive[mediaType.rawValue])
        isValidityCheckInProgress = false
        //
        if link.url.removeWhiteSpace() == Constants.emptyString {
            resetCells()
        } else {
            if let cells = mediaTableView.visibleCells as? [MediaTableViewCell] {
                cells.forEach { (mediaCell) in
                    if mediaCell != cell {
                        mediaCell.isUserInteractionEnabled = false
                    }
                }
            }
        }
    }
    func mediaCell(_ cell: MediaTableViewCell, didCheckValidityForMedia mediaLink: Link?, for mediaType: MediaType) {
        guard let link = mediaLink else {
            return
        }
        switch mediaType {
        case .youtube:
            media.youtubeLink = link
        case MediaType.music:
            media.musicLink = link
        case MediaType.podcast:
            media.podcastLink = link
        default:
            media.articleLink = link
        }
        if media.isValid() {
            // This delegate is only called when every link is valid.
            isValidityCheckInProgress = false
            delegate?.mediaViewController(self, didCompleteValidation: true)
        }
    }
}
