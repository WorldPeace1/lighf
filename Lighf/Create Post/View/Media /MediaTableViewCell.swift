//
//  MediaTableViewCell.swift
//  Lighf
//
//  Created by L.
//  Copyright © 2019 L. All rights reserved.
//

import UIKit
import MBProgressHUD

protocol MediaTableViewCellDelegate: class {
    func mediaCell(_ cell: MediaTableViewCell, didChangeMedia mediaLink: Link?, for mediaType: MediaType)
    func mediaCell(_ cell: MediaTableViewCell, didCheckValidityForMedia mediaLink: Link?, for mediaType: MediaType)
    func mediaReset()
}

class MediaTableViewCell: UITableViewCell {
    //
    @IBOutlet weak var mediaImageView: UIImageView!
    @IBOutlet weak var mediaTextField: UITextField!
    @IBOutlet weak var borderView: UIView!
    @IBOutlet weak var removeButtonWidthConstraint: NSLayoutConstraint!
    @IBOutlet weak var activityView: UIView!
    //
    weak var delegate: MediaTableViewCellDelegate?
    var parentView: UIView?
    private var mediaLink: Link?
    let buttonWidth: CGFloat = 32
    //
    static var cellIdentifier: String {
        return "MediaCell"
    }
    //
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        mediaTextField.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
        mediaTextField.delegate = self
        activityView.isHidden = true
        hideRemoveButton()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    //
    @objc func textFieldDidChange(_ textField: UITextField) {
        //
        guard let text = textField.text, let type = MediaType(rawValue: textField.tag) else {
            return
        }
        resetRemoveButton()
        mediaLink?.url = text
        mediaLink?.isValidityChecked = false
        if text.isEmpty {
            mediaLink?.isValid = true
            modifyCell(with: true)
        } else {
            mediaLink?.isValid = false
        }
        delegate?.mediaCell(self, didChangeMedia: mediaLink, for: type)
    }
    //
    func prepareCell(with media: Link?, image: UIImage?, indexPath: IndexPath) {
        mediaTextField.text = media?.url
        mediaImageView.image = image
        mediaTextField.tag = indexPath.row
        mediaLink = media
        guard let url = media?.url,
            media?.isValid == false,
            media?.isValidityChecked == false,
            let type = MediaType(rawValue: indexPath.row) else {
                if media?.isValid == false && media?.isValidityChecked == true {
                    self.borderView.backgroundColor = UIColor.red
                }
            return
        }
        if url.removeWhiteSpace().isEmpty {
            mediaLink?.isValid = true
            delegate?.mediaCell(self, didCheckValidityForMedia: mediaLink, for: type)
        } else {
            checkValidity(for: url, for: type)
        }
    }
    //
    @IBAction func removeButtonClicked(_ sender: UIButton) {
        mediaTextField.text = ""
        resetRemoveButton()
        textFieldDidChange(mediaTextField)
        delegate?.mediaReset()
    }
    //
    func resetRemoveButton() {
        guard let text = mediaTextField.text else {
            return
        }
        removeButtonWidthConstraint.constant = text.removeWhiteSpace().isEmpty ? 0 : buttonWidth
    }
    private func hideRemoveButton() {
        removeButtonWidthConstraint.constant = 0.0
    }
    //
    func modifyCell(with isUrlValid: Bool) {
        if isUrlValid {
            self.borderView.backgroundColor = UIColor.seperatorColor
        } else {
            self.borderView.backgroundColor = UIColor.red
            Utility.showToast(with: "Invalid Url", inView: self.parentView)
        }
    }
    //
    func showActivtyIndicator(_ show: Bool) {
        if show {
            activityView.isHidden = false
            MBProgressHUD.showAdded(to: activityView, animated: true)
        } else {
            activityView.isHidden = true
        }
    }
}

extension MediaTableViewCell: UITextFieldDelegate {
    //
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    //
    func textFieldDidBeginEditing(_ textField: UITextField) {
       resetRemoveButton()
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        hideRemoveButton()
        guard let text = textField.text, let type = MediaType(rawValue: textField.tag) else {
            self.borderView.backgroundColor = UIColor.seperatorColor
            return
        }
        let url = text.removeWhiteSpace()
        if url.isEmpty {
            self.borderView.backgroundColor = UIColor.seperatorColor
            return
        }
        if mediaLink?.isValidityChecked == false {
            checkValidity(for: url, for: type)
        }
    }
    func checkValidity(for text: String, for mediaType: MediaType) {
        switch mediaType {
        case .youtube:
            checkYoutubeUrlValidity(for: text, mediaType: mediaType)
        case .music:
            checkUrlValidity(for: text, mediaType: mediaType)
        case .podcast:
            checkUrlValidity(for: text, mediaType: mediaType)
        default:
            checkIfArticleLinkIsValid(for: text, mediaType: mediaType)
        }
    }
    private func checkIfArticleLinkIsValid(for url: String, mediaType: MediaType) {
        if url.removeWhiteSpace() != Constants.emptyString && Media.isValidArticleUrl(url.removeWhiteSpace()) {
            mediaLink?.isValid = true
            mediaLink?.isValidityChecked = true
            delegate?.mediaCell(self, didCheckValidityForMedia: mediaLink, for: mediaType)
        } else {
            modifyCell(with: false)
        }
    }
    private func checkUrlValidity(for url: String, mediaType: MediaType) {
        if SoundCloudHelper.isValidUrl(url) {
            modifyCell(with: true)
            mediaLink?.isValid = true
            mediaLink?.isValidityChecked = true
            delegate?.mediaCell(self, didCheckValidityForMedia: mediaLink, for: mediaType)
            return
        }
        checkYoutubeUrlValidity(for: url, mediaType: mediaType)
    }
    private func checkYoutubeUrlValidity(for url: String, mediaType: MediaType) {
        showActivtyIndicator(true)
        YoutubeHelper.checkUrlValidity(url) { (isValid) in
            self.showActivtyIndicator(false)
            self.mediaLink?.isValid = isValid
            self.mediaLink?.isValidityChecked = true
            self.modifyCell(with: isValid)
            self.delegate?.mediaCell(self, didCheckValidityForMedia: self.mediaLink, for: mediaType)
        }
    }
}
