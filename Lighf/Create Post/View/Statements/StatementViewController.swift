//
//  StatementViewController.swift
//  Lighf
//
//  Created by L.
//  Copyright © 2019 L. All rights reserved.
//

import UIKit

protocol StatementViewProtocol: class {
    func addNewStatement()
}

class StatementViewController: UIViewController {
    //
    let statementTitle = "Add up to 5 statements to your article"
    var bottomMargin: CGFloat {
        return UIApplication.shared.delegate?.window??.safeAreaInsets.bottom ?? 0
    }
    let topMargin: CGFloat = 80
    let topSpace: CGFloat = 15.0
    let bottomSpace: CGFloat = 15.0
    //
    @IBOutlet weak var statementTableView: UITableView!
    @IBOutlet weak var viewTitle: UILabel!
    //
    var statements = [RelatableStatement]() {
        didSet {
            viewTitle.text = statements.isEmpty ? statementTitle : "\(statements.count) of \(Article.maxStatements) statements added"
            statementTableView.reloadData()
        }
    }
    weak var delegate: StatementViewProtocol?
    static var storyboardId: String {
        return "StatementVC"
    }
    var contentHeight: CGFloat {
        let font = UIFont(name: Font.sfProTextRegular.rawValue, size: 16.0)
        let height = statements.map {UILabel.height(for: $0.statementText, width: view.frame.width - 30.0, font: font) + topSpace + bottomSpace }.reduce(0, +)
        return height + topMargin + bottomMargin
    }
    //
    override func viewDidLoad() {
        super.viewDidLoad()
        statementTableView.tableFooterView = UIView()
        viewTitle.text = statementTitle
    }
    //
    @IBAction func addButtonClicked(_ sender: UIButton) {
        delegate?.addNewStatement()
    }
    //
    func reload() {
        statementTableView.reloadData()
    }
}

extension StatementViewController: UITableViewDataSource {
    //
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    //
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return statements.count
    }
    //
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        //
        if let cell = tableView.dequeueReusableCell(withIdentifier: StatementTableViewCell.cellIdentifier, for: indexPath) as? StatementTableViewCell {
            cell.statementLabel.text = statements[indexPath.row].statementText
            return cell
        }
        return UITableViewCell()
    }
}
