//
//  CreatePostRouter.swift
//  Lighf
//
//  Created by L.
//  Copyright © 2019 L. All rights reserved.
//

import UIKit

class CreatePostRouter: NSObject {

    weak var presentedViewController: PostCreationViewController?
    var isUpdated: Bool = false
    //
    static func createDefaultModule(for type: PostType, post: Post?) -> ContentCreationInitialViewController? {
        let view = UIStoryboard.story.instantiateViewController(withIdentifier: ContentCreationInitialViewController.storyBoardId) as? ContentCreationInitialViewController
        //
        let presenter = CreatePostPresenter()
        let interactor = CreatePostInteractor()
        let router = CreatePostRouter()
        //
        presenter.postType = type
        interactor.postType = type
        //
        view?.presenter = presenter
        //
        presenter.view = view
        presenter.interactor = interactor
        presenter.router = router
        //
        interactor.presenter = presenter
        interactor.post = post
        //
        router.presentedViewController = view
        //
        return view
    }
    static func createAlternateModule(for type: PostType, post: Post?) -> SingleContentCreationViewController? {
        let view = UIStoryboard.story.instantiateViewController(withIdentifier: SingleContentCreationViewController.storyboardId) as? SingleContentCreationViewController
        //
        let presenter = CreatePostPresenter()
        let interactor = CreatePostInteractor()
        let router = CreatePostRouter()
        //
        presenter.postType = type
        interactor.postType = type
        view?.postType = type
        //
        view?.presenter = presenter
        //
        presenter.view = view
        presenter.interactor = interactor
        presenter.router = router
        //
        interactor.presenter = presenter
        interactor.post = post
        //
        router.presentedViewController = view
        //
        return view
    }
    //
    func updatePresentedView() {
        guard presentedViewController == nil else {
            return
        }
        if let navigationController = UIApplication.shared.keyWindow?.rootViewController as? UINavigationController {
            presentedViewController = navigationController.visibleViewController as? PostCreationViewController
        }
    }
    //
    func updateView() {
        updatePresentedView()
        let presenter = presentedViewController?.presenter as? CreatePostPresenter
        guard presenter?.view == nil else {
            return
        }
        presenter?.view = presentedViewController
    }
    // Create Final Content creationViewController
    func createFinalContentCreationView() -> ContentCreationFinalViewController? {
        let view = UIStoryboard.story.instantiateViewController(withIdentifier: ContentCreationFinalViewController.storyboardId) as? ContentCreationFinalViewController
        updatePresentedView()
        //
        let presenter = presentedViewController?.presenter as? CreatePostPresenter
        view?.presenter = presenter
        presenter?.view = view
        //
        return view
    }
    //
    func createMoreOptionsView() -> MoreOptionsViewController? {
        let view = UIStoryboard.story.instantiateViewController(withIdentifier: MoreOptionsViewController.storyboardId) as? MoreOptionsViewController
        updatePresentedView()
        //
        let presenter = presentedViewController?.presenter as? CreatePostPresenter
        view?.presenter = presenter
        presenter?.view = view
        //
        return view
    }
    //
    func createAddStatementsView() -> AddStatementViewController? {
        let view = UIStoryboard.story.instantiateViewController(withIdentifier: AddStatementViewController.storyboardId) as? AddStatementViewController
        updatePresentedView()
        //
        let presenter = presentedViewController?.presenter as? CreatePostPresenter
        view?.presenter = presenter
        presenter?.view = view
        //
        return view
    }
}

extension CreatePostRouter: CreatePostPresenterToRouterProtocol {
    //
    func moveToFinalContentCreationView() {
        if let view = createFinalContentCreationView() {
            presentedViewController?.navigationController?.pushViewController(view, animated: true)
            presentedViewController = view
        }
    }
    //
    func moveToMoreOptionsView() {
        if let view = createMoreOptionsView() {
            presentedViewController?.navigationController?.pushViewController(view, animated: true)
            presentedViewController = view
        }
    }
    //
    func moveToAddStatementView() {
        if let view = createAddStatementsView() {
            presentedViewController?.navigationController?.pushViewController(view, animated: true)
            presentedViewController = view
        }
    }
    //
    func moveToPreviewScreen(with postType: PostType, post: Post) {
        if let view = PostDetailsRouter.createModule(), presentedViewController?.navigationController?.viewControllers.last == presentedViewController {
            view.postType = postType
            view.viewType = .preview
            view.post = post
            view.isPostEdited = isUpdated
            updatePresentedView()
            presentedViewController?.navigationController?.pushViewController(view, animated: true)
        }
    }
    //
    func popView() {
        updatePresentedView()
        presentedViewController?.navigationController?.popViewController(animated: true)
    }
    //
    func popToContentCreationFinalView() {
        if let viewController = presentedViewController?.navigationController?.viewControllers[2] {
            presentedViewController?.navigationController?.popToViewController(viewController, animated: true)
        }
    }
}
