//
//  SoundCloudPlayerViewController.swift
//  Lighf
//
//  Created by L.
//  Copyright © 2019 L. All rights reserved.
//

import UIKit
import WebKit

class SoundCloudHelper {
    //
    static func isValidUrl(_ urlString: String) -> Bool {
        //
        if let url = NSURL(string: urlString), let host = url.host {
            return host == "soundcloud.com" || host == "m.soundcloud.com"
        }
        return false
    }
    //
    static func modify(_ urlString: String) -> String {
        // If the soundcloud link is copied from the mobile, replace the ".m" prefix from url
        if let url = NSURL(string: urlString), let host = url.host, host == "m.soundcloud.com" {
            return urlString.replacingOccurrences(of: "m.soundcloud.com", with: "soundcloud.com")
        }
        return urlString
    }
}

class SoundCloudPlayerViewController: MediaContentViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
    }
    @IBOutlet weak var webView: WKWebView!
    //
    static var storyboardId: String {
        return "SoundCloudVC"
    }
    var url: String = "" {
        didSet {
            let modifiedUrl = SoundCloudHelper.modify(url)
            webView.loadHTMLString("<html><head></head><body><iframe width=\"100%\" height=\"100%\" scrolling=\"no\" frameborder=\"no\" allow=\"autoplay\" src=\"https://w.soundcloud.com/player/?url=\(modifiedUrl)&buying=false&sharing=false\"></iframe></body></html>", baseURL: URL(string: ""))
        }
    }
    //
    override var contentHeight: CGFloat {
        return 128.0
    }
}
