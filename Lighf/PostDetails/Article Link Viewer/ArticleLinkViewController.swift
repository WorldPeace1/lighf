//
//  ArticleLinkViewController.swift
//  Lighf
//
//  Created by L.
//  Copyright © 2019 L. All rights reserved.
//

import UIKit

protocol ArticleLinkProtocol: class {
    func showArticleLink(_ link: String)
}

class ArticleLinkViewController: MediaContentViewController {
    //
    let margin: CGFloat = 30.0
    var articleLink: String = "" {
        didSet {
            let link = NSAttributedString(string: articleLink,
                                          attributes: [NSAttributedString.Key.underlineColor: UIColor.appBlueColor,
                                                       NSAttributedString.Key.underlineStyle: NSNumber(value: 1),
                                                       NSAttributedString.Key.foregroundColor: UIColor.appBlueColor])
            articleLinkLabel.attributedText = link
        }
    }
    static var storyBoardId: String {
        return "articleLinkVC"
    }
    weak var delegate: ArticleLinkProtocol?
    @IBOutlet weak var articleLinkLabel: UILabel!
    //
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    //
    override var contentHeight: CGFloat {
        return margin + articleLinkLabel.frame.height
    }
    //
    func handleTap(at point: CGPoint) {
        if articleLinkLabel.frame.contains(point) {
            showOpenArticleLinkAlert()
        }
    }
    //
    private func showOpenArticleLinkAlert() {
        let alert = UIAlertController(title: "Are you sure?", message: Message.showArticle, preferredStyle: .alert)
        let confirm = UIAlertAction(title: "Yes", style: .default) { (_) in
            self.delegate?.showArticleLink(self.articleLink)
        }
        alert.addAction(confirm)
        let cancel = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        alert.addAction(cancel)
        self.present(alert, animated: true, completion: nil)
    }
}
