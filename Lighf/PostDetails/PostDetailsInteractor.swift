//
//  PostDetailsInteractor.swift
//  Lighf
//
//  Created by L.
//  Copyright © 2019 L. All rights reserved.
//

import Foundation
import UIKit

class PostDetailsInteractor: PostOperationsInteractor {
    weak var presenter: PostDetailsInteractorToPresenterProtocol?
    weak var homePresenter: PostDetailsInteractorToHomePresenterProtocol?
    weak var searchPresenter: PostDetailsInteractorToSearchPresenterProtocol?
    //
    override func performedOperation(_ operation: PostOperation, onPost post: Post, with error: String?) {
        presenter?.performedOperation(operation, onPost: post, with: error)
    }
}

extension PostDetailsInteractor: PostDetailsPresenterToInteractorProtocol {
    func invokeDeleteComment(_ comment: PostComment) {
        //invoke delete api.
        let defaults = UserDefaults.standard
        let userID = defaults.value(forKey: UserDefaultsKey.userID) ?? 0
        APIManager.post(endPoint: EndPoint.deleteComment, parameters: ["comment_id": comment.commentID, "id": userID]) { (response, error) in
            guard error == nil, let response = response else {
                self.presenter?.deleteCommentFailResponse(comment, with: error?.message ?? Constants.emptyString)
                return
            }
            let message = response[APIResponse.apiResponse][APIResponse.apiResponseMessage].stringValue
            let status = response[APIResponse.apiResponse][APIResponse.apiResponseStatus].stringValue
            if status == Constants.success {
                self.presenter?.deleteCommentResponse(comment, with: message)
            } else {
                self.presenter?.deleteCommentFailResponse(comment, with: message)
            }
        }
    }
    func markPost(_ post: Post, asRelate markRelate: Bool) {
        mark(post, asRelated: markRelate)
    }
    func markPost(_ post: Post, asRead markAsRead: Bool) {
        mark(post, asRead: markAsRead)
    }
    func addPost(_ post: Post, toBackPack addToBackPack: Bool) {
        add(post, toBackPack: addToBackPack)
    }
    //invoke api to send leaf to author
    func sendLeafToAuthor() {
        //
    }
    //invoke api to get options for drop down
    func getDropDownOptions() {
    }
    //
    func getDetails(for post: Post, type: PostType) {
        guard let postId = post.postId else {
            return
        }
        let defaults = UserDefaults.standard
        var parameters: [String: Any] = [:]
        parameters[APIParams.userID] = defaults.value(forKey: UserDefaultsKey.userID)
        if postId == 0 {
           parameters["post_id"] = "random"
        } else {
            parameters["post_id"] = postId
        }
        invokeGetDetailsAPI(endPoint: EndPoint.getDetails, parameters: parameters, post: post)
    }
    func invokeGetDetailsAPI(endPoint: String, parameters: [String: Any], post: Post) {
        APIManager.post(endPoint: endPoint, parameters: parameters) { (response, error) in
            guard let response = response, error == nil else {
                self.presenter?.detailFetchFailed(for: post, with: error!.message)
                self.homePresenter?.detailFetchFailed(for: post, with: (error?.message)!)
                self.searchPresenter?.detailFetchFailed(for: post, with: (error?.message)!)
                return
            }
            let responseStatus = response[APIResponse.apiResponse][APIResponse.apiResponseStatus].string
            let postType = PostType(rawValue: response[APIResponse.apiResponse]["post_type"].stringValue)
            if responseStatus == "ok" {
                switch postType {
                case .story?, .statement? :
                    let story = Story(storyObj: response[APIResponse.apiResponse])
                    self.presenter?.handle(post: story, type: PostType.story)
                    self.homePresenter?.handle(post: story, type: PostType.story)
                    self.searchPresenter?.handle(post: story, type: PostType.story)
                case .article?:
                    let article = Article(articleObj: response[APIResponse.apiResponse])
                    self.presenter?.handle(post: article, type: PostType.article)
                    self.homePresenter?.handle(post: article, type: PostType.article)
                    self.searchPresenter?.handle(post: article, type: PostType.article)
                case .window?:
                    let window = Window(windowObj: response[APIResponse.apiResponse])
                    self.presenter?.handle(post: window, type: PostType.window)
                    self.homePresenter?.handle(post: window, type: PostType.window)
                    self.searchPresenter?.handle(post: window, type: PostType.window)
                case .admin_post:
                    let adminPost = AdminPost(adminPostObj: response[APIResponse.apiResponse])
                    self.presenter?.handle(post: adminPost, type: PostType.admin_post)
                    self.homePresenter?.handle(post: adminPost, type: PostType.admin_post)
                    self.searchPresenter?.handle(post: adminPost, type: PostType.admin_post)
                default: break
                }
            } else {
                let responseMessage = response[APIResponse.apiResponse][APIResponse.apiResponseMessage].stringValue
                var currentPost = post
                currentPost.isDeleted = response[APIResponse.apiResponse]["is_deleted"].boolValue
                self.presenter?.detailFetchFailed(for: currentPost, with: responseMessage)
                self.homePresenter?.detailFetchFailed(for: currentPost, with: responseMessage)
                self.searchPresenter?.detailFetchFailed(for: currentPost, with: responseMessage)
            }
        }
    }
    //
    func getAllComments(for post: Post) {
        getComments(for: post, commentType: nil, parentCommentId: nil)
    }
    //
    func markStatement(_ statement: RelatableStatement, asRelate: Bool) {
        let defaults = UserDefaults.standard
        var parameters: [String: Any] = [:]
        parameters["id"] = defaults.value(forKey: UserDefaultsKey.userID)
        parameters["post_id"] = statement.statementID
        parameters["action"] = asRelate == true ? "related" : "unrelated"
        //
        APIManager.post(endPoint: EndPoint.markPost, parameters: parameters) { (response, error) in
            guard let response = response, error == nil else {
                self.presenter?.relateStatement(statement, completedWith: error!.message)
                return
            }
            let status = response[APIResponse.apiResponse][APIResponse.apiResponseStatus].stringValue
            let responseMessage = response[APIResponse.apiResponse][APIResponse.apiResponseMessage].stringValue
            if status == Constants.success {
                var newStatement = statement
                newStatement.isRelated = response[APIResponse.apiResponse]["related_to"].boolValue
                self.presenter?.relateStatement(newStatement, completedWith: nil)
            } else {
                self.presenter?.relateStatement(statement, completedWith: responseMessage)
            }
        }
    }
    //
    func getComments(for post: Post, commentType: CommentType?, parentCommentId: Int?) {
        guard let postId = post.postId else {
            return
        }
        var parameters: [String: Any] = ["post_id": postId]
        //
        let defaults = UserDefaults.standard
        parameters["id"] = defaults.value(forKey: UserDefaultsKey.userID)
        if let type = commentType {
            parameters["comment_type"] = type.rawValue
        }
        //
        if let parentCommentId = parentCommentId {
            parameters["comment_parent_id"] = parentCommentId
        }
        //
        APIManager.post(endPoint: EndPoint.comments, parameters: parameters) { (response, error) in
            guard let response = response, error == nil else {
                self.presenter?.commentFetchFailed(with: error!.message)
                return
            }
            let commentsJson = response[APIResponse.apiResponse]["comments"].arrayValue
            let comments = commentsJson.map {PostComment(from: $0)}
            self.presenter?.handleComments(comments)
        }
    }
    //
    func savePost(_ post: Post, type: PostType) {
        // Invoke api to save post
        let defaults  = UserDefaults.standard
        var parameters: [String: Any] = [:]
        parameters["id"] = defaults.value(forKey: UserDefaultsKey.userID)
        if let postId = post.postId {
            parameters["post_id"] = postId
        }
        parameters["post_type"] = type.rawValue
        //
        switch post {
        case let story as Story:
            parameters["relatable_statements"] = [["statement_id": 0, "statement": story.shortStory.string]]
            parameters["post_body"] = story.longStory.string
        case let article as Article:
            parameters["title"] = article.title.string
            parameters["post_body"] = article.body.string
            let statements = article.statements.map({["statement_id": $0.statementID, "statement": $0.statementText]})
            parameters["relatable_statements"] = statements
        default:
            break
        }
        //
        if post.imageUrl != Constants.emptyString, let url = URL(string: post.imageUrl), let imageData = try? Data(contentsOf: url) {
            let image: UIImage = UIImage(data: imageData) ?? UIImage()
            let jpegData = image.jpegData(compressionQuality: 0.5)
            let encodedImage = jpegData?.base64EncodedString()
            parameters["image"] = encodedImage
        } else if post.postImage != nil, let imageData = post.postImage?.jpegData(compressionQuality: 0.5) {
            let encodedImage = imageData.base64EncodedString()
            parameters["image"] = encodedImage
        } else {
            parameters["image"] = Constants.emptyString
        }
        parameters["image_credits"] = post.photoCredit
        parameters["video_link"] = post.media.youtubeLink.url.removeWhiteSpace()
        parameters["audio_link"] = post.media.musicLink.url.removeWhiteSpace()
        parameters["podcast_link"] = post.media.podcastLink.url.removeWhiteSpace()
        parameters["article_link"] = post.media.articleLink.url.removeWhiteSpace()
        parameters["aspects"] = []
        parameters["tags"] = post.tags
        invokeSavePostAPI(post, withParams: parameters)
    }
    func invokeSavePostAPI(_ post: Post, withParams: [String: Any]) {
        APIManager.post(endPoint: EndPoint.createPost, parameters: withParams) { (json, error) in
            guard error == nil, let response = json else {
                self.presenter?.savePostFailed(with: error!.message)
                return
            }
            let responseStatus = response[APIResponse.apiResponse][APIResponse.apiResponseStatus].stringValue
            if responseStatus == Constants.success {
                // Check whether it is an edit operation by checking post id
                if post.postId != nil {
                    self.presenter?.savePostComplete(with: Message.editpost, withPostID: post.postId ?? 0)
                } else {
                    // Post id is nil means it's post creation
                    self.presenter?.savePostComplete(with: Message.addPost, withPostID: 0)
                }
            } else {
                self.presenter?.savePostFailed(with: response[APIResponse.apiResponse][APIResponse.apiResponseMessage].stringValue)
            }
        }
    }
    /// Delete post
    ///
    /// - Parameter post: The post that should be deleted
    func deletePost(_ post: Post) {
        let defaults = UserDefaults.standard
        var parameters: [String: Any] = [:]
        parameters["id"] = defaults.value(forKey: UserDefaultsKey.userID)
        parameters["post_id"] = post.postId
        //
        APIManager.post(endPoint: EndPoint.deletePost, parameters: parameters) { (response, error) in
            guard error == nil, let response = response else {
                self.presenter?.deletePost(post, failedWithMessage: error!.message)
                return
            }
            let message = response[APIResponse.apiResponse][APIResponse.apiResponseMessage].stringValue
            let status = response[APIResponse.apiResponse][APIResponse.apiResponseStatus].stringValue
            if status == Constants.success {
                self.presenter?.deletedPost(post, with: message)
            } else {
                var currentPost = post
                currentPost.isDeleted = response[APIResponse.apiResponse]["is_deleted"].boolValue
                self.presenter?.deletePost(currentPost, failedWithMessage: message)
            }
        }
    }
}
extension PostDetailsInteractor: HomePresenterToPostDetailsInteractorProtocol {
    func getDetailsToEdit(for post: Post, type: PostType) {
        getDetails(for: post, type: type)
    }
}

extension PostDetailsInteractor: SearchPresenterToPostDetailsInteractorProtocol {
    func getDetailsToEditFromSearch(for post: Post, type: PostType) {
        getDetails(for: post, type: type)
    }
}
