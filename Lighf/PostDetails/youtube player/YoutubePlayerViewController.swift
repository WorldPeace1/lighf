//
//  YoutubePlayerViewController.swift
//  Lighf
//
//  Created by L.
//  Copyright © 2019 L. All rights reserved.
//

import UIKit
import YoutubePlayer_in_WKWebView

class YoutubePlayerViewController: MediaContentViewController {

    @IBOutlet weak var playerView: WKYTPlayerView!
    @IBOutlet weak var descriptionLabel: UILabel!
    //
    let margin: CGFloat = 20
    var video: Video? {
        didSet {
            loadVideo(with: video?.videoId)
//            descriptionLabel.text = video?.description
        }
    }
    static var storyboardId: String {
        return "YoutubePlayerView"
    }
    override var contentHeight: CGFloat {
        return margin + playerView.frame.height
    }
    //
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    //
    private func loadVideo(with videoId: String?) {
        //
        guard let videoId = videoId else {
            return
        }
        //
        let dict = ["playsinline": 0]
        playerView.load(withVideoId: videoId, playerVars: dict)
    }
}
