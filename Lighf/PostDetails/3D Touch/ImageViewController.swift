//
//  ImageViewController.swift
//  Lighf
//
//  Created by L.
//  Copyright © 2019 L. All rights reserved.
//

import UIKit

class ImageViewController: UIViewController {

    @IBOutlet weak var imageView: UIImageView!
    //
    var image: UIImage?
    //
    static var storyboardId: String {
        return "ImageView"
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        imageView.image = image
    }
}
