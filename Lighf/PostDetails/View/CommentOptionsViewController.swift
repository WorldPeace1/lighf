//
//  CommentOptionsViewController.swift
//  Lighf
//
//  Created by L.
//  Copyright © 2019 L. All rights reserved.
//

import Foundation
import UIKit
import PopupDialog

protocol CommentTypeSelectionProtocol: class {
    func commentTypeSelectionViewController(_ selectionVC: CommentOptionsViewController, didSelectType type: CommentType)
}

class CommentOptionsViewController: UIViewController {
    @IBOutlet var subTextOrigin: NSLayoutConstraint!
    @IBOutlet var mainText: UILabel!
    @IBOutlet var doNotShowButton: UIButton!
    @IBOutlet var okButtonOrigin: NSLayoutConstraint!
    @IBOutlet var nonAdminPostTypeButtons: [UIButton]!
    var isRelated: Bool = false
    var isDisplayingAdminPost: Bool = false
    weak var delegate: CommentTypeSelectionProtocol?
    var parentPopUp: PopupDialog?
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if !isRelated {
            subTextOrigin?.constant = 15
            mainText?.isHidden = true
            doNotShowButton.isHidden = true
            okButtonOrigin.constant = 11
        } else {
            let isPopUpRequired =  UserDefaults.standard.value(forKey: UserDefaultsKey.showPopUp) as? Bool ?? true
            if isPopUpRequired {
                doNotShowButton.isSelected = false
            } else {
                doNotShowButton.isSelected = true
            }
        }
        
        self.disableNonAdminPostButtons()
    }
    //
    static var storyBoardId: String {
        return "CommentOptionsViewController"
    }
    //
    func disableNonAdminPostButtons() {
        for button in self.nonAdminPostTypeButtons {
            button.isEnabled = !self.isDisplayingAdminPost
            button.alpha = button.isEnabled ? 1.0 : 0.3
        }
    }
    @IBAction func commentRelateableStory(sender: UIButton) {
        dismiss()
        delegate?.commentTypeSelectionViewController(self, didSelectType: .comment)
    }
    @IBAction func commentAdvice(sender: UIButton) {
        dismiss()
        delegate?.commentTypeSelectionViewController(self, didSelectType: .advice)
    }
    @IBAction func commentQuestion(sender: UIButton) {
        dismiss()
        delegate?.commentTypeSelectionViewController(self, didSelectType: .question)
   }
    @IBAction func closeButtonClicked(sender: UIButton) {
        dismiss()
    }
    func dismiss() {
        guard let parentPopUp = parentPopUp else {
            return
        }
        parentPopUp.dismiss()
    }
    @IBAction func doNotShowButtonTapped(sender: UIButton) {
        if sender.isSelected {
            UserDefaults.standard.set(true, forKey: UserDefaultsKey.showPopUp)
            sender.isSelected = false
        } else {
            UserDefaults.standard.set(false, forKey: UserDefaultsKey.showPopUp)
            sender.isSelected = true
        }
    }
}
