//
//  PostDetailViewcontroller+Media.swift
//  Lighf
//
//  Created by L.
//  Copyright © 2020 L. All rights reserved.
//

import UIKit

extension PostDetailsViewController {
    ///
    func updateMediaView(with media: Media) {
        mediaSubViews.removeAll()
        let youtubeUrl = media.youtubeLink.url.removeWhiteSpace()
        let musicUrl = media.musicLink.url.removeWhiteSpace()
        let podcastUrl = media.podcastLink.url.removeWhiteSpace()
        urls = [podcastUrl, musicUrl, youtubeUrl]
        showActivityIndicator(onFullScreen: true)
        // The urls loaded one by one
        runQueue()
    }
    private func runQueue() {
        if urls.isEmpty {
            dismissActivityIndicator()
            backPackButton?.isUserInteractionEnabled = true
            showArticleLink(post?.media.articleLink.url.removeWhiteSpace())
        } else {
            if let url = urls.last {
                if SoundCloudHelper.isValidUrl(url) {
                    showSoundCloudView(for: url)
                } else if YoutubeHelper.videoId(for: url) != nil {
                    showYoutubeView(for: url)
                } else {
                    continueQueue()
                }
            }
        }
    }
    private func continueQueue() {
        if !urls.isEmpty {
            urls.removeLast()
        }
        runQueue()
    }
    //
    private func showArticleLink(_ link: String?) {
        guard let articleLink = link, articleLink != Constants.emptyString else {
            return
        }
        if let articleLinkVC = UIStoryboard.postDetails.instantiateViewController(withIdentifier: ArticleLinkViewController.storyBoardId) as? ArticleLinkViewController {
            addMediaView(articleLinkVC)
            articleLinkVC.articleLink = articleLink
            articleLinkVC.delegate = self
            articleLinkViewController = articleLinkVC
            let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(handleTap(recognizer:)))
            articleLinkVC.view.addGestureRecognizer(tapGestureRecognizer)
        }
    }
    @objc
    func handleTap(recognizer: UITapGestureRecognizer) {
        let touchPoint = recognizer.location(in: recognizer.view)
        articleLinkViewController?.handleTap(at: touchPoint)
    }
    //
    private func showYoutubeView(for url: String) {
        guard url != Constants.emptyString else {
            continueQueue()
            return
        }
        YoutubeHelper.details(for: url) { (video, error) in
            if error == nil {
                if let youtubeVC = self.youtubePlayerView(with: video) {
                    self.addMediaView(youtubeVC)
                    self.continueQueue()
                }
            } else {
                self.continueQueue()
                let alert = UIAlertController.alert(with: (error as? YoutubeError)?.message ?? error!.localizedDescription)
                self.present(alert, animated: true, completion: nil)
            }
        }
    }
    //sound cloud
    private func showSoundCloudView(for url: String) {
        //
        guard url != Constants.emptyString else {
            continueQueue()
            return
        }
        //
        if let soundcloudVC = UIStoryboard.postDetails.instantiateViewController(withIdentifier: SoundCloudPlayerViewController.storyboardId) as? SoundCloudPlayerViewController {
            addMediaView(soundcloudVC)
            soundcloudVC.url = url
            continueQueue()
        }
    }
    //
    private func addMediaView(_ view: MediaContentViewController) {
        let margin: CGFloat = 15.0
        var contentHeight = postMediaViewHeight.constant
        mediaStackView.addArrangedSubview(view.view)
        contentHeight += view.contentHeight
        postMediaViewHeight.constant = contentHeight + margin
        self.view.setNeedsLayout()
        self.view.layoutIfNeeded()
        //        updateWindowSize()
        self.perform(#selector(updateWindowSize), with: nil, afterDelay: 0.5)
    }
    // Add youtube player view
    func youtubePlayerView(with video: Video?) -> YoutubePlayerViewController? {
        if let youtubeVC = UIStoryboard.postDetails.instantiateViewController(withIdentifier: YoutubePlayerViewController.storyboardId) as? YoutubePlayerViewController {
            youtubeVC.view.translatesAutoresizingMaskIntoConstraints = false
            youtubeVC.video = video
            return youtubeVC
        }
        return nil
    }
}
