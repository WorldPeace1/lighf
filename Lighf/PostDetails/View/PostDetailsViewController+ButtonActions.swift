//
//  PostDetailsViewController+ButtonActions.swift
//  Lighf
//
//  Created by L.
//  Copyright © 2019 L. All rights reserved.
//

import Foundation
import UIKit
import PopupDialog
extension PostDetailsViewController: UIPopoverControllerDelegate {
    @IBAction func leafButtonClicked(sender: UIButton) {
        print("leaf button clicked")
        if let selectedPost = post {
            presenter?.leafButtonTapped(forPost: selectedPost, withCount: 1)
        }
    }
    @IBAction func relateButtonClicked(sender: UIButton) {
        guard let post = post else {
           return
        }
        presenter?.markPost(post, asRelate: !sender.isSelected)
    }
    public func showRelateSuccessPopup() {
        guard let optionsViewController = UIStoryboard(name: "PostDetails", bundle: Bundle.main).instantiateViewController(withIdentifier: CommentOptionsViewController.storyBoardId) as? CommentOptionsViewController else {
            return
        }
        optionsViewController.delegate = self
        optionsViewController.isRelated = true
        optionsViewController.isDisplayingAdminPost = post?.type == PostType.admin_post
        let popUp = PopupDialog(viewController: optionsViewController,
                                buttonAlignment: .vertical,
                                transitionStyle: .zoomIn,
                                tapGestureDismissal: true,
                                panGestureDismissal: false,
                                hideStatusBar: false)
        optionsViewController.parentPopUp = popUp
        present(popUp, animated: false, completion: nil)
    }

    /// Change the topview color properties based on the related state of the post
    ///
    /// - Parameters:
    ///   - post: the post of which the properties are to be changed
    ///   - isRelated: the relate status of the post
    func changeBackgroundForRelatedPost(_ post: Post, isRelated: Bool) {
        if isRelated {
            switch post.type {
            case .article:
                topView.backgroundColor = UIColor.black
            case .story:
                topView.backgroundColor = UIColor.appGreenColor
            case .window:
                topView.backgroundColor = UIColor.appBlueColor
            case .admin_post:
                topView.backgroundColor = UIColor.appGreenColor
            default:
                break
            }
            titleLabel.textColor = UIColor.white
            usernameLabel.textColor = UIColor.white
            alternateRelateButton.backgroundColor = UIColor.relatableGreenColor
        } else {
            topView.backgroundColor = UIColor.white
            titleLabel.textColor = UIColor.black
            usernameLabel.textColor = UIColor.black
            alternateRelateButton.backgroundColor = UIColor.appGreenColor
        }
    }
    //
    @objc func postButtonClicked(sender: UIButton) {
        if let post = post, let postType = postType {
            presenter?.savePost(post, type: postType)
            postButton?.isEnabled = false
        }
    }
    @IBAction func moreButtonClicked(sender: UIButton) {
        print("more button clicked")
            guard let optionsViewController = UIStoryboard(name: "PostDetails", bundle: Bundle.main).instantiateViewController(withIdentifier: CommentOptionsViewController.storyBoardId) as? CommentOptionsViewController else {
                return
            }
        optionsViewController.delegate = self
        optionsViewController.isDisplayingAdminPost = post?.type == PostType.admin_post
        let popUp = PopupDialog(viewController: optionsViewController,
                                    buttonAlignment: .vertical,
                                    transitionStyle: .zoomIn,
                                    tapGestureDismissal: true,
                                    panGestureDismissal: false,
                                    hideStatusBar: false)
            optionsViewController.parentPopUp = popUp
            present(popUp, animated: false, completion: nil)
    }
    @objc
    func backpackButtonClicked(sender: UIButton) {
        if let post = post {
            presenter?.addPost(post, toBackPack: !sender.isSelected)
            sender.isSelected = !sender.isSelected
            sender.tintColor = sender.isSelected ? UIColor.appBlueColor : UIColor.white
        }
    }
    @IBAction func deleteButtonClicked(_ sender: UIButton) {
        let alert = UIAlertController(title: "", message: Message.deleteConfirmation, preferredStyle: .alert)
        let confirmAction = UIAlertAction(title: "Yes", style: .default, handler: { (_) in
            if let post = self.post {
                self.presenter?.deletePost(post)
            }
        })
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        alert.addAction(confirmAction)
        alert.addAction(cancelAction)
        self.present(alert, animated: true, completion: nil)
    }
    @IBAction func editButtonClicked(_ sender: UIButton) {
        if let post = post, let type = postType {
            presenter?.editPost(post, type: type)
        }
    }
    @IBAction func flagOptionsButtonClicked(sender: UIButton) {
        let optionMenu = UIAlertController(title: nil, message: FlagOptions.header.rawValue, preferredStyle: .actionSheet)
        if flagFromComment || !((post?.isOwnPost ?? false) && !flagFromComment) { //check to show flagging options for comments from other users.
            flagFromComment = false
            let notRelatedAction = UIAlertAction(title: FlagOptions.notRelated.rawValue, style: .default, handler: { (_ action) -> Void in
                self.alertActionSelected(selectedOption: FlagOptions.notRelated)
            })
            optionMenu.addAction(notRelatedAction)
            let spamAction = UIAlertAction(title: FlagOptions.spam.rawValue, style: .default, handler: { (_ action) -> Void in
                self.alertActionSelected(selectedOption: FlagOptions.spam)
            })
            optionMenu.addAction(spamAction)
            let abuseAction = UIAlertAction(title: FlagOptions.abuse.rawValue, style: .default, handler: { (_ action) -> Void in
                self.alertActionSelected(selectedOption: FlagOptions.abuse)
            })
            optionMenu.addAction(abuseAction)
            let copyRightAction = UIAlertAction(title: FlagOptions.copyright.rawValue, style: .default, handler: { (_ action) -> Void in
                self.alertActionSelected(selectedOption: FlagOptions.copyright)
            })
            /* optionMenu.addAction(abuseAction)
            let otherAction = UIAlertAction(title: FlagOptions.other.rawValue, style: .default, handler: { (_ action) -> Void in
                self.alertActionSelected(selectedOption: FlagOptions.other)
            })
             */
            optionMenu.addAction(copyRightAction)
            if sender.tag == 1 {
                let dontSeeThisAction = UIAlertAction(title: FlagOptions.dontSeeThis.rawValue, style: .default, handler: { (_ action) -> Void in
                    self.alertActionSelected(selectedOption: FlagOptions.dontSeeThis)
                })
            optionMenu.addAction(dontSeeThisAction)
                let dontSeeLikeThisAction = UIAlertAction(title: FlagOptions.dontSeeLikeThis.rawValue, style: .default, handler: { (_ action) -> Void in
                    self.alertActionSelected(selectedOption: FlagOptions.dontSeeLikeThis)
                })
                optionMenu.addAction(dontSeeLikeThisAction)
            }
        }
        if post?.isNotificationEnabled ?? true {
            let turnOffNotificationAction = UIAlertAction(title: FlagOptions.turnOffNotification.rawValue, style: .default, handler: { (_ action) -> Void in
                self.alertActionSelected(selectedOption: FlagOptions.turnOffNotification)
            })
            optionMenu.addAction(turnOffNotificationAction)
        } else {
            let turnOnNotificationAction = UIAlertAction(title: FlagOptions.turnOnNotification.rawValue, style: .default, handler: { (_ action) -> Void in
                self.alertActionSelected(selectedOption: FlagOptions.turnOnNotification)
            })
            optionMenu.addAction(turnOnNotificationAction)
        }
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel)
        optionMenu.addAction(cancelAction)
        self.present(optionMenu, animated: true, completion: nil)
    }
    func alertActionSelected(selectedOption: FlagOptions) {
        if selectedOption != FlagOptions.turnOffNotification, selectedOption != FlagOptions.turnOnNotification, let post = post {
            presenter?.alertActionTapped(selectedFlagOption: selectedOption, for: post, parentComment: parentForReply)
        } else if selectedOption == FlagOptions.turnOffNotification || selectedOption == FlagOptions.turnOnNotification {
            print("turn off notifications")
            if let postToToggle = post, postToToggle.isNotificationEnabled {
                presenter?.toggleNotification(forItem: postToToggle, toState: NotificationState.turnOff)
            } else if let postToToggle = post {
                presenter?.toggleNotification(forItem: postToToggle, toState: NotificationState.turnOn)
            }
        }
    }
}

extension PostDetailsViewController: CommentTypeSelectionProtocol {
    func commentTypeSelectionViewController(_ selectionVC: CommentOptionsViewController, didSelectType type: CommentType) {
        if let postParam = post {
            presenter?.addComment(commentType: type, for: postParam, as: nil)
        }
    }
}
