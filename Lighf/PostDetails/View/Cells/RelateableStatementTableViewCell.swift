//
//  RelateableStatementTableViewCell.swift
//  Lighf
//
//  Created by L.
//  Copyright © 2019 L. All rights reserved.
//

import Foundation
import UIKit

protocol StatementCellDelegate: class {
    func statementCell(_ cell: RelateableStatementTableViewCell, shouldRelateStatement relate: Bool)
}

class RelateableStatementTableViewCell: UITableViewCell {
    @IBOutlet weak var statementText: UITextView!
    @IBOutlet weak var relateButton: UIButton!
    @IBOutlet weak var cellBackground: UIView!
    //
    weak var delegate: StatementCellDelegate?
    var statement: RelatableStatement?
    //
    public func prepareCell(with statement: RelatableStatement) {
        self.statement = statement
        statementText.text = statement.statementText
        statementText.textColor = UIColor.relateableStatementsColor
        relateButton.isSelected = statement.isRelated
        self.cellBackground.backgroundColor = statement.isRelated == true ? UIColor.appGreenColor : UIColor.white
    }
    //
    @IBAction func relateButtonClicked(_ sender: UIButton) {
        if let statement = statement {
            delegate?.statementCell(self, shouldRelateStatement: !statement.isRelated)
        }
    }
}
