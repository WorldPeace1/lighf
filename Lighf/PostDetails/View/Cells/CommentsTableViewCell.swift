//
//  CommentsTableViewCell.swift
//  Lighf
//
//  Created by L.
//  Copyright © 2019 L. All rights reserved.
//

import Foundation
import UIKit

protocol CommentCellDelegate: class {
    func commentCell(_ cell: CommentsTableViewCell, didClickReplyFor comment: PostComment)
}

class CommentsTableViewCell: UITableViewCell {
    //
    var commentType: CommentType = CommentType.comment
    var comment: PostComment?
    weak var delegate: CommentCellDelegate?
    //
    @IBOutlet weak var commentTypeImage: UIImageView!
    @IBOutlet weak var commentTextLabel: UITextView!
    @IBOutlet weak var commentTimeLabel: UILabel!
    @IBOutlet weak var commentReplyButton: UIButton!
    @IBOutlet weak var replyViewWidth: NSLayoutConstraint!
    @IBOutlet weak var imageViewWidth: NSLayoutConstraint!
    @IBOutlet weak var replyView: UIView!
    @IBOutlet weak var distanceBetweenTimestampAndReply: NSLayoutConstraint!
    //
    func configureCell(with comment: PostComment) {
        self.comment = comment
        commentTextLabel.text = comment.commentText
//        resize(textView: commentTextLabel)
        let fixedWidth = commentTextLabel.frame.size.width
        let newSize = commentTextLabel.sizeThatFits(CGSize(width: fixedWidth, height: CGFloat.greatestFiniteMagnitude))
        commentTextLabel.frame.size = CGSize(width: max(newSize.width, fixedWidth), height: newSize.height)
        //
        if comment.parentCommentID == 0 {
            setImageForComment(cell: self, commentObject: comment)
        } else {
            imageViewWidth.constant = 0
            replyViewWidth.constant = 65
            replyView.backgroundColor = UIColor.commentsReplyBar
        }
        commentTimeLabel.text = Constants.emptyString//commentObject.commentTimeStamp
        commentTimeLabel.sizeToFit()
        distanceBetweenTimestampAndReply.constant = 0// remove this when comment timestamp is showing value.
    }
    //method to set image for cell
    func setImageForComment(cell: CommentsTableViewCell, commentObject: PostComment) {
        cell.replyViewWidth.constant = 0
        cell.replyView.backgroundColor = UIColor.clear
        cell.imageViewWidth.constant = 22
        if commentObject.commentType == CommentType.advice {
            cell.commentTypeImage.image = UIImage.init(named: "CommentAdvice")
        } else if commentObject.commentType == CommentType.comment {
            cell.commentTypeImage.image = UIImage.init(named: "CommentRelatable")
        } else {
            cell.commentTypeImage.image = UIImage.init(named: "CommentQuestion")
        }
    }
    //
    @IBAction func replyButtonClicked(_ sender: UIButton) {
        if let comment = comment {
            delegate?.commentCell(self, didClickReplyFor: comment)
        }
    }
}
