//
//  PostDetailsViewController+TableViewMethods.swift
//  Lighf
//
//  Created by L.
//  Copyright © 2019 L. All rights reserved.
//

import Foundation
import UIKit

extension PostDetailsViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == relateableStatementsTableView {
            return relateableStatementsArray.count
        }
        return commentsArray.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if tableView == relateableStatementsTableView {
            return configureCellForRelateableTable(tableView: relateableStatementsTableView, cellForRowAt: indexPath)
        }
        return configureCellForCommentsTable(tableView: commentsTableView, cellForRowAt: indexPath)
    }
    //setup delegates and set default row height
    func setTableViewDelegates(tableView: UITableView) {
        tableView.delegate = self
        tableView.dataSource = self
        if tableView == relateableStatementsTableView {
            tableView.estimatedRowHeight = 44
        } else {
            tableView.estimatedRowHeight = 56
        }
        tableView.rowHeight = UITableView.automaticDimension
    }
    //method to setup the height of the relateableStatementsTableView
    func updateTableHeight(tableView: UITableView) {
        if tableView == relateableStatementsTableView {
            let margin: CGFloat = 10.0
            var tableHeight = 45 as CGFloat
            for loopVar in 0..<relateableStatementsArray.count {
                let relatableStatementObject = relateableStatementsArray[loopVar]
                let height = UITextView.height(for: relatableStatementObject.statementText, width: relateableStatementsTableView.frame.width - 73.0, font: UIFont(name: Font.sfProTextRegular.rawValue, size: 15.0))
                tableHeight += height + 10
            }
            relatableStatementsTableHeight.constant = tableHeight + margin
        } else {
            var tableHeight: CGFloat = 0.0
            let margin: CGFloat = 33.0
            for loopVar in 0..<commentsArray.count {
                let commentObject = commentsArray[loopVar]
                let height = UITextView.height(for: commentObject.commentText, width: 250, font: UIFont(name: Font.sfProTextRegular.rawValue, size: 14.0)) + margin
                tableHeight += height
            }
            commentsTableHeight.constant = tableHeight + margin
        }
        updateWindowSize()
    }
    //
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if tableView == relateableStatementsTableView {
            return 45
        }
        return 1
    }
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0
    }
    //Setup header - section will show section tile.
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        if tableView == relateableStatementsTableView {
            let headerViewframe = CGRect(x: 0, y: 0, width: relateableStatementsTableView.frame.size.width, height: 20)
            let headerView = UIView.init(frame: headerViewframe)
            headerView.backgroundColor = UIColor.white
            let headerName = UILabel()
            if relateableStatementsArray.isEmpty {
                headerName.text = Constants.emptyString
            } else {
                 headerName.text = viewType == .detailView ? "AUTHOR’s ADDITIONAL RELATABLE STATEMENTS" : "Article’s Added Relatable Statements"
            }
            headerName.frame = CGRect(x: 15, y: 15, width: relateableStatementsTableView.frame.size.width - 30, height: 20)
            headerName.textAlignment = .left
            headerName.textColor = UIColor.relateableStatementsColor
            headerName.font = UIFont(name: "SFProText-Regular", size: 11)
            headerView.addSubview(headerName)
            return headerView
        } else {
            let headerViewframe = CGRect(x: 0, y: 0, width: commentsTableView.frame.size.width, height: 1)
            let headerView = UIView.init(frame: headerViewframe)
            headerView.backgroundColor = UIColor.init(red: 181/255.0, green: 181/255.0, blue: 181/255.0, alpha: 0.7)
            return headerView
        }
    }
    //configure cell for RelateableStatementTableView
    func configureCellForRelateableTable( tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cellIdentifier = "RelateableStatementTableViewCell"
        if let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as? RelateableStatementTableViewCell {
            let relatableStatementObject = relateableStatementsArray[indexPath.row]
//            cell.statementText.text = relatableStatementObject.statementText
//            cell.statementText.textColor = UIColor.relateableStatementsColor
//            if relatableStatementObject.isRelated {
//                cell.cellBackground.backgroundColor = UIColor.appGreenColor
//            }
            cell.prepareCell(with: relatableStatementObject)
            resize(textView: cell.statementText)
            cell.relateButton.isEnabled = viewType == .detailView
            cell.delegate = self
            return cell
        }
        return UITableViewCell()
    }
    //configure cell for CommentsTableView
    func configureCellForCommentsTable( tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cellIdentifier = "CommentsTableViewCell"
        if let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as? CommentsTableViewCell {
            let commentObject = commentsArray[indexPath.row]
            cell.configureCell(with: commentObject)
            cell.delegate = self
//            cell.commentTextLabel.text = commentObject.commentText
//            resize(textView: cell.commentTextLabel)
//            if commentObject.parentCommentID == 0 {
//                setImageForComment(cell: cell, commentObject: commentObject)
//            } else {
//                cell.imageViewWidth.constant = 0
//                cell.replyViewWidth.constant = 83
//                cell.replyView.backgroundColor = UIColor.commentsReplyBar
//            }
//            cell.commentTimeLabel.text = Constants.emptyString//commentObject.commentTimeStamp
//            cell.commentTimeLabel.sizeToFit()
//            cell.distanceBetweenTimestampAndReply.constant = 0// remove this when comment timestamp is showing value.
//            cell.commentReplyButton.tag = indexPath.row
//            cell.commentReplyButton.addTarget(self, action: #selector(replyButtonTapped(sender:)), for: .touchDown)
            return cell
        }
        return UITableViewCell()
    }
//    @objc func replyButtonTapped(sender: UIButton) {
//        if commentsArray.indices.contains(sender.tag) {
//            parentForReply = commentsArray[sender.tag]
//        }
//        //
//        if let parentComment = parentForReply, let post = post {
//            presenter?.addComment(commentType: CommentType.comment, for: post, as: parentComment)
//        }
//    }
//    //method to set image for cell
//    func setImageForComment(cell: CommentsTableViewCell, commentObject: PostComment) {
//        cell.replyViewWidth.constant = 10
//        cell.replyView.backgroundColor = UIColor.clear
//        cell.imageViewWidth.constant = 22
//        if commentObject.commentType == CommentType.advice {
//            cell.commentTypeImage.image = UIImage.init(named: "CommentAdvice")
//        } else if commentObject.commentType == CommentType.comment {
//            cell.commentTypeImage.image = UIImage.init(named: "CommentRelatable")
//        } else {
//            cell.commentTypeImage.image = UIImage.init(named: "CommentQuestion")
//        }
//    }
    // method to set swipe actions
    func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        if tableView == commentsTableView {
            let defaults = UserDefaults.standard
            let commentObject = commentsArray[indexPath.row]
            if let userID = defaults.value(forKey: UserDefaultsKey.userID) as? NSInteger, commentObject.userID == userID {
                let editAction = editPost(atIndex: indexPath, for: commentObject)
                let deleteAction = deletePost(atIndex: indexPath, for: commentObject)
                let toggleNotificationAction = toggleNotificationForPost(atIndex: indexPath, for: commentObject)
                let swipeConfig = UISwipeActionsConfiguration(actions: [toggleNotificationAction, deleteAction, editAction])
                swipeConfig.performsFirstActionWithFullSwipe = false
                return swipeConfig
            }
            let giveLeafAction = giveLeafToComment(atIndex: indexPath)
            let flagAction = flagComment(atIndex: indexPath)
            let toggleNotificationAction = toggleNotificationForPost(atIndex: indexPath, for: commentObject)
            let swipeConfig = UISwipeActionsConfiguration(actions: [toggleNotificationAction, giveLeafAction, flagAction])
            swipeConfig.performsFirstActionWithFullSwipe = false
            return swipeConfig
        }
        let swipeConfig = UISwipeActionsConfiguration(actions: [])
        return swipeConfig
    }
    //method to handle flagging
    func flagComment(atIndex indexPath: IndexPath) -> UIContextualAction {
        var postObject = commentsArray[indexPath.row]
        let action = UIContextualAction(style: .destructive,
                                        title: Constants.emptyString) { (_ contextAction: UIContextualAction, _ sourceView: UIView, completionHandler: (Bool) -> Void) in
                                            if postObject.toggleFlaggedFlag() {
                                                //invoke an API to flag this comment
                                                print("flagged")
                                                // Need to update the flow to send the comment related data to flag this particular comment
                                                // Sample story for testing
                                                if (self.post != nil) {
                                                    self.parentForReply = postObject
                                                    if let userID = UserDefaults.standard.value(forKey: UserDefaultsKey.userID) as? NSInteger, postObject.userID != userID {
                                                        self.flagFromComment = true
                                                    }
                                                    self.flagOptionsButtonClicked(sender: UIButton.init())
                                                }
                                            }
                                            // This is a work around to fix the swipe overlap issue in tableview
                                            completionHandler(false)
        }
        if let cgImage = UIImage(named: "MoreButton")?.cgImage {
            action.image = ImageWithoutRender(cgImage: cgImage, scale: UIScreen.main.nativeScale, orientation: .up)
        }
        action.backgroundColor = UIColor.white
        return action
    }
    //method to handle giving a leaf
    func giveLeafToComment(atIndex indexPath: IndexPath) -> UIContextualAction {
        var postObject = commentsArray[indexPath.row]
        let action = UIContextualAction(style: .destructive,
                                        title: Constants.emptyString) { (_ contextAction: UIContextualAction, _ sourceView: UIView, completionHandler: (Bool) -> Void) in
                                            if postObject.leafGiven() {
                                                //invoke an API to give leaf
                                                print("leaf given")
                                                // Need to update the flow to send the comment related data to send leaf for this particular comment
                                                self.presenter?.leafButtonTapped(forComment: postObject, withCount: 1)
                                            }
                                            // This is a work around to fix the swipe overlap issue in tableview
                                            completionHandler(false)
        }
        if let cgImage = UIImage(named: "BlueLeaf")?.cgImage {
            action.image = ImageWithoutRender(cgImage: cgImage, scale: UIScreen.main.nativeScale, orientation: .up)
        }
        action.backgroundColor = UIColor.white
        return action

    }
    func editPost(atIndex indexPath: IndexPath, for post: PostComment) -> UIContextualAction {
        let action = createEditAction(comment: post, atIndex: indexPath)
        action.title = "Edit"
        return action
    }
    func deletePost(atIndex indexPath: IndexPath, for post: PostComment) -> UIContextualAction {
        let action = createDeleteAction(post: post, atIndex: indexPath)
        action.title = "Delete"
        return action
    }
    func toggleNotificationForPost(atIndex indexPath: IndexPath, for post: PostComment) -> UIContextualAction {
        let action = createToggleNotificationAction(post: post, atIndex: indexPath)
        action.title = "Notification"
        return action
    }
    func createToggleNotificationAction(post: PostComment, atIndex indexPath: IndexPath) -> UIContextualAction {
        //
        //
        let action = UIContextualAction(style: .destructive, title: Constants.emptyString) { (_ contextAction: UIContextualAction, _ sourceView: UIView, completionHandler: (Bool) -> Void) in
            if post.isNotificationEnabled {
                self.presenter?.toggleNotification(forItem: post, toState: NotificationState.turnOff)
            } else {
                self.presenter?.toggleNotification(forItem: post, toState: NotificationState.turnOn)
            }
            // Completion handler always returns false. This is a workaround to fix swipe issue in tableview
            completionHandler(false)
        }
        if post.isNotificationEnabled {
            action.image = UIImage.init(named: "notification")
        } else {
            action.image = UIImage.init(named: "notification_off")
        }
        action.backgroundColor = UIColor.toggleNotificationBackgroundColor
        return action
    }
    func createDeleteAction(post: PostComment, atIndex indexPath: IndexPath) -> UIContextualAction {
        //
        //
        let action = UIContextualAction(style: .destructive, title: Constants.emptyString) { (_ contextAction: UIContextualAction, _ sourceView: UIView, completionHandler: (Bool) -> Void) in
            let alert = UIAlertController(title: "", message: Message.deleteConfirmation, preferredStyle: .alert)
            let confirmAction = UIAlertAction(title: "Yes", style: .default, handler: { (_) in
                self.presenter?.deleteComment(post)
            })
            let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
            alert.addAction(confirmAction)
            alert.addAction(cancelAction)
            self.present(alert, animated: true, completion: nil)

            // Completion handler always returns false. This is a workaround to fix swipe issue in tableview
            completionHandler(false)
        }
        return action
    }
    // to edit to a post
    func createEditAction(comment: PostComment, atIndex indexPath: IndexPath) -> UIContextualAction {
        //
        let action = UIContextualAction(style: .destructive, title: Constants.emptyString) { (_ contextAction: UIContextualAction, _ sourceView: UIView, completionHandler: (Bool) -> Void) in
                //invoke an API to flag this comment
                self.presenter?.editComment(comment)
            // Completion handler always returns false. This is a workaround to fix swipe issue in tableview
            completionHandler(false)
        }
        action.backgroundColor = UIColor.lightGray
        return action
    }
}

extension PostDetailsViewController: StatementCellDelegate {
    func statementCell(_ cell: RelateableStatementTableViewCell, shouldRelateStatement relate: Bool) {
        if let statement = cell.statement {
            presenter?.markStatement(statement, asRelate: relate)
        }
    }
}

extension PostDetailsViewController: CommentCellDelegate {
    func commentCell(_ cell: CommentsTableViewCell, didClickReplyFor comment: PostComment) {
//        if commentsArray.indices.contains(sender.tag) {
//            parentForReply = commentsArray[sender.tag]
//        }
        //
        if let post = post {
            presenter?.addComment(commentType: CommentType.comment, for: post, as: comment)
        }
    }
}
