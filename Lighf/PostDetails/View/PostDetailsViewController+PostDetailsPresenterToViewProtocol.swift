//
//  PostDetailsViewController+PostDetailsPresenterToViewProtocol.swift
//  Lighf
//
//  Created by L.
//  Copyright © 2019 L. All rights reserved.
//

import Foundation
import UIKit
extension PostDetailsViewController: PostDetailsPresenterToViewProtocol {
    func updateNotificationStatus(postID: Int?, commentID: Int?, notificationStatus: NotificationState) {
        if postID != nil {
            if notificationStatus == NotificationState.turnOn {
                post?.isNotificationEnabled = true
            } else {
                post?.isNotificationEnabled = false
            }
        } else if commentID != nil {
            let commentIndex = self.commentsArray.firstIndex { (comment) -> Bool in
                return comment.commentID == commentID
            }
            if let indexToEdit = commentIndex, self.commentsArray.count > indexToEdit {
                var updatedComment = self.commentsArray[indexToEdit]
                if notificationStatus == NotificationState.turnOn {
                    updatedComment.isNotificationEnabled = true
                } else {
                    updatedComment.isNotificationEnabled = false
                }
                self.commentsArray[indexToEdit] = updatedComment
                self.commentsTableView.reloadRows(at: [IndexPath.init(row: indexToEdit, section: 0)], with: .none)
            }
        } else {
            print("Error!")
        }
    }
    //
    func resetStatementStatus(for statement: RelatableStatement) {
        if let index = relateableStatementsArray.firstIndex(where: {$0.statementID == statement.statementID}) {
            relateableStatementsArray[index] = statement
            relateableStatementsTableView.reloadRows(at: [IndexPath(row: index, section: 0)], with: .none)
        }
    }
    func replaceEditedComment(_ comment: PostComment) {
        let postIndex = commentsArray.firstIndex { (presentComment) -> Bool in
            return presentComment.commentID == comment.commentID
        }
        if let indexToReplace = postIndex, indexToReplace < commentsArray.count {
            commentsArray[indexToReplace] = comment
            commentsTableView.reloadRows(at: [IndexPath.init(row: indexToReplace, section: 0)], with: .none)
        }
        updateTableHeight(tableView: commentsTableView)
    }
    func updateDeleteComment(_ comment: Int) {
        dismissActivityIndicator()
        while let childComment = getCommentWithParent(with: comment) {
            var commentObj = childComment.0
            commentObj.parentCommentID = 0
            commentsArray[childComment.1] = commentObj
            UIView.performWithoutAnimation {
                commentsTableView.reloadRows(at: [IndexPath(row: childComment.1, section: 0)], with: .none)
            }
        }
        if let comment = getComment(with: comment) {
            commentsArray.remove(at: comment.1)
            UIView.performWithoutAnimation {
                commentsTableView.deleteRows(at: [IndexPath(row: comment.1, section: 0)], with: .none)
            }
            if commentsArray.isEmpty {
                commentsTableView.reloadData()
                showActivityIndicator(onFullScreen: true)
                if let post = self.post {
                    presenter?.getAllComments(for: post)
                }
            }
        }
        updateTableHeight(tableView: commentsTableView)
    }
    func getComment(with commentId: Int) -> (PostComment, Int)? {
        let comment = commentsArray.filter {$0.commentID == commentId}.first
        let postIndex = commentsArray.firstIndex { (comment) -> Bool in
            return comment.commentID == commentId
        }
        if let index = postIndex, let value = comment {
            return (value, index)
        }
        return nil
    }
    func getCommentWithParent(with commentId: Int) -> (PostComment, Int)? {
        let comment = commentsArray.filter {$0.parentCommentID == commentId}.first
        let postIndex = commentsArray.firstIndex { (comment) -> Bool in
            return comment.parentCommentID == commentId
        }
        if let index = postIndex, let value = comment {
            return (value, index)
        }
        return nil
    }
    func updateBackpackStatus(addToBackPack: Bool) {
        backPackButton?.isSelected = addToBackPack
    }
    //
    func updateRelateStatus(relate: Bool) {
        let isPopUpRequired =  UserDefaults.standard.value(forKey: UserDefaultsKey.showPopUp) as? Bool ?? true
        if relate && isPopUpRequired {
            showRelateSuccessPopup()
        }
        if let post = post {
            changeBackgroundForRelatedPost(post, isRelated: relate)
        }
        // Keep the same status for both buttons that are used for relate
        alternateRelateButton.isSelected = relate
        relateButton.isSelected = relate
    }
    //
    func updateComments(_ update: Bool) {
        getComments = update
    }
    func showGotoHomeAlert(with message: String) {
        var alertMessage = message
        if message.lowercased().contains(Message.requestTimeOut) || message.lowercased().contains(Message.messageNotFound) {
            alertMessage = Message.connectionLost
        } else {
            alertMessage = message
        }
        let alert = UIAlertController(title: "", message: alertMessage, preferredStyle: .alert)
        let alertAction = UIAlertAction(title: "Go to Home", style: .cancel) { (_) in
            self.presenter?.gotoHomeAfterAlert()
        }
        alert.addAction(alertAction)
        self.present(alert, animated: true, completion: nil)
    }
    func showComments(_ comments: [PostComment]) {
        commentsArray = comments
        commentsTableView.reloadData()
        updateTableHeight(tableView: commentsTableView)
        if commentIDFromNotification ?? 0 > 0 {
            showActivityIndicator(onFullScreen: true)
            self.perform(#selector(scrollToCommentTable), with: nil, afterDelay: TimeInterval.init(exactly: 1.0) ?? 1.0)
        } else {
            dismissActivityIndicator()
        }
    }
    // scroll to a particular comment position
    @objc func scrollToCommentTable() {
        var scrollToPosition = commentsTableView.frame.origin.y
        var index: Int?
        if commentIDFromNotification ?? 0 > 0 {
            index = commentsArray.firstIndex(where: {$0.commentID == commentIDFromNotification})
            let cellPosition = commentsTableView.rectForRow(at: IndexPath.init(row: index ?? 0, section: 0))
            scrollToPosition += cellPosition.origin.y
        }
        if index != nil {
            let screenHeight: CGFloat = UIScreen.main.bounds.size.height
            if (baseScrollView.contentSize.height - scrollToPosition) < screenHeight {
                baseScrollView.scrollToBottom()
            } else {
                baseScrollView.setContentOffset(CGPoint.init(x: 0, y: scrollToPosition), animated: true)
            }
        } else {
            showAlert(with: "Comment not available.")
        }
        dismissActivityIndicator()
        commentIDFromNotification = 0
    }
    func showPost(newPost: Post, type: PostType) {
        post = newPost
        postType = type
        if viewType == .detailView {
            configureHeaderView()
        }
        setupFromViewDidLoad()
        configureDetailsPage()
        showPost(newPost)
        // Mark the post as read
        if newPost.isRead == false {
            presenter?.markPost(newPost, asRead: true)
        }
        if isFromNotificationScreen && newPost.isOwnPost {
            titleBottomConstraint.constant = 35.0
            editStackView.isHidden = false
        }
        // Change topview backgound if the post is related
        changeBackgroundForRelatedPost(newPost, isRelated: newPost.isRelated)
        // Set status of relate button
        relateButton.isSelected = newPost.isRelated
        alternateRelateButton.isSelected = newPost.isRelated
        if alternateRelateButton.isSelected {
            alternateRelateButton.backgroundColor = UIColor.relatableGreenColor
        } else {
            alternateRelateButton.backgroundColor = UIColor.appGreenColor
        }
        // change the status of backpack button
        backPackButton?.isSelected = newPost.isBackPacked
        backPackButton?.tintColor = newPost.isBackPacked ? UIColor.appBlueColor : UIColor.white
        setupFromViewWillAppear()
    }
    func showAlert(with message: String) {
        dismissActivityIndicator()
        postButton?.isEnabled = true
        let alert = UIAlertController.alert(with: message)
        self.present(alert, animated: true, completion: nil)
    }
    func popToFeed() {
        self.navigationController?.popViewController(animated: true)
    }
}
