//
//  PostDetailsViewController.swift
//  Lighf
//
//  Created by L.
//  Copyright © 2019 L. All rights reserved.
//

import Foundation
import UIKit
import CardViewList
import WebKit
import PureLayout
import MatomoTracker

class PostDetailsViewController: BaseViewController, UIScrollViewDelegate, WKUIDelegate, WKNavigationDelegate, UIGestureRecognizerDelegate {
    //
    var presenter: PostDetailsViewToPresenterProtocol?
    var postType: PostType?
    var viewType: PostViewOption = .detailView
    var post: Post?
    var parentForReply: PostComment?
    var relateableStatementsArray: [RelatableStatement] = []
    var commentsArray: [PostComment] = []
    var commentIDFromNotification: Int?
    fileprivate var cardViewList: CardViewList!
    var mediaSubViews: [MediaContentViewController] = []
    var articleLinkViewController: ArticleLinkViewController?
    var isPostEdited: Bool = false
    var isFromNotificationScreen: Bool = false
    var forceUpdate: Bool = false
    var flagFromComment: Bool = false
    var imageViewController: ImageViewController?
    //
    @IBOutlet weak var topViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var moreOptionsCentreXConstraint: NSLayoutConstraint!
    @IBOutlet weak var baseView: UIView!
    @IBOutlet var postMediaViewHeight: NSLayoutConstraint!
    @IBOutlet var cardViewHeight: NSLayoutConstraint!
    @IBOutlet weak var baseScrollView: UIScrollView!
    @IBOutlet weak var cardsContainerView: UIView!
    @IBOutlet weak var mainDescriptionTextView: UITextView!
    @IBOutlet weak var postMediaView: UIView!
    @IBOutlet weak var relateableStatementsTableView: UITableView!
    @IBOutlet weak var relatableStatementsTableHeight: NSLayoutConstraint!
    @IBOutlet weak var commentsTableView: UITableView!
    @IBOutlet weak var commentsTableHeight: NSLayoutConstraint!
    @IBOutlet weak var bottomView: UIView!
    @IBOutlet weak var bottomViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var tagsViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var titleBottomConstraint: NSLayoutConstraint!
    @IBOutlet weak var blankViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var scrollContentView: UIView!
    @IBOutlet weak var imageViewHeightConstraint: NSLayoutConstraint!
    //
    @IBOutlet weak var tagsView: UIView!
    @IBOutlet weak var relateButton: UIButton!
    @IBOutlet weak var alternateRelateButton: UIButton!
    @IBOutlet weak var moreButton: UIButton!
    @IBOutlet weak var moreOptionsButton: UIButton!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var usernameLabel: UILabel!
    @IBOutlet weak var descriptionTextView: UITextView!
    @IBOutlet weak var postImageView: UIImageView!
    @IBOutlet weak var photoCreditLabel: UILabel!
    @IBOutlet weak var tagsLabel: UILabel!
    @IBOutlet weak var mediaStackView: UIStackView!
    @IBOutlet weak var editStackView: UIStackView!
    @IBOutlet weak var topView: UIView!
    @IBOutlet weak var relateView: UIView!
    @IBOutlet weak var leafButton: UIButton!
    @IBOutlet weak var photoCreditView: UIView!
    //
    var postButton: UIButton?
    var urls: [String] = [String]()
    let imageViewDefaultHeight: CGFloat = 155.0
    var backPackButton: UIButton?
    var getComments: Bool = true
    let tagFieldView = TagField()
    //
    override func viewDidLoad() {
        super.viewDidLoad()
        baseScrollView.isScrollEnabled = true
        baseScrollView.contentSize = CGSize(width: baseScrollView.frame.size.width, height: 2300)
        resetConstraints()
        setTableViewDelegates(tableView: commentsTableView)
        if traitCollection.forceTouchCapability == .available {
            registerForPreviewing(with: self, sourceView: view)
        }
        if let post = post, let type = postType, viewType == .detailView {
            presenter?.getDetails(for: post, type: type)
        }
        setupFromViewDidLoad()
        // set username
        let userName = UserDefaults.standard.object(forKey: UserDefaultsKey.userName) as? String ?? Constants.emptyString
        usernameLabel.text = "@\(userName as String)"
        MatomoTracker.shared.track(view: ["View a piece of lighf's details screen"])
        presenter?.forceUpdate(isRequired: forceUpdate)
        presenter?.setNotificationScreenFlag(isFromNotificationScreen)
        NotificationCenter.default.addObserver(self, selector: #selector(updateMyPosts(notification:)), name: NSNotification.Name(rawValue: "scrollToComment"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(updateMyPosts(notification:)), name: NSNotification.Name(rawValue: "removeComment"), object: nil)
        if postImageView != nil {
            let longPress = UILongPressGestureRecognizer(target: self, action: #selector(longPressed(sender:)))
            longPress.minimumPressDuration = 0.5
            longPress.delaysTouchesBegan = true
            longPress.delegate = self
            postImageView.isUserInteractionEnabled = true
            postImageView.addGestureRecognizer(longPress)
        }
    }
    func setupFromViewDidLoad() {
        relateView.isHidden = true
        imageViewHeightConstraint.constant = 0.0
        addTagField()
        if viewType == .preview {
            prepareForPreview()
            showPost(post)
            blankViewHeightConstraint.constant = 0.0
        } else {
            if post?.isOwnPost == true {
                titleBottomConstraint.constant = 35.0
                editStackView.isHidden = false
                self.navigationItem.rightBarButtonItem = nil
            } else {
                addBackPack()
            }
            if post?.isEditable == false {
                hideEditOption()
            }
            usernameLabel.isHidden = postType != .window
            blankViewHeightConstraint.constant = UIScreen.main.bounds.height
            if post?.postId != 0 {
                setUpSwipeGestures()//to invoke next or previous post. Gestures are not needed for preview
            }
            navigationController?.titleColor(UIColor.white, font: Font.sfProDisplayMedium, size: 18.0)
        }
    }
    func setupFromViewWillAppear() {
        if viewType == .detailView {
            configureHeaderView()
            if let post = post, getComments == true {
                getComments = false
                parentForReply = nil
                presenter?.getAllComments(for: post)
            }
        }
    }
    @objc func updateMyPosts(notification: NSNotification) {
        if notification.name.rawValue == "scrollToComment" {
            commentIDFromNotification = notification.object as? Int ?? 0
        } else if notification.name.rawValue == "removeComment" {
            if let post = self.post {
                presenter?.getAllComments(for: post)
            }
        }
    }
    //setup swipe gestures
    func setUpSwipeGestures() {
        let swipeRight = UISwipeGestureRecognizer(target: self, action: #selector(respondToSwipeGesture(gesture:)))
        swipeRight.direction = UISwipeGestureRecognizer.Direction.right
        self.view.addGestureRecognizer(swipeRight)
        let swipeLeft = UISwipeGestureRecognizer(target: self, action: #selector(respondToSwipeGesture(gesture:)))
        swipeLeft.direction = UISwipeGestureRecognizer.Direction.left
        self.view.addGestureRecognizer(swipeLeft)
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setupFromViewWillAppear()
    }
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
    }
    /// Hide edit/delete options for post
    private func hideEditOption() {
        titleBottomConstraint.constant = 25.0
        editStackView.isHidden = true
    }
    // Prepare view for showing preview of post
    private func prepareForPreview() {
        // Hide all views that are not necessary for preview
        bottomViewHeightConstraint.constant = 0.0
        commentsTableHeight.constant = 0.0
        baseView.backgroundColor = UIColor.white
        moreButton.isHidden = true
        relateButton.isHidden = true
        relateView.isHidden = true
        hideEditOption()
        // modify navigation bar
        postButton = UIButton.customNavigationRightBarButton(withTitle: "Float")
        postButton?.addTarget(self, action: #selector(postButtonClicked(sender:)), for: .touchUpInside)
        let rightButton = UIBarButtonItem(customView: postButton ?? UIButton.init())
        self.navigationItem.rightBarButtonItem = rightButton
        //
        if let postType = postType, postType == .story {
            self.navigationItem.title = Title.storyPreview
        } else {
            self.navigationItem.title = Title.articlePreview
        }
        if isPostEdited {
            postButton?.isEnabled = true
        } else {
            postButton?.isEnabled = false
        }
    }
    // Show details of post
    func showPost(_ post: Post?) {
        guard let post = post else {
            return
        }
        showTextContent(for: post)
        configureDetailsPage()
        // Show the media contents
        updateMediaView(with: post.media)
        //Show photo credit
        photoCreditView.isHidden = post.photoCredit == Constants.emptyString
        photoCreditLabel.text = "Photo by: \(post.photoCredit)"
        // Set tags
        if post.tags.isEmpty {
            tagsViewHeightConstraint.constant = 0
            tagsLabel.text = Constants.emptyString
        } else {
            tagFieldView.tags.removeAll()
            let tagsWithHash = post.tags.map { (tag) -> String in
                if tag.first == "#" {
                    return tag
                } else {
                    return "#" + tag
                }
            }
            tagFieldView.tags = tagsWithHash
        }
        // show the image selected from camera
        if (post.postImage != nil) {
            imageViewHeightConstraint.constant = imageViewDefaultHeight
            postImageView.image = post.postImage
        }
        // Show the image selected from the gallery
        guard post.imageUrl != Constants.emptyString, let fileUrl = URL(string: post.imageUrl) else {
            return
        }
        imageViewHeightConstraint.constant = imageViewDefaultHeight
        if fileUrl.isFileURL {
            do {
                let data = try Data(contentsOf: fileUrl)
                postImageView.image = UIImage(data: data)
            } catch {}
        } else {
            guard post.fullSizeImageUrl != Constants.emptyString else {
                return
            }
            postImageView.kf.setImage(with: URL(string: post.fullSizeImageUrl))
        }
    }
    // Show text content
    private func showTextContent(for post: Post) {
        if let story = post as? Story {
            let mutableCopy = NSMutableAttributedString(attributedString: story.shortStory)
            mutableCopy.setFont(font: UIFont(name: Font.sfProDisplayMedium.rawValue, size: 22)!, changeInSize: 6.0)
            titleLabel.attributedText = mutableCopy
            let descmutableCopy = NSMutableAttributedString(attributedString: story.longStory)
            descmutableCopy.removeAttribute(NSAttributedString.Key.font, range: NSRange(location: 0, length: descmutableCopy.string.count))
            descmutableCopy.addAttributes([NSAttributedString.Key.font: UIFont(name: Font.sfProTextRegular.rawValue, size: 16)!], range: NSRange(location: 0, length: descmutableCopy.string.count))
            descriptionTextView.attributedText = descmutableCopy
        } else if let article = post as? Article {
            let mutableCopy = NSMutableAttributedString(attributedString: article.title)
            mutableCopy.setFont(font: UIFont(name: Font.sfProDisplayMedium.rawValue, size: 22)!, changeInSize: 6.0)
            titleLabel.attributedText = mutableCopy
            let descmutableCopy = NSMutableAttributedString(attributedString: article.body)
            descmutableCopy.removeAttribute(NSAttributedString.Key.font, range: NSRange(location: 0, length: descmutableCopy.string.count))
            descmutableCopy.addAttributes([NSAttributedString.Key.font: UIFont(name: Font.sfProTextRegular.rawValue, size: 16)!], range: NSRange(location: 0, length: descmutableCopy.string.count))
            descriptionTextView.attributedText = descmutableCopy
        } else if let window = post as? Window {
            let mutableCopy = NSMutableAttributedString(attributedString: window.title)
            mutableCopy.setFont(font: UIFont(name: Font.sfProDisplayMedium.rawValue, size: 22)!, changeInSize: 6.0)
            titleLabel.attributedText = mutableCopy
            let descmutableCopy = NSMutableAttributedString(attributedString: window.body)
            descmutableCopy.removeAttribute(NSAttributedString.Key.font, range: NSRange(location: 0, length: descmutableCopy.string.count))
            descmutableCopy.addAttributes([NSAttributedString.Key.font: UIFont(name: Font.sfProTextRegular.rawValue, size: 16)!], range: NSRange(location: 0, length: descmutableCopy.string.count))
            descriptionTextView.attributedText = descmutableCopy
            usernameLabel.text = window.authorName
        } else if let adminPost = post as? AdminPost {
            if !adminPost.shortStory.isEqual(to: NSAttributedString(string: "")) {
                let mutableCopy = NSMutableAttributedString(attributedString: adminPost.shortStory)
                mutableCopy.setFont(font: UIFont(name: Font.sfProDisplayMedium.rawValue, size: 22)!, changeInSize: 6.0)
                titleLabel.attributedText = mutableCopy
            } else {
                titleLabel.isHidden = true
            }
            
            let descmutableCopy = NSMutableAttributedString(attributedString: adminPost.longStory)
            descmutableCopy.removeAttribute(NSAttributedString.Key.font, range: NSRange(location: 0, length: descmutableCopy.string.count))
            descmutableCopy.addAttributes([NSAttributedString.Key.font: UIFont(name: Font.sfProTextRegular.rawValue, size: 16)!], range: NSRange(location: 0, length: descmutableCopy.string.count))
            descriptionTextView.attributedText = descmutableCopy
            usernameLabel.text = "Admin"
        } else {
            return
        }
    }
    // MARK: - Prepare view
    private func addTagField() {
        tagFieldView.frame = CGRect(x: 12.0, y: 5.0, width: tagsView.frame.width - 24.0, height: tagsView.frame.height - 10.0)
        tagFieldView.backgroundColor = UIColor.white
        tagFieldView.isEditable = false
        tagFieldView.tagFieldDelegate = self
        tagFieldView.textField.backgroundColor = UIColor.white
        tagFieldView.textField.textColor = UIColor.black
        tagsView.addSubview(tagFieldView)
    }
    /// Method to keep a fixed height for the window even if there are no comments or media // Show a blank white view to have a fixed frame for detail page
    @objc
    func updateWindowSize() {
        let navigationBarHeight = self.navigationController?.navigationBar.frame.height ?? 0.0
        let screenHeight = UIScreen.main.bounds.height - navigationBarHeight - Utility.getSafeAreaInsets()
        let currentScreenHeight = getScreenHeight()
        if currentScreenHeight < screenHeight {
            blankViewHeightConstraint.constant = screenHeight - currentScreenHeight
        } else {
            blankViewHeightConstraint.constant = 0.0
        }
    }
    // Height of screen content is calculated through the constraint's values
    private func getScreenHeight() -> CGFloat {
        let top = topViewHeightConstraint.constant + imageViewHeightConstraint.constant + postMediaViewHeight.constant
        let mid = descriptionTextView.contentSize.height + cardViewHeight.constant + tagsViewHeightConstraint.constant
        let bottom = relatableStatementsTableHeight.constant + commentsTableHeight.constant + bottomViewHeightConstraint.constant
        return top + mid + bottom
    }
    private func addBackPack() {
        if backPackButton == nil {
            backPackButton = UIButton(type: .custom)
        }
        backPackButton?.isUserInteractionEnabled = false
        backPackButton!.addTarget(self, action: #selector(backpackButtonClicked(sender:)), for: .touchUpInside)
        let backButtonImage = UIImage(named: "BackpackNavigationBar")?.withRenderingMode(.alwaysTemplate)
        backPackButton!.setImage(backButtonImage, for: .normal)
        let rightBarButtonItem = UIBarButtonItem(customView: backPackButton!)
        self.navigationItem.rightBarButtonItem = rightBarButtonItem
    }
    //method to configure cards for window post
    func configureCardsView(post: Post?) {
        if let window = post as? Window, window.statements.isEmpty {
            cardViewHeight.constant = 0.0
            return
        }
        self.cardViewList = CardViewList()
        self.cardViewList.delegete = self
        var cardsView = [UIView]()
        for _ in 1 ... 5 {
            let cardView = CardView(frame: CGRect(x: 0, y: 0, width: 148, height: 104))
            cardView.setData(statement: "Statement goes here. This is a long statement example.", authorName: "Author")
            cardsView.append(cardView)
        }
        self.cardViewList.animationScroll = .scaleBounce
        self.cardViewList.isClickable = true
        self.cardViewList.clickAnimation = .bounce
        self.cardViewList.cardSizeType = .autoSize
        self.cardViewList.grid = 1
        self.cardViewList.cardSizeType = .square
        self.cardViewList.listType = .horizontal
        self.cardViewList.shadowColor = UIColor.clear
        self.cardViewList.generateCardViewList(containerView: cardsContainerView, views: cardsView, listType: .horizontal, identifier: "CardWithUIViews1")
    }
    func configureDetailsPage() {
        // if post has MEDIA like YOUTUBE link or SOUNDCLOUD. set postMediaViewHeight.constant to desired value and add the view/webview to postMediaView
        if postType == PostType.article {
            configureForArticleDetailPage(post: post)
        } else if postType == PostType.window {
            configureForWindowDetailPage(post: post)
        } else if postType == PostType.story {
            configureForStoryDetailPage(post: post)
        } else {
            configureForAdminPost(post: post)
        }
        
        if viewType != .preview {
            relateButton.isHidden = (postType == .admin_post) || (postType == .article) || (post?.isOwnPost ?? false)
            alternateRelateButton.isHidden = (postType == .admin_post) || (postType == .article) || (post?.isOwnPost ?? false)
            leafButton.isHidden = (postType == .admin_post) || (postType == .article || postType == .window) || (post?.isOwnPost ?? false)
            if (postType == .article) || (post?.isOwnPost ?? false) {
                moreOptionsCentreXConstraint.constant = 0.0
            } else {
                moreOptionsCentreXConstraint.constant = 105.0
            }
            relateView.isHidden = false
        }
    }
    func resize(textView: UITextView) {
        let fixedWidth = textView.frame.size.width
        let newSize = textView.sizeThatFits(CGSize(width: fixedWidth, height: CGFloat.greatestFiniteMagnitude))
        textView.frame.size = CGSize(width: max(newSize.width, fixedWidth), height: newSize.height)
    }
    func resetConstraints() {
        cardViewHeight.constant = 0
        postMediaViewHeight.constant = 0
        relatableStatementsTableHeight.constant = 0
        commentsTableHeight.constant = 0
        tagsViewHeightConstraint.constant = 0
        tagFieldView.removeAll()
        tagFieldView.removeFromSuperview()
        mediaStackView.removeAllArrangedSubviews()
        self.perform(#selector(updateWindowSize), with: nil, afterDelay: 0.5)
    }
}
