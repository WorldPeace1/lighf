//
//  PostDetailsViewController+Configuration.swift
//  Lighf
//
//  Created by L.
//  Copyright © 2019 L. All rights reserved.
//

import Foundation
import UIKit
import CardViewList
import WebKit
import PureLayout

extension PostDetailsViewController {
    func configureForWindowDetailPage(post: Post?) {
        cardViewHeight.constant = 128
        resize(textView: mainDescriptionTextView)
        configureCardsView(post: post)
    }
    
    func configureForArticleDetailPage(post: Post?) {
        resize(textView: mainDescriptionTextView)
        setTableViewDelegates(tableView: relateableStatementsTableView)
        if let article = post as? Article {
            relateableStatementsArray = article.statements
        }
        //
        if relateableStatementsArray.isEmpty {
            // If statements are empty, there is no need to show the tableview
            relatableStatementsTableHeight.constant = 0
        } else {
            relateableStatementsTableView.reloadData()
            updateTableHeight(tableView: relateableStatementsTableView)
        }
    }
    
    func configureForStoryDetailPage(post: Post?) {
        resize(textView: mainDescriptionTextView)
    }
    
    func configureForAdminPost(post: Post?) {
        if let adminPost = post as? AdminPost {
            
        }
    }
    
    //to configure header.
    func configureHeaderView() {
        let barView = UIView.init(frame: CGRect(x: 0, y: 0, width: self.view.frame.width - 150, height: 40))
        let imageView = UIImageView(frame: CGRect(x: 70, y: 0, width: 70, height: 40))
        imageView.contentMode = .scaleAspectFit
        let image = UIImage(named: "LighfLogoNavigationBar")
        imageView.image = image
        let logoButton = UIButton(frame: CGRect(x: 70, y: 0, width: 70, height: 40))
        logoButton.setImage(image, for: .normal)
        logoButton.addTarget(self, action: #selector(logoTapped), for: .touchUpInside)
        barView.addSubview(logoButton)
        let typeLabel = UILabel(frame: CGRect(x: 150, y: -1, width: 70, height: 40))
        typeLabel.textColor = UIColor.white
        typeLabel.font = UIFont.init(name: "SFProText-Light", size: 14.0)
        if postType == PostType.article {
            barView.backgroundColor = UIColor.clear
            baseView.backgroundColor = UIColor.black
            self.navigationController?.navigationBar.barTintColor = UIColor.black
            typeLabel.text = "Article"
        } else if postType == PostType.window {
            barView.backgroundColor = UIColor.clear
            baseView.backgroundColor = UIColor.appBlueColor
            self.navigationController?.navigationBar.barTintColor = UIColor.appBlueColor
            typeLabel.text = "Window"
        } else if postType == PostType.story {
            barView.backgroundColor = UIColor.clear
            baseView.backgroundColor = UIColor.appGreenColor
            typeLabel.text = "Story"
        } else {
            barView.backgroundColor = UIColor.clear
            baseView.backgroundColor = UIColor.appGreenColor
            typeLabel.text = "Admin Post"
        }
        barView.addSubview(typeLabel)
        navigationItem.titleView = barView
        customizeNavigationBar()
    }
    @objc func logoTapped() {
        baseScrollView.scrollToTop(animated: true)
    }
    // Set navigation bar color based on the post selected
    private func customizeNavigationBar() {
        // Change navigation bar color
        var navBarColor = UIColor.clear
        switch postType {
        case .story?:
            navBarColor = UIColor.appGreenColor
        case .article?:
            navBarColor = .black
        case .window?:
            navBarColor = UIColor.appBlueColor
        case .admin_post?:
            navBarColor = UIColor.appGreenColor
        default: break
        }
        self.navigationController?.view.backgroundColor = navBarColor
        navigationController?.setBackGroundColor(navBarColor)
        navigationController?.navigationBar.tintColor = UIColor.white
    }
    //method to handle swipe gestures
    @objc func respondToSwipeGesture(gesture: UIGestureRecognizer) {
        if let swipeGesture = gesture as? UISwipeGestureRecognizer {
            switch swipeGesture.direction {
            case .left :
                print("Swiped left")// logic for next post should come here
                if var postHistoryArray = UserDefaults.standard.value(forKey: UserDefaultsKey.postHistoryArray) as? [Int] {
                    postHistoryArray.append(post?.postId ?? 0)
                    UserDefaults.standard.set(postHistoryArray, forKey: UserDefaultsKey.postHistoryArray)
                } else {
                    let postHistoryArray = [post?.postId ?? 0]
                    UserDefaults.standard.set(postHistoryArray, forKey: UserDefaultsKey.postHistoryArray)
                }
                post?.postId = 0
                getComments = true
                resetConstraints()
                blankViewHeightConstraint.constant = UIScreen.main.bounds.height
                if let post = post, let type = postType {
                    presenter?.getDetails(for: post, type: type)
                }
            case .right:
                print("Swiped right")// logic for prev post should come here
                if var postHistoryArray = UserDefaults.standard.value(forKey: UserDefaultsKey.postHistoryArray) as? [Int], !postHistoryArray.isEmpty {
                    post?.postId = postHistoryArray.last
                    postHistoryArray.removeLast()
                    UserDefaults.standard.set(postHistoryArray, forKey: UserDefaultsKey.postHistoryArray)
                    getComments = true
                    resetConstraints()
                    blankViewHeightConstraint.constant = UIScreen.main.bounds.height
                    if let post = post, let type = postType {
                        presenter?.getDetails(for: post, type: type)
                    }
                } else {
                    self.navigationController?.popViewController(animated: true)
                }
            default:
                break
            }
        }
    }
    @objc func longPressed(sender: UILongPressGestureRecognizer) {
        if sender.state == UIGestureRecognizer.State.began {
            imageViewController = UIStoryboard.postDetails.instantiateViewController(withIdentifier: ImageViewController.storyboardId) as? ImageViewController
            imageViewController?.image = postImageView.image
            if let imageview = imageViewController?.view {
                self.view.addSubview(imageview)
            }
        } else if sender.state == UIGestureRecognizer.State.ended {
            if let imageview = imageViewController?.view {
                imageview.removeFromSuperview()
            }
        }
    }
}
extension PostDetailsViewController: CardViewListDelegete {
    func cardView(_ scrollView: UIScrollView, didSelectCardView cardView: UIView, identifierCards identifier: String, index: Int) {
        print("didSelectCardView!")
    }
}
extension PostDetailsViewController: UIViewControllerPreviewingDelegate {
    func previewingContext(_ previewingContext: UIViewControllerPreviewing, commit viewControllerToCommit: UIViewController) {
    }
    func previewingContext(_ previewingContext: UIViewControllerPreviewing, viewControllerForLocation location: CGPoint) -> UIViewController? {
        let relativeLocation = view.convert(location, to: postImageView)
        if postImageView.bounds.contains(relativeLocation), postImageView.image != nil {
            if let imageViewController = UIStoryboard.postDetails.instantiateViewController(withIdentifier: ImageViewController.storyboardId) as? ImageViewController {
                imageViewController.image = postImageView.image
                previewingContext.sourceRect = postImageView.frame
                return imageViewController
            }
            return UIViewController()
        }
        return nil
    }
}

extension PostDetailsViewController: ArticleLinkProtocol {
    func showArticleLink(_ link: String) {
        presenter?.openArticleLink(link)
    }
}

extension PostDetailsViewController: TagFieldDelegate {
    func textFieldDidEndEditing(_ textField: UITextField) {
    }
    func didUpdateFrameSize(_ size: CGSize) {
        if tagFieldView.tags.isEmpty {
            tagsViewHeightConstraint.constant = 0
        } else {
            tagsViewHeightConstraint.constant = size.height + 10
        }
    }
}
