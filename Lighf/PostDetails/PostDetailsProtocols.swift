//
//  PostDetailsProtocol.swift
//  Lighf
//
//
//  Copyright © 2020 L. All rights reserved.
//

import Foundation
import UIKit

protocol PostDetailsViewToPresenterProtocol: class {
    func leafButtonTapped(forPost: Post, withCount: Int)
    func leafButtonTapped(forComment: PostComment, withCount: Int)
    func dropDownButtonTapped()
    func alertActionTapped(selectedFlagOption: FlagOptions, for post: Post, parentComment: PostComment?)
    func savePost(_ post: Post, type: PostType)
    func addComment(commentType: CommentType, for post: Post, as reply: PostComment?)
    func getDetails(for post: Post, type: PostType)
    func getAllComments(for post: Post)
    func editPost(_ post: Post, type: PostType)
    func deletePost(_ post: Post)
    func openArticleLink(_ link: String)
    func gotoHomeButtonClicked()
    func gotoHomeAfterAlert()
    func addPost(_ post: Post, toBackPack addToBackPack: Bool)
    func markPost(_ post: Post, asRelate markRelate: Bool)
    func markPost(_ post: Post, asRead markAsRead: Bool)
    func forceUpdate(isRequired: Bool)
    func deleteComment(_ comment: PostComment)
    func editComment(_ comment: PostComment)
    func setNotificationScreenFlag(_ fromNotificationScreen: Bool)
    func markStatement(_ statement: RelatableStatement, asRelate: Bool)
    func toggleNotification(forItem: Any, toState: NotificationState)
}

protocol PostDetailsPresenterToViewProtocol: BasePresenterToViewProtocol {
    func showAlert(with message: String)
    func showGotoHomeAlert(with message: String)
    func showPost(newPost: Post, type: PostType)
    func showComments(_ comments: [PostComment])
    func popToFeed()
    func updateComments(_ update: Bool)
    func updateBackpackStatus(addToBackPack: Bool )
    func updateRelateStatus(relate: Bool)
    func updateDeleteComment(_ comment: Int)
    func replaceEditedComment(_ comment: PostComment)
    func resetStatementStatus(for statement: RelatableStatement)
    func updateNotificationStatus(postID: Int?, commentID: Int?, notificationStatus: NotificationState)
}

protocol PostDetailsPresenterToInteractorProtocol: class {
    func sendLeafToAuthor()
    func getDropDownOptions()
    func savePost(_ post: Post, type: PostType)
    func getDetails(for post: Post, type: PostType)
    func getAllComments(for post: Post)
    func deletePost(_ post: Post)
    func addPost(_ post: Post, toBackPack addToBackPack: Bool)
    func markPost(_ post: Post, asRelate markRelate: Bool)
    func markPost(_ post: Post, asRead markAsRead: Bool)
    func invokeDeleteComment(_ comment: PostComment)
    func markStatement(_ statement: RelatableStatement, asRelate: Bool)
}

protocol PostDetailsPresenterToRouterProtocol: class {
    func alertActionTapped(selectedOption: FlagOptions, post: Post, parentComment: PostComment?, with interface: PostDetailsInterface?)
    func addCommentTapped(selectedCommentOption: CommentType, post: Post, as reply: PostComment?, with interface: PostDetailsInterface?)
    func moveToRootScreen()
    func moveToHomeScreenOnAlert()
    func moveToEditPostView(with post: Post, type: PostType)
    func loadUrl(_ url: URL)
    func editComment(with comment: PostComment, with interface: PostDetailsInterface?)
}

protocol PostDetailsInteractorToPresenterProtocol: class {
    func savePostFailed(with message: String)
    func savePostComplete(with message: String, withPostID: Int)
    func handle(post: Post, type: PostType)
    func detailFetchFailed(for post: Post, with message: String)
    func handleComments(_ comments: [PostComment])
    func commentFetchFailed(with message: String)
    func deletedPost(_ post: Post, with message: String)
    func deletePost(_ post: Post, failedWithMessage message: String)
    func performedOperation(_ operation: PostOperation, onPost post: Post, with error: String?)
    func deleteCommentResponse(_ comment: PostComment, with message: String)
    func deleteCommentFailResponse(_ comment: PostComment, with message: String)
    func relateStatement(_ statement: RelatableStatement, completedWith error: String?)
}

protocol PostDetailsInterface: class {
    func updateComments(_ update: Bool)
    func updateComments(_ comment: PostComment)
}
protocol HomePresenterToPostDetailsInteractorProtocol: class {
    func getDetailsToEdit(for post: Post, type: PostType)
}
protocol SearchPresenterToPostDetailsInteractorProtocol: class {
    func getDetailsToEditFromSearch(for post: Post, type: PostType)
}
protocol PostPresenterToGiveLeafInteractorProtocol: class {
    func invokeGiveLeaf(forPost: Post, leafCount: Int)
    func invokeGiveLeaf(forComment: PostComment, leafCount: Int)
}
protocol GiveLeafInteractorToPostPresentProtocol: class {
    func giveLeafAPIResponse(_ status: String, error: String?)
}
protocol PostDetailsPresenterToToggleNotificationProtocol: class {
    func invokeToggleNotification(forItem: Any, toState: NotificationState)
}
protocol ToggleNotificationToPostDetailsPresenterProtocol: class {
    func toggleNotificationAPIResponse(_ itemID: Int, itemType: String, notificationStatus: NotificationState)
    func toggleNotificationAPIResponseFailure(_ status: String, error: String?)
}
