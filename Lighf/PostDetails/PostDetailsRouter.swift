//
//  PostDetailsRouter.swift
//  Lighf
//
//  Created by L.
//  Copyright © 2019 L. All rights reserved.
//

import Foundation
import UIKit

class PostDetailsRouter: NSObject {
    weak var presentedViewController: UIViewController?
    static let viewIdentifier = "PostDetails"
    static func createModule(with interface: PostOperationsProtocol? = nil) -> PostDetailsViewController? {
        let view = UIStoryboard(name: viewIdentifier, bundle: Bundle.main).instantiateViewController(withIdentifier: viewIdentifier) as? PostDetailsViewController
        let presenter = PostDetailsPresenter()
        let router = PostDetailsRouter()
        let interactor = PostDetailsInteractor()
        let leafInteractor = GiveLeafInteractor()
        let toggleNotificationInteractor = ToggleNotificationInteractor()

        //setting view delegates
        view?.presenter = presenter
        //setting presenter delegates
        presenter.view = view
        presenter.router = router
        presenter.interactor = interactor
        presenter.moduleInterface = interface
        presenter.leafInteractor = leafInteractor
        presenter.toggleNotificationInteractor = toggleNotificationInteractor
        //setting interactor delegates
        interactor.presenter = presenter
        //setting router presentedViewController
        router.presentedViewController = view
        leafInteractor.postDetailsPresenter = presenter
        toggleNotificationInteractor.postDetailsPresenter = presenter
        return view
    }
}

extension PostDetailsRouter: PostDetailsPresenterToRouterProtocol {
    func editComment(with comment: PostComment, with interface: PostDetailsInterface?) {
        if let commentsViewController = CommentsViewRouter.createModule(with: interface) {
            commentsViewController.commentType = comment.commentType
            commentsViewController.commentToEdit = comment
            if comment.commentType == CommentType.advice {
                commentsViewController.titleLabel = CommentsViewTitle.advice
            } else if comment.commentType == CommentType.comment {
                commentsViewController.titleLabel = CommentsViewTitle.comment
            } else {
                commentsViewController.titleLabel = CommentsViewTitle.question
            }
            presentedViewController?.navigationController?.setTransparent()
            presentedViewController?.navigationItem.setHidesBackButton(false, animated: true)
            presentedViewController?.navigationController?.pushViewController(commentsViewController, animated: true)
        }
    }
    func moveToHomeScreenOnAlert() {
        presentedViewController?.navigationController?.popViewController(animated: true)
    }
    func loadUrl(_ url: URL) {
        if UIApplication.shared.canOpenURL(url) {
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
        }
    }
    func moveToRootScreen() {
        if let viewController = presentedViewController?.navigationController?.viewControllers[1], viewController is SearchViewController {
            moveToSearchScreen()
        } else {
            moveToHomeScreen()
        }
    }
    func moveToHomeScreen() {
        presentedViewController?.navigationController?.popToRootViewController(animated: true)
    }
    func moveToSearchScreen() {
        if let viewController = presentedViewController?.navigationController?.viewControllers[1] {
            presentedViewController?.navigationController?.popToViewController(viewController, animated: true)
        }
    }
    func addCommentTapped(selectedCommentOption: CommentType, post: Post, as reply: PostComment?, with interface: PostDetailsInterface?) {
        if let commentsViewController = CommentsViewRouter.createModule(with: interface) {
            commentsViewController.commentType = selectedCommentOption
            commentsViewController.post = post
            commentsViewController.parentComment = reply
            if reply != nil {
                commentsViewController.titleLabel = CommentsViewTitle(rawValue: "Add your reply") ?? CommentsViewTitle.comment
            } else {
                if selectedCommentOption == CommentType.advice {
                    commentsViewController.titleLabel = CommentsViewTitle.advice
                } else if selectedCommentOption == CommentType.comment {
                    commentsViewController.titleLabel = CommentsViewTitle.comment
                } else {
                    commentsViewController.titleLabel = CommentsViewTitle.question
                }
            }
            presentedViewController?.navigationController?.setTransparent()
            presentedViewController?.navigationItem.setHidesBackButton(false, animated: true)
            presentedViewController?.navigationController?.pushViewController(commentsViewController, animated: true)
        }
    }
    //
    func alertActionTapped(selectedOption: FlagOptions, post: Post, parentComment: PostComment?, with interface: PostDetailsInterface?) {
        print(selectedOption)
        //write logic to navigate to new screen to input data.
        if let commentsViewController = CommentsViewRouter.createModule(with: interface) {
            commentsViewController.flagOption = selectedOption
            commentsViewController.post = post
            commentsViewController.parentComment = parentComment
            presentedViewController?.navigationController?.setTransparent()
            presentedViewController?.navigationItem.setHidesBackButton(false, animated: true)
            presentedViewController?.navigationController?.pushViewController(commentsViewController, animated: true)
        }
    }
    func moveToEditPostView(with post: Post, type: PostType) {
//        if let createPostView = CreatePostRouter.createModule(for: type, post: post) {
//            self.presentedViewController?.navigationController?.pushViewController(createPostView, animated: true)
//        }
        let useAlternatePostCreationOption = UserDefaults.standard.value(forKey: UserDefaultsKey.showAlternatePostCreationOption) as? Bool ?? true
        if useAlternatePostCreationOption {
            if let createPostView = CreatePostRouter.createAlternateModule(for: type, post: post) {
                self.presentedViewController?.navigationController?.pushViewController(createPostView, animated: true)
            }
        } else {
            if let createPostView = CreatePostRouter.createDefaultModule(for: type, post: post) {
                self.presentedViewController?.navigationController?.pushViewController(createPostView, animated: true)
            }
        }
    }
}
