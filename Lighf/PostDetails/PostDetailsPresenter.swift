//
//  PostDetailsPresenter.swift
//  Lighf
//
//  Created by L.
//  Copyright © 2019 L. All rights reserved.
//

import Foundation
import UIKit

class PostDetailsPresenter: NSObject {
    var interactor: PostDetailsPresenterToInteractorProtocol?
    var router: PostDetailsPresenterToRouterProtocol?
    weak var view: PostDetailsPresenterToViewProtocol?
    weak var moduleInterface: PostOperationsProtocol?
    var forceUpdate: Bool = false
    var isEditing: Bool = false
    var isFromNotificationScreen: Bool = false
    var leafInteractor: PostPresenterToGiveLeafInteractorProtocol?
    var toggleNotificationInteractor: PostDetailsPresenterToToggleNotificationProtocol?
}

extension PostDetailsPresenter: PostDetailsViewToPresenterProtocol {
    func toggleNotification(forItem: Any, toState: NotificationState) {
        view?.showActivityIndicator(onFullScreen: true)
        toggleNotificationInteractor?.invokeToggleNotification(forItem: forItem, toState: toState)
    }
    func leafButtonTapped(forComment: PostComment, withCount: Int) {
        view?.showActivityIndicator(onFullScreen: true)
        leafInteractor?.invokeGiveLeaf(forComment: forComment, leafCount: withCount)
    }
    func leafButtonTapped(forPost: Post, withCount: Int) {
        view?.showActivityIndicator(onFullScreen: true)
        leafInteractor?.invokeGiveLeaf(forPost: forPost, leafCount: withCount)
    }
    func setNotificationScreenFlag(_ fromNotificationScreen: Bool) {
        isFromNotificationScreen = fromNotificationScreen
    }
    func deleteComment(_ comment: PostComment) {
        view?.showActivityIndicator(onFullScreen: false)
        interactor?.invokeDeleteComment(comment)
    }
    func editComment(_ comment: PostComment) {
        // navigate to router and then to comments page.
        router?.editComment(with: comment, with: self)
    }
    func forceUpdate(isRequired: Bool) {
        forceUpdate = isRequired
    }
    func gotoHomeAfterAlert() {
        router?.moveToHomeScreenOnAlert()
    }
    func markPost(_ post: Post, asRelate markRelate: Bool) {
        view?.showActivityIndicator(onFullScreen: true)
        interactor?.markPost(post, asRelate: markRelate)
    }
    func markPost(_ post: Post, asRead markAsRead: Bool) {
        interactor?.markPost(post, asRead: markAsRead)
    }
    func addPost(_ post: Post, toBackPack addToBackPack: Bool) {
        view?.showActivityIndicator(onFullScreen: true)
        interactor?.addPost(post, toBackPack: addToBackPack)
    }
    func gotoHomeButtonClicked() {
        router?.moveToRootScreen()
    }
    func openArticleLink(_ link: String) {
        if let url = URL(string: link) {
            if UIApplication.shared.canOpenURL(url) {
                router?.loadUrl(url)
            } else {
                view?.showAlert(with: "Not a valid article URL")
            }
        }
    }
    //
    func deletePost(_ post: Post) {
        view?.showActivityIndicator()
        interactor?.deletePost(post)
    }
    //
    func addComment(commentType: CommentType, for post: Post, as reply: PostComment?) {
        router?.addCommentTapped(selectedCommentOption: commentType, post: post, as: reply, with: self)
    }
    //
    func getDetails(for post: Post, type: PostType) {
        view?.showActivityIndicator()
        interactor?.getDetails(for: post, type: type)
    }
    //
    func alertActionTapped(selectedFlagOption: FlagOptions, for post: Post, parentComment: PostComment?) {
        router?.alertActionTapped(selectedOption: selectedFlagOption, post: post, parentComment: parentComment, with: self)
    }
    func dropDownButtonTapped() {
        interactor?.getDropDownOptions()
    }
    func savePost(_ post: Post, type: PostType) {
        view?.showActivityIndicator(onFullScreen: true)
        interactor?.savePost(post, type: type)
    }
    func editPost(_ post: Post, type: PostType) {
        view?.showActivityIndicator(onFullScreen: true)
        isEditing = true
//        router?.moveToEditPostView(with: post, type: type)
        interactor?.getDetails(for: post, type: type)
    }
    func getAllComments(for post: Post) {
        view?.showActivityIndicator()
        interactor?.getAllComments(for: post)
    }
    func markStatement(_ statement: RelatableStatement, asRelate: Bool) {
        view?.showActivityIndicator()
        interactor?.markStatement(statement, asRelate: asRelate)
    }
}

extension PostDetailsPresenter: PostDetailsInterface {
    func updateComments(_ comment: PostComment) {
        view?.replaceEditedComment(comment)
    }
    func updateComments(_ update: Bool) {
        view?.updateComments(update)
    }
}

extension PostDetailsPresenter: PostDetailsInteractorToPresenterProtocol {
    func relateStatement(_ statement: RelatableStatement, completedWith error: String?) {
        view?.dismissActivityIndicator()
        if error == nil {
            view?.resetStatementStatus(for: statement)
            moduleInterface?.updateFeedForRelate(with: statement.statementID, toRelate: statement.isRelated)
        } else {
            view?.showAlert(with: error!)
        }
    }
    //
    func deleteCommentResponse(_ comment: PostComment, with message: String) {
        view?.updateDeleteComment(comment.commentID)
        view?.showToast(with: message, completion: {})
    }
    func deleteCommentFailResponse(_ comment: PostComment, with message: String) {
        view?.showAlert(with: message)
    }
    func performedOperation(_ operation: PostOperation, onPost post: Post, with error: String?) {
        view?.dismissActivityIndicator()
        if error == nil, let postId = post.postId {
            var toastMessage = ""
            switch operation {
            case .addBackPack:
                toastMessage = Message.addToBackpack
                view?.updateBackpackStatus(addToBackPack: true)
                moduleInterface?.addPost(with: postId, toBackPack: true)
                setRefreshKey()
            case .removeBackPack:
                toastMessage = Message.removeFromBackPack
                view?.updateBackpackStatus(addToBackPack: false)
                moduleInterface?.addPost(with: postId, toBackPack: false)
                setRefreshKey()
            case .related:
                toastMessage = Message.markAsRelate
                view?.updateRelateStatus(relate: true)
                moduleInterface?.updateFeedForRelate(with: postId, toRelate: true)
                setRefreshKey()
            case .unRelated:
                toastMessage = Message.markAsNotRelated
                view?.updateRelateStatus(relate: false)
                moduleInterface?.updateFeedForRelate(with: postId, toRelate: false)
                setRefreshKey()
            case .read:
                moduleInterface?.markPost(with: postId, asRead: true)
            case .unRead:
                moduleInterface?.markPost(with: postId, asRead: false)
            }
            if toastMessage != Constants.emptyString {
                view?.showToast(with: toastMessage, completion: {})
            }
        } else {
            // If the post is deleted by the author or backend, remove it from the feed
            if post.isDeleted, let postId = post.postId {
                view?.showGotoHomeAlert(with: error! == Constants.emptyString ? Message.removePost : error!)
                moduleInterface?.removePostFromFeed(with: postId)
            } else {
                view?.showAlert(with: error!)
            }
        }
    }
    func setRefreshKey() {
        if forceUpdate {
            let defaults  = UserDefaults.standard
            defaults.set("true", forKey: UserDefaultsKey.updateFromSearch)
        }
    }
    // successfully deletion handling
    func deletedPost(_ post: Post, with message: String) {
        // Handle post deleted scenario
        view?.dismissActivityIndicator()
        view?.showToast(with: message, completion: {})
        if let postId = post.postId {
            moduleInterface?.removePostFromFeed(with: postId)
        }
        view?.popToFeed()
    }
    //
    func deletePost(_ post: Post, failedWithMessage message: String) {
        view?.dismissActivityIndicator()
        //
        if post.isDeleted, let postId = post.postId {
            view?.showGotoHomeAlert(with: message == Constants.emptyString ? Message.removePost : message)
            moduleInterface?.removePostFromFeed(with: postId)
        } else {
            view?.showAlert(with: message)
        }
    }
    //
    func handleComments(_ comments: [PostComment]) {
        var fullCommentArray: [PostComment] = []
        for loopVar in 0..<comments.count {
            let commentObj = comments[loopVar]
            fullCommentArray.append(commentObj)
            if !commentObj.commentReplies.isEmpty {
                fullCommentArray += commentObj.commentReplies
            }
        }
        view?.showComments(fullCommentArray)
    }
    //
    func commentFetchFailed(with message: String) {
        view?.showAlert(with: message)
    }
    //
    func handle(post: Post, type: PostType) {
        // Show post in the view
        view?.dismissActivityIndicator()
        if isEditing {
            router?.moveToEditPostView(with: post, type: type)
            isEditing = false
        } else {
            view?.showPost(newPost: post, type: type)
        }
    }
    func detailFetchFailed(for post: Post, with message: String) {
        isEditing = false
        view?.dismissActivityIndicator()
        view?.showGotoHomeAlert(with: message == Constants.emptyString ? Message.removePost : message)
        // If the post is deleted by the author or backend, remove it from the feed
        if post.isDeleted, let postId = post.postId {
            moduleInterface?.removePostFromFeed(with: postId)
        }
    }
    //
    func savePostFailed(with message: String) {
        view?.dismissActivityIndicator()
        view?.showAlert(with: message)
    }
    //
    func savePostComplete(with message: String, withPostID: Int) {
        view?.dismissActivityIndicator()
        view?.showToast(with: message, completion: {})
        if !isFromNotificationScreen {
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "updateMyPosts"), object: withPostID)
        }
        router?.moveToRootScreen()
    }
}

extension PostDetailsPresenter: GiveLeafInteractorToPostPresentProtocol {
    func giveLeafAPIResponse(_ status: String, error: String?) {
        view?.dismissActivityIndicator()
        if status != "ok" {
            view?.showAlert(with: error ?? Constants.emptyString)
        } else {
            view?.showToast(with: error ?? Constants.emptyString, completion: {})
        }
    }
}
extension PostDetailsPresenter: ToggleNotificationToPostDetailsPresenterProtocol {
    func toggleNotificationAPIResponse(_ itemID: Int, itemType: String, notificationStatus: NotificationState) {
        view?.dismissActivityIndicator()
        if itemType == "post" {
            view?.updateNotificationStatus(postID: itemID, commentID: nil, notificationStatus: notificationStatus)
            moduleInterface?.toggleNotification(with: itemID, isEnabled: notificationStatus)
        } else {
            view?.updateNotificationStatus(postID: nil, commentID: itemID, notificationStatus: notificationStatus)
        }
    }
    func toggleNotificationAPIResponseFailure(_ status: String, error: String?) {
        view?.dismissActivityIndicator()
        if status != "ok" {
            view?.showAlert(with: error ?? Constants.emptyString)
        } else {
            view?.showToast(with: error ?? Constants.emptyString, completion: {})
        }
    }
}
