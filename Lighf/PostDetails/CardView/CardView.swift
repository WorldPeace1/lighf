//
//  CardViewViewController.swift
//  CardViewList
//
//  Created by an open source developer. ©2017 | All rights reserved.
//

import UIKit

class CardView: UIView {
    @IBOutlet weak var statementLabel: UILabel!
    @IBOutlet weak var authorNameLabel: UILabel!
    //
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupView()
    }
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupView()
    }
    private func setupView() {
        if let view = viewFromNibForClass() {
            view.frame = bounds
            view.autoresizingMask = [
                UIView.AutoresizingMask.flexibleWidth,
                UIView.AutoresizingMask.flexibleHeight
            ]
            addSubview(view)
        }
    }
    func setData(statement: String, authorName: String) {
        self.statementLabel.text = statement
        self.authorNameLabel.text = authorName
    }
    // Loads a XIB file into a view and returns this view.
    private func viewFromNibForClass() -> UIView? {
        let bundle = Bundle(for: type(of: self))
        let nib = UINib(nibName: String(describing: type(of: self)), bundle: bundle)
        let view = nib.instantiate(withOwner: self, options: nil).first as? UIView
        return view
    }

}
