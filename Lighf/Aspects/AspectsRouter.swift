//
//  AspectsRouter.swift
//  Lighf
//
//  Created by L.
//  Copyright © 2019 L. All rights reserved.
//

import Foundation
import UIKit

class AspectsRouter: NSObject {
    weak var presentedViewController: UIViewController?
    static let viewIdentifier = "Aspects"
    //create AspectsViewController object
    static func createModule(isFromSettings: Bool) -> AspectsViewController? {
        let view = UIStoryboard(name: viewIdentifier, bundle: Bundle.main).instantiateViewController(withIdentifier: viewIdentifier) as? AspectsViewController
        let presenter = AspectsPresenter()
        let router = AspectsRouter()
        let interactor = AspectsInteractor()
        //setting view delegates
        view?.presenter = presenter
        view?.isFromSettings = isFromSettings
        //setting presenter delegates
        presenter.view = view
        presenter.router = router
        presenter.interactor = interactor
        presenter.isFromSettings = isFromSettings
        //setting interactor delegates
        interactor.presenter = presenter
        //setting router presentedViewController
        router.presentedViewController = view
        return view
    }
}

extension AspectsRouter: AspectPresenterToRouterProtocol {
    func moveToHomeScreen() {
        if let homePageViewController = HomeRouter.createModule() {
            presentedViewController?.navigationController?.isNavigationBarHidden = false
            presentedViewController?.navigationController?.pushViewController(homePageViewController, animated: true)
            presentedViewController?.navigationController?.viewControllers = [homePageViewController]
        }
    }
}
