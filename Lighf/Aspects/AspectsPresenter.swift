//
//  AspectsPresenter.swift
//  Lighf
//
//  Created by L.
//  Copyright © 2019 L. All rights reserved.
//

import Foundation
import UIKit
import SwiftyJSON

class AspectsPresenter: NSObject {
    weak var view: AspectPresenterToViewProtocol?
    var interactor: AspectPresenterToInteractorProtocol?
    var router: AspectPresenterToRouterProtocol?
    var isFromSettings: Bool = false
}

extension AspectsPresenter: AspectViewToPresenterProtocol {
    func saveAspects(savedAspects: [Int]) {
        interactor?.invokeSaveAspectsAPI(savedAspects: savedAspects)
    }
    //invoke method in interactor to get aspects
    func fetchAspects() {
        view?.showActivityIndicator()
        interactor?.invokeAspectsAPI()
    }
}

extension AspectsPresenter: AspectInteractorToPresenterProtocol {
    // delegate to view on api response of save aspects
    func saveAspectsResponse(aspectsArray: [JSON], error: String) {
        if aspectsArray.isEmpty {
            view?.saveAspectsResponse(success: false, error: error)
        } else {
            var responseArray: [Aspect] = []
            for loopVar in 0..<aspectsArray.count {
                let aspectDict = aspectsArray[loopVar]
                let aspectID = aspectDict["id"].intValue
                let aspectName = aspectDict["name"].stringValue
                let aspectSlug = aspectDict["slug"].stringValue
                let aspectsObject = Aspect(aspectId: aspectID, name: aspectName, slug: aspectSlug)
                responseArray.append(aspectsObject)
            }
            view?.updateSavedAspects(responseArray: responseArray)
            if isFromSettings {
               view?.saveAspectsResponse(success: true, error: Constants.emptyString)
            } else {
                router?.moveToHomeScreen()
            }
        }
    }
    // delegate to update list of saved aspects
    func updateListOfSavedAspects(aspectsArray: [JSON], error: String) {
        view?.dismissActivityIndicator()
        var responseArray: [Aspect] = []
        for loopVar in 0..<aspectsArray.count {
            let aspectDict = aspectsArray[loopVar]
            let aspectID = aspectDict["id"].intValue
            let aspectName = aspectDict["name"].stringValue
            let aspectSlug = aspectDict["slug"].stringValue
            let aspectsObject = Aspect(aspectId: aspectID, name: aspectName, slug: aspectSlug)
            responseArray.append(aspectsObject)
        }
        view?.listOfSavedAspects(responseArray: responseArray)
    }
    //pass aspects from interactor to viewcontroller.
    func updateListOfAspects(aspectsArray: [JSON], error: String) {
//        if aspectsArray.isEmpty {
            view?.dismissActivityIndicator()
//        }
        var responseArray: [Aspect] = []
        for loopVar in 0..<aspectsArray.count {
            let aspectDict = aspectsArray[loopVar]
            let aspectID = aspectDict["id"].intValue
            let aspectName = aspectDict["name"].stringValue
            let aspectSlug = aspectDict["slug"].stringValue
            let aspectsObject = Aspect(aspectId: aspectID, name: aspectName, slug: aspectSlug)
            responseArray.append(aspectsObject)
        }
        view?.listOfAspects(responseArray: responseArray, error: error)
    }
}
