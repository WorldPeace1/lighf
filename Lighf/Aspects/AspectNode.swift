//
//  AspectNode.swift
//  Lighf
//
//  Created by L.
//  Copyright © 2019 L. All rights reserved.
//

import Foundation
import Magnetic

class AspectNode: Node {
    var aspectID: Int!
    var selectedColor: UIColor?
    
    convenience init(text: String?, image: UIImage?, color: UIColor, selectedColor: UIColor?, radius: CGFloat, aspectID: Int) {
        self.init(text: text, image: image, color: color, radius: radius)
        
        self.aspectID = aspectID
        self.selectedColor = selectedColor
        self.strokeColor = .clear
        self.lineWidth = 8.0
    }
    
    override func selectedAnimation() {
        run(.scale(to: 5 / 4, duration: 0.2))
        
        if let texture = self.texture {
            self.fillTexture = texture
        }
        
        if let color = self.selectedColor {
            self.fillColor = color
        }
    }
    
    override func deselectedAnimation() {
        run(.scale(to: 1, duration: 0.2))
        
        self.fillColor = self.color
    }
}
