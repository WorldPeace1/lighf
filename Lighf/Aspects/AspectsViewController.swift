//
//  AspectsViewController.swift
//  Lighf
//
//  Created by L.
//  Copyright © 2019 L. All rights reserved.
//

import Foundation
import UIKit
import MatomoTracker
import Magnetic

class AspectsViewController: BaseViewController {

    @IBOutlet weak var magneticView: MagneticView! {
        didSet {
            self.magnetic.backgroundColor = .clear
            self.magnetic.magneticDelegate = self
        }
    }
    @IBOutlet weak var doneButton: UIButton!
    @IBOutlet weak var selectedCountLabel: UILabel!
    
    var presenter: AspectViewToPresenterProtocol?
    var aspectsArray: [Aspect] = []
    var savedAspectsArray: [Aspect] = []
    var selectedAspectsArray: [Int] = []
    var isFromSettings: Bool = false
    var magnetic: Magnetic {
        return self.magneticView.magnetic
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        presenter?.fetchAspects()
        doneButton.disable()
        selectedCountLabel.text = Constants.emptyString
        
        if isFromSettings {
            self.navigationController?.isNavigationBarHidden = false
            modifyHeaderView()
        } else {
            self.navigationController?.isNavigationBarHidden = true
        }
        
        MatomoTracker.shared.track(view: ["Select aspects screen"])
    }
    
    //capture done button click action
    @IBAction func doneButtonClicked(sender: UIButton) {
        showActivityIndicator()
        presenter?.saveAspects(savedAspects: selectedAspectsArray)
    }
    
    private func modifyHeaderView() {
        // Configure background color
        let backButton = UIBarButtonItem(title: "Back", style: .plain, target: self, action: #selector(backButtonClicked))
        backButton.image = UIImage(named: "BackArrowNavigationBar")?.withRenderingMode(.alwaysTemplate)
        self.navigationItem.leftBarButtonItem = backButton
        self.navigationController?.navigationBar.isHidden = false
        self.navigationController?.view.backgroundColor = UIColor.clear
        self.navigationController?.setBackGroundColor(UIColor.appGreenColor)
    }
    
    @objc func backButtonClicked() {
        self.magnetic.removeAllChildren()
        self.navigationController?.popViewController(animated: true)
        self.navigationItem.setHidesBackButton(false, animated: true)
    }
}

// MARK: - AspectPresenterToViewProtocol

extension AspectsViewController: AspectPresenterToViewProtocol {
    //navigate back or show alert on api response.
    func saveAspectsResponse(success: Bool, error: String) {
        dismissActivityIndicator()
        if success {
            self.magnetic.removeAllChildren()
            self.navigationController?.isNavigationBarHidden = false
            self.navigationController?.popViewController(animated: true)
        } else {
            Utility().showAlert(Constants.emptyString, message: error, delegate: self)
        }
    }
    
    // refresh on getting saved aspects
    func listOfSavedAspects(responseArray: [Aspect]) {
        savedAspectsArray = responseArray
        var savedIndices = [Int]()
        for savedAspect in responseArray {
            savedIndices.append(savedAspect.aspectID)
        }
    
        selectedAspectsArray = savedIndices
        if selectedAspectsArray.count >= 3 {
            doneButton.enable()
        }
        updateSavedAspects(responseArray: responseArray)

        self.createAspectNodes()
    }
    
    //get list of aspects
    func listOfAspects(responseArray: [Aspect], error: String) {
        if responseArray.isEmpty {
            Utility().presentSingleActionAlertFromViewController(viewController: self, titleString: Constants.emptyString, messageString: error, buttonTitle: "Ok") {
                // do action
            }
        } else {
            aspectsArray = responseArray
        }
    }
    
    func updateSavedAspects(responseArray: [Aspect]) {
        if !responseArray.isEmpty {
            let responseDict: NSMutableDictionary = [:]
            for loopVar in 0..<responseArray.count {
                let aspectDict = responseArray[loopVar]
                let aspectID = aspectDict.aspectID
                let aspectName = aspectDict.aspectName
                responseDict[aspectID] = aspectName
            }
            do {
                let defaults  = UserDefaults.standard
                let data = try NSKeyedArchiver.archivedData(withRootObject: responseDict, requiringSecureCoding: false)
                defaults.set(data, forKey: UserDefaultsKey.aspects)
                selectedCountLabel.text = "\(responseArray.count) Selected"
            } catch {
                print("Couldn't write file")
            }
        }
    }
    
    func createAspectNodes() {
        self.magneticView.backgroundColor = .clear
        
        for aspect in self.aspectsArray {
            let node = AspectNode(
                text: aspect.aspectName ?? Constants.emptyString,
                image: nil,
                color: UIColor.nodeUnSelectedColor,
                selectedColor: UIColor.nodeSelectedColor,
                radius: 40,
                aspectID: aspect.aspectID
            )
            
            if self.savedAspectsArray.contains(where: { $0.aspectID == aspect.aspectID }) {
                node.isSelected = true
            }

            self.magnetic.addChild(node)
        }
    }
}

// MARK: - MagneticDelegate

extension AspectsViewController: MagneticDelegate {
    func magnetic(_ magnetic: Magnetic, didSelect node: Node) {
        guard let selectedNodes = magnetic.selectedChildren as? [AspectNode] else { return }
        self.selectedAspectsArray = selectedNodes.compactMap({ return $0.aspectID })
        
        if self.selectedAspectsArray.count >= 3 {
            self.doneButton.enable()
        }
               
        self.selectedCountLabel.text = "\(selectedAspectsArray.count) Selected"
        
        print(self.selectedAspectsArray)
    }

    func magnetic(_ magnetic: Magnetic, didDeselect node: Node) {
        guard let selectedNodes = magnetic.selectedChildren as? [AspectNode] else { return }
        self.selectedAspectsArray = selectedNodes.compactMap({ return $0.aspectID })
                      
        if self.selectedAspectsArray.count < 3 {
            self.doneButton.disable()
        }
        
        if self.selectedAspectsArray.isEmpty {
            self.selectedCountLabel.text = Constants.emptyString
        }
        
        self.selectedCountLabel.text = "\(selectedAspectsArray.count) Selected"
        
        print(self.selectedAspectsArray)
    }
}
