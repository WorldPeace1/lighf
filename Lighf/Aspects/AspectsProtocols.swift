//
//  AspectsProtocols.swift
//  Lighf
//
//  Created by L.
//  Copyright © 2019 L. All rights reserved.
//

import Foundation
import UIKit
import SwiftyJSON
protocol AspectViewToPresenterProtocol: class {
    func fetchAspects()
    func saveAspects(savedAspects: [Int])
}
protocol AspectPresenterToViewProtocol: BasePresenterToViewProtocol {
    func listOfAspects(responseArray: [Aspect], error: String)
    func listOfSavedAspects(responseArray: [Aspect])
    func saveAspectsResponse(success: Bool, error: String)
    func updateSavedAspects(responseArray: [Aspect])
}
protocol AspectPresenterToInteractorProtocol: class {
    func invokeAspectsAPI()
    func invokeSaveAspectsAPI(savedAspects: [Int])
    func invokeGetSavedAspectsAPI()
}
protocol AspectInteractorToPresenterProtocol: class {
    func updateListOfAspects(aspectsArray: [JSON], error: String)
    func updateListOfSavedAspects(aspectsArray: [JSON], error: String)
    func saveAspectsResponse(aspectsArray: [JSON], error: String)
}
protocol AspectPresenterToRouterProtocol: class {
    func moveToHomeScreen()
}
