//
//  AspectsInteractor.swift
//  Lighf
//
//  Created by L.
//  Copyright © 2019 L. All rights reserved.
//

import Foundation
import UIKit

class AspectsInteractor: NSObject {
    weak var presenter: AspectInteractorToPresenterProtocol?
}

extension AspectsInteractor: AspectPresenterToInteractorProtocol {
    //save aspects
    func invokeSaveAspectsAPI(savedAspects: [Int]) {
        let defaults  = UserDefaults.standard
        guard let userID = defaults.object(forKey: UserDefaultsKey.userID) else {
            return
        }
        let params = ["id": userID, "aspects": savedAspects] as [String: Any]
        APIManager.post(endPoint: EndPoint.saveAspectList, parameters: params as [String: Any]) {(response, error) in
            if error == nil, let response = response {
                let responseMessage = response["api_response"]["message"].string
                if (responseMessage != nil) && responseMessage != Constants.emptyString {
                    self.presenter?.saveAspectsResponse(aspectsArray: [], error: error?.message ?? Constants.emptyString)
                } else {
                    let responseArray = response["api_response"]["aspects"].array
                    let message = response["api_status"]["message"].stringValue
                    self.presenter?.saveAspectsResponse(aspectsArray: responseArray ?? [], error: message)
                }
            } else {
                self.presenter?.saveAspectsResponse(aspectsArray: [], error: error?.message ?? Constants.emptyString)
            }
        }
    }
    //get list of aspects
    func invokeAspectsAPI() {
       let apiEndpoint = EndPoint.getAspectList
        let params = [String: String]()
        APIManager.post(endPoint: apiEndpoint, parameters: params) { (response, error) in
            guard let response = response, error == nil else {
                self.presenter?.updateListOfAspects(aspectsArray: [], error: error?.message ?? Constants.emptyString)
                return
            }
            let responseArray = response["api_response"]["aspects"].array
            let message = response["api_status"]["message"].stringValue
            self.presenter?.updateListOfAspects(aspectsArray: responseArray ?? [], error: message)
            self.invokeGetSavedAspectsAPI()
        }
    }
    //get list of saved aspects.
    func invokeGetSavedAspectsAPI() {
        let apiEndPoint = EndPoint.getSavedAspectList
        let defaults = UserDefaults.standard
        if let userID = defaults.object(forKey: UserDefaultsKey.userID) as? Int {
            let params = ["id": userID]
            APIManager.post(endPoint: apiEndPoint, parameters: params as [String: Any]) { (response, error) in
                guard let response = response, error == nil else {
                    self.presenter?.updateListOfSavedAspects(aspectsArray: [], error: error?.message ?? Constants.emptyString)
                    return
                }
                let responseArray = response["api_response"]["aspects"].array
                let message = response["api_status"]["message"].stringValue
                self.presenter?.updateListOfSavedAspects(aspectsArray: responseArray ?? [], error: message)
            }
        }
    }
}
