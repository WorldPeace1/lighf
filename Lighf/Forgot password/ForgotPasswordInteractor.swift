//
//  ForgotPasswordInteractor.swift
//  Lighf
//
//  Created by L.
//  Copyright © 2019 L. All rights reserved.
//

import UIKit

class ForgotPasswordInteractor: NSObject {
    weak var presenter: ForgotPasswordInteractorToPresenterProtocol?
}

extension ForgotPasswordInteractor: ForgotPasswordPresenterToInteractorProtocol {
    //
    func resetPassword(for email: String, username: String) {
        //
        APIManager.post(endPoint: EndPoint.forgotPassword, parameters: [APIParams.userName: username, APIParams.email: email]) { (response, error) in
            //
            guard let response = response, error == nil else {
                self.presenter?.passwordReset(status: false, errorMessage: error!.message)
                return
            }
            //
            let message = response["api_response"]["message"].stringValue
            let status = response[APIResponse.apiResponse][APIResponse.apiResponseStatus].stringValue
            let reset = status == Constants.success ? true : false
            self.presenter?.passwordReset(status: reset, errorMessage: message)
        }
    }
}
