//
//  ForgotPasswordViewController.swift
//  Lighf
//
//  Created by L.
//  Copyright © 2019 L. All rights reserved.
//

import UIKit
import SkyFloatingLabelTextField
import MatomoTracker
class ForgotPasswordViewController: BaseViewController {
    var presenter: ForgotPasswordViewToPresenterProtocol?
    let screenShiftValue: Float = 50.0
    
    @IBOutlet weak var usernameTextField: SkyFloatingLabelTextField!
    @IBOutlet weak var emailTextField: SkyFloatingLabelTextField!
    @IBOutlet weak var contentView: UIView!
    @IBOutlet weak var successView: UIView!
    @IBOutlet weak var submitButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.keyboardDismissalRegistrer.registerControls([self.usernameTextField, self.emailTextField])
        
        successView.isHidden = true
        usernameTextField.modifyTitleFormatter()
        emailTextField.modifyTitleFormatter()
        submitButton.disable()
        MatomoTracker.shared.track(view: ["ForgotPassword Screen"])
    }
    //
    @IBAction func submitButtonClicked(sender: UIButton) {
        //
        guard !usernameTextField.isEmpty(), !emailTextField.isEmpty() else {
            return
        }
        dismissKeyBoard(resetScreen: false)
        moveScreenDown(toValue: screenShiftValue)
        //
        let matomoEvent = Event.init(tracker: MatomoTracker.shared, action: ["Forgot password screen", "Clicked on button"])
        MatomoTracker.shared.track(matomoEvent)
        if let username = usernameTextField.text, let email = emailTextField.text {
           presenter?.submitButtonClicked(with: email, username: username)
        }
    }
    //
    @IBAction func contactButtonClicked(sender: UIButton) {
    }
    //
    @IBAction func requestNewAccountButtonClicked(sender: UIButton) {
    }
    //
    @IBAction func textFieldDidChange(_ sender: UITextField) {
        updateStatusOfSubmitButton()
    }
    // Update enable/Disable state od submit button based on user entry
    func updateStatusOfSubmitButton() {
        if usernameTextField.isEmpty() || emailTextField.isEmpty() {
            submitButton.disable()
        } else if usernameTextField.text!.count >= Constants.minCharacterCount && emailTextField.isEmpty() == false {
            submitButton.enable()
        } else {
            submitButton.disable()
        }
    }
}

extension ForgotPasswordViewController: ForgotPasswordPresenterToViewProtocol {
    //
    func showAlert(message: String) {
        let resetPasswordAlert = UIAlertController.alert(with: message)
        self.present(resetPasswordAlert, animated: true, completion: nil)
    }
    //
    func showSuccessView() {
        contentView.isHidden = true
        successView.isHidden = false
    }
}

extension ForgotPasswordViewController: UITextFieldDelegate {
    //
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        //
        if textField == usernameTextField {
            emailTextField.becomeFirstResponder()
        } else {
            textField.resignFirstResponder()
            moveScreenDown(toValue: screenShiftValue)
            if submitButton.isEnabled {
                submitButtonClicked(sender: submitButton)
            }
        }
        return true
    }
    //
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        guard let text = textField.text as NSString? else {
            return true
        }
        let newText = text.replacingCharacters(in: range, with: string)
        //
        if textField == usernameTextField {
            if newText.count > Constants.maxCharacterCount {
                return false
            }
        }
        return true
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        moveScreenUp(toValue: screenShiftValue)
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        moveScreenDown(toValue: screenShiftValue)
    }
}
