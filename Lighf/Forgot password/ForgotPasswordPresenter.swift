//
//  ForgotPasswordPresenter.swift
//  Lighf
//
//  Created by L
//  Copyright © 2019 L. All rights reserved.
//

import UIKit
import EmailValidator

class ForgotPasswordPresenter: NSObject {

    var interactor: ForgotPasswordPresenterToInteractorProtocol?
    var router: ForgotPasswordPresenterToRouterProtocol?
    weak var view: ForgotPasswordPresenterToViewProtocol?
}

extension ForgotPasswordPresenter: ForgotPasswordViewToPresenterProtocol {
    //
    func requestNewAccountButtonClicked() {
    }
    //
    func contactButtonClicked() {
    }
    //
    func submitButtonClicked(with email: String, username: String) {
        let isValidEmail = EmailValidator.validate(email: email)
        if isValidEmail {
            view?.showActivityIndicator()
            interactor?.resetPassword(for: email, username: username)
        } else {
           view?.showAlert(message: SignUpMessage.invalidEmail)
        }
    }
}

extension ForgotPasswordPresenter: ForgotPasswordInteractorToPresenterProtocol {
    func passwordReset(status: Bool, errorMessage: String) {
        view?.dismissActivityIndicator()
        if status == true {
            view?.showSuccessView()
        } else {
            view?.showAlert(message: errorMessage)
        }
    }
}
