//
//  ForgotPasswordProtocols.swift
//  Lighf
//
//  Created by L.
//  Copyright © 2019 L. All rights reserved.
//

import UIKit

protocol ForgotPasswordViewToPresenterProtocol: class {
    func submitButtonClicked(with email: String, username: String)
    func requestNewAccountButtonClicked()
    func contactButtonClicked()
}

protocol ForgotPasswordPresenterToInteractorProtocol: class {
    func resetPassword(for email: String, username: String)
}

protocol ForgotPasswordInteractorToPresenterProtocol: class {
    func passwordReset(status: Bool, errorMessage: String)
}

protocol ForgotPasswordPresenterToViewProtocol: BasePresenterToViewProtocol {
    func showAlert(message: String)
    func showSuccessView()
}

protocol ForgotPasswordPresenterToRouterProtocol: class {
}
