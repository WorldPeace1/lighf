//
//  ForgotPasswordRouter.swift
//  Lighf
//
//  Created by L.
//  Copyright © 2019 L. All rights reserved.
//

import UIKit

class ForgotPasswordRouter: NSObject {

    weak var presentedViewController: UIViewController?
    // Identifier need to be modified
    static let viewIdentifier = "ForgotPasswordScreen"
    //
    static func createModule() -> ForgotPasswordViewController? {
        let view = UIStoryboard.main.instantiateViewController(withIdentifier: viewIdentifier) as? ForgotPasswordViewController
        //
        let presenter = ForgotPasswordPresenter()
        let interactor = ForgotPasswordInteractor()
        let router = ForgotPasswordRouter()
        //
        view?.presenter = presenter
        //
        presenter.view = view
        presenter.interactor = interactor
        presenter.router = router
        //
        interactor.presenter = presenter
        //
        router.presentedViewController = view
        //
        return view
    }
}

extension ForgotPasswordRouter: ForgotPasswordPresenterToRouterProtocol {   }
