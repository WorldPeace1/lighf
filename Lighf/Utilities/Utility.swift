//
//  Utility.swift
//  Lighf
//
//  Created by L.
//  Copyright © 2019 L. All rights reserved.
//

import Foundation
import UIKit
import MBProgressHUD
import AttributedStringBuilder
class Utility {
    //Method to display alert
    func showAlert(_ title: String, message: String?, delegate: AnyObject) {
        var alert: UIAlertController!
        // This is a way to check the alert message and replace it wih custom message as per the request from client
        if let message = message, message.lowercased().contains(Message.requestTimeOut) || message.lowercased().contains(Message.messageNotFound) {
            alert = UIAlertController(title: "Uh oh!", message: Message.connectionLost, preferredStyle: .alert)
        } else {
             alert = UIAlertController(title: title, message: message ?? Constants.emptyString, preferredStyle: .alert)
        }
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
        delegate.present(alert, animated: true, completion: nil)
    }
    // Present Single Alert Action from a view controller and action callback
    func presentSingleActionAlertFromViewController(viewController: UIViewController, titleString: String, messageString: String, buttonTitle: String, butttonTapped:@escaping () -> Void) {
        let alert = UIAlertController(title: titleString, message: messageString, preferredStyle: .alert)
        let alertAction = UIAlertAction(title: buttonTitle, style: .default, handler: { (_ action) -> Void in
            butttonTapped()
        })
        alert.addAction(alertAction)
        viewController.present(alert, animated: true, completion: nil)
    }
    // Present Two Alert Actions from a view controller with cancel and action callback
    func presentTwoActionAlerController(titleString: String, messageString: String, rightButtonTitle: String, leftButtonTitle: String, rightButtonTapped:@escaping (Int) -> Void) {
        let alert = UIAlertController(title: titleString, message: messageString, preferredStyle: .alert)
        let rightButtontAction = UIAlertAction(title: rightButtonTitle, style: .default, handler: { (_ action) -> Void in
            rightButtonTapped(1)
        })
        let leftButtontAction = UIAlertAction(title: leftButtonTitle, style: .default, handler: { (_ action) -> Void in
            rightButtonTapped(2)
        })
        let cancelButtontAction = UIAlertAction(title: "Cancel", style: .cancel, handler: { (_ action) -> Void in
            UIApplication.shared.delegate?.window??.rootViewController?.dismiss(animated: true, completion: nil)
        })
        alert.addAction(rightButtontAction)
        alert.addAction(leftButtontAction)
        alert.addAction(cancelButtontAction)
        UIApplication.shared.delegate?.window??.rootViewController?.present(alert, animated: true, completion: nil)
    }
//
    func shiftScreen(isAlreadyShifted: Bool, navigationController: UINavigationController, screenShiftUpValue: CGFloat) -> Bool {
        var isScreenShiftedUp = isAlreadyShifted
        if isScreenShiftedUp && UIDevice.current.userInterfaceIdiom == .phone {
            var baseScreenFrame = navigationController.view.frame as CGRect
            baseScreenFrame.origin.y += screenShiftUpValue
            navigationController.view.frame = baseScreenFrame
            isScreenShiftedUp = false
        } else if !isScreenShiftedUp && UIDevice.current.userInterfaceIdiom == .phone {
            var baseScreenFrame = navigationController.view.frame as CGRect
            baseScreenFrame.origin.y -= screenShiftUpValue
            navigationController.view.frame = baseScreenFrame
            isScreenShiftedUp = true
        }

        navigationController.view.setNeedsLayout()

        return isScreenShiftedUp
    }
    // Method that returns the version of the app
    static func appVersion() -> String {
        //
        guard let infoDictionary = Bundle.main.infoDictionary else {
            return ""
        }
        //
        guard let version = infoDictionary["CFBundleShortVersionString"] as? String else {
            return ""
        }
        //
        guard let build = infoDictionary["CFBundleVersion"] as? String else {
            return ""
        }
        return "Version: \(version).\(build)"
    }
    // insert a sub string to textview. To add quote or bullet point.
    func insertStringToTextView(textview: UITextView, cursorPos: Int, withString: NSAttributedString) {
        let attrText = NSMutableAttributedString.init(attributedString: textview.attributedText)
        attrText.insert(withString, at: cursorPos)
        textview.attributedText = attrText
    }
    // Show toast
    static func showToast(with meassage: String, inView view: UIView?) {
        guard let view = view else {
            return
        }
        let toast = MBProgressHUD.showAdded(to: view, animated: true)
        toast.mode = .text
        toast.detailsLabel.text = meassage
        toast.removeFromSuperViewOnHide = true
        toast.margin = 10.0
        toast.hide(animated: true, afterDelay: 2.0)
    }
    //to get inset for devices
    static func getSafeAreaInsets() -> CGFloat {
        let topArea = UIApplication.shared.delegate?.window??.safeAreaInsets.top ?? 20
        let bottomArea = UIApplication.shared.delegate?.window??.safeAreaInsets.bottom ?? 0
        return topArea + bottomArea
    }
    static func getAttributtedAspectName(aspectName: String) -> NSAttributedString {
        let builder = AttributedStringBuilder()
        if aspectName != Constants.emptyString {
            let firstChar = aspectName.subString(indexPosition: 0)
            let subString = aspectName.split(separator: firstChar, maxSplits: 2, omittingEmptySubsequences: false)
            builder
                .text("\(firstChar as Character)", attributes: [.font(UIFont(name: Font.sfProDisplayBold.rawValue, size: 18)!), .alignment(.center)])
                .text(String(subString[1]), attributes: [.font(UIFont(name: Font.sfProDisplayLight.rawValue, size: 14)!), .alignment(.center)])
        } else {
            builder
                .text("\(Constants.emptyString as String)", attributes: [.font(UIFont(name: Font.sfProDisplayLight.rawValue, size: 18)!), .alignment(.center)])
        }
        return builder.attributedString
    }
    static func getAttributtedString(string: String, fontName: String, size: CGFloat) -> NSAttributedString {
        let builder = AttributedStringBuilder()
        if string != Constants.emptyString {
            builder
                .text("\(string)", attributes: [.font(UIFont(name: fontName, size: size)!), .alignment(.center)])
        } else {
            builder
                .text("\(Constants.emptyString as String)", attributes: [.font(UIFont(name: fontName, size: size)!), .alignment(.center)])
        }
        return builder.attributedString

    }
    func daysBetween(date1: Date, date2: Date) -> String {
        var timeDiff = ""
        let calendar = Calendar.current
        let components = calendar.dateComponents([Calendar.Component.year, Calendar.Component.day, Calendar.Component.hour, Calendar.Component.minute], from: date1, to: date2)
        if components.year ?? 0 > 0 {
            timeDiff = String.init("\(components.year ?? 0)y")
        } else if components.day ?? 0 > 7 {
            timeDiff = String.init("\((components.day ?? 0)/7)w")
        } else if components.day ?? 0 > 0 && components.day ?? 0 < 7 {
            timeDiff = String.init("\(components.day ?? 0)d")
        } else if components.hour ?? 0 > 0 {
            timeDiff = String.init("\(components.hour ?? 0)h")
        } else if components.minute ?? 0 > 0 {
            timeDiff = String.init("\(components.minute ?? 0)m")
        } else {
            timeDiff = "Just now"
        }
        return timeDiff as String
    }
}
// Class used to override the default image handling of UIContextualAction.
class ImageWithoutRender: UIImage {
    override func withRenderingMode(_ renderingMode: UIImage.RenderingMode) -> UIImage {
        return self
    }
}
