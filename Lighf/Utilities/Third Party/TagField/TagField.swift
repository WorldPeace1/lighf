//
//  TagField.swift
//  TagField
//
//  Created by chutatsu on 2019/02/24.
//  Copyright © 2019 churabou. All rights reserved.
//

import UIKit

protocol TagFieldDelegate: class {
    func textFieldDidEndEditing(_ textField: UITextField)
    func didUpdateFrameSize(_ size: CGSize)
}

public class BackspaceDetectableTextField: UITextField {
    //
    var onDeleteBackwards: (() -> Void)?
    //
    override public func deleteBackward() {
        onDeleteBackwards?()
        super.deleteBackward()
    }
}

public final class TagField: TagListView {
    //
    var placeholderColor: UIColor = .lightGray
    var placeholderText = "Add upto 5 tags"
    weak var tagFieldDelegate: TagFieldDelegate?
    let maxTagCount: Int = 5
    var isModified: Bool = false
    var isEditable: Bool = true {
        didSet {
            if isEditable == false {
                style.showDeleteButton = false
            }
        }
    }
    //
    func attributedPlaceholder() -> NSAttributedString {
        let attributes: [NSAttributedString.Key: Any] = [.foregroundColor: placeholderColor]
        return NSAttributedString(string: placeholderText, attributes: attributes)
    }
    //
    public var style = Style()
    public var tags: [String] {
        get {
            return tagViews.compactMap { $0.text }
        }
        set {
            newValue.forEach {addTag($0, style: style)}
        }
    }
    public func removeAll() {
        tagViews.forEach {remove($0)}
    }
    public lazy var textField: BackspaceDetectableTextField = {
        let textEntry = BackspaceDetectableTextField()
        textEntry.backgroundColor = .clear
        textEntry.autocorrectionType = .no
        textEntry.autocapitalizationType = .none
        textEntry.spellCheckingType = .no
        textEntry.delegate = self
        textEntry.backgroundColor = UIColor.orange.withAlphaComponent(0.1)
        textEntry.attributedPlaceholder = attributedPlaceholder()
        return textEntry
    }()
    //
    override init(frame: CGRect) {
        super.init(frame: frame)
        contentView.addSubview(textField)
        delegate = self
        //
        textField.onDeleteBackwards = { [weak self] in
            self?.deleteBackward()
        }
    }
    //
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    //
    override func repositionView() {
        super.repositionView()
        //
        guard isEditable == true else {
            setContentSize(size: contentView.contentSize)
            return
        }
        //
        let height: CGFloat = ("" as NSString).size(withAttributes: [.font: style.font]).height + (style.padding.top+style.padding.bottom)
        //
        if tagViews.count == maxTagCount && textField.superview == nil {
            setContentSize(size: contentView.contentSize)
            return
        }
        if tagViews.count != maxTagCount && textField.superview == nil {
            contentView.addSubview(textField)
        }
        //
        if let last = tagViews.last {
            let minmumTextFieldWidth: CGFloat = 80
            textField.attributedPlaceholder = tagViews.count == maxTagCount ? NSAttributedString(string: "") :                NSAttributedString(string: "Enter tag") // Remove placeholder if tagcount is reached maximum
            if bounds.width - last.frame.maxX - style.margin.x < minmumTextFieldWidth {
                textField.frame.origin.x = style.margin.x
                textField.frame.origin.y = last.frame.maxY + style.margin.y
                textField.frame.size = CGSize(width: bounds.width-2*style.margin.x, height: height)
            } else {
                textField.frame.origin = CGPoint(x: last.frame.maxX+style.margin.x, y: last.frame.minY)
                textField.frame.size = CGSize(width: (bounds.width - last.frame.maxX), height: last.frame.height)
            }
        } else {
            textField.attributedPlaceholder = attributedPlaceholder()
            textField.frame.origin = CGPoint(x: style.margin.x, y: style.margin.y)
            textField.frame.size = CGSize(width: bounds.width-2*style.margin.x, height: height)
        }
        if textField.frame.maxY > contentView.contentSize.height {
            contentView.contentSize = CGSize(width: bounds.width, height: textField.frame.maxY + style.margin.y)
        }
        setContentSize(size: contentView.contentSize)
    }
    //
    func setContentSize(size: CGSize) {
        self.frame.size = size
        tagFieldDelegate?.didUpdateFrameSize(size)
    }
    //
    override func remove(_ tagView: TagView) {
        super.remove(tagView)
        isModified = true
    }
}
//
extension TagField: UITextFieldDelegate {
    //
    public func textFieldDidBeginEditing(_ textField: UITextField) {
        tagViews.forEach { $0.isSelected = false }
    }
    //
    public func textFieldDidEndEditing(_ textField: UITextField) {
        if let text = textField.text, !text.removeWhiteSpace().isEmpty {
            // This is a workaround to find out that if any spaces are added in used pasted text
            let components = text.components(separatedBy: " ").filter({$0 != ""})
            if components.count != 1, let value = components.first {
                addTag(value.removeWhiteSpace(), style: style)
            } else {
                addTag(text.removeWhiteSpace(), style: style)
            }
            textField.text = ""
        }
    }
    //
    public func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if let text = textField.text, !text.removeWhiteSpace().isEmpty {
            // This is a workaround to find out that if any spaces are added in used pasted text
            let components = text.components(separatedBy: " ").filter({$0 != ""})
            if components.count != 1, let value = components.first {
                addTag(value.removeWhiteSpace(), style: style)
            } else {
                addTag(text.removeWhiteSpace(), style: style)
            }
        }
        textField.text = ""
        textField.resignFirstResponder()
        //
        if tagViews.count == maxTagCount, let last = tagViews.last {
            textField.removeFromSuperview()
            if last.frame.maxY < contentView.contentSize.height {
                contentView.contentSize = CGSize(width: bounds.width, height: last.frame.maxY + style.margin.y)
            }
            setContentSize(size: contentView.contentSize)
        }
        //
        tagFieldDelegate?.textFieldDidEndEditing(textField)
        return true
    }
    //
    public func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        //
        if let text = textField.text, !text.removeWhiteSpace().isEmpty, string == " " {
            // This is a workaround to find out that if any spaces are added in used pasted text
            let components = text.components(separatedBy: " ").filter({$0 != ""})
            if components.count != 1, let value = components.first {
                addTag(value.removeWhiteSpace(), style: style)
            } else {
                addTag(text.removeWhiteSpace(), style: style)
            }
            textField.text = ""
            return false
        } else if string.count > 1 {
            // This is a workaround to find out that if any spaces are added in used pasted text
            let components = string.components(separatedBy: " ").filter({$0 != ""})
            var count = maxTagCount - tagViews.count
            if components.count < maxTagCount {
                count = components.count
            }
            for index in 0..<count {
                addTag(components[index])
                print(components[index])
            }
            textField.text = ""
        }
        isModified = true
        return tagViews.count < maxTagCount
    }
    //
    func deleteBackward() {
        // delete tag
        if let selected = tagViews.first(where: { $0.isSelected }) {
            remove(selected)
            isModified = true
            return
        }
        // select last tag
        if (textField.text ?? "").isEmpty, let tagView = tagViews.last {
            tagViews.forEach { $0.isSelected = $0 === tagView }
        }
    }
}
//
extension TagField: TagListViewDelegate {
    func didTapTag(_ tagView: TagView) {
        tagViews.forEach { $0.isSelected = $0 === tagView}
    }
}
