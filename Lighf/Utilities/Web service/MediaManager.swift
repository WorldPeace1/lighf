//
//  MediaManager.swift
//  Lighf
//
//  Created by L.
//  Copyright © 2019 L. All rights reserved.
//

import UIKit
import Alamofire

class MediaManager: NSObject {

    static let serverEndpoint = ""
    //
    class func uploadImage(path: String) {
        guard let url = URL(string: path) else { return }
        guard let uploadUrl = URL(string: serverEndpoint) else {return}
        //
        do {
            let imageData = try Data(contentsOf: url)
            Alamofire.upload(multipartFormData: { (multipartFormData) in
                multipartFormData.append(imageData,
                                         withName: url.lastPathComponent,
                                         fileName: url.absoluteString,
                                         mimeType: "image/\(url.pathExtension)")
            }, to: uploadUrl) { (result) in
                //
                switch result {
                case .success(let upload, _, _):
                    //
                    upload.uploadProgress(closure: { (progress) in
                        print("uploadProgress : \(progress.fractionCompleted)")
                    })
                    //
                    upload.responseJSON(completionHandler: { (response) in
                        print("response.result : \(String(describing: response.result.value))")
                    })
                case .failure(let error):
                    print("Error in upload: \(error)")
                }
            }
        } catch let error {
            print("Error: \(error.localizedDescription)")
        }
    }
}
