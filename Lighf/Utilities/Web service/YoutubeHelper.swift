//
//  YoutubeHelper.swift
//  Lighf
//
//  Created by L.
//  Copyright © 2019 L. All rights reserved.
//

import UIKit
import SwiftyJSON
import Alamofire

enum YoutubeError: Error {
    case invalidVideoId
    case invalidEndpoint
    case invalidResponse
    case noNetWork
    var message: String {
        switch self {
        case .invalidEndpoint:
            return ""
        case .invalidResponse:
            return ""
        case .invalidVideoId:
            return Message.invalidYoutubeUrl
        case .noNetWork:
            return Message.networkUnavailable
        }
    }
}

struct Video {
    var videoId: String
    var thumbnail: String
    var description: String
}

class YoutubeHelper: NSObject {
    //
    static let apiKey = "AIzaSyBJDGxj-tvk7K_koa2pPuBth-lLNI7K0HU"
    //
    static func details(for url: String, completion: @escaping (Video?, Error?) -> Void) {
        //
        guard APIManager.isNetWorkReachable() else {
            completion(nil, YoutubeError.noNetWork)
            return
        }
        //
        guard let videoId = videoId(for: url) else {
            completion(nil, YoutubeError.invalidVideoId)
            return
        }
        //
        let youtubeEndPoint = "https://www.googleapis.com/youtube/v3/videos?part=snippet&id=\(videoId)&key=\(apiKey)"
        //
        guard let youtubeUrl = URL(string: youtubeEndPoint) else {
            completion(nil, YoutubeError.invalidEndpoint)
            return
        }
        //
        var request = URLRequest(url: youtubeUrl)
        request.httpMethod = "GET"
        Alamofire.request(request).responseJSON { (response) in
            switch response.result {
            case .success(let value):
                let json = JSON(value)
                let totalResults = json["pageInfo"]["totalResults"].intValue
                // if search results are zero, then that's an invalid video id
                if totalResults == 0 {
                    completion(nil, YoutubeError.invalidVideoId)
                    return
                }
                let thumbnail = json["items"][0]["snippet"]["thumbnails"]["high"]["url"]
                let description = json["items"][0]["snippet"]["description"]
                let video = Video(videoId: videoId, thumbnail: thumbnail.stringValue,
                                  description: description.stringValue)
                completion(video, nil)
            case .failure(let error):
                completion(nil, error)
            }
        }
    }
    //
    static func videoId(for urlString: String) -> String? {
        guard NSURL(string: urlString) != nil, let youtubeID = urlString.youtubeID else {
            return nil
        }
        return youtubeID
    }
    //
    static func checkUrlValidity(_ url: String, completion: @escaping (Bool) -> Void) {
        if url.youtubeID != nil {
            YoutubeHelper.details(for: url) { (_, error) in
                if let error = error as? YoutubeError, error == YoutubeError.invalidVideoId {
                    // inValid video id
                    completion(false)
                } else {
                   completion(true)
                }
            }
        } else {
          completion(false)
        }
    }
}
