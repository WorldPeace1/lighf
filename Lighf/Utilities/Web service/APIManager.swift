//
//  APIManager.swift
//  Lighf
//
//  Created by L.
//  Copyright © 2019 L. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

enum NetworkError: Error {
    case notReachable
    case unknown
}

class APIManager: NSObject {

    static func isNetWorkReachable() -> Bool {
        //
        if let reachabilityManager = NetworkReachabilityManager(), reachabilityManager.isReachable {
            return true
        }
      return false
    }
    /// Method for handling the post requests
    ///
    /// - Parameters:
    ///   - url: API url
    ///   - parameters: Parameters associated with the API request
    ///   - completion: Returns the result as a dict upon completion along with the Error
    static func post(endPoint: String, parameters: [String: Any], completion: @escaping (JSON?, Error?) -> Void) {
        guard isNetWorkReachable() else {
            completion(nil, NetworkError.notReachable)
            return
        }
        let urlString = Constants.kBaseURL + endPoint
        var header: [String: String] = [:]
        if let token = UserDefaults.standard.object(forKey: UserDefaultsKey.accessToken) as? String {
            header = ["Authorization": "Bearer \(token)"]
        }
        Alamofire.request(urlString, method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: header).responseJSON { (response) in
            print("API Response : \(response)")
            switch response.result {
            case .success(let value):
                let json = JSON(value)
                completion(json, nil)
            case .failure(let error):
                completion(nil, error)
            }
        }
    }
    /// Method for handling the get requests
    ///
    /// - Parameters:
    ///   - url: API url
    ///   - parameters: Parameters associated with the API request
    ///   - completion: Returns the result as a dict upon completion along with the Error
    static func get(endPoint: String, parameters: [String: Any], completion: @escaping (JSON?, Error?) -> Void) {
        //
        guard isNetWorkReachable() else {
            completion(nil, NetworkError.notReachable)
            return
        }
        //
        let urlString = Constants.kBaseURL + endPoint
        var header: [String: String] = [:]
        if let token = UserDefaults.standard.object(forKey: UserDefaultsKey.accessToken) as? String {
            header = ["Authorization": "Bearer \(token)"]
        }
        //
        Alamofire.request(urlString, method: .get, parameters: parameters, encoding: JSONEncoding.default, headers: header).responseJSON { (response) in
            print("API Response : \(response)")
            switch response.result {
            case .success(let value):
                let json = JSON(value)
                completion(json, nil)
            case .failure(let error):
                completion(nil, error)
            }
        }
    }
}
