//
//  GiveLeafInteractor.swift
//  Lighf
//
//  Created by L.
//  Copyright © 2019 L. All rights reserved.
//

import Foundation
import UIKit

class GiveLeafInteractor: NSObject {
    weak var homePresenter: GiveLeafToHomePresenterProtocol?
    weak var searchPresenter: GiveLeafInteractorToSearchPresenterProtocol?
    weak var postDetailsPresenter: GiveLeafInteractorToPostPresentProtocol?
    //invoke API to give leaf to a user.
    func invokeGiveLeafAPI(to user: Int, numberOfLeaves: Int) {
        let defaults = UserDefaults.standard
        var parameters: [String: Any] = [:]
        parameters[APIParams.userID] = defaults.value(forKey: UserDefaultsKey.userID)
        parameters[APIParams.receivingUserID] = user
        parameters[APIParams.leafCount] = numberOfLeaves
        APIManager.post(endPoint: EndPoint.giveLeaf, parameters: parameters) { (response, error) in
            guard error == nil, let response = response else {
                self.homePresenter?.giveLeafAPIResponse("nok", error: error?.message)
                self.searchPresenter?.giveLeafAPIResponse("nok", error: error?.message)
                self.postDetailsPresenter?.giveLeafAPIResponse("nok", error: error?.message)
                return
            }
            let responseStatus = response[APIResponse.apiResponse][APIResponse.apiResponseStatus].stringValue
            let message = response[APIResponse.apiResponse][APIResponse.apiResponseMessage].stringValue
            let userPoints = response[APIResponse.apiResponse][APIResponse.apiUserPoints].intValue
            UserDefaults.standard.set(userPoints, forKey: UserDefaultsKey.leafCount)
            self.homePresenter?.giveLeafAPIResponse(responseStatus, error: message)
            self.searchPresenter?.giveLeafAPIResponse(responseStatus, error: message)
            self.postDetailsPresenter?.giveLeafAPIResponse(responseStatus, error: message)
        }
    }
}

extension GiveLeafInteractor: HomePresenterToGiveLeafProtocol, SearchPresenterToGiveLeafInteractorProtocol, PostPresenterToGiveLeafInteractorProtocol {
// give leaf to comment author
    func invokeGiveLeaf(forComment: PostComment, leafCount: Int) {
        invokeGiveLeafAPI(to: forComment.userID, numberOfLeaves: leafCount)
    }
// give leaf to post author
    func invokeGiveLeaf(forPost: Post, leafCount: Int) {
        switch forPost {
        case let article as Article:
            invokeGiveLeafAPI(to: article.authorID ?? 0, numberOfLeaves: leafCount)
        case let story as Story:
            invokeGiveLeafAPI(to: story.authorID ?? 0, numberOfLeaves: leafCount)
        case let window as Window:
            invokeGiveLeafAPI(to: window.authorID ?? 0, numberOfLeaves: leafCount)
        default: break
        }
    }
}
