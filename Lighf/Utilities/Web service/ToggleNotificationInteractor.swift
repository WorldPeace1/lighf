//
//  ToggleNotificationInteractor.swift
//  Lighf
//
//  Created by L.
//  Copyright © 2019 L. All rights reserved.
//

import Foundation
import UIKit

class ToggleNotificationInteractor: NSObject {
    var homePresenter: ToggleNotificationToHomePresenterProtocol?
    var searchPresenter: ToggleNotificationToSearchPresenterProtocol?
    var postDetailsPresenter: ToggleNotificationToPostDetailsPresenterProtocol?
    func invokeToggleNotificationsAPI(forItem: Any, toState: NotificationState) {
        var itemID: Int = 0
        var itemType: String = "post"
        switch forItem {
        case let post as Post:
            itemID = post.postId ?? 0
            itemType = "post"
        case let comment as PostComment:
            itemID = comment.commentID
            itemType = "comment"
        default:
            break
        }
        let defaults = UserDefaults.standard
        var parameters: [String: Any] = [:]
        parameters[APIParams.userID] = defaults.value(forKey: UserDefaultsKey.userID)
        parameters[APIParams.itemID] = itemID
        parameters[APIParams.itemType] = itemType
        parameters[APIParams.notificationAction] = toState.rawValue
        APIManager.post(endPoint: EndPoint.toggleNotification, parameters: parameters) { (response, error) in
            guard error == nil, let response = response else {
                self.homePresenter?.toggleNotificationAPIResponseFailure("nok", error: error?.message)
                self.searchPresenter?.toggleNotificationAPIResponseFailure("nok", error: error?.message)
                self.postDetailsPresenter?.toggleNotificationAPIResponseFailure("nok", error: error?.message)
                return
            }
            let notificationStatus = response[APIResponse.apiResponse]["notifications_status"].stringValue
            let itemType = response[APIResponse.apiResponse]["item_type"].stringValue
            let itemID = response[APIResponse.apiResponse]["item_id"].intValue
            if notificationStatus == "notifications_on" {
                self.homePresenter?.toggleNotificationAPIResponse(itemID, itemType: itemType, notificationStatus: NotificationState.turnOn)
                self.searchPresenter?.toggleNotificationAPIResponse(itemID, itemType: itemType, notificationStatus: NotificationState.turnOn)
                self.postDetailsPresenter?.toggleNotificationAPIResponse(itemID, itemType: itemType, notificationStatus: NotificationState.turnOn)
            } else {
                self.homePresenter?.toggleNotificationAPIResponse(itemID, itemType: itemType, notificationStatus: NotificationState.turnOff)
                self.searchPresenter?.toggleNotificationAPIResponse(itemID, itemType: itemType, notificationStatus: NotificationState.turnOff)
                self.postDetailsPresenter?.toggleNotificationAPIResponse(itemID, itemType: itemType, notificationStatus: NotificationState.turnOff)
            }
        }
    }
}
extension ToggleNotificationInteractor: HomePresenterToToggleNotificationProtocol, SearchPresenterToToggleNotificationProtocol, PostDetailsPresenterToToggleNotificationProtocol {
    func invokeToggleNotification(forItem: Any, toState: NotificationState) {
        invokeToggleNotificationsAPI(forItem: forItem, toState: toState)
    }
}
