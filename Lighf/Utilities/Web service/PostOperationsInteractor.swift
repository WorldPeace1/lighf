//
//  PostOperationsInteractor.swift
//  Lighf
//
//  Created by L.
//  Copyright © 2019 L. All rights reserved.
//

import UIKit

enum PostOperation: String {
    case read = "read"
    case unRead = "unread"
    case related = "related"
    case unRelated = "unrelated"
    case addBackPack = "added_backpack"
    case removeBackPack = "removed_backpack"
    //
    var parameterValue: String {
        return self.rawValue
    }
}

class PostOperationsInteractor: NSObject {

    func add(_ post: Post, toBackPack addToBackPack: Bool) {
        let operation: PostOperation = addToBackPack == true ? .addBackPack : .removeBackPack
        performOperation(operation, onPost: post)
    }
    func mark(_ post: Post, asRelated markRelate: Bool) {
        let operation: PostOperation = markRelate == true ? .related : .unRelated
        performOperation(operation, onPost: post)
    }
    func mark(_ post: Post, asRead markRead: Bool) {
        let operation: PostOperation = markRead == true ? .read : .unRead
        performOperation(operation, onPost: post)
    }
    //
    private func performOperation(_ operation: PostOperation, onPost post: Post) {
        let defaults = UserDefaults.standard
        var parameters: [String: Any] = [:]
        parameters["id"] = defaults.value(forKey: UserDefaultsKey.userID)
        //
        guard let postId = post.postId else {
            self.performedOperation(operation, onPost: post, with: "Invalid PostId")
            return
        }
        parameters["post_id"] = postId
        parameters["action"] = operation.parameterValue
        //
        APIManager.post(endPoint: EndPoint.markPost, parameters: parameters) { (response, error) in
            guard let response = response, error == nil else {
                self.performedOperation(operation, onPost: post, with: error!.message)
                return
            }
            let status = response[APIResponse.apiResponse][APIResponse.apiResponseStatus].stringValue
            let responseMessage = response[APIResponse.apiResponse][APIResponse.apiResponseMessage].stringValue
            if status == Constants.success {
                self.performedOperation(operation, onPost: post, with: nil)
            } else {
                var currentPost = post
                currentPost.isDeleted = response[APIResponse.apiResponse]["is_deleted"].boolValue
                self.performedOperation(operation, onPost: currentPost, with: responseMessage)
            }
        }
    }
    //
    func performedOperation(_ operation: PostOperation, onPost post: Post, with error: String?) {
        // Override in subclass
    }
}
