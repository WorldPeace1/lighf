//
//  GetFeedsInteractor.swift
//  Lighf
//
//  Created by L.
//  Copyright © 2019 L. All rights reserved.
//

import Foundation
import UIKit

class GetFeedsInteractor: NSObject {
    var homePresenter: GetFeedsInteractorToHomePresenterProtocol?
    var searchPresenter: GetFeedsInteractorToSearchPresenterProtocol?
}

extension GetFeedsInteractor: HomePresenterToGetFeedsInteractorProtocol {
    func fetchUpdatedFeedItems(count: Int, withParams: NSDictionary) {
        let defaults = UserDefaults.standard
        var parameters: [String: Any] = [:]
        parameters[APIParams.userID] = defaults.value(forKey: UserDefaultsKey.userID)
        parameters[APIParams.numberOfPosts] = count
        parameters[APIParams.postOffset] = 0
        if let mergeParams = withParams as? [String: Any] {
            parameters.merge(dict: mergeParams)
        }
        //
        APIManager.post(endPoint: EndPoint.getFeeds, parameters: parameters) { (response, error) in
            guard error == nil, let response = response else {
                self.homePresenter?.handleUpdatedItems([], error: error?.message)
                return
            }
            let posts = response[APIResponse.apiResponse][APIResponse.apiPosts].array
            let message = response[APIResponse.apiResponse][APIResponse.apiResponseMessage].stringValue
            self.homePresenter?.handleUpdatedItems(posts ?? [], error: message)
        }
    }
    func fetchMyPosts(from index: Int, to count: Int) {
        let parameters = NSDictionary(dictionary: [GetFeedOptions.feedTypeKey.rawValue: GetFeedOptions.feedCreatedPostFilterKey.rawValue])
        fetchFeeds(from: index, to: count, withParams: parameters)
    }
    func fetchFeeds(from index: Int, to count: Int, withParams: NSDictionary) {
        let defaults = UserDefaults.standard
        var parameters: [String: Any] = [:]
        parameters[APIParams.userID] = defaults.value(forKey: UserDefaultsKey.userID)
        parameters[APIParams.numberOfPosts] = count
        parameters[APIParams.postOffset] = index
        if let mergeParams = withParams as? [String: Any] {
            parameters.merge(dict: mergeParams)
        }
        //
        APIManager.post(endPoint: EndPoint.getFeeds, parameters: parameters) { (response, error) in
            guard error == nil, let response = response else {
                self.homePresenter?.handleFeeds([], error: error?.message)
                return
            }
            let posts = response[APIResponse.apiResponse][APIResponse.apiPosts].array
            let message = response[APIResponse.apiResponse][APIResponse.apiResponseMessage].stringValue
            let notificationCount = response[APIResponse.apiResponse]["has_new_notifications"].intValue
            let defaults  = UserDefaults.standard
            defaults.set(notificationCount, forKey: UserDefaultsKey.newNotificationsAvailable)
            self.homePresenter?.handleFeeds(posts ?? [], error: message)
        }
    }
}
extension GetFeedsInteractor: SearchPresenterToGetFeedsInteractorProtocol {
    func fetchSearchFeeds(from index: Int, to count: Int, searchWith searchString: String, filterWith filterParam: NSMutableDictionary) {
        let defaults = UserDefaults.standard
        var parameters: [String: Any] = [:]
        parameters[APIParams.userID] = defaults.value(forKey: UserDefaultsKey.userID)
        parameters[APIParams.numberOfPosts] = count
        parameters[APIParams.postOffset] = index
        let param = filterParam as Dictionary
        if let mergeParams = param as? [String: Any] {
            parameters.merge(dict: mergeParams)
        }
        if !searchString.isEmpty {
            parameters[APIParams.searchKey] = searchString
        } else {
            parameters[APIParams.searchKey] = Constants.emptyString
        }
        //
        APIManager.post(endPoint: EndPoint.getFeeds, parameters: parameters) { (response, error) in
            guard error == nil, let response = response else {
                self.searchPresenter?.handleFeeds([], error: error?.message)
                return
            }
            let posts = response[APIResponse.apiResponse][APIResponse.apiPosts].array
            let message = response[APIResponse.apiResponse][APIResponse.apiResponseMessage].stringValue
            self.searchPresenter?.handleFeeds(posts ?? [], error: message)
        }
    }
}
