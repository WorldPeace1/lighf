//
//  UILabel.swift
//  Lighf
//
//  Created by L.
//  Copyright © 2019 L. All rights reserved.
//

import UIKit

extension UILabel {

    static func height(for text: String, width: CGFloat, font: UIFont?) -> CGFloat {
        //
        let label = UILabel(frame: CGRect(x: 0, y: 0, width: width, height: CGFloat.greatestFiniteMagnitude))
        label.numberOfLines = 0
        label.lineBreakMode = NSLineBreakMode.byWordWrapping
        label.font = font
        label.text = text
        //
        label.sizeToFit()
        return label.frame.height
    }
    //
    static func width(for text: String, height: CGFloat, font: UIFont?) -> CGFloat {
        //
        let label = UILabel(frame: CGRect(x: 0.0, y: 0.0, width: CGFloat.greatestFiniteMagnitude, height: height))
        label.numberOfLines = 0
        label.lineBreakMode = NSLineBreakMode.byWordWrapping
        label.font = font
        label.text = text
        //
        label.sizeToFit()
        return label.frame.width
    }
}
