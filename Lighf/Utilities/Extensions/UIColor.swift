//
//  UIColor.swift
//  Lighf
//
//  Created by L.
//  Copyright © 2019 L. All rights reserved.
//

import UIKit

extension UIColor {
    //
    static var appGreenColor: UIColor {
        return UIColor(red: 50/255.0, green: 203/255.0, blue: 3/255.0, alpha: 1.0)
    }
    static var relatableGreenColor: UIColor {
        return UIColor(red: 50/255.0, green: 203/255.0, blue: 3/255.0, alpha: 0.5)
    }

    // base color for Window
    static var appBlueColor: UIColor {
        return UIColor(red: 29/255.0, green: 134/255.0, blue: 250/255.0, alpha: 1.0)
    }
    //
    static var nodeUnSelectedColor: UIColor {
        return UIColor.init(red: 0.129, green: 0.568, blue: 0.984, alpha: 1.0)
    }
    //
    static var nodeSelectedColor: UIColor {
        return UIColor.init(red: 0.2, green: 0.8, blue: 0.2, alpha: 1.0)
    }
    static var relateableStatementsColor: UIColor {
        return UIColor.init(red: 88/255.0, green: 88/255.0, blue: 88/255.0, alpha: 1.0)
    }
    // navigation bar subtitle color
    static var appGreyColor: UIColor {
        return UIColor(red: 118/255.0, green: 118/255.0, blue: 118/255.0, alpha: 1.0)
    }
    //
    static var seperatorColor: UIColor {
        return UIColor(red: 112/255.0, green: 112/255.0, blue: 112/255.0, alpha: 1.0)
    }
    static var commentsReplyBar: UIColor {
        return UIColor(red: 181/255.0, green: 181/255.0, blue: 181/255.0, alpha: 1.0)
    }
    static var textFieldTitleColor: UIColor {
        return UIColor(displayP3Red: 76/255.0, green: 82/255.0, blue: 100/255.0, alpha: 1.0)
    }
    static var giveLeafBackgroundColor: UIColor {
        return UIColor(red: 70/255.0, green: 95/255.0, blue: 70/255.0, alpha: 1.0)
    }
    static var giveFlagBackgroundColor: UIColor {
        return UIColor(red: 41/255.0, green: 34/255.0, blue: 34/255.0, alpha: 1.0)
    }
    static var toggleNotificationBackgroundColor: UIColor {
        return UIColor(red: 149/255.0, green: 149/255.0, blue: 149/255.0, alpha: 1.0)
    }
}
