//
//  UITextField.swift
//  Lighf
//
//  Created by L.
//  Copyright © 2019 L. All rights reserved.
//

import UIKit
import SkyFloatingLabelTextField

extension UITextField {
    //
    /// Method to check whether the textfield contains text
    ///
    /// - Returns: returns false if textfield contains any characters
    func isEmpty() -> Bool {
        if let text = text, !text.isEmpty {
            return false
        }
        return true
    }
    //
    /// Add a placeholder to the textfield with custom color
    ///
    /// - Parameters:
    ///   - text: placeholder text
    ///   - color: Custom color for the placeholder
    func placeHolder(text: String, color: UIColor) {
        self.attributedPlaceholder = NSAttributedString(string: text, attributes: [NSAttributedString.Key.foregroundColor: color])
    }
}

extension SkyFloatingLabelTextField {
    //
    /// Modify the title to the text exactly as the placeholder
    func modifyTitleFormatter() {
        self.titleFormatter = { (text: String) in return text }
    }
}

// MARK: Show/Hide Support

extension UITextField {
    @IBAction func showHideButtonTapped(sender: UIButton) {
        self.isSecureTextEntry.toggle()
        
        guard let button = self.rightView as? UIButton else { return }
        
        if self.isSecureTextEntry {
            button.setImage(Images.passwordShow.image, for: .normal)
        } else {
            button.setImage(Images.passwordHide.image, for: .normal)
        }
        
        button.setImageTintColor(sender.tintColor, for: .normal)
    }
    
    func supportShowHide(tintColor: UIColor = .white) {
        let button = UIButton(frame: .zero)
        button.setImage(Images.passwordShow.image, for: .normal)
        button.setImageTintColor(tintColor, for: .normal)
        button.addTarget(self, action: #selector(showHideButtonTapped(sender:)), for: .touchUpInside)
        button.frame = CGRect(x: CGFloat(self.frame.size.width - 25), y: CGFloat(5), width: CGFloat(25), height: CGFloat(25))
        self.rightViewMode = .always
        self.rightView = button
    }
}
