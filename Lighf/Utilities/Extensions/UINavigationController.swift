//
//  UINavigationController.swift
//  Lighf
//
//  Created by L.
//  Copyright © 2019 L. All rights reserved.
//

import UIKit

extension UINavigationController {

    // Set navigation bar background transparent
    func setTransparent() {
        self.navigationBar.setBackgroundImage(UIImage(), for: .default)
        self.navigationBar.shadowImage = UIImage()
        self.navigationBar.isTranslucent = true
        self.view.backgroundColor = .clear
    }
    //
    func resetBottomBarLine() {
        self.navigationBar.setBackgroundImage(nil, for: .default)
        self.navigationBar.shadowImage = nil
    }
    //
    // Set navigation bar color properties
    func titleColor(_ color: UIColor, font: Font, size: CGFloat) {
        self.navigationBar.tintColor = color
        self.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: color,
                                                            NSAttributedString.Key.font: UIFont(name: font.rawValue, size: size)!]
    }
    //
    // Set navigationbar background color
    func setBackGroundColor(_ color: UIColor) {
        self.navigationBar.barTintColor = color
    }
    //
    // Reset background color
    func resetBackgroundColor() {
        self.navigationBar.barTintColor = nil
    }
}

extension UINavigationItem {
    // Add custom view to navigation item with title and subtitle
    func setTitle(title: String, subTitle: String) {
        //
        let titleLabel = UILabel()
        titleLabel.text = title
        titleLabel.font = UIFont(name: Font.sfProDisplayMedium.rawValue, size: 18.0)
        titleLabel.textColor = UIColor.black
        titleLabel.sizeToFit()
        //
        let subTitleLabel = UILabel()
        subTitleLabel.text = subTitle
        subTitleLabel.font = UIFont(name: Font.sfProTextRegular.rawValue, size: 13.0)
        subTitleLabel.textColor = UIColor.appGreyColor
        subTitleLabel.textAlignment = .center
        subTitleLabel.sizeToFit()
        //
        let stackView = UIStackView(arrangedSubviews: [titleLabel, subTitleLabel])
        stackView.distribution = .equalCentering
        stackView.axis = .vertical
        //
        let width = max(titleLabel.frame.size.width, subTitleLabel.frame.size.width)
        stackView.frame = CGRect(x: 0, y: 0, width: width, height: 35)
        //
        titleLabel.sizeToFit()
        subTitleLabel.sizeToFit()
        self.titleView = stackView
    }
}
