//
//  Date.swift
//  Lighf
//
//  Created by L.
//  Copyright © 2019 L. All rights reserved.
//

import UIKit

extension Date {
    //
    func differenceFromCurrentDate() -> String {
        let currentDate = Date()
        let calender = Calendar.current
        let components: [Calendar.Component] = [.year, .month, .day, .hour, .minute, .second]
        let dateComponents = calender.dateComponents(Set(components), from: self, to: currentDate)
        //
        for component in components {
            if let value = dateComponents.value(for: component), value != 0 {
                return "\(value) \(component.stringValue)"
            }
        }
        return ""
    }
}

extension Calendar.Component {
   //
    var stringValue: String {
        switch self {
        case .year:
            return "year"
        case .month:
            return "month"
        case .day:
            return "day"
        case .hour:
            return "hour"
        case .minute:
            return "minute"
        case .second:
            return "second"
        default:
            return ""
        }
    }
}
