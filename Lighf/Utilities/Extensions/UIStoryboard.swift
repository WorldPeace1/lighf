//
//  UIStoryBoard.swift
//  Lighf
//
//  Created by L.
//  Copyright © 2019 L. All rights reserved.
//

import UIKit

enum StoryBoard: String {
    case main = "Main"
    case story = "Story"
    case postDetails = "PostDetails"
    case settings = "Settings"
    //
    var instance: UIStoryboard {
        return UIStoryboard(name: self.rawValue, bundle: Bundle.main)
    }
}

extension UIStoryboard {
    static var main: UIStoryboard {
        return StoryBoard.main.instance
    }
    //
    static var story: UIStoryboard {
        return StoryBoard.story.instance
    }
    //
    static var postDetails: UIStoryboard {
        return StoryBoard.postDetails.instance
    }
    static var settings: UIStoryboard {
        return StoryBoard.settings.instance
    }
}
