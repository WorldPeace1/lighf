//
//  UIAlertController.swift
//  Lighf
//
//  Created by L.
//  Copyright © 2019 L. All rights reserved.
//

import UIKit

extension UIAlertController {

    static func alert(with message: String) -> UIAlertController {
//        let alert = UIAlertController(title: "", message: message, preferredStyle: .alert)
        var alert: UIAlertController!
        // This is a way to check the alert message and replace it wih custom message as per the request from client
        if message.lowercased().contains(Message.requestTimeOut) || message.lowercased().contains(Message.messageNotFound) {
            alert = UIAlertController(title: "Uh oh!", message: Message.connectionLost, preferredStyle: UIAlertController.Style.alert)
        } else {
            alert = UIAlertController(title: Constants.emptyString, message: message, preferredStyle: .alert)
        }
        let alertAction = UIAlertAction(title: "OK", style: .cancel, handler: nil)
        alert.addAction(alertAction)
        return alert
    }
}
