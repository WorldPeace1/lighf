//
//  String.swift
//  Lighf
//
//  Created by L.
//  Copyright © 2019 L. All rights reserved.
//

import UIKit

extension String {
    //
    func isValidUsername() -> Bool {
        // Make sure the name does not contain special characters except ".", "-" and "_"
        let requiredSet = CharacterSet(charactersIn: ".-_").union(.alphanumerics)
        if self.rangeOfCharacter(from: requiredSet.inverted) != nil {
            return false
        }
        return true
    }
//    Let's say at least 7 characters for now. This is flexible correct?
//
//    Pattern should be:
//    at least 1 lowercase
//    at least 1 uppercase
//    at least 1 number
//    at least 1 special character
    func isValidPassword() -> Bool {
        if self.count < 7 {
            return false
        }
        if !checkTextSufficientComplexity(text: self) {
            return false
        }
        return true
    }
    func checkTextSufficientComplexity(text: String) -> Bool {
        let upperLetterRegEx  = ".*[A-Z]+.*"
        let upperCheck = NSPredicate(format: "SELF MATCHES %@", upperLetterRegEx)
        let upperresult = upperCheck.evaluate(with: text)
        let lowerLetterRegEx  = ".*[a-z]+.*"
        let lowerCheck = NSPredicate(format: "SELF MATCHES %@", lowerLetterRegEx)
        let lowerresult = lowerCheck.evaluate(with: text)
        let numberRegEx  = ".*[0-9]+.*"
        let numberCheck = NSPredicate(format: "SELF MATCHES %@", numberRegEx)
        let numberresult = numberCheck.evaluate(with: text)
        let regex = try? NSRegularExpression(pattern: ".*[^A-Za-z0-9].*", options: NSRegularExpression.Options())
        var isSpecial: Bool = false
        if regex?.firstMatch(in: self, options: NSRegularExpression.MatchingOptions(), range: NSRange(location: 0, length: self.count)) != nil {
            isSpecial = true
        } else {
            isSpecial = false
        }
        return upperresult && lowerresult && numberresult && isSpecial
    }
    //to remove leading and trailing white spaces.
    func removeWhiteSpace() -> String {
        return self.trimmingCharacters(in: .whitespaces)
    }
    //
    func removeWhiteSpaceAndNewLine() -> String {
        return self.trimmingCharacters(in: .whitespacesAndNewlines)
    }
    //
    var youtubeID: String? {
        let pattern = "((?<=(v|V)/)|(?<=be/)|(?<=(\\?|\\&)v=)|(?<=embed/))([\\w-]++)"
        let regex = try? NSRegularExpression(pattern: pattern, options: .caseInsensitive)
        let range = NSRange(location: 0, length: count)
        //
        guard let result = regex?.firstMatch(in: self, range: range) else {
            return nil
        }
        //
        return (self as NSString).substring(with: result.range)
    }
    func subString (indexPosition: Int) -> Character {
        return self[index(startIndex, offsetBy: indexPosition)]
    }
    func substring(to: Int) -> String {
        var subStringLength = to
        if subStringLength < 0 {
            subStringLength = 0
        }
        let toIndex = self.index(self.startIndex, offsetBy: subStringLength)
        return String(self[...toIndex])
    }
}

extension NSMutableAttributedString {
    /// Method to remove space and new line from the attributed string
    ///
    /// - Returns: Attributed string trimmed of newlines and space
    func removeWhiteSpacesAndNewline() -> NSAttributedString {
        let invertedSet = CharacterSet.whitespacesAndNewlines.inverted
        let startRange = string.rangeOfCharacter(from: invertedSet)
        let endRange = string.rangeOfCharacter(from: invertedSet, options: .backwards)
        guard let startLocation = startRange?.upperBound, let endLocation = endRange?.lowerBound else {
            return NSAttributedString(string: string)
        }
        let location = string.distance(from: string.startIndex, to: startLocation) - 1
        let length = string.distance(from: startLocation, to: endLocation) + 2
        let range = NSRange(location: location, length: length)
        return attributedSubstring(from: range)
    }
    /// Method to trim characters from the attributted string
    ///
    /// - Parameter charSet: The character set of which the chacters should be removed
    /// - Returns: Returns the modified attributted string
    func trimCharactersInSet(charSet: CharacterSet) -> NSAttributedString {
        var range = (string as NSString).rangeOfCharacter(from: charSet)
        // Trim leading characters from character set.
        while range.length != 0 && range.location == 0 {
            replaceCharacters(in: range, with: "")
            range = (string as NSString).rangeOfCharacter(from: charSet)
        }
        // Trim trailing characters from character set.
        range = (string as NSString).rangeOfCharacter(from: charSet, options: .backwards)
        while range.length != 0 && NSMaxRange(range) == length {
            replaceCharacters(in: range, with: "")
            range = (string as NSString).rangeOfCharacter(from: charSet, options: .backwards)
        }
        return self
    }
    /// Method to set just the font of attributed string
    ///
    /// - Parameters:
    ///   - font: Font to be changed
    ///   - color: color to be added
    func setFont(font: UIFont, changeInSize: CGFloat = 0, color: UIColor? = nil) {
        beginEditing()
        self.enumerateAttribute(.font, in: NSRange(location: 0, length: self.length)) { (value, range, _) in
            if let defaultFont = value as? UIFont, let newFontDescriptor = defaultFont.fontDescriptor.withFamily(font.familyName).withSymbolicTraits(defaultFont.fontDescriptor.symbolicTraits) {
                let newFont = UIFont(descriptor: newFontDescriptor, size: defaultFont.pointSize + changeInSize)
                removeAttribute(.font, range: range)
                addAttribute(.font, value: newFont, range: range)
                if let color = color {
                    removeAttribute(.foregroundColor, range: range)
                    addAttribute(.foregroundColor, value: color, range: range)
                }
            }
        }
        endEditing()
    }
}
extension Dictionary {
    mutating func merge(dict: [Key: Value]) {
        for (key, value) in dict {
            updateValue(value, forKey: key)
        }
    }
}
