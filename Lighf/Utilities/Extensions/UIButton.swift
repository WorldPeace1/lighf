//
//  UIButton.swift
//  Lighf
//
//  Created by L.
//  Copyright © 2019 L. All rights reserved.
//

import UIKit

extension UIButton {
    //
    func enable() {
        if !self.isEnabled {
            self.isEnabled = true
        }
    }
    //
    func disable() {
        if self.isEnabled {
            self.isEnabled = false
        }
    }
    //
    static func customNavigationRightBarButton(withTitle title: String) -> UIButton {
        let customButton = UIButton(type: .custom)
        customButton.setTitle(title, for: .normal)
        customButton.setTitleColor(UIColor.appGreenColor, for: .normal)
        customButton.setTitleColor(UIColor.appGreenColor.withAlphaComponent(0.5), for: .disabled)
        customButton.titleLabel?.font = UIFont(name: Font.sfProDisplayMedium.rawValue, size: 16)
        return customButton
    }
    //
    class func button(with imageName: String) -> UIButton {
        let button = UIButton(type: .custom)
        let image = UIImage(named: imageName)
        button.setImage(image, for: .normal)
        return button
    }
    
    func setImageTintColor(_ color: UIColor, for state: UIControl.State = .normal) {
        guard let image = self.currentImage else { return }
        self.setImage(image.withRenderingMode(.alwaysTemplate), for: state)
        self.tintColor = color
    }
}
