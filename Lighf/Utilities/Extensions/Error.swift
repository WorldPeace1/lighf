//
//  Error.swift
//  Lighf
//
//  Created by L.
//  Copyright © 2019 L. All rights reserved.
//

import UIKit

extension Error {
    //
    var message: String {
        if let error = self as? NetworkError, error == NetworkError.notReachable {
            return Message.networkUnavailable
        } else {
            return self.localizedDescription
        }
    }
}
