//
//  Media.swift
//  Lighf
//
//  Created by L.
//  Copyright © 2019 L. All rights reserved.
//

import UIKit

struct Media {
    var youtubeLink: Link = Link()
    var musicLink: Link = Link()
    var podcastLink: Link = Link()
    var articleLink: Link = Link()
    //
    func linkForType(_ type: MediaType?) -> Link? {
        //
        guard let type = type else {
            return nil
        }
        //
        switch type {
        case .youtube:
            return youtubeLink
        case .music:
            return musicLink
        case .podcast:
            return podcastLink
        case .article:
            return articleLink
        }
    }
    //
    func isValid() -> Bool {
        return youtubeLink.isValid &&
            musicLink.isValid &&
            podcastLink.isValid &&
            articleLink.isValid
    }
    //
    static func isValidArticleUrl(_ urlString: String) -> Bool {
        let urlRegEx = "^(https?://)?(www\\.)?([-a-z0-9]{1,63}\\.)*?[a-z0-9][-a-z0-9]{0,61}[a-z0-9]\\.[a-z]{2,6}(/[-\\w@\\+\\.~#\\?&/=%]*)?$"
        let urlTest = NSPredicate(format: "SELF MATCHES %@", urlRegEx)
        let result = urlTest.evaluate(with: urlString)
        if result == false {
             return result
        } else {
            // This is an additional check to ensure the valildity of url
            if let url = URL(string: urlString), UIApplication.shared.canOpenURL(url) {
                return true
            }
            return false
        }
    }
    //
    func isEmpty() -> Bool {
        return youtubeLink.url.removeWhiteSpace() == Constants.emptyString &&
            musicLink.url.removeWhiteSpace() == Constants.emptyString &&
            podcastLink.url.removeWhiteSpace() == Constants.emptyString &&
            articleLink.url.removeWhiteSpace() == Constants.emptyString
    }
}

struct Link {
    var url: String = ""
    var isValid: Bool = true
    var isValidityChecked = false
}
