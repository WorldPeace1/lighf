//
//  Tag.swift
//  Lighf
//
//  Created by L.
//  Copyright © 2019 L. All rights reserved.
//

import UIKit

/// Model for tags
struct Tag {
    var id: Int
    var name: String
    var slug: String
    var isFollowed: Bool = false
}
