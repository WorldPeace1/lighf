//
//  Post.swift
//  Lighf
//
//  Created by L.
//  Copyright © 2019 L. All rights reserved.
//

import UIKit

protocol Post {
    var postId: Int? {get set}
    var imageUrl: String {get set}
    var fullSizeImageUrl: String { get set }
    var postImage: UIImage? {get set}
    var media: Media {get set}
    var tags: [String] {get set}
    var photoCredit: String {get set}
    var type: PostType { get }
    // flags
    var isRead: Bool {get set}
    var isRelated: Bool {get set}
    var isBackPacked: Bool {get set}
    var isOwnPost: Bool {get set}
    var isNotificationEnabled: Bool {get set}
    var isDeleted: Bool { get set }
    var isEditable: Bool { get set }
}

extension Post {
    static var maxTagsCount: Int {
        return 5
    }
}
