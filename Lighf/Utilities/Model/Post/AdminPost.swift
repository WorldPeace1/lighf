//
//  AdminPost.swift
//  Lighf
//
//  Created by L.
//  Copyright © 2019 L. All rights reserved.
//

import UIKit
import SwiftyJSON

struct AdminPost: Post {
    var postImage: UIImage?
    
    var shortStory: NSAttributedString
    var longStory: NSAttributedString
    
    // Post Protocol
    var postId: Int?
    var fullSizeImageUrl: String
    var imageUrl: String
    var media: Media
    var tags: [String]
    var photoCredit: String
    var type: PostType
    var isFlagged: Bool = false
    var isLeafGiven: Bool = false
    var isOwnPost: Bool = false
    var isEditable: Bool = false
    var isDeleteable: Bool = false
    var isRead: Bool
    var isRelated: Bool
    var isBackPacked: Bool
    var isNotificationEnabled: Bool = false
    var isDeleted: Bool = false
    var authorID: Int?
    
    // Character constaints for Story
    static let shortStoryMaxCharCount = 365
    static let longStoryMaxCharCount = 610
    
    init(adminPostObj: JSON) {
        self.postId = adminPostObj["post_id"].int
        
        let title = adminPostObj["title"].stringValue
        if title == Constants.emptyString {
            shortStory = NSAttributedString(string: "")
        } else {
            shortStory = NSAttributedString(string: "\(title)")
        }
        
        longStory = NSAttributedString(string: adminPostObj["post_body"].stringValue)
        fullSizeImageUrl = adminPostObj["image_urls"]["full"].stringValue
        
        if let compactImageUrl = adminPostObj["image_urls"]["l_image_360_230"].string {
            imageUrl = compactImageUrl
        } else {
            imageUrl = fullSizeImageUrl
        }
        
        media = Media()
        media.youtubeLink.url = adminPostObj["video_link"].stringValue
        media.musicLink.url = adminPostObj["audio_link"].stringValue
        media.podcastLink.url = adminPostObj["podcast_link"].stringValue
        media.articleLink.url = adminPostObj["article_link"].stringValue
        
        // Get tags from JSON
        let tagArray = adminPostObj["tags"].arrayValue
        tags = tagArray.map {$0["name"].stringValue}
        photoCredit = adminPostObj["image_credits"].stringValue
        type = .story
        isRelated = adminPostObj["related_to"].boolValue
        isRead = adminPostObj["is_read"].boolValue
        isBackPacked = adminPostObj["saved_to_backpack"].boolValue
        isOwnPost = adminPostObj["is_own_post"].boolValue
        authorID = adminPostObj["post_author"].intValue
        isEditable = adminPostObj["is_editable"].boolValue
        
        if isOwnPost {
            isDeleteable = true
        }
        
        let notificationStatus = adminPostObj["notifications_status"].stringValue
        
        if notificationStatus == "notifications_on" {
            isNotificationEnabled = true
        } else {
            isNotificationEnabled = false
        }
    }
    
    init(with shortStory: NSAttributedString) {
        self.shortStory = shortStory
        longStory = NSAttributedString(string: Constants.emptyString)
        imageUrl = ""
        fullSizeImageUrl = ""
        media = Media()
        tags = []
        photoCredit = ""
        type = .story
        isRead = false
        isRelated = false
        isBackPacked = false
    }
    
    mutating func leafGiven() -> Bool {
        self.isLeafGiven = !self.isLeafGiven
        return true
    }
    
    mutating func toggleFlaggedFlag() -> Bool {
        self.isFlagged = !self.isFlagged
        return true
    }
    
    mutating func editTapped() -> Bool {
        return true
    }
    
    mutating func deleteTapped() -> Bool {
        return true
    }
    
    mutating func toggleNotificationSettings() -> Bool {
        self.isNotificationEnabled = !self.isNotificationEnabled
        return true
    }
}
