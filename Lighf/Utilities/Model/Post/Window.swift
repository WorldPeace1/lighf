//
//  Window.swift
//  Lighf
//
//  Created by L.
//  Copyright © 2019 L. All rights reserved.
//

import UIKit
import SwiftyJSON

struct Window: Post {
    var isEditable: Bool = false
    var title: NSAttributedString
    var body: NSAttributedString
    var authorName: String
    var statements: [String]
    //
    var isOwnPost: Bool
    var postImage: UIImage?
    var imageUrl: String
    var fullSizeImageUrl: String
    var type: PostType
    var postId: Int?
    var media: Media
    var tags: [String]
    var photoCredit: String
    var isFlagged: Bool = false
    var isLeafGiven: Bool = false
    var isDeletable: Bool = false
    var isRead: Bool
    var isRelated: Bool
    var isBackPacked: Bool
    var isNotificationEnabled: Bool = false
    var isDeleted: Bool = false
    var authorID: Int?
    //
    mutating func leafGiven() -> Bool {
        self.isLeafGiven = !self.isLeafGiven
        return true
    }
    mutating func toggleFlaggedFlag() -> Bool {
        self.isFlagged = !self.isFlagged
        return true
    }
    init(title: NSAttributedString) {
        self.title = NSAttributedString(string: title.string)
        body = NSAttributedString(string: Constants.emptyString)
        authorName = Constants.emptyString
        statements = []
        self.imageUrl = ""
        fullSizeImageUrl = ""
        self.photoCredit = ""
        type = .window
        media = Media()
        tags = []
        isRead = false
        isRelated = false
        isBackPacked = false
        isOwnPost = false
    }
    init(windowObj: JSON) {
//        let imageUrlArray = windowObj["image_urls"].array
//        let excerpt = windowObj["excerpt"].stringValue
//        let postURL =  windowObj["post_url"].stringValue
        title = NSAttributedString(string: windowObj["title"].stringValue)
        body = NSAttributedString(string: windowObj["post_body"].stringValue)
        //
        fullSizeImageUrl = windowObj["image_urls"]["full"].stringValue
        if let compactImageUrl = windowObj["image_urls"]["l_image_150_160"].string {
            imageUrl = compactImageUrl
        } else {
            imageUrl = fullSizeImageUrl
        }
        type = .window
        photoCredit = windowObj["image_credits"].stringValue
        //Get media details from json
        media = Media()
        media.youtubeLink.url = windowObj["video_link"].stringValue
        media.musicLink.url = windowObj["audio_link"].stringValue
        media.podcastLink.url = windowObj["podcast_link"].stringValue
        media.articleLink.url = windowObj["article_link"].stringValue
        // Get tags from json
        let tagArray = windowObj["tags"].arrayValue
        tags = tagArray.map {$0["name"].stringValue}
        isRead = windowObj["is_read"].boolValue
        isRelated = windowObj["related_to"].boolValue
        isBackPacked = windowObj["saved_to_backpack"].boolValue
        isOwnPost = windowObj["is_own_post"].boolValue
        postId = windowObj["post_id"].intValue
        authorID = windowObj["post_author"].intValue
        authorName = windowObj["window_author_name"].stringValue
        let notificationStatus = windowObj["notifications_status"].stringValue
        if notificationStatus == "notifications_on" {
            isNotificationEnabled = true
        } else {
            isNotificationEnabled = false
        }
        statements = []
    }
    mutating func toggleNotificationSettings() -> Bool {
        self.isNotificationEnabled = !self.isNotificationEnabled
        return true
    }
}
