//
//  Story.swift
//  Lighf
//
//  Created by L.
//  Copyright © 2019 L. All rights reserved.
//

import UIKit
import SwiftyJSON

struct Story: Post {
    var postImage: UIImage?
    //
    var shortStory: NSAttributedString
    var longStory: NSAttributedString
    //
    // Post Protocol
    var postId: Int?
    var fullSizeImageUrl: String
    var imageUrl: String
    var media: Media
    var tags: [String]
    var photoCredit: String
    var type: PostType
    var isFlagged: Bool = false
    var isLeafGiven: Bool = false
    var isOwnPost: Bool = false
    var isEditable: Bool = false
    var isDeleteable: Bool = false
    var isRead: Bool
    var isRelated: Bool
    var isBackPacked: Bool
    var isNotificationEnabled: Bool = false
    var isDeleted: Bool = false
    var authorID: Int?
    //
    // Character constaints for Story
    static let shortStoryMaxCharCount = 365
    static let longStoryMaxCharCount = 610
    //
    init(storyObj: JSON) {
        self.postId = storyObj["post_id"].int
//        let imageUrlArray = storyObj["image_urls"].array
//        let excerpt = storyObj["excerpt"].stringValue
//        let postURL =  storyObj["post_url"].stringValue
        let title = storyObj["title"].stringValue
        if title == Constants.emptyString {
            shortStory = NSAttributedString(string: "")
        } else {
            shortStory = NSAttributedString(string: "\(title)")
        }
        longStory = NSAttributedString(string: storyObj["post_body"].stringValue)
        fullSizeImageUrl = storyObj["image_urls"]["full"].stringValue
        if let compactImageUrl = storyObj["image_urls"]["l_image_360_230"].string {
            imageUrl = compactImageUrl
        } else {
            imageUrl = fullSizeImageUrl
        }
        media = Media()
        media.youtubeLink.url = storyObj["video_link"].stringValue
        media.musicLink.url = storyObj["audio_link"].stringValue
        media.podcastLink.url = storyObj["podcast_link"].stringValue
        media.articleLink.url = storyObj["article_link"].stringValue
        // Get tags from JSON
        let tagArray = storyObj["tags"].arrayValue
        tags = tagArray.map {$0["name"].stringValue}
        photoCredit = storyObj["image_credits"].stringValue
        type = .story
        isRelated = storyObj["related_to"].boolValue
        isRead = storyObj["is_read"].boolValue
        isBackPacked = storyObj["saved_to_backpack"].boolValue
        isOwnPost = storyObj["is_own_post"].boolValue
        authorID = storyObj["post_author"].intValue
        isEditable = storyObj["is_editable"].boolValue
        if isOwnPost {
            isDeleteable = true
        }
        let notificationStatus = storyObj["notifications_status"].stringValue
        if notificationStatus == "notifications_on" {
            isNotificationEnabled = true
        } else {
            isNotificationEnabled = false
        }
    }
    init(with shortStory: NSAttributedString) {
        self.shortStory = shortStory
//        let excerpt = ""
//        let postURL =  ""
        longStory = NSAttributedString(string: Constants.emptyString)
        imageUrl = ""
        fullSizeImageUrl = ""
        media = Media()
        tags = []
        photoCredit = ""
        type = .story
        isRead = false
        isRelated = false
        isBackPacked = false
    }
    mutating func leafGiven() -> Bool {
        self.isLeafGiven = !self.isLeafGiven
        return true
    }
    mutating func toggleFlaggedFlag() -> Bool {
        self.isFlagged = !self.isFlagged
        return true
    }
    mutating func editTapped() -> Bool {
        return true
    }
    mutating func deleteTapped() -> Bool {
        return true
    }
    mutating func toggleNotificationSettings() -> Bool {
        self.isNotificationEnabled = !self.isNotificationEnabled
        return true
    }
}
