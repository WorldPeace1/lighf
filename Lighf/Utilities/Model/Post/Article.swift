//
//  Article.swift
//  Lighf
//
//  Created by L.
//  Copyright © 2019 L. All rights reserved.
//

import UIKit
import SwiftyJSON
struct Article: Post {
    var postImage: UIImage?
    //
    var title: NSAttributedString
    var body: NSAttributedString
    var statements: [RelatableStatement]
    //
    // Post Protocol
    var postId: Int?
    var imageUrl: String
    var fullSizeImageUrl: String
    var media: Media
    var tags: [String]
    var photoCredit: String
    var type: PostType
    var isFlagged: Bool = false
    var isLeafGiven: Bool = false
    var isOwnPost: Bool = false
    var isEditable: Bool = false
    var isDeleteable: Bool = false
    var isRead: Bool
    var isRelated: Bool
    var isBackPacked: Bool
    var isNotificationEnabled: Bool = false
    var isDeleted: Bool = false
    var authorID: Int?
    //
    //Article character constraints
    static let titleMaxCharCount = 70
    static let bodyMaxCharCount = 10946
    static let maxStatements = 5
    //
    init(title: NSAttributedString) {
        self.title = title
        body = NSAttributedString(string: Constants.emptyString)
        statements = []
        imageUrl = ""
        fullSizeImageUrl = ""
        media = Media()
        tags = []
        photoCredit = ""
        type = .article
        isRead = false
        isRelated = false
        isBackPacked = false
    }
    init(articleObj: JSON) {
        self.postId = articleObj["post_id"].int
//        let imageUrlArray = articleObj["image_urls"].array
        let excerpt = articleObj["excerpt"].stringValue
//        let postURL =  articleObj["post_url"].string
        title = NSAttributedString.init(string: "\(String(describing: articleObj["title"].stringValue))")
        // If post body is nil go for excerpt
        var postBody = articleObj["post_body"].stringValue
        if postBody ==  Constants.emptyString {
            postBody = excerpt
        }
        body = NSAttributedString(string: postBody)
        fullSizeImageUrl = articleObj["image_urls"]["full"].stringValue
        if let compactImageUrl = articleObj["image_urls"]["l_image_150_210"].string {
            imageUrl = compactImageUrl
        } else {
            imageUrl = fullSizeImageUrl
        }
        let statementsArray = articleObj["relatable_statements"].arrayValue
        statements = statementsArray.map({RelatableStatement(statementId: $0["statement_id"].intValue, text: $0["statement"].stringValue, related: $0["related_to"].boolValue)})
        media = Media()
        media.youtubeLink.url = articleObj["video_link"].stringValue
        media.musicLink.url = articleObj["audio_link"].stringValue
        media.podcastLink.url = articleObj["podcast_link"].stringValue
        media.articleLink.url = articleObj["article_link"].stringValue
        // Get tags from JSON
        let tagArray = articleObj["tags"].arrayValue
        tags = tagArray.map {$0["name"].stringValue}
        photoCredit = articleObj["image_credits"].stringValue
        type = .article
        isRead = articleObj["is_read"].boolValue
        isRelated = articleObj["related_to"].boolValue
        isBackPacked = articleObj["saved_to_backpack"].boolValue
        isOwnPost = articleObj["is_own_post"].boolValue
        authorID = articleObj["post_author"].intValue
        isEditable = articleObj["is_editable"].boolValue
        if isOwnPost {
            isDeleteable = true
        }
        let notificationStatus = articleObj["notifications_status"].stringValue
        if notificationStatus == "notifications_on" {
            isNotificationEnabled = true
        } else {
            isNotificationEnabled = false
        }
    }
    mutating func leafGiven() -> Bool {
        self.isLeafGiven = !self.isLeafGiven
        return true
    }
    mutating func toggleFlaggedFlag() -> Bool {
        self.isFlagged = !self.isFlagged
        return true
    }
    mutating func toggleNotificationSettings() -> Bool {
        self.isNotificationEnabled = !self.isNotificationEnabled
        return true
    }
}
