//
//  Aspect.swift
//  Lighf
//
//  Created by L.
//  Copyright © 2019 L. All rights reserved.
//

import Foundation
import UIKit

struct Aspect {
    var aspectID: NSInteger
    var aspectName: String?
    var aspectSlug: String?
    //
    init(aspectId: NSInteger, name: String, slug: String) {
        aspectID = aspectId
        aspectName = name
        aspectSlug = slug
    }
}
