//
//  User.swift
//  Lighf
//
//  Created by L.
//  Copyright © 2019 L. All rights reserved.
//

import UIKit

struct User {
    var inviteCode: String
    var userName: String
    var password: String = ""
    var email: String = ""
    var firstName: String = ""
    var lastName: String = ""
    var phoneNumber: String = ""
    var mailOption: EmailOptions = .none
    
    init(name: String, inviteCode: String) {
        self.userName = name
        self.inviteCode = inviteCode
    }
}
