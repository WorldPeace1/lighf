//
//  Activity.swift
//  Lighf
//
//  Created by L.
//  Copyright © 2019 L. All rights reserved.
//

import UIKit

struct Activity {
    var title: String
    var detail: String
}
