//
//  PostComment.swift
//  Lighf
//
//  Created by L.
//  Copyright © 2019 L. All rights reserved.
//

import Foundation
import SwiftyJSON

struct PostComment {
    var commentID: NSInteger
    var commentText: String = ""
    var commentTimeStamp: String = ""
    var commentType: CommentType
    var parentCommentID: NSInteger
    var isFlagged: Bool = false
    var isLeafGiven: Bool = false
    var url: String
    var isApproved: Bool
    var userID: NSInteger
    var commentReplies: [PostComment]
    var isNotificationEnabled: Bool = false
    //
    init(postCommentID: NSInteger, text: String, timestamp: String, type: CommentType) {
        commentID = postCommentID
        commentText = text
        commentType = type
        commentTimeStamp = timestamp
        parentCommentID = 0
        url = Constants.emptyString
        isApproved = false
        userID = 0
        commentReplies = []
    }
    //
    init(from json: JSON) {
        commentType = CommentType(rawValue: json["comment_type"].stringValue) ?? .comment
        commentID = json["comment_id"].intValue
        url = json["comment_url"].stringValue
        commentText = json["comment_body"].stringValue
        isApproved = json["comment_approved"].boolValue
        userID = json["comment_user_id"].intValue
        let timeStamp = json["comment_date"].doubleValue
        let date = NSDate(timeIntervalSince1970: timeStamp)
        parentCommentID = json["comment_parent_id"].intValue
        commentReplies = json["comment_replies"].arrayValue.map {PostComment(from: $0)}
        commentTimeStamp = Utility().daysBetween(date1: date as Date, date2: Date()) as String
        let notificationStatus = json["notifications_status"].stringValue
        if notificationStatus == "notifications_on" {
            isNotificationEnabled = true
        } else {
            isNotificationEnabled = false
        }
    }
    //
    mutating func leafGiven() -> Bool {
        self.isLeafGiven = !self.isLeafGiven
        return true
    }
    mutating func toggleFlaggedFlag() -> Bool {
        self.isFlagged = !self.isFlagged
        return true
    }
}
