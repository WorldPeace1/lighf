//
//  RelatableStatement.swift
//  Lighf
//
//  Created by L.
//  Copyright © 2019 L. All rights reserved.
//

import Foundation

struct RelatableStatement {
    var statementID: NSInteger
    var statementText: String = ""
    var isRelated: Bool = false
    init(statementId: NSInteger, text: String, related: Bool) {
        statementID = statementId
        statementText = text
        isRelated = related
    }
}
