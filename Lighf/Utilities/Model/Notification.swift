//
//  Notification.swift
//  Lighf
//
//  Created by L.
//  Copyright © 2019 L. All rights reserved.
//

import Foundation
import SwiftyJSON
struct NotificationModel {
    var notificationID: NSInteger
    var notificationText: String = ""
    var notificationTimeStamp: String = ""
    var notificationDetailText: String = ""
    var notificationReadStatus: String = ""
    var notificationPlaceholder: String = ""
    var notificationStaticText: String = ""
    var notificationDynamicText: String = ""
    var notificationURL: String = ""
    var commentID: Int = 0
    var postID: Int = 0
    var notificationType: NotificationType?
    init(commentNotificationID: NSInteger, text: String, timestamp: String, detailText: String) {
        notificationID = commentNotificationID
        notificationText = text
        notificationTimeStamp = timestamp
        notificationDetailText = detailText
    }
    init(notificationObj: JSON) {
        self.notificationID = notificationObj["notification_id"].intValue
        self.notificationReadStatus = notificationObj["status"].stringValue
        self.notificationStaticText = notificationObj["content_static"].stringValue
        self.notificationDynamicText = notificationObj["content_dynamic"].stringValue
        self.notificationPlaceholder = notificationObj["content_placeholder"].stringValue
        self.notificationText = self.notificationStaticText.replacingOccurrences(of: self.notificationPlaceholder, with: self.notificationDynamicText)
        self.notificationURL = notificationObj["url"].stringValue
        let timeStamp = notificationObj["timestamp"].doubleValue
        let date = NSDate(timeIntervalSince1970: timeStamp)
        self.notificationTimeStamp = Utility().daysBetween(date1: date as Date, date2: Date()) as String
        self.commentID = notificationObj["comment_id"].intValue
        self.postID = notificationObj["post_id"].intValue
        self.notificationType = NotificationType(rawValue: notificationObj["type"].stringValue)
    }
}
