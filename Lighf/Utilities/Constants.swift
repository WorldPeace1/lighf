//
//  Constants.swift
//  Lighf
//
//  Created by L.
//  Copyright © 2019 L. All rights reserved.
//

import Foundation
import UIKit
open class Constants: NSObject {
    // MARK: - URLS
//    #if QA
//    static let kBaseURL = "https://lighf.com/l-api/v1.0/username_available/"
//    #elseif Staging
//    static let kBaseURL = ""
//    #endif
    static let kBaseURL = "https://lighf.com/l-api/v1.0/"
    static let kUseGuidelinesURL = "https://lighf.com/tou/"
    static let kGetHelpURL = "https://lighf.com/help"
    // MARK: - Storyboard segue identifiers
    static let kForgotPasswordSegue = "showForgotPasswordSegue"
    static let kNewUserNameSegue = "createNewUserNameSegue"
    //
    //
    // Max and min Character count for username
    static let maxCharacterCount = 24
    static let minCharacterCount = 3
    // Min Character count for password
    static let minPasswordCharacterCount = 7
    //
    static let emptyString = ""
    static let singleSpace = " "
    static let screenShiftUpValue: Float = 100.0
    static let kErrorKey = "Error"
    static let bulletCharacter = "\u{2022}"
    static let newLineCharacter = "\n"
    static let success = "ok"
    static let failure = "nok"
    static let kDeprecatedAPIVersionCode = "2"
    static let kWarningAPIVersionCode = "1"
    static let kSuccessAPIVersionCode = "0"
}
// API endpoints
enum EndPoint {
    static let signIn = "login"
    static let signOut = "signout"
    static let forgotPassword = "forgot_password"
    static let username = "username_available"
    static let signUp = "signup"
    static let signUpBeta = "signup_beta"
    static let getAspectList = "get_all_aspects"
    static let saveAspectList = "save_user_aspects"
    static let getSavedAspectList = "get_user_aspects"
    static let createPost = "create_post"
    static let getFeeds = "get_feed"
    static let addComment = "add_comment"
    static let getDetails = "get_post"
    static let comments = "get_comments"
    static let deletePost = "delete_post"
    static let getUserPoints = "get_user_leaves"
    static let markPost = "mark_post_as"
    static let getNotifications = "get_notifications"
    static let notificationMarkRead = "mark_notification_as"
    static let imageUpload = "upload_image"
    static let changeHeaderImage = "change_header_image"
    static let flagPost = "flag_item"
    static let userProfile = "get_user_profile"
    static let changePassword = "change_password"
    static let headerImage = "get_image"
    static let getTags = "get_tags"
    static let followTag = "follow_tag"
    static let deleteComment = "delete_comment"
    static let editComment = "edit_comment"
    static let giveLeaf = "give_leaves"
    static let toggleNotification = "turn_off_notifications"
    static let changeEmail = "change_email"
    static let confirm = "confirm"
    static let validateUserKey = "validate_user_key"
}
// API parameters
enum APIParams {
    static let userName = "username"
    static let password = "password"
    static let email = "email"
    static let userID = "id"
    static let numberOfPosts = "num_posts"
    static let postOffset = "offset"
    static let searchKey = "search_key"
    static let numberOfNotifications = "num_notifications"
    static let unReadNotifications = "unread_only"
    static let notificationID = "notification_id"
    static let notificationReadAction = "action"
    static let receivingUserID = "receiving_user_id"
    static let leafCount = "num_leaves"
    static let itemID = "item_id"
    static let itemType = "item_type"
    static let notificationAction = "action"
    static let key = "key"
    static let confirmationAtion = "action"
}

enum APIResponse {
    static let apiResponse = "api_response"
    static let apiUserPoints = "user_leaves"
    static let apiPosts = "posts"
    static let apiResponseMessage = "message"
    static let apiNotifications = "notifications"
    static let apiResponseStatus = "response_status"
    static let userId = "id"
    static let keyValidated = "key_validated"
}
enum UserDefaultsKey {
    static let deviceId = "deviceId"
    static let accessToken = "token"
    static let userName = "userName"
    static let userID = "userID"
    static let aspects = "aspects"
    static let updateFromSearch = "updateFromSearch"
    static let imageId = "imageId"
    static let newNotificationsAvailable = "hasNewNotifications"
    static let showPopUp = "showPopUp"
    static let showAlternatePostCreationOption = "showAlternatePostCreationOption"
    static let leafCount = "leafCount"
    static let introPageNum = "introPageNum"
    static let createOptions = "createOptions"
    static let postHistoryArray = "postHistoryArray"
}
// General messages involved in the app
enum Message {
    static let networkUnavailable = "You currently appear to be offline. Reconnect to current wi-fi or try another network including switching to your phone's data plan."
    static let signinError = "Incorrect lighfName or password"
    static let usernameUnavailable = "Sorry. lighfName not available"
    static let invalidUsername = "lighfName should contain only letters, #Numbers, (.)periods, (-)dashes and (_)underscores"
    static let passwordsNotMatching = "Passwords don't match"
    static let forgotPasswordError = "Incorrect lighfName or email"
    static let statementsFull = "You have added max number of statements"
    static let invalidYoutubeUrl = "This Youtube URL doesn't work.  Please try another."
    static let showArticle = "You are about to step out of lighf and visit a third-party external link."
    static let addComment = "Addition made successfully"
    static let addAdvice = "Addition made successfully"
    static let addQuestion = "Addition made successfully"
    static let addReply = "Reply added successfully"
    static let addPost = "You just floated (shared) a piece of your lighf successfully"
    static let editpost = "Edit successful"
    static let addToBackpack = "You just stored something in your BackPack"
    static let removeFromBackPack = "You just removed something from your BackPack"
    static let markAsRelate = "Just marked something relatable"
    static let markAsNotRelated = "Just unrelated to a piece of lighf"
    static let addFlag = "Action successful.  Thanks a lot for looking out for lighf.  If further participation is required, you will receive a follow up message.  Thanks again."
    static let feedbackSuccess = "Thanks a lot. lighf will keep an eye on this for you. After review, if further participation is required, you'll receive a follow up message. If you have a question, send lighf an SOS by Tapping the More Options / Vertical Ellipsis icon button where visible, e.g. on any dedicated lighfStory screen."
    static let deleteConfirmation = "I'm sure I want to delete"
    static let signOut = "Leavin' lighf? If you do, just make sure you get to enjoy some genuine time with friends. Get outside! Explore what's around you. Spend some time doing what you love. Help someone in need.  Or do all those things at the same time. When you get back, share pieces of your lighf with the world. It's waiting... "
    static let changePassword = "Password updated successfully"
    static let changeEmail = "Email updated successfully"
    static let invalidOldPassword = "Incorrect Old Password"
    static let invalidPost = "This piece of lighf does not yet exist.  Try viewing another lighfStory or piece of lighf."
    static let removePost = "You just removed a piece of your lighf"
    static let invalidSearchText = "Almost. Now enter more than 3 characters."
    static let followTag = "You just added a new tag to your lighf.  To remove it, search above. Then, swipe left on the tag you want to remove and Tap the green heart.  "
    static let unfollowTag = "You removed a tag from your lighf.  To add it again, search for it above.  Then, swipe left on the tag and Tap the gray heart. "
    static let messageNotFound = "This destination seems to be out of reach. Try again or send lighf an SOS for help. (To do so, navigate to any lighfStory's dedicated screen and Tap the Vertical Ellipsis icon to open the More Options Menu. Choose any option and leave a message. If further participation is required, you will receive a follow up message.)"
    static let requestTimeOut = "This destination is unavailable at the moment. Try again or send lighf an SOS for help. (To do so, navigate to any lighfStory's dedicated screen and Tap the Vertical Ellipsis icon to open the More Options Menu. Choose any option and leave a message. If further participation is required, you will receive a follow up message.)"
    static let connectionLost = "Seems you have temporarily lost connection. Reconnect to current wi-fi or try another network including switching to your phone's data plan."
    static let deviceIdNotFound = "Your device ID could not been found. Try again or send lighf as SOS for help."
    static let activationFailure = "Could not activate your Account"
    static let activationSuccess = "You're ready to go!"
    static let accountCreated = "Account created successfully!"
}

// Messages in Sign up screen
enum SignUpMessage {
    static let invalidEmail = "Incorrect email address"
    static let invalidPassword = "Almost there. Password need at least 1 CAP, lowercase, $pecial, and #number character."
    static let passwordsNotMatching = "Passwords not aligned"
}
// Type of posts involved
enum PostType: String {
    case window
    case story
    case article
    case statement
    case admin_post
}
// Fonts used in the app
enum Font: String {
    //
    case robotoBold = "Roboto-Bold"
    case robotoMedium = "Roboto-Medium"
    case robotoRegular = "Roboto-Regular"
    //
    case sfProDisplayBold = "SFProDisplay-Bold"
    case sfProDisplayMedium = "SFProDisplay-Medium"
    case sfProDisplayLight = "SFProDisplay-Light"
    case sfProDisplayRegular = "SFProDisplay-Regular"
    //
    case sfProTextBold = "SFProText-Bold"
    case sfProTextSemiBold = "SFProText-Semibold"
    case sfProTextLight = "SFProText-Light"
    case sfProTextRegular = "SFProText-Regular"
}
// Navigation bar titles

enum Title {
    static let shortStory = "Write Relatable lighfStory"
    static let longStory = "Add Longer Story"
    static let articleHeader = "Add Article Title"
    static let articleBody = "Add Article Body"
    static let storyPreview = "lighfStory Preview"
    static let articlePreview = "Article Preview"
    static let moreOptions = "More Options"
    static let optional = "Optional"
}

// PlaceHolder strings

enum PlaceHolder {
    static let shortStory = "Share a lighfStory for others to relate to in 365 chars."
    static let longStory = "Need more room? Add a longer story to go with your relatable statement. Doesn't have to be relatable."
    static let articleHeader = "Add your Article’s Title"
    static let articleBody = "Add your Article’s Body"
    static let flagBody = "Add Details or just click Done."
    static let advice = "Add your advice"
    static let question = "Add your question"
}

enum CommentType: String {
    case comment
    case advice
    case question
    case reply
}

// Media types
enum MediaType: Int {
    case youtube
    case music
    case podcast
    case article
}

enum EmailOptions: Int {
    case none
    case hashed //Email will only be used for account recovery
    case emailLess
    //
    var keyValue: String {
        switch self {
        case .none:
            return "in_clear"
        case .hashed:
            return "hashed"
        case .emailLess:
            return "no_email"
        }
    }
    //
    static func option(for value: String) -> EmailOptions? {
        switch value {
        case "in_clear":
            return .none
        case "hashed":
            return .hashed
        case "no_email":
            return .emailLess
        default:
            return nil
        }
    }
}

enum InfoContentMode {
    case email
    case password
}

enum FlagOptions: String {
    case header = "Choose an option"
    case flag = "Flag"
    case notRelated = "Not Relatable To Anyone"
    case spam = "Spam"
    case abuse = "Abuse"
    case copyright = "Copyright Issue"
    case dontSeeThis = "I don’t want to see this again"
    /*case otherThis = "Other"*/
    case dontSeeLikeThis = "I don’t want to see anything like this again"
    case turnOffNotification = "Turn off notifications"
    case turnOnNotification = "Turn on notifications"
    //
    var message: String {
        switch self {
        case .notRelated:
            return "not_relatable"
        case .spam:
            return "spam"
        case .abuse:
            return "abuse"
        case .copyright:
            return "copyright_issue"
        case .dontSeeThis:
            return "dont_repeat"
       /* case .otherThis:
            return "other"
             */
        case .dontSeeLikeThis:
            return "dont_repeat_related"
        default:
            break
        }
        return ""
    }
}

// Types of view options for posts
enum PostViewOption {
    case preview
    case detailView
}

enum FontSize: String {
    case small = "Small"
    case medium = "Medium"
    case large = "Large"
}
enum CommentsViewTitle: String {
    case question = "Ask a question"
    case advice = "Give Advice"
    case comment = "Share how you relate"
    case reply = "Add your reply"
// need to turn off options which are not required.
    case flag = "Flag"
    case notRelated = "Not Relatable To Anyone"
    case spam = "Spam"
    case abuse = "Abuse"
    case copyright = "Copyright Issue"
    case dontSeeThis = "I don’t want to see this again"
    // case otherThis = "Other"
    case dontSeeLikeThis = "I don’t want to see anything like this again"
    case turnOffNotification = "Turn off notifications"
}
enum WebViewOptions: String {
    case privacyPolicy = "Privacy"
    case terms = "Terms"
    case legal = "Legal"
    case useGuidelines = "Use Guidelines"
    case getHelp = "Get Help"
}
//parameters for feeds
enum GetFeedOptions: String {
    case feedTypeKey = "feed_type"
    case feedFilterKey = "feed_filter"
    case feedAspectFilterKey = "aspect_feed"
    case feedBackpackedFilterKey = "backpacked_posts"
    case feedCreatedPostFilterKey = "created_posts"
    case feedRelatedPostFilterKey = "related_posts"
    case feedPostTypeFilterKey = "post_type_feed"
    case feedPostsByIDsFilterKey = "posts_by_ids"
}

enum NotificationType: String {
    case userCommentType = "user_comment"
    case commentReplyType = "comment_reply"
}

enum NotificationState: String {
    case turnOn = "turn_on"
    case turnOff = "turn_off"
}

enum IntroScreenOption {
    case initialPlay
    case replay
    case playFromPausedState
}

enum PasswordValidationError: String {
    case atLeast7Chars = "Password needs at least 7 characters"
    case atLeast1Uppercase = "Password must contain at least 1 CAPITAL letter"
    case atLeast1Lowercase = "Password must contain at least 1 lowercase letter"
    case atLeast1Digit = "Password must contain at least 1 #Number"
    case atLeast1SpecialChar = "Password must contain at least 1 $pecial character"
}

enum Images: String {
    case passwordShow = "show-pw-eye-yes-white"
    case passwordHide = "hide-pw-eye-no-white"
    
    var image: UIImage? {
        return UIImage(named: self.rawValue)
    }
}
