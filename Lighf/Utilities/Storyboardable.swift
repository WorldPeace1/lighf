//
//  Storyboardable.swift
//  Lighf
//
//  Created by L on 10/23/19.
//  Copyright © 2019 L. All rights reserved.
//

import UIKit

protocol Storyboardable {

    // MARK: - Properties

    static var storyboardName: String { get }
    static var storyboardBundle: Bundle { get }

    // MARK: -

    static var storyboardIdentifier: String { get }

    // MARK: - Methods
    
    static func instantiate(name: String?) -> Self
    
}

//MARK: - Extension for UIViewController

extension Storyboardable where Self: UIViewController {
    
    // MARK: - Properties
    
    static var storyboardName: String {
        return "Main"
    }
    
    static var storyboardBundle: Bundle {
        return .main
    }
    
    // MARK: -

    static var storyboardIdentifier: String {
        return String(describing: self)
    }

    // MARK: - Methods

    static func instantiate(name: String? = nil) -> Self {
        guard let viewController = UIStoryboard(name: name ?? self.storyboardName, bundle: self.storyboardBundle)
            .instantiateViewController(withIdentifier: self.storyboardIdentifier) as? Self else {
                fatalError("Unable to Instantiate View Controller With Storyboard Identifier \(self.storyboardIdentifier)")
        }

        return viewController
    }
    
}
