//
//  DeviceIdentifier.swift
//  Lighf
//
//  Created by Snake Loop LLC on 12/7/19.
//  Copyright © 2019 L. All rights reserved.
//

import Foundation
import CommonCrypto

class DeviceIdentifier {
    func generateDeviceId(vendorIdentifier: String) -> String {
        let combo = "\(vendorIdentifier)@\(UUID().uuidString)"
        
        guard let data = combo.data(using: .utf8) else { fatalError("Could not convert combo to data") }
        
        var digest = [UInt8](repeating: 0, count: Int(CC_SHA256_DIGEST_LENGTH))
        _ = data.withUnsafeBytes { CC_SHA256($0.baseAddress, UInt32(data.count), &digest) }
        
        var comboAsSha256 = ""
        for byte in digest {
            comboAsSha256 += String(format: "%02x", UInt8(byte))
        }
        
        return comboAsSha256
    }
}
