//
//  Validation+Email.swift
//  Lighf
//
//  Created by Snake Loop LLC on 11/18/19.
//  Copyright © 2019 L. All rights reserved.
//

import Foundation

class PasswordValidator: Validator<String> {
    convenience init() {
        let rule1 = ValidationRule<String>(validator: { (value) -> Bool in
            guard let value = value else { return false }
            return value.count >= 7
        }, errorMessage: PasswordValidationError.atLeast7Chars.rawValue)
        
        let rule2 = ValidationRule<String>(validator: { (value) -> Bool in
            guard let value = value else { return false }
            return value.range(of: #".*[A-Z]+.*"#, options: .regularExpression) != nil
        }, errorMessage: PasswordValidationError.atLeast1Uppercase.rawValue)
        
        let rule3 = ValidationRule<String>(validator: { (value) -> Bool in
            guard let value = value else { return false }
            return value.range(of: #".*[a-z]+.*"#, options: .regularExpression) != nil
        }, errorMessage: PasswordValidationError.atLeast1Lowercase.rawValue)
        
        let rule4 = ValidationRule<String>(validator: { (value) -> Bool in
            guard let value = value else { return false }
            return value.range(of: #".*[0-9]+.*"#, options: .regularExpression) != nil
        }, errorMessage: PasswordValidationError.atLeast1Digit.rawValue)
        
        let rule5 = ValidationRule<String>(validator: { (value) -> Bool in
            guard let value = value else { return false }
            return value.range(of: #".*[\.\?\^\=\~\|\:\<\>;'"!&^%$#@\(\)/\[\]\{\}_\-\+\\/]+.*"#, options: .regularExpression) != nil
        }, errorMessage: PasswordValidationError.atLeast1SpecialChar.rawValue)

        self.init(rules: [rule1, rule2, rule3, rule4, rule5])
    }
}
