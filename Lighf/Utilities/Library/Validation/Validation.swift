//
//  Validation.swift
//  Lighf
//
//  Created by Snake Loop LLC on 11/18/19.
//  Copyright © 2019 L. All rights reserved.
//

import Foundation

// MARK: - Rule Protocol

protocol Rule {
    associatedtype T
    func validate(_ value: T?) -> Bool
    func getErrorMessage() -> String
}


// MARK: - Rule

class ValidationRule<T>: Rule {
    private let validator: (T?) -> Bool
    private let errorMessage: String
    
    init(validator: @escaping (T?) -> Bool, errorMessage: String) {
        self.validator = validator
        self.errorMessage = errorMessage
    }
    
    func validate(_ value: T?) -> Bool {
        return self.validator(value)
    }
    
    func getErrorMessage() -> String {
        return self.errorMessage
    }
}

// MARK: - Validator

class Validator<T> {
    let rules: [ValidationRule<T>]
    var errors: [String] = []
    
    init(rules: [ValidationRule<T>]) {
        self.rules = rules
    }
    
    func validate(_ value: T?) -> Bool {
        self.errors.removeAll()
        
        return self.rules.reduce(true) { (result, rule) -> Bool in
            let newResult = rule.validate(value)
            if !newResult {
                self.errors.append(rule.getErrorMessage())
            }
            return result && newResult
        }
    }
}
