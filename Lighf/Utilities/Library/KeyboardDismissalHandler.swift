//
//  KeyboardDismissal.swift
//  Lighf
//
//  Created by L on 10/23/19.
//  Copyright © 2019 L. All rights reserved.
//

import UIKit
import KMPlaceholderTextView

// MARK: - Visitor

protocol KeyboardDismissalHandlerVisitor {
    var keyboardToolbar: UIToolbar { get }
    func visit(_ control: UITextField)
    func visit(_ control: KMPlaceholderTextView)
}

// MARK: - Visitee

protocol KeyboardDismissalHandlerVisitee: UIResponder {
    func accept(visitor: KeyboardDismissalHandlerVisitor)
}

// MARK: - Concrete Visitor

class KeyboardDismissalHandler: KeyboardDismissalHandlerVisitor {
    var view: UIView!
    var controls: [KeyboardDismissalHandlerVisitee] = []

    lazy var keyboardToolbar: UIToolbar = {
        let toolbar: UIToolbar = UIToolbar(frame: CGRect(origin: CGPoint.zero, size: CGSize(width: self.view.frame.size.width, height: 30)))
        toolbar.barTintColor = nil
        toolbar.isTranslucent = true
        toolbar.tintColor = UIColor.textFieldTitleColor
        
        let flexSpace = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let doneBtn: UIBarButtonItem = UIBarButtonItem(title: "Done", style: .done, target: self, action: #selector(dismissKeyboardAction(sender:)))
        toolbar.setItems([flexSpace, doneBtn], animated: false)
        toolbar.sizeToFit()
        
        return toolbar
    }()
    
    // MARK: Init
    
    init(in view: UIView) {
        self.view = view
    }
    
    // MARK: Done button handler
    
    @objc func dismissKeyboardAction(sender: UIBarButtonItem) {
        self.view.endEditing(true)
    }
    
    // MARK: Visiting the Visitees
    
    func visit(_ control: UITextField) {
        self.controls.append(control)
        control.inputAccessoryView = self.keyboardToolbar
    }
    
    func visit(_ control: KMPlaceholderTextView) {
        self.controls.append(control)
        control.inputAccessoryView = self.keyboardToolbar
    }
}

// MARK: - Concrete Visitees

extension KMPlaceholderTextView: KeyboardDismissalHandlerVisitee {
    func accept(visitor: KeyboardDismissalHandlerVisitor) {
        visitor.visit(self)
    }
}

extension UITextField: KeyboardDismissalHandlerVisitee {
    func accept(visitor: KeyboardDismissalHandlerVisitor) {
        visitor.visit(self)
    }
}

// MARK: - Keyboard Dismissal Registrer

class KeyboardDismissalRegistrer {
    private var handler: KeyboardDismissalHandlerVisitor!
    private var controls: [KeyboardDismissalHandlerVisitee] = []
    
    init(view: UIView) {
        self.handler = KeyboardDismissalHandler(in: view)
    }
    
    func registerControls(_ controls: [KeyboardDismissalHandlerVisitee]) {
        self.controls = controls

        for control in self.controls {
            control.accept(visitor: self.handler)
        }
    }
    
    func registerControl(_ control: KeyboardDismissalHandlerVisitee) {
        guard !self.controls.contains(where: { $0 == control }) else { return }
        
        self.controls.append(control)
        control.accept(visitor: self.handler)
    }
}
