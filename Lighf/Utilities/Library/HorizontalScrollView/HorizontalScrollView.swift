//
//  HorizontalScrollView.swift
//  Lighf
//
//  Created by L on 11/8/19.
//  Copyright © 2019 L. All rights reserved.
//

import UIKit

// MARK: Delegate

protocol HorizontalScrollViewDelegate: NSObjectProtocol {
    func horizontalScrollViewDidScrollToPage(_ page: Int)
}

// MARK: Class

class HorizontalScrollView: UIView {
    
    private let kXibFileName = "HorizontalScrollView"
    weak var delegate: HorizontalScrollViewDelegate?
    
    @IBOutlet var contentView: UIView!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var pageControl: UIPageControl!
    
    var views: [UIView] = [] {
        didSet {
            self.layoutIfNeeded()
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        self.uiSetup()
    }
    
    required  init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        self.xibSetup()
        self.uiSetup()
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        self.updateScrollViewContentSize()
        self.updatePageCount()
    }
}

// MARK: - Custom Init

extension HorizontalScrollView {
    func uiSetup() {
        self.translatesAutoresizingMaskIntoConstraints = false
        
        self.scrollView.backgroundColor = .gray
        self.scrollView.delegate = self
        self.scrollView.isPagingEnabled = true
    }
    
    func xibSetup() {
        guard let view = self.loadNib() else { fatalError() }
        self.addSubview(view)
        
        /*view.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint(item: view, attribute: .leading, relatedBy: .equal, toItem: self, attribute: .leading, multiplier: 1.0, constant: 1.0).isActive = true
        NSLayoutConstraint(item: view, attribute: .top, relatedBy: .equal, toItem: self, attribute: .top, multiplier: 1.0, constant: 1.0).isActive = true
        NSLayoutConstraint(item: view, attribute: .trailing, relatedBy: .equal, toItem: self, attribute: .trailing, multiplier: 1.0, constant: 1.0).isActive = true
        NSLayoutConstraint(item: view, attribute: .bottom, relatedBy: .equal, toItem: self, attribute: .bottom, multiplier: 1.0, constant: 1.0).isActive = true
        self.layoutIfNeeded()*/
    }
    
    func loadNib() -> UIView? {
        guard let views = Bundle.main.loadNibNamed(self.kXibFileName, owner: self, options: nil) else { fatalError() }
        return views.first as? UIView
    }
}

// MARK: - Actions

extension HorizontalScrollView {
    @IBAction func pageChanged(_ sender: UIPageControl) {
        
    }
}

// MARK: - UIScrollView Delegate

extension HorizontalScrollView: UIScrollViewDelegate {
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        guard scrollView == self.scrollView else { return }
        let offset = scrollView.contentOffset.x / scrollView.frame.size.width
        self.pageControl.currentPage = Int(round(offset))
        
        guard let delegate = self.delegate else { return }
        delegate.horizontalScrollViewDidScrollToPage(self.pageControl.currentPage)
    }
}

// MARK: - Internals

extension HorizontalScrollView {
    func updateScrollViewContentSize() {
        _ = self.scrollView.subviews.compactMap { $0.removeFromSuperview() }
        
        var frame = CGRect.zero
        for index in 0..<self.views.count {
            frame.origin.x = self.scrollView.frame.size.width * CGFloat(index)
            frame.size = self.scrollView.frame.size

            let subview = self.views[index]
            subview.frame = frame
            
            self.scrollView.addSubview(subview)
        }

        self.scrollView.contentSize = CGSize(width: self.scrollView.frame.size.width * CGFloat(self.views.count), height: self.scrollView.frame.size.height)
        self.scrollView.contentOffset = .zero
    }
    
    func updatePageCount() {
        self.pageControl.numberOfPages = self.views.count
        self.pageControl.currentPage = 0
    }
}

// MARK: - API

extension HorizontalScrollView {
    func setViews(_ views: [UIView]) {
        _ = self.scrollView.subviews.compactMap { $0.removeFromSuperview() }
        self.views = views
    }
    
    func navigateToPage(_ page: Int) {
        self.pageControl.currentPage = page
//        self.scrollView.contentOffset = CGPoint(x: CGFloat(page * self.scrollView.contentSize.width), y: 0)
    }
}
