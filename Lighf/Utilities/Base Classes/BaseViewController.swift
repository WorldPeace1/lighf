//
//  BaseViewController.swift
//  Lighf
//
//  Created by L.
//  Copyright © 2019 L. All rights reserved.
//

import UIKit
import MBProgressHUD
import Toast_Swift
import Kingfisher

// MARK: - BasePresenterToViewProtocol

protocol BasePresenterToViewProtocol: class {
    func showActivityIndicator(onFullScreen: Bool)
    func dismissActivityIndicator()
    func showToast(with message: String, completion: @escaping () -> Void)
}

// MARK: - BasePresenterToViewProtocol

extension BasePresenterToViewProtocol {
    func showActivityIndicator(onFullScreen: Bool = false) {
        showActivityIndicator(onFullScreen: onFullScreen)
    }
}

// MARK: - BaseViewController

class BaseViewController: UIViewController, BasePresenterToViewProtocol {
    var keyboardDismissalRegistrer: KeyboardDismissalRegistrer!
    var isScreenShiftedUp = false
    var activityIndicator: MBProgressHUD?
    
    lazy var overlayView: UIView = {
        let view = UIView(frame: .zero)
        view.backgroundColor = .black
        view.alpha = 0.3
        return view
    }()
    
    lazy var blurredView: UIVisualEffectView = {
        let blurView = UIVisualEffectView()
        blurView.frame = CGRect.zero
        blurView.effect = UIBlurEffect(style: .dark)
        blurView.alpha = 0.95
        return blurView
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.keyboardDismissalRegistrer = KeyboardDismissalRegistrer(view: self.view)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        ImageCache.default.clearMemoryCache()
        ImageCache.default.clearDiskCache {}
    }
    
    func showActivityIndicator(onFullScreen: Bool) {
        dismissActivityIndicator()
        if onFullScreen, let completeView = self.navigationController?.view {
            activityIndicator = MBProgressHUD.showAdded(to: completeView, animated: true)
        } else {
            activityIndicator = MBProgressHUD.showAdded(to: view, animated: true)
        }
    }
    
    func dismissActivityIndicator() {
        if let activity = activityIndicator {
            activity.hide(animated: true)
            activityIndicator = nil
        } else {
            print("activity indicator is nil")
        }
    }
    
    func showToast(with message: String, completion: @escaping () -> Void) {
        if let window = UIApplication.shared.keyWindow {
            window.hideToast()
            var toastStyle = ToastStyle()
            toastStyle.messageAlignment = .center
            var duration = 2.0
            if message.count > 35 {
                duration = 7.0
            }
            window.makeToast(message, duration: duration, position: .bottom, title: nil, image: nil, style: toastStyle) { (_) in
                completion()
            }
        }
    }
    
    func showBlurredBackground(style: UIBlurEffect.Style? = nil) {
        let blurView = self.blurredView
        
        if style != nil {
            blurView.effect = UIBlurEffect(style: style!)
        }
        
        blurView.addCenteredInParentView(self.view)
    }
    
    func hideBlurredBackground() {
        self.blurredView.removeFromSuperview()
    }
    
    func showOverlay() {
        self.overlayView.addCenteredInParentView(self.view)
    }
    
    func hideOverlay() {
        self.overlayView.removeFromSuperview()
    }
    
    /// Move the entire screen content to top upto a particular value
    ///
    /// - Parameter toValue: The value to which the screen showuld be moved upward
    func moveScreenUp(toValue: Float = Constants.screenShiftUpValue) {
        if let navigationController = self.navigationController, !isScreenShiftedUp {
            isScreenShiftedUp = Utility().shiftScreen(isAlreadyShifted: isScreenShiftedUp, navigationController: navigationController, screenShiftUpValue: CGFloat(toValue))
        }
    }
    
    // Reset the screen content to it's original position
    func moveScreenDown(toValue: Float = Constants.screenShiftUpValue) {
        if let navigationController = self.navigationController, isScreenShiftedUp {
            isScreenShiftedUp = Utility().shiftScreen(isAlreadyShifted: isScreenShiftedUp, navigationController: navigationController, screenShiftUpValue: CGFloat(toValue))
        }
    }
    
    /// Dismiss keyboard and reset screen
    ///
    /// - Parameter resetScreen: Bool value which specifies whether the view should be moved down
    func dismissKeyBoard(resetScreen: Bool = true) {
        view!.endEditing(true)
        if isScreenShiftedUp && resetScreen {
            moveScreenDown()
        }
    }
}
